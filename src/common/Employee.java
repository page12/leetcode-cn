package common;

import java.util.List;

/**
 * 一些题目中公共的类，属性修改为了public，可以跨package使用
 */
public class Employee {
    public int id;
    public int importance;
    public List<Integer> subordinates;
};
