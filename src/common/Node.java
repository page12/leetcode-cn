package common;

import java.util.List;

/**
 * N叉树节点
 * 一些题目中公共的类，属性修改为了public，可以跨package使用
 */
public class Node {
    public int val;
    public List<Node> children;

    public Node() {
    }

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
}
