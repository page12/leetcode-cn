package common;

/**
 * 链表节点
 * 一些题目中公共的类，属性修改为了public，可以跨package使用
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
