package p10;

/**
 * https://leetcode-cn.com/problems/video-stitching/
 */
public class Problem1024 {

    public static void main(String[] args) {
        Problem1024 p = new Problem1024();
        System.err.println(p.videoStitching(new int[][]{{0, 2}, {4, 6}, {8, 10}, {1, 9}, {1, 5}, {5, 9}}, 10) == 3);
        System.err.println(p.videoStitching(new int[][]{{0, 1}, {6, 8}, {0, 2}, {5, 6}, {0, 4}, {0, 3}, {6, 7}, {1, 3}, {4, 7}, {1, 4}, {2, 5}, {2, 6}, {3, 4}, {4, 5}, {5, 7}, {6, 9}}, 9) == 3);
        System.err.println(p.videoStitching(new int[][]{{0, 2}, {4, 8}}, 5) == -1);
        System.err.println(p.videoStitching(new int[][]{{0, 4}}, 2) == 1);
        System.err.println(p.videoStitching(new int[][]{{3, 6}, {3, 6}, {0, 4}, {6, 6}, {8, 10}, {6, 10}, {0, 1}, {6, 9}}, 2) == 1);
    }

    /**
     * 思路：
     * 用一个数组 maxEnd 记录每个片段能到达的终点时间，这样就变成和第55题类似的题
     * maxEnd的下标为跳跃起点，对应的元素值为能跳跃的最远点
     * 然后用贪心算法求解
     */
    public int videoStitching(int[][] clips, int time) {
        // 题目有假设，不做校验
        int[] maxEnd = new int[time];
        for (int i = 0; i < clips.length; i++) {
            int s = clips[i][0];
            if (s < time) {
                maxEnd[s] = Math.max(maxEnd[s], clips[i][1]);
            }
        }
        if (maxEnd[0] <= 0) {
            return -1;
        }
        int count = 0;
        int start = 0;
        int end = 0;
        int farthest = 0;
        while (start < time) {
            count++;
            for (int i = start; i <= end; i++) {
                farthest = Math.max(maxEnd[i], farthest);
                if (farthest >= time) {
                    return count;
                }
            }
            if (farthest == end) {
                return -1;
            }
            start = end + 1;
            end = farthest;
        }
        return count;
    }
}
