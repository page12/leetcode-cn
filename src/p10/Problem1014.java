package p10;

/**
 * https://leetcode-cn.com/problems/best-sightseeing-pair/
 */
public class Problem1014 {

    public static void main(String[] args) {
        Problem1014 p = new Problem1014();
        System.err.println(p.maxScoreSightseeingPair2(new int[]{8, 1, 5, 2, 6}) == 11);
        System.err.println(p.maxScoreSightseeingPair2(new int[]{1, 2}) == 2);
        System.err.println(p.maxScoreSightseeingPair2(new int[]{1, 1, 1}) == 1);
    }

    /**
     * 思路：
     * 可以把 values[i] + values[j] + i - j，i < j 拆分成两部分
     * 第一部分是 values[i] + i
     * 第二一部分是 values[j] - j
     * 题目要求最大值
     * 第一部分的最大值是 max{values[x] + x} 是 x 属于 [0,i]
     * 第二部分的最大值是 max{values[x] + x} 是 j 属于 [i+1,values.length-1]
     * 从左至右遍历一次可以得到每一个下标对应的第一部分的最大值
     * 从右至左遍历一次可以得到每一个下标对应的第二部分的最大值
     * 最后再遍历一次可以得到两个部分的和的最大值
     */
    public int maxScoreSightseeingPair(int[] values) {
        // 题目有假设，不做校验
        // 求第一部分的最大值，它是包含当前下标的左边部分的最大值
        int[] firstPartMax = new int[values.length];
        firstPartMax[0] = values[0];
        for (int i = 1; i < values.length; i++) {
            firstPartMax[i] = Math.max(values[i] + i, firstPartMax[i - 1]);
        }
        // 求第二部分的最大值，它是不包含当前下标的右边部分的最大值
        int[] secondPartMax = new int[values.length];
        // 因为 values[j] - j 可能是负数，所以这里用 Integer.MIN_VALUE
        secondPartMax[values.length - 1] = Integer.MIN_VALUE;
        for (int i = values.length - 2; i >= 0; i--) {
            // 注意，这里不包含当前下标
            secondPartMax[i] = Math.max(values[i + 1] - (i + 1), secondPartMax[i + 1]);
        }
        // 求两部分的和的最大值
        // 因为 i < j 所以遍历时不需要管最后一个元素
        int max = 0;
        for (int i = 0; i < values.length - 1; i++) {
            max = Math.max(max, firstPartMax[i] + secondPartMax[i]);
        }
        return max;
    }

    /**
     * 对上面的思路进行优化
     * values[i] + values[j] + i - j = (values[i] + i) + (values[j] - j) = firstPartMax[i] + (values[j] - j)
     * 如果知道了 firstPartMax [i]，则只需要循环j就可以了，这样就可以省掉求 secondPartMax 和 最终max 的两次次遍历，也不需要 secondPartMax 这个数组
     * 题目要求的是和的最大值，实际上并不需要知道所有的 firstPartMax
     * 按j遍历时，只需要知道下标 j 左边不包括自己在内的那一个 firstPartMax 即可
     * 又因为 firstPartMax 从左至右是升序排序好了的
     * 因此可以省去 firstPartMax 数组，只需要用当前下标对应的 firstPartMax 的最大值这一个遍历代替
     * 这样就优化为一次遍历，不再需要额外使用数组
     */
    public int maxScoreSightseeingPair2(int[] values) {
        // 题目有假设，不做校验
        int max = 0;
        int firstPartMax = values[0];
        // 因为 i < j，所以要从第二个值开始遍历，下标j就是题目描述中的j
        // 这样变换一下，firstPartMax 就是j左边不包含本身下标的第一部分的最大值
        // 所以要先求max，再求 firstPartMax，不能颠倒这个顺序
        for (int j = 1; j < values.length; j++) {
            max = Math.max(max, firstPartMax + values[j] - j);
            firstPartMax = Math.max(firstPartMax, values[j] + j);
        }
        return max;
    }
}
