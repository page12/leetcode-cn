package p03;

/**
 * https://leetcode-cn.com/problems/ransom-note/
 */
public class Problem0383 {

    public static void main(String[] args) {
        Problem0383 p = new Problem0383();
        System.err.println(p.canConstruct("aab", "baa"));
    }

    /**
     * 思路：题目的意思简单点说就是，杂志可以构成赎金信 等价于 把重复元素也视为正常元素看时，ransomNote是magazine的子集
     * 也就是ransomNote中所有字母都在magazine中存在，并且在ransomNote中对应的次数小于等于在magazine中对应的次数
     * 不满足这个条件就不能构成
     * 判断是否包含，并且需要关注次数，可以使用hash表
     * 假设N为ransomNote的长度，M为magazine的长度
     * 则时间复杂度为O(N + M)：遍历两次ransomNote，遍历一次magazine
     * 空间复杂度为O(字符集的大小)：如果只有英文字母不区分大小写，则固定是26，也就是O(1)；如果是其他情况，则最大不超过O(N)
     * 题目中假定只有小写字母，因此是固定大小，也就是O(1)的空间复杂度
     */
    public boolean canConstruct(String ransomNote, String magazine) {
        // 不考虑字符串为空的情况
        // 使用固定大小的hash表，使用数组特殊代替
        int[] charMap = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int i = 0; i <= ransomNote.length() - 1; i++) {
            char c = ransomNote.charAt(i);
            int idx = c - 'a';
            charMap[idx] = charMap[idx] + 1;
        }
        for (int i = 0; i <= magazine.length() - 1; i++) {
            char c = magazine.charAt(i);
            int idx = c - 'a';
            if (charMap[idx] > 0) {
                charMap[idx] = charMap[idx] - 1;
            }
        }
        for (int i = 0; i <= ransomNote.length() - 1; i++) {
            char c = ransomNote.charAt(i);
            int idx = c - 'a';
            if (charMap[idx] > 0) {
                return false;
            }
        }
        return true;
    }
}
