package p03;

/**
 * https://leetcode-cn.com/problems/find-the-difference/
 */
public class Problem0389 {

    public static void main(String[] args) {
        Problem0389 p = new Problem0389();
        System.err.println(p.findTheDifference("abcd", "abcde"));
        System.err.println(p.findTheDifference("", "y"));
        System.err.println(p.findTheDifference("a", "aa"));
        System.err.println(p.findTheDifference("ae", "aea"));
    }

    /**
     * 思路：本题和第268题一样，可以直接照搬思路
     * 都是先对一个数组进行累计进行某种运算，然后对另一个数组进行累计逆运算来抵消掉相同的元素
     * 时间复杂度O(N)，空间复杂度O(1)
     * 下面的代码是使用异或运算，使用加法减法效果也一样
     */
    public char findTheDifference(String s, String t) {
        // 题目有假设 t.length == s.length + 1，不考虑不符合格式的输入
        if (s.length() == 0) {
            return t.charAt(0);
        }
        int xor = 0;
        // 题目假定只包含小写字母
        for (int i = 0; i < s.length(); i++) {
            xor ^= s.charAt(i);
        }
        for (int i = 0; i < t.length(); i++) {
            xor ^= t.charAt(i);
        }
        return (char) xor;
    }
}
