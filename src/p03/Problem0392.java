package p03;

/**
 * https://leetcode-cn.com/problems/is-subsequence/
 */
public class Problem0392 {

    public static void main(String[] args) {
        Problem0392 p = new Problem0392();
        System.err.println(p.isSubsequence("ace", "abcde"));
        System.err.println(p.isSubsequence("aec", "abcde"));
        System.err.println(p.isSubsequence("abc", "ahbgdc"));
        System.err.println(p.isSubsequence("axc", "ahbgdc"));
        System.err.println(p.isSubsequence("ab", "baab"));
    }

    /**
     * 思路：双指针pS, pT分别遍历两个字符串
     * 如果pS == pT，则pS++, pT++
     * 如果pS != pT，则pT++
     * 如果pS先遍历完，或者和pT一同遍历完，则s为t的子序列
     * 否则不是子序列
     * 设s的长度为N，t的长度为M，则时间复杂度为O(N+M)，空间复杂度为O(1)
     */
    public boolean isSubsequence(String s, String t) {
        if (s.length() == 0) {
            return true;
        }
        if (s.length() > t.length()) {
            return false;
        }
        int pS = 0;
        int pT = 0;
        while (pS != s.length() && pT != t.length()) {
            if (s.charAt(pS) == t.charAt(pT)) {
                pS++;
            }
            pT++;
        }
        return pS == s.length();
    }
}
