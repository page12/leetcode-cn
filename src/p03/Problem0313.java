package p03;

/**
 * https://leetcode-cn.com/problems/super-ugly-number/
 */
public class Problem0313 {

    public static void main(String[] args) {
        Problem0313 p = new Problem0313();
        System.err.println(p.nthSuperUglyNumber(12, new int[]{2, 7, 13, 19}) == 32);
        System.err.println(p.nthSuperUglyNumber(1, new int[]{2, 3, 5}) == 1);
    }

    /**
     * 思路：
     * 跟第264题差不多
     * 如果使用最小堆，则基本不用改
     * 如果是自己写逻辑判断最小值，则把三个指针变成n个指针即可
     */
    public int nthSuperUglyNumber(int n, int[] primes) {
        // 题目有假设，不做校验
        int[] uglyNums = new int[n];
        int[] p = new int[primes.length];
        uglyNums[0] = 1;
        for (int i = 1; i < n; i++) {
            int ugly = uglyNums[p[0]] * primes[0];
            for (int j = 1; j < primes.length; j++) {
                ugly = Math.min(ugly, uglyNums[p[j]] * primes[j]);
            }
            for (int j = 0; j < p.length; j++) {
                if (uglyNums[p[j]] * primes[j] == ugly) {
                    p[j] += 1;
                }
            }
            uglyNums[i] = ugly;
        }
        return uglyNums[n - 1];
    }
}
