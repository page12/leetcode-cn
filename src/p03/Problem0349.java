package p03;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/intersection-of-two-arrays/
 */
public class Problem0349 {

    public static void main(String[] args) {
        Problem0349 p = new Problem0349();
        System.err.println(Arrays.toString(p.intersection(new int[]{1, 2, 2, 1}, new int[]{2, 2})));
        System.err.println(Arrays.toString(p.intersection(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4})));
    }

    /**
     * 思路：利用一个hash表，key为数组元素，value为布尔值，true表示是交集的元素，false表示不是交集的元素
     * 根据交集的性质，返回结果中最多有min(N, M)个元素，N,M分别为nums1和nums2的长度
     * hash表的大小等于返回结果的大小，所以空间复杂度为O(min(N, M))
     * 两个数组都分别要遍历一次，所以空间复杂度为O(N+M)
     */
    public int[] intersection(int[] nums1, int[] nums2) {
        if (nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0) {
            return new int[]{};
        }
        int[] smaller;
        int[] bigger;
        if (nums1.length < nums2.length) {
            smaller = nums1;
            bigger = nums2;
        } else {
            smaller = nums2;
            bigger = nums1;
        }
        // 所有value的值初始化为false
        // 最后所有value == true的元素就是交集的元素
        Map<Integer, Boolean> map = new HashMap<>();
        for (int i = 0; i < smaller.length; i++) {
            map.put(smaller[i], Boolean.FALSE);
        }
        int resultLength = 0;
        for (int i = 0; i < bigger.length; i++) {
            // 如果另一个数组的值在hash表中存在，则设置value的值为true
            Boolean exists = map.get(bigger[i]);
            if (exists != null && !exists) {
                map.put(bigger[i], Boolean.TRUE);
                resultLength++;
            }
        }
        int[] result = new int[resultLength];
        for (Map.Entry<Integer, Boolean> entry : map.entrySet()) {
            if (Boolean.TRUE.equals(entry.getValue())) {
                result[--resultLength] = entry.getKey();
            }
        }
        return result;
    }
}
