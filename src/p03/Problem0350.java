package p03;

import java.util.*;

/**
 * https://leetcode-cn.com/problems/intersection-of-two-arrays-ii/
 */
public class Problem0350 {

    public static void main(String[] args) {
        Problem0350 p = new Problem0350();
//        System.err.println(Arrays.toString(p.intersect(new int[]{1, 2, 2, 1}, new int[]{2, 2})));
//        System.err.println(Arrays.toString(p.intersect(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4})));
//        System.err.println(Arrays.toString(p.intersect(new int[]{1, 2}, new int[]{1, 1})));
        System.err.println(Arrays.toString(p.intersect(new int[]{4, 7, 9, 7, 6, 7}, new int[]{5, 0, 0, 6, 1, 6, 2, 2, 4})));
    }

    /**
     * 思路：和第349题基本一致，可以看看那一题我写的思路，本题需要改下hash表的value的定义来解答
     * 设较大的数组为bigger，较小的数组为smaller
     * 把value定义为数字，使用smaller初始化hash表时，value的值为对应元素在smaller中出现的次数
     * 遍历bigger时，如果发现一个元素在hash表中存在，则把value的值减1
     * 然后再次遍历一次smaller，它的所有元素都在hash表中，根据value的值进行不同的操作
     * 如果value < 0，代表该元素在bigger中的次数 大于 在smaller中的次数，因此smaller中的所有该元素都要加入到结果集中（不需要管具体的次数）
     * 如果value == 0，代表该元素在bigger中的次数 等于 在smaller中的次数，可以采用和value < 0相同的操作
     * 如果value > 0，代表该元素在bigger中的次数 小于于 在smaller中的次数，这个差值就是value的值，此时该元素在结果中出现的次数应该是在bigger中出现的次数
     * <p>
     * 该元素在bigger中出现的次数我们没有直接保存，但是可以用另一个方式求出
     * 设第一次遍历完smaller时value的值为initValue，它就是该元素在samller中出现的次数
     * 设遍历完bigger时value的值为finalValue，它就是该元素在samller中出现的次数 减去 该元素在bigger中出现的次数
     * 那么该元素在bigger中出现的次数 = initValue - finalValue = -(finalValue - initValue)
     * intiValue已经丢失了，但是再次遍历smaller时，可以通过递减操作来求(finalValue - initValue)
     * 即碰见相同元素时，把finalValue--，遍历完成后value的值就是(finalValue - initValue)
     * 进一步思考，其实不需要遍历完成，例如initValue = 5, finalValue = 2，那么改元素在最终结果中会出现3次，再次遍历smaller时会递减5次
     * 递减两次时，value = 0
     * 接下来的三次，因为value == 0，可以直接添加到最终结果中，即流程走到value <= 的分支
     * 这样的实际效果和先遍历完成一次smaller求(finalValue - initValue)，再遍历smaller添加到最终结果中是等价的，少一次遍历，性能更好
     * intiValue == fianlValue时，递减到value == 0时，后续不会再遇到同样的元素，也就不会添加到最终结果中
     * <p>
     * 复杂度分析，设N,M分别为nums1和nums2的长度
     * 流程中要先遍历一次smaller，再遍历一次bigger，最后还要遍历一次smaller，所以时间复杂度为O(N+M)
     * 需要一个hash表，它的大小是较小的那个数组的长度，所以空间复杂度为O(min(N, M))
     * 如果算上List转换成array的操作，则需要额外O(min(N, M))的空间和时间，不过时间个空间复杂度不变
     */
    public int[] intersect(int[] nums1, int[] nums2) {
        if (nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0) {
            return new int[]{};
        }
        int[] smaller;
        int[] bigger;
        if (nums1.length < nums2.length) {
            smaller = nums1;
            bigger = nums2;
        } else {
            smaller = nums2;
            bigger = nums1;
        }
        // 第一次遍历完smaller后，所有value的值初始化为对应元素在smaller中出现的次数
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < smaller.length; i++) {
            map.merge(smaller[i], 1, (oldValue, newValue) -> oldValue + 1);
        }
        // 遍历bigger，对应元素的value--
        for (int i = 0; i < bigger.length; i++) {
            // 如果另一个数组的值在hash表中存在，则设置value的值为value - 1
            if (map.containsKey(bigger[i])) {
                map.merge(bigger[i], 1, (oldValue, newValue) -> oldValue - 1);
            }
        }
        // 因为value变成了次数，所以遍历bigger时无法直接判断该元素是不是交集的元素
        List<Integer> resultList = new ArrayList<>();
        // 最后再遍历一次smaller，把最终结果填充进去
        for (int i = 0; i < smaller.length; i++) {
            if (map.get(smaller[i]) <= 0) {
                resultList.add(smaller[i]);
            } else {
                map.merge(smaller[i], 1, (oldValue, newValue) -> oldValue - 1);
            }
        }
        // java中Integer[]和int[]不是同一个东西，需要特殊处理
        int[] result = new int[resultList.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = resultList.get(i);
        }
        return result;
    }
}
