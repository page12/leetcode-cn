package p03;

import common.TreeNode;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/house-robber-iii/
 */
public class Problem0337 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 有两种情况
     * 1、选择根节点 root，此时金额 = root.val + rob(root.left.left) + rob(root.right.right)
     * 2、不选择根节点root，此时金额 = rob(root.left) + rob(root.right)
     * 两个金额中较大的那一个就是最大金额
     * 利用递归，很容易就能得到结果
     * 正常的递归比较慢，需要加一个缓存来避免重复处理某些节点
     */
    public int rob(TreeNode root) {
        Map<TreeNode, Integer> cache = new HashMap<>();
        return recurse(root, cache);
    }

    private int recurse(TreeNode root, Map<TreeNode, Integer> cache) {
        if (root == null) {
            return 0;
        }
        if (cache.containsKey(root)) {
            return cache.get(root);
        }
        int way1 = root.val;
        if (root.left != null) {
            way1 += recurse(root.left.left, cache);
            way1 += recurse(root.left.right, cache);
        }
        if (root.right != null) {
            way1 += recurse(root.right.left, cache);
            way1 += recurse(root.right.right, cache);
        }
        int way2 = recurse(root.left, cache) + recurse(root.right, cache);
        int result = Math.max(way1, way2);
        cache.put(root, result);
        return result;
    }
}
