package p03;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/counting-bits/
 */
public class Problem0338 {

    public static void main(String[] args) {
        Problem0338 p = new Problem0338();
        System.err.println(Arrays.toString(p.countBits(2)));
        System.err.println(Arrays.toString(p.countBits(5)));
        System.err.println(Arrays.toString(p.countBits(1)));
        System.err.println(Arrays.toString(p.countBits(3)));
        System.err.println(Arrays.toString(p.countBits(7)));
        System.err.println(Arrays.toString(p.countBits(15)));
    }

    /**
     * 思路：不允许用内置函数，又要O(N)时间，我第一想到的就是找规律，还真给找到了
     * 下面简单描述下这个规律
     * 设x为数字i的二进制的有效的bit位的数量（二进制表示，去除左边的0，不考虑负数）
     * x = 1时，i = 0, 1，结果为[0, 1]
     * x = 2时，i = 0 -> 3，结果为[0, 1, 1, 2]
     * x = 3时，i = 0 -> 7，结果为[0, 1, 1, 2, 1, 2, 2, 3]
     * x = 4时，i = 0 -> 15，结果为[0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4]
     * 设f(x)为上面的例子的的结果对应的数组
     * 那么可以观察到f(x+1)与f(x)的差别就等于{f(x)中所有元素分别加1后得到的数组}
     * 这个很好理解，x = i 与x = i + 1的差别的来源在于x = i + 1的有效二进制表示中最右边多了一位1
     * 假设n = 2的幂减1，这样就与上面的x表示相符合
     * 那么有f(2n + 1) = f(n) 合并 {f(n)中的每个元素分别加1得到的数组}
     * n从0开始，所以合并操作两边的数组的长度一致
     * 利用这个操作，可以从x = 1开始，不断复制已经生成的结果并加1到后面
     * 直接在返回结果的数组上操作，每次复制操作时前面一半是已经在数组中生成好了，也不用创建新数组
     * 除了x = 1时的两个元素（0和1），其余都是通过复制操作生成的，所以总的时间复杂度为O(n)
     * 除了返回结果外，只需要常数个变量的空间，所以空间复杂度为O(1)
     */
    public int[] countBits(int n) {
        // 题目有假设n >= 0
        if (n == 0) {
            return new int[]{0};
        }
        int[] result = new int[n + 1];
        // 设x为数字i的二进制的有效的bit位的数量（二进制表示，去除左边的0，不考虑负数）
        // 初始化x = 1时的数组
        result[0] = 0;
        result[1] = 1;
        // len是已经生成好的长度
        int len = 2;
        while (len < n + 1) {
            // 因为数组复制后要执行加1操作，所以不用System.arraycopy
            for (int i = len; i < Math.min(n + 1 - len, len) + len; i++) {
                result[i] = result[i - len] + 1;
            }
            len = len * 2;
        }
        return result;
    }
}
