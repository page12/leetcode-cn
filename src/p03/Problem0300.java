package p03;

/**
 * https://leetcode-cn.com/problems/longest-increasing-subsequence/
 */
public class Problem0300 {

    public static void main(String[] args) {
        Problem0300 p = new Problem0300();
        System.err.println(p.lengthOfLIS(new int[]{10, 9, 2, 5, 3, 7, 101, 18}));
        System.err.println(p.lengthOfLIS(new int[]{0, 1, 0, 3, 2, 3}));
        System.err.println(p.lengthOfLIS(new int[]{7, 7, 7, 7, 7, 7, 7}));
    }

    /**
     * 设f(i)为以i为结尾的递增序列的长度
     * 则f(i) = max{f(x)} + 1，其中x代指i左边所有比它小的数
     * 如果i左边没有比它小的数，则 f(i) = 1，因此 f(0) = 1
     * 利用这个递推公式，很容易写出自底向上的动态规划的代码
     */
    public int lengthOfLIS(int[] nums) {
        // 题目有假设，不做校验
        int result = 0;
        int[] dp = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            result = Math.max(result, dp[i]);
        }
        return result;
    }
}
