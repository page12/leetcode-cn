package p03;

/**
 * https://leetcode-cn.com/problems/integer-replacement/
 */
public class Problem0397 {

    public static void main(String[] args) {
        Problem0397 p = new Problem0397();
//        System.err.println(p.integerReplacement(8) == 3);
//        System.err.println(p.integerReplacement(7) == 4);
//        System.err.println(p.integerReplacement(4) == 2);
//        System.err.println(p.integerReplacement(6) == 3);
        System.err.println(p.integerReplacement(Integer.MAX_VALUE));
    }

    /**
     * 思路：
     * 因为偶数会除以2，这个的下降速度是最快的
     * 所以应该尽量创造除以2的条件，也就是让除以2的次数变多
     * 奇数加1减1都能变成偶数，此时应该看那种能够连续除2的次数更多
     * 举个例子，13，变成12和14都行，但是变成12后可以连续两次除2，变成14后只能一次除2，所以应该变成12
     * <br/>
     * 用二进制观察更清楚，二进制最右边的连续0的个数就是能够连续除2的次数
     * 13 = 1101
     * 12 = 1100
     * 14 = 1110
     * 题目就是要把二进制变成1
     * 对于奇数，可以分成两种情况，一种是4n+1，一种是4n+3，也就是以01结尾的和以11结尾的
     * 其他情况：
     * 比如二进制以101结尾的，减1之后连除两次，总共只需要3次
     * 而加1再除2，至少需要4次（变成11时，再减1是次数最少的方法），比减1的方式慢
     * 因此可以归纳到以01结尾的
     * 再比如二进制以111结尾的
     * 加1之后连除3次，总共需要4次，而减1变成0再移位最少也需要4次，不会比加1的方式快
     * 因此可以归纳到以11结尾的
     * <br/>
     * 综合上面的可以得到，次数最少的方式为
     * 如果是偶数，则除2
     * 如果是4n+1，则减1再除2
     * 如果是4n+1，则加1再除2
     * <br/>
     * 两个特殊情况：
     * 如果是1，则直接返回0
     * 如果是3，则先减1会更快，只需要两次
     */
    public int integerReplacement(int n) {
        // 题目有假设，不做校验
        if (n == 1) {
            return 0;
        }
        // 特殊处理3这个数
        if (n == 3) {
            return 2;
        }
        int count = 0;
        while (n != 1 && n != 3) {
            int lowTwo = n & 0x3;
            if (lowTwo == 1) {
                n = n - 1;
            } else if (lowTwo == 3) {
                n = n + 1;
            } else {
                // 使用无符号右移
                // 因为 Integer.MAX_VALUE 这个特殊的数，加一后变成负数了
                n = n >>> 1;
            }
            count++;
        }
        // 特殊处理3这个数
        return n == 1 ? count : count + 2;
    }
}
