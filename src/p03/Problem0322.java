package p03;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/coin-change/
 */
public class Problem0322 {

    public static void main(String[] args) {
        Problem0322 p = new Problem0322();
        System.err.println(p.coinChange(new int[]{1, 2, 5}, 11) == 3);
        System.err.println(p.coinChange(new int[]{2}, 3) == -1);
        System.err.println(p.coinChange(new int[]{1}, 0) == 0);
        System.err.println(p.coinChange(new int[]{1}, 1) == 1);
        System.err.println(p.coinChange(new int[]{1}, 2) == 2);
    }

    /**
     * 跟第279题差不多
     * 设所求结果为f(n)，则有
     * f(n) = min{ f(n-1) + f(1), f(n-2) + f(2), ... }
     * 细分讨论的话
     * 如果 n in coins，则 f(n) = 1
     * 对于分解情况 f(n) = f(n-i) + f(i)
     * 如果 n-i in coins && f(i) in coins，则 f(n) = 2
     * 如果 i in coins，则 f(n) = f(n-i) + 1
     * <br/>
     * 所以可以只用 i in coins 来进行min操作
     * 如果 f(n-i) == -1 or f(i) == -1，则代表此种情况不存在，跳过即可
     * 如果全都跳过了，就代表一定不存在拆分情况
     * 使用自底向上的方式时，从1开始，额外需要一个长度为n的数组来保存f(i)
     */
    public int coinChange(int[] coins, int amount) {
        // 题目有假设，不做校验
        if (amount == 0) {
            return 0;
        }
        if (coins.length == 0) {
            return -1;
        }
        Arrays.sort(coins);
        if (amount < coins[0]) {
            return -1;
        }
        int[] dp = new int[amount];
        Arrays.fill(dp, -1);
        dp[coins[0] - 1] = 1;
        for (int i = coins[0] + 1; i <= amount; i++) {
            int min = Integer.MAX_VALUE;
            for (int j = 0; j < coins.length && coins[j] <= i; j++) {
                int coin = coins[j];
                if (coin == i) {
                    min = 1;
                    break;
                }
                if (coin > i || dp[coin - 1] == -1 || dp[i - coin - 1] == -1) {
                    continue;
                }
                min = Math.min(dp[coin - 1] + dp[i - coin - 1], min);
            }
            dp[i - 1] = min == Integer.MAX_VALUE ? -1 : min;
        }
        return dp[dp.length - 1];
    }
}
