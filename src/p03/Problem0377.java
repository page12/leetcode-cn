package p03;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/combination-sum-iv/
 */
public class Problem0377 {

    public static void main(String[] args) {
        Problem0377 p = new Problem0377();
        System.err.println(p.combinationSum42(new int[]{1, 2, 3}, 4));
        System.err.println(p.combinationSum42(new int[]{9}, 3));
        System.err.println(p.combinationSum42(new int[]{2, 5}, 3));
    }

    /**
     * 思路：
     * 为了方便处理，可以先对nums进行升序排序
     * 先把target拆分成两部分，第一部分是一个数字，第二部分是待拆分
     * 总的拆分情况 = 第一部分的拆法 * 第二部分的所有拆法
     * 第一部分的拆法总共有nums.length种情况
     * 如果待拆分的数小于nums的最小值，则不能拆分，所有拆法 = 0
     * 递归中会重复拆分很多相同的数，因此可以使用带缓存的递归，记录相同的数的所有可能拆分情况
     */
    public int combinationSum4(int[] nums, int target) {
        // 递归解法代码
        // 题目有假设，不做校验
        Arrays.sort(nums);
        Map<Integer, Integer> cache = new HashMap<>();
        return recurse(nums, target, cache);
    }

    /**
     * 递归拆分
     *
     * @param nums  拆分单位的数组
     * @param t     待拆分的数字
     * @param cache 递归缓存
     * @return 所有拆分情况的数量
     */
    private int recurse(int[] nums, int t, Map<Integer, Integer> cache) {
        if (cache.containsKey(t)) {
            return cache.get(t);
        }
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            int count = 0;
            int second = t - nums[i];
            if (second == 0 || second == nums[0]) {
                // 拆分完毕
                count = 1;
            } else if (second < nums[0]) {
                // 不能拆分
                count = 0;
            } else {
                // 继续拆分
                count = recurse(nums, second, cache);
            }
            cache.put(second, count);
            result += count;
        }
        return result;
    }

    /**
     * 思路：
     * 还是先升序排序nums数组，根据上面递归的拆分思路
     * 如果t能被拆分，设拆分情况总是为f(t)
     * 对于一种拆分情况 t = i + (t-i)，其中 i in nums
     * 有 f(t) = f(t-i)
     * 因此 f(t) = sum(f(t-i))，其中 i in nums
     * 这样就得到了动态规划的递推公式
     * 特殊值
     * 如果 t = nums[0]，则 f(t) = 1
     * 如果 0 < t < nums[0]，则f(t) = 0
     * 如果 t in nums，则还有一种情况，那就是不拆分，此时f(t)要加1
     * 因为不能随便拆分，拆分出来的数必须是nums指定的数，所以可以把i的范围限制在nums数组中
     */
    public int combinationSum42(int[] nums, int target) {
        // 动态规划解法代码
        // 题目有假设，不做校验
        Arrays.sort(nums);
        // 多申请一格空间，这样下标就是数字本身
        int[] dp = new int[target + 1];
        for (int i = 1; i <= target; i++) {
            if (i < nums[0]) {
                dp[i] = 0;
                continue;
            }
            int count = 0;
            for (int num : nums) {
                if (i < num) {
                    break;
                }
                if (num == i) {
                    count += 1;
                    break;
                }
                count += dp[i - num];
            }
            dp[i] = count;
        }
        return dp[dp.length - 1];
    }
}
