package p03;

/**
 * https://leetcode-cn.com/problems/reverse-string/
 */
public class Problem0344 {

    public static void main(String[] args) {

    }

    /**
     * 思路：循环一半，交换对应下标的值，即可
     */
    public void reverseString(char[] s) {
        if (s == null || s.length <= 1) {
            return;
        }
        for (int i = 0; i < s.length / 2; i++) {
            char temp = s[i];
            s[i] = s[s.length - 1 - i];
            s[s.length - 1 - i] = temp;
        }
    }
}
