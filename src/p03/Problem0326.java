package p03;

/**
 * https://leetcode-cn.com/problems/power-of-three/
 */
public class Problem0326 {

    public static void main(String[] args) {

    }

    /**
     * 思路：最直观的方法，循环除以3，判断最后能不能得到1
     * 要用取模运算提前判断能不能整除
     */
    public boolean isPowerOfThree(int n) {
        if (n <= 0) {
            return false;
        }
        while (n % 3 == 0) {
            n = n / 3;
        }
        return n == 1;
    }
}
