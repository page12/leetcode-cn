package p03;

/**
 * https://leetcode-cn.com/problems/count-numbers-with-unique-digits/
 */
public class Problem0357 {

    public static void main(String[] args) {
        Problem0357 p = new Problem0357();
        System.err.println(p.countNumbersWithUniqueDigits(1) == 10);
        System.err.println(p.countNumbersWithUniqueDigits(2) == 91);
        System.err.println(p.countNumbersWithUniqueDigits(3) == 739);
    }

    /**
     * 思路：
     * 推导数学规律
     * 如果x是1位数，则 x = 10
     * 如果x是2位数：
     * 当 x 不包含0时，所有情况是排列数 A(9,2)（以9为底，下同）
     * 当 x 包含0时，0不能打头，只有1个位置可以放0，剩下的1位任意，所以此时所有情况是排列 A(9,1)
     * 如果x是3位数：
     * 当 x 不包含0时，所有情况是排列数 A(9,3)（以9为底，下同）
     * 当 x 包含0时，0不能打头，有2个位置可以放0，剩下的2位任意，所以此时所有情况是排列 2 * A(9,2)
     * 如果x是4位数：
     * 当 x 不包含0时，所有情况是排列数 A(9,4)（以9为底，下同）
     * 当 x 包含0时，0不能打头，有3个位置可以放0，剩下的3位任意，所以此时所有情况是排列 3 * A(9,3)
     * ...
     * 可以归纳出，当x是i位数时，所有情况 = A(9,i) + (i-1)*A(9,i-1)
     * 特殊情况：
     * 当x是10位数时，此时一定包含0，所以此时的情况 = 9*A(9,9)
     * 当x是11位数时，此时一定有重复数字，情况为0
     * <br/>
     * 本题要求的是i=1到i=n时的和，循环累加一下就可以得到
     */
    public int countNumbersWithUniqueDigits(int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return 10;
        }
        int a9i = 9;
        int result = 10;
        for (int i = 2; i <= n; i++) {
            // 把n <= 10放在循环条件上，循环体可以简单合并下
            // 不过不合并时，逻辑可以看得更清楚
            if (n == 10) {
                result += (i - 1) * a9i;
                break;
            }
            result = result + (i - 1) * a9i + a9i * (9 - i + 1);
            a9i = a9i * (9 - i + 1);
        }
        return result;
    }
}
