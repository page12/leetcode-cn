package p03;

/**
 * https://leetcode-cn.com/problems/integer-break/
 */
public class Problem0343 {

    public static void main(String[] args) {
        Problem0343 p = new Problem0343();
        System.err.println(p.integerBreak(2) == 1);
        System.err.println(p.integerBreak(10) == 36);
    }

    /**
     * 跟第279题差不多
     * 设所求结果为f(n)，先把n分解为两个数相加 n-i 和 i
     * 得到 f(n) = f(n-i) * f(i)
     * 但是这样做是有点问题的
     * 对于较小的数（小于4），拆分之后的最大乘积会小于本身，这种情况要特殊处理下
     * 初始值f(1) = 1，其余的直接套用递推公式就行
     * 使用自底向上的方式时，从1开始，额外需要一个长度为n的数组来保存f(i)
     */
    public int integerBreak(int n) {
        // 题目有假设，不做校验
        int[] dp = new int[n];
        dp[0] = 1;
        for (int i = 2; i <= n; i++) {
            int max = 0;
            for (int j = 1; j <= i / 2; j++) {
                int a = j < 4 ? j : dp[j - 1];
                int b = i - j < 4 ? i - j : dp[i - j - 1];
                max = Math.max(a * b, max);
            }
            dp[i - 1] = max;
        }
        return dp[n - 1];
    }
}
