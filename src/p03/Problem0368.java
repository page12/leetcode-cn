package p03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/largest-divisible-subset/
 */
public class Problem0368 {

    public static void main(String[] args) {
        Problem0368 p = new Problem0368();
        System.err.println(p.largestDivisibleSubset(new int[]{1, 2, 3}));
        System.err.println(p.largestDivisibleSubset(new int[]{1, 2, 4, 8}));
    }

    /**
     * 思路：
     * 先进行一次升序排序
     * 然后参考第300题和第354题，思路基本一样
     * 那两题都是递增，一个是普通递增，另外一个是长宽都大于的递增
     * 本题的递增是能整除
     * 设f(i)为排序后以第i个信封结尾的套娃长度
     * 则f(i) = max{f(x)} + 1，其中x代指i左边所有i的约数对应的下标
     * 如果i左边没有它的约数，则 f(i) = 1，因此 f(0) = 1
     * 利用这个递推公式，很容易写出自底向上的动态规划的代码
     * <br/>
     * 本题最终是求集合的内容
     * 可以在求出dp数组后，最大值就是集合的大小，设这个最大值对应的原数组的数为t
     * 先找到dp最大值的下标（或者求dp最大值时提前记录下标）
     * 然后向左遍历，找到dp值为dp最大值减1，并且原数组对应的数是t的约数，把它加入集合，然后 t = 当前原数组对应的数
     * 继续向左，找到dp值为dp最大值减2，并且原数组对应的数是t的约数，把它加入集合，然后 t = 当前原数组对应的数
     * 以此类推即可
     */
    public List<Integer> largestDivisibleSubset(int[] nums) {
        // 题目有假设，不做校验
        // 先升序排序
        Arrays.sort(nums);
        int[] dp = new int[nums.length];
        int globalMax = 0;
        int globalMaxIdx = -1;
        // 求dp数组，以及dp最大值和dp最大值对应的下标
        for (int i = 0; i < nums.length; i++) {
            int max = 1;
            for (int j = 0; j < i; j++) {
                if (nums[i] % nums[j] == 0) {
                    max = Math.max(dp[j] + 1, max);
                }
            }
            dp[i] = max;
            if (max > globalMax) {
                globalMax = max;
                globalMaxIdx = i;
            }
        }
        // 组装集合内容
        List<Integer> result = new ArrayList<>();
        int t = nums[globalMaxIdx];
        for (int i = globalMaxIdx; i >= 0; i--) {
            if (dp[i] == globalMax - result.size() && t % nums[i] == 0) {
                result.add(nums[i]);
                t = nums[i];
            }
        }
        return result;
    }
}
