package p03;

/**
 * https://leetcode-cn.com/problems/reverse-vowels-of-a-string/
 */
public class Problem0345 {

    public static void main(String[] args) {

    }

    /**
     * 思路：左右双指针
     * 左指针初始最左边，右指针初始最右边
     * 左指针往中间走，找到元音字母就停止，然后右指针往中间走，找到元音字母就停止，并执行左右交换操作，之后左右各自往中间走一步，接着从头开始继续循环
     * 左右相遇即结束整个流程
     * 除了返回的字符串需要新创建外，只需要常数个变量的空间，所以空间复杂度为O(1)
     * 总共遍历次数为字符串的长度，所以时间复杂度为O(n)
     */
    public String reverseVowels(String s) {
        if (s == null || s.length() <= 1) {
            return s;
        }
        int left = 0;
        int right = s.length() - 1;
        char[] chars = s.toCharArray();
        while (left < right) {
            if (isVowels(chars[left])) {
                while (left < right && !isVowels(chars[right])) {
                    right--;
                }
                if (left < right) {
                    char temp = chars[left];
                    chars[left] = chars[right];
                    chars[right] = temp;
                }
                right--;
            }
            left++;
        }
        return new String(chars);
    }

    /**
     * 判断是不是元音字母
     */
    private boolean isVowels(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'
                || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U';
    }
}
