package p03;

/**
 * https://leetcode-cn.com/problems/power-of-four/
 */
public class Problem0342 {

    public static void main(String[] args) {
        Problem0342 p = new Problem0342();
        System.err.println(p.isPowerOfFour(0));
        System.err.println(p.isPowerOfFour(1));
        System.err.println(p.isPowerOfFour(2));
        System.err.println(p.isPowerOfFour(4));
        System.err.println(p.isPowerOfFour(5));
        System.err.println(p.isPowerOfFour(16));
    }

    /**
     * 思路：
     * 1、如果n是4的幂，那它也是2的幂，其二进制中只有1位是1，满足 n & (n - 1) == 0
     * 2、x = 4x等于把x右移两位
     * 4的二进制表示为0x100，1在右数第3位
     * 1也是4的幂，它的二进制表示中1在右数第1位
     * 所以4的幂的二进制表示唯一的那一位1在右数的第奇数位
     * 判断二进制中某一位是否是1可以用&运算特殊构造的二进制数
     * 要判断奇数位是不是1可以构造一个二进制数，它的右数的所有奇数位都是1
     * 它就是01010101010101010101010101010101，十六进制表示为0x55555555
     */
    public boolean isPowerOfFour(int n) {
        return (n & (n - 1)) == 0 && (0x55555555 & n) != 0;
    }
}
