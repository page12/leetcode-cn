package p03;

/**
 * https://leetcode-cn.com/problems/guess-number-higher-or-lower/
 */
public class Problem0374 {

    public static void main(String[] args) {

    }

    /**
     * 思路：二分法，不多说，只有一个注意点：
     * -1 if num is lower than the guess number，表示此时我猜的数字大于答案，此时移动右边
     * 1 if num is higher than the guess number，表示此时我猜的数字小于答案，此时移动左边
     * 不要理解反了
     */
    public int guessNumber(int n) {
        if (n == 1) {
            return 1;
        }
        int start = 1;
        int end = n;
        while (start <= end) {
            int mid = (end - start) / 2 + start;
            int guessResult = guess(mid);
            if (guessResult == 0) {
                return mid;
            } else if (guessResult < 0) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        // 一定能找到，为了编译通过，这里补充个return
        return (end - start) / 2 + start;
    }

    /**
     * 这个方法是题目内置的，不用实现
     * 这里写出来只是为了编译通过
     */
    private int guess(int num) {
        return 0;
    }
}
