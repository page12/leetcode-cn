package p03;

/**
 * https://leetcode-cn.com/problems/wiggle-subsequence/
 */
public class Problem0376 {

    public static void main(String[] args) {
        Problem0376 p = new Problem0376();
        System.err.println(p.wiggleMaxLength(new int[]{1, 7, 4, 9, 2, 5}) == 6);
        System.err.println(p.wiggleMaxLength(new int[]{1, 17, 5, 10, 13, 15, 10, 5, 16, 8}) == 7);
        System.err.println(p.wiggleMaxLength(new int[]{0, 0, 0}) == 1);
        System.err.println(p.wiggleMaxLength(new int[]{3, 3, 3, 2, 5}) == 3);
    }

    /**
     * 思路：
     * 摆动数组是上下上下上下上下这种，对应下上下上下上这种可以通过移位来得到
     * 也就是任意一段摆动序列，都可以被其他摆动序列拼接上
     * 拼接方式为：上下 + 下上 = 上下上，两个下可以是同一个数，其余的类推
     * 前提是两个摆动序列最后的方向发生了变化
     * 摆动序列的方向可以通过差值的符号来定义，有正负零三种情况
     * 考虑到相等数字的情况，如果当前符号是0时，不能被之前的序列拼接
     * 其余符号不一致的情况，都可以拼接
     */
    public int wiggleMaxLength(int[] nums) {
        // 题目有假设，不做校验
        if (nums.length == 1) {
            return 1;
        }
        int sign = nums[1] - nums[0];
        int result = sign == 0 ? 1 : 2;
        for (int i = 2; i < nums.length; i++) {
            int currentSign = nums[i] - nums[i - 1];
            if (currentSign != 0 && (sign == 0 || currentSign * sign < 0)) {
                result += 1;
                sign = currentSign;
            }
        }
        return result;
    }
}
