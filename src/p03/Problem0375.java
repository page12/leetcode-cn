package p03;

/**
 * https://leetcode-cn.com/problems/guess-number-higher-or-lower-ii/
 */
public class Problem0375 {

    public static void main(String[] args) {
        Problem0375 p = new Problem0375();
        System.err.println(p.getMoneyAmount(5));
    }

    /**
     * 思路：
     * 重点是理解题目的意思
     * 题目的意思是求每一步都是可能是最坏情况下的花费总和的最小值
     * 不是故意选择最坏情况
     * 可以理解为数字总是想躲着你，你选择一步后，如果不是必中，那么总是不中，并且它出总是现在剩余花费较大的那一边
     * <br/>
     * 设区间[i,j]的总代价为cost(i,j)
     * 某一步可能是最坏情况 等价于 这一步没有猜中，其花费为 k
     * 剩下的只能在左边或者右边找，左边的花费为 cost(i,k-1)，右边的花费为 cost(k+1,j)
     * 剩下的情况也可能是最坏情况，此时花费应该是左右两边的较大值
     * 因此以i为第一步的总花费为 cost(i,j) = k + max{ cost(i,k-1), cost(k+1,j) }
     * 这一步有[i,k]种情况，其中的最小值就是第一步的最小值
     * 设区间内总代价的最小值为 min(i,j)
     * 则 min(i,j) = min{ k + max{ cost(i,k-1), cost(k+1,j) } }, k从i到j
     * 因为这个公式最终是要求最小值
     * 而dp(i,k-1)是cost(i,k-1)在所有最坏情况下的最小值，因此上面的公式可以变为
     * min(i,j) = min{ k + max{ min(i,k-1), min(k+1,j) } }, k从i到j
     * 题目要求的最终结果就是 min(1,n)
     * <br/>
     * 特殊情况：
     * 当i=j时，区间内只有一个数字，此时必中，花费为0，因此 i=j 时，f(i,j) = 0
     * 当i=j-1时，此时最坏情况是第一步猜错，花费为 min(i,j) = i
     * 剩下的通常情况可以用递推公式推导出来
     */
    public int getMoneyAmount(int n) {
        // 题目有假设，不做校验
        int[][] min = new int[n][n];
        // 为了跟上面公式保持一致，因此j放在外层
        // 因为min[i][k-1]是左边元素，min[k+1][j]是下边元素，所以从最下最左开始循环
        // 数组下标等于数字减1
        for (int i = n; i >= 1; i--) {
            for (int j = i; j <= n; j++) {
                if (i == j) {
                    min[i - 1][j - 1] = 0;
                } else if (i == j - 1) {
                    min[i - 1][j - 1] = i;
                } else {
                    int minCost = Integer.MAX_VALUE;
                    for (int k = i + 1; k < j; k++) {
                        int cost = k + Math.max(min[i - 1][k - 1 - 1], min[k + 1 - 1][j - 1]);
                        minCost = Math.min(cost, minCost);
                    }
                    min[i - 1][j - 1] = minCost;
                }
            }
        }

        return min[0][n - 1];
    }
}
