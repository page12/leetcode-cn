package p03;

/**
 * https://leetcode-cn.com/problems/range-sum-query-immutable/
 */
public class Problem0303 {

    public static void main(String[] args) {

    }

    /**
     * 思路：很简单，下面的代码是最直接的方式
     * 不过提交这个答案执行起来很慢
     * 看了下官方的答案，利用NumArray中内置数组初始化后不可变性质
     * 用O(n)的额外空间提前计算并存储sum([0, i=0到n])的n个结果
     * sum([left, right])的结果 =  sum([0, right]) - sum[0, left - 1]
     * 提前计算并存储n个结果花费O(n)的时间和O(n)的空间
     * 后续每次计算只需要花费O(1)的时间，平均性能更高
     * 官方这种还是有道理的，用一个类来专门计算的场景确实大多数会缓存结果来提高性能
     * 如果是单独用一个函数，那么就不需要缓存结果了，根下面一样
     */
    private static class NumArray {

        private int[] nums;

        public NumArray(int[] nums) {
            this.nums = nums;
        }

        public int sumRange(int left, int right) {
            // 题目有假设，不考虑越界情况
            int sum = 0;
            while (left <= right) {
                sum += nums[left++];
            }
            return sum;
        }
    }
}
