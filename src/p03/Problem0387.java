package p03;

/**
 * https://leetcode-cn.com/problems/first-unique-character-in-a-string/
 */
public class Problem0387 {

    public static void main(String[] args) {
        Problem0387 p = new Problem0387();
        System.err.println(p.firstUniqChar("leetcode"));
        System.err.println(p.firstUniqChar("loveleetcode"));
    }

    /**
     * 思路：利用一个hash表
     * 先遍历一次，记录所有次数
     * 再遍历一次，如果次数是1，就直接返回下标
     * 如果循环自动遍历完了，就代表没有找到，返回-1
     * 时间复杂度为O(N)
     * 空间复杂度为O(字符集的大小)：如果只有英文字母不区分大小写，则固定是26，也就是O(1)；如果是其他情况，则最大不超过O(N)
     * 题目中假定只有小写字母，因此是固定大小，也就是O(1)的空间复杂度
     */
    public int firstUniqChar(String s) {
        if (s == null || s.length() == 0) {
            return -1;
        }
        if (s.length() == 1) {
            return 0;
        }
        int[] charMap = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int idx = c - 'a';
            charMap[idx] = charMap[idx] + 1;
        }
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int idx = c - 'a';
            if (charMap[idx] == 1) {
                return i;
            }
        }
        return -1;
    }
}
