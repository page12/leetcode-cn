package p03;

/**
 * https://leetcode-cn.com/problems/valid-perfect-square/
 */
public class Problem0367 {

    public static void main(String[] args) {
        Problem0367 p = new Problem0367();
        System.err.println(p.isPerfectSquare(1));
        System.err.println(p.isPerfectSquare(2));
        System.err.println(p.isPerfectSquare(4));
        System.err.println(p.isPerfectSquare(7));
        System.err.println(p.isPerfectSquare(16));
        System.err.println(p.isPerfectSquare(49));
        System.err.println(p.isPerfectSquare(808201));
    }

    /**
     * 思路：二分查找
     * 根据sqrt性质，直接在[2, n/2]内查找，为了避免乘法溢出，要使用long类型，时间空间复杂度同二分查找
     * 下面的是二分法的代码
     * 其他思路：n比较小时，利用公式1 + 3 + 5 + ... + (2n - 1) = n^2来验证
     * 这需要O(n)的时间复杂度和O(1)的空间复杂度，不过实现起来比较简单，除了循环条件外没有分支判断，n比较小时实际反而比二分法快
     * 这个方法代码就不贴了
     */
    public boolean isPerfectSquare(int num) {
        if (num == 1) {
            return true;
        }
        long start = 2;
        long end = num / 2;
        while (start <= end) {
            long mid = (end - start) / 2 + start;
            long square = mid * mid;
            if (square == num) {
                return true;
            } else if (square < num) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return false;
    }
}
