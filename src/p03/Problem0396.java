package p03;

/**
 * https://leetcode-cn.com/problems/rotate-function/
 */
public class Problem0396 {

    public static void main(String[] args) {
        Problem0396 p = new Problem0396();
        System.err.println(p.maxRotateFunction(new int[]{4, 3, 2, 6}) == 26);
        System.err.println(p.maxRotateFunction(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) == 330);
    }

    /**
     * 思路：
     * 这好像是哪里的数学题
     * 举例：nums数组为abcd四个元素，那么有
     * f(0) = 0a + b + 2c+ 3d
     * f(1) = a + 2b + 3c + 0d
     * f(1) - f(0) = a + b + c - 3d = (a + b + c + d) - 4d
     * 同理可以推出 f(2) - f(1) = (a + b + c + d) - 4c
     * 设 S = a + b + c + d
     * 则一般情况为 f(n) - f(n-1) = S - nums.length * nums[nums.length - n]，n >= 1
     * f(n) = f(n-1) + S - nums.length * nums[nums.length - n]
     * S与f(0)可以一次遍历就计算出来，利用递推公式，因此很容易算出所有f(n)的值
     */
    public int maxRotateFunction(int[] nums) {
        if (nums.length == 0 || nums.length == 1) {
            return 0;
        }
        int s = 0;
        int f = 0;
        for (int i = 0; i < nums.length; i++) {
            s += nums[i];
            f += i * nums[i];
        }
        int max = f;
        for (int i = 1; i < nums.length; i++) {
            int current = f + s - nums.length * nums[nums.length - i];
            max = Math.max(max, current);
            f = current;
        }
        return max;
    }
}
