package p03;

import java.util.Arrays;
import java.util.Comparator;

/**
 * https://leetcode-cn.com/problems/russian-doll-envelopes/
 */
public class Problem0354 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 先按长或者宽进行一次升序排序
     * 然后参考第300题最长递增子序列
     * 那一题是需要一个序列递增
     * 本题需要两个序列都递增
     * 设f(i)为排序后以第i个信封结尾的套娃长度
     * 则f(i) = max{f(x)} + 1，其中x代指i左边所有比它小的信封（长宽都要小）的下标
     * 如果i左边没有比它小的信封，则 f(i) = 1，因此 f(0) = 1
     * 利用这个递推公式，很容易写出自底向上的动态规划的代码
     */
    public int maxEnvelopes(int[][] envelopes) {
        // 题目有假设，不做校验
        int result = 0;
        Arrays.sort(envelopes, Comparator.comparingInt(array -> array[0]));
        int[] dp = new int[envelopes.length];
        for (int i = 0; i < envelopes.length; i++) {
            int max = 1;
            for (int j = 0; j < i; j++) {
                if (envelopes[j][0] < envelopes[i][0] && envelopes[j][1] < envelopes[i][1]) {
                    max = Math.max(dp[j] + 1, max);
                }
            }
            dp[i] = max;
            result = Math.max(result, dp[i]);
        }
        return result;
    }
}
