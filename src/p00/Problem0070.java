package p00;

/**
 * https://leetcode-cn.com/problems/climbing-stairs/
 */
public class Problem0070 {

    public static void main(String[] args) {
        Problem0070 p = new Problem0070();
        for (int i = 1; i <= 50; i++) {
            System.err.println(i + " result is " + p.climbStairs(i));
        }
    }

    /**
     * 思路：设f(n)为n阶楼梯的所有方法
     * 因为一次只能上1个台阶阶或者2个台阶
     * 所以只有两种方法可以上到第n阶
     * 1、在第n-1阶上1个台阶
     * 2、在第n-2阶上2个台阶
     * 最终得到递推公式：f(n) = f(n-1) + f(n-2)
     * 由题目描述得到特殊值：f(1) = 1，f(2) = 2
     * 因此结果就是斐波那契数列：fib(n+1)
     * 递归会溢出，因此使用带有缓存的循环来处理
     * 斐波那契数列是指数级增长，有符号int型结果最多只能算到46，本题最多算到45
     */
    public int climbStairs(int n) {
        // 题目有假设，不考虑负数和0
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        // 缓存f(n-1)
        int minusOne = 2;
        // 缓存f(n-2)
        int minusTwo = 1;
        int result = 0;
        for (int i = 3; i <= n; i++) {
            result = minusOne + minusTwo;
            minusTwo = minusOne;
            minusOne = result;
        }
        return result;
    }
}
