package p00;

/**
 * https://leetcode-cn.com/problems/minimum-path-sum/
 */
public class Problem0064 {

    public static void main(String[] args) {
        Problem0064 p = new Problem0064();
        System.err.println(p.minPathSum(new int[][]{{1, 3, 1}, {1, 5, 1}, {2, 4, 1}}) == 7);
        System.err.println(p.minPathSum(new int[][]{{1, 2, 3}, {4, 5, 6}}) == 12);
    }

    /**
     * 思路：
     * 和第62题 63题相似
     * 很容易得出递推公式
     * f(m-1, n-1) = grid[m-1][n-1] + min{ f(m-2, n-1), f(m-1, n-2) }
     * 对于特殊点(0, 0)，f(0,0) = grid[0][0]
     * 对于第一行，f(0, n-1) = grid[0][n-1] + f(0, n-2)
     * 对于第一列，f(m-1, 0) = grid[m-1][0] + f(m-2, 0)
     * 这样做时间复杂度O(n * m)，空间复杂度O(n * m)
     * 也可以只用一行或者一列数组，此时空间复杂度降为 O(min(n, m))
     */
    public int minPathSum(int[][] grid) {
        // 题目有假设，不做校验
        int m = grid.length;
        int n = grid[0].length;
        int[][] temp = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) {
                    temp[i][j] = grid[i][j];
                } else if (i == 0) {
                    temp[i][j] = grid[i][j] + temp[i][j - 1];
                } else if (j == 0) {
                    temp[i][j] = grid[i][j] + temp[i - 1][j];
                } else {
                    temp[i][j] = grid[i][j] + Math.min(temp[i][j - 1], temp[i - 1][j]);
                }
            }
        }
        return temp[m - 1][n - 1];
    }
}
