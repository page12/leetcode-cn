package p00;

/**
 * https://leetcode-cn.com/problems/add-binary/
 */
public class Problem0067 {

    public static void main(String[] args) {
        System.err.println(new Problem0067().addBinary("11", "1"));
        System.err.println(new Problem0067().addBinary("1", "0"));
        System.err.println(new Problem0067().addBinary("0", "0"));
        System.err.println(new Problem0067().addBinary("1", "1"));
        System.err.println(new Problem0067().addBinary("1111", "1111"));
    }

    /**
     * 思路：和第66题很像，转成可能数字会溢出。这题两个数是任意的，进位导致长度加1是一般情况
     */
    public String addBinary(String a, String b) {
        // 题目有假设，不做校验
        // 进位导致长度加1的标志
        boolean addDigitFlag = false;
        char[] sumCharArray = new char[Math.max(a.length(), b.length())];
        // 先加
        for (int i = 0; i < sumCharArray.length; i++) {
            sumCharArray[sumCharArray.length - 1 - i] = sumOfDigit(a, b, i);
        }
        // 再处理进位
        for (int i = sumCharArray.length - 1; i >= 0; i--) {
            if (sumCharArray[i] > '1') {
                if (i != 0) {
                    // 进位，最多进1
                    sumCharArray[i - 1] = (char) (sumCharArray[i - 1] + 1);
                } else {
                    // 进位导致长度加1
                    addDigitFlag = true;
                }
                // 当前位进位后减去进位的进制数值（二进制是2，十进制是10）
                sumCharArray[i] = (char) (sumCharArray[i] - 2);
            }
        }
        String result = new String(sumCharArray);
        // 进位导致长度加1
        if (addDigitFlag) {
            result = '1' + result;
        }
        return result;
    }

    /**
     * 按位相加，兼容长度不相等的情况
     */
    private char sumOfDigit(String a, String b, int i) {
        if (i >= a.length()) {
            return b.charAt(b.length() - 1 - i);
        }
        if (i >= b.length()) {
            return a.charAt(a.length() - 1 - i);
        }
        return (char) (a.charAt(a.length() - 1 - i) + b.charAt(b.length() - 1 - i) - '0');
    }
}
