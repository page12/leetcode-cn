package p00;

/**
 * https://leetcode-cn.com/problems/unique-binary-search-trees/
 */
public class Problem0096 {

    public static void main(String[] args) {
        Problem0096 p = new Problem0096();
        System.err.println(p.numTrees(1) == 1);
        System.err.println(p.numTrees(2) == 2);
        System.err.println(p.numTrees(3) == 5);
        System.err.println(p.numTrees(4) == 14);
        System.err.println(p.numTrees(5) == 42);
    }

    /**
     * 思路：
     * 一、递归，自顶向下的思路
     * 这个题和第95题基本一样，那个题求所有可能情况，这个题求所有可能情况的数量
     * 参考第95题的递归解法，左子树的集合为leftChildren，右子树的集合为rightChildren
     * 当根节点是i时，能组成的数量 = leftChildren.size() * rightChildren.size()
     * leftChildren.size() = recurse(min, i - 1).size()
     * rightChildren.size() = recurse(i + 1, max).size()
     * 因此遍历所有i = 1 -> n，对 recurse(min, i - 1).size() * recurse(i + 1, max).size() 累加求和就行
     * 这样就只需要改下第95题的代码，把递归方法中的求集合改成求数量
     * 只求数量时，1 -> 4 和 6 -> 9 的数量是一样的，都有4个节点，所以只需要知道recurse(a,b)中有几个节点就行
     * 这样递归函数可以简化为一个参数 recurse(int count), count为节点个数
     * 最终结果 recurse(n) = i从1到n，公式recurse(i-1)*(n-i)的累加和
     * 递归会重复求解相同参数的问题，因此需要使用带缓存的递归
     * <br/>
     * 二、动态规划，自底向上的思路
     * 将递归改为动态规划自底向上，把之前的已经计算过值用数组缓存起来，也是一种避免重复求解相同参数的问题的方法
     * <br/>
     * 三、另一种动态规划，但不实用
     * 还有一种不实用的解法，参考第95题的动态规划
     * 设root -> root.right -> root.right.right -> ... null为right链，它的长度为rightLength
     * right链的最小长度为2，最大长度为 n+1
     * 对于n-1的树，它的每一种情况
     * 都可以通过在right链中插入节点n来生成n的树，插入位置有n个
     * 插入后生成的树的rightLength = 2,3,4,...,n+1，每个取值有且仅有一次
     * 设rightLength[n]为所有情况的rightLength，则根据rightLength[n-1]可以计算出rightLength[n]
     * 举个例子：
     * n=2时，树有两种情况，rightLength[2] = [2, 3]
     * 节点1是根节点时，rightLength = 3
     * 节点2是根节点时，rightLength = 2
     * rightLength = 2时，通过插入节点3来生成新的树，生成的新的树有2种情况，它们的rightLength分别为2和3
     * rightLength = 3时，通过插入节点3来生成新的树，生成的新的树有3种情况，它们的rightLength分别为2和3和4
     * 也就是
     * rightLength[2] = [2,3]
     * rightLength[3] = [(2,3),(2,3,4)]，利用rightLength[3]进一步可以得到
     * rightLength[4] = [((2,3),(2,3,4)),((2,3),(2,3,4),(2,3,4,5))]
     * 上面数组的每一个元素x，到下面数组时都分裂为了2,3,4,...,n+1
     * 这样无限分裂下去，就看以得到rightLength[n]，它的长度就是最终结果
     * 因为这个无限分裂增长太快了，花费空间太多，所以这个方法不实用，代码就不写了
     * 好像也能总结出数学规律，但是我总结不出来
     */
    public int numTrees(int n) {
        // 带缓存的递归，自顶向下
        int[] cache = new int[n + 1];
        return recurse(n, cache);
    }

    /**
     * 计算有count个节点的二叉搜索树的所有可能情况的数量
     * cache为缓存，命中时直接返回，不必继续往下递归
     */
    private int recurse(int count, int[] cache) {
        if (cache[count] != 0) {
            return cache[count];
        }
        if (count == 0 || count == 1) {
            cache[count] = 1;
            return 1;
        }
        int sum = 0;
        for (int i = 1; i <= count; i++) {
            int leftCount = recurse(i - 1, cache);
            int rightCount = recurse(count - i, cache);
            sum += leftCount * rightCount;
        }
        cache[count] = sum;
        return sum;
    }

    public int numTrees2(int n) {
        // 动态规划，自底向上
        int[] result = new int[n + 1];
        result[0] = 1;
        result[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                result[i] += result[j - 1] * result[i - j];
            }
        }
        return result[n];
    }
}
