package p00;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/remove-element/
 */
public class Problem0027 {

    public static void main(String[] args) {
        int[] array = new int[]{3, 2, 2, 3};
        System.err.println(new Problem0027().removeElement(array, 3));
        System.err.println(Arrays.toString(array));

        array = new int[]{0, 1, 2, 2, 3, 0, 4, 2};
        System.err.println(new Problem0027().removeElement(array, 2));
        System.err.println(Arrays.toString(array));
    }

    /**
     * 思路：因为是在原数组上更改，因此可以加一个变量记录遍历时当前已经删除的元素的个数n
     * 遍历到不需要删除的元素时，把它往前移动n个下标即可
     */
    public int removeElement(int[] nums, int val) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == val) {
                n++;
            } else {
                nums[i - n] = nums[i];
            }
        }
        return nums.length - n;
    }
}
