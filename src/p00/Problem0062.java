package p00;

/**
 * https://leetcode-cn.com/problems/unique-paths/
 */
public class Problem0062 {

    public static void main(String[] args) {
        Problem0062 p = new Problem0062();
        System.err.println(p.uniquePaths(7, 3) == 28);
        System.err.println(p.uniquePaths(3, 3) == 6);
    }

    /**
     * 思路：
     * 简单的动态规划
     * 设矩阵为m x n
     * 则到达右下顶点(m-1, n-1)必定是两种情况
     * 1、从(m-2, n-1)向下
     * 2、从(m-1, n-2)向右
     * 设到达右下顶点(m-1, n-1)的路径总数为 f(m-1, n-1)
     * 则 f(m-1, n-1) = f(m-2, n-1) + f(m-1, n-2)
     * 因为只能向下或者向左走
     * 上面的公式可以推广到除了第一行和第一列之外的所有点
     * <br/>
     * 对于特殊点(0, 0)，只有一种方法，那就是不走，因此 f(0,0) = 1
     * 对于第一行，只有一种走法，那就是向右，所以有 f(0, n-1) = f(0, n-2) = 1
     * 对于第一列，只有一种走法，那就是向下，所以有 f(m-1, 0) = f(m-2, 0) = 1
     * 综合上面的讨论出来的递推公式，反向操作，从0开始
     * 再利用一个二维数组记录对应坐标的中间结果
     * 就可以遍历一次矩阵就得到最终结果
     * 这样做时间复杂度O(n * m)，空间复杂度O(n * m)
     * <br/>
     * 因为第一行第一列的值都是已知的，求下一行的数据只需要上一行的数据就行，因此可以只用一行数组
     * 行列是可以交换的，此时空间复杂度降为 O(min(n, m))
     * 这个优化点的代码就不写了
     */
    public int uniquePaths(int m, int n) {
        if (m == 1 || n == 1) {
            return 1;
        }
        int[][] temp = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 || j == 0) {
                    temp[i][j] = 1;
                } else {
                    temp[i][j] = temp[i - 1][j] + temp[i][j - 1];
                }
            }
        }
        return temp[m - 1][n - 1];
    }
}
