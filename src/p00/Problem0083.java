package p00;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list/
 */
public class Problem0083 {

    public static void main(String[] args) {

    }

    /**
     * 思路：这是基础的链表操作
     * 遍历链表，节点有后继时，比较下后继和当前节点的值
     * 值一样则删除后继，值不一样则p = p.next继续遍历
     */
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode p = head;
        while (p.next != null) {
            if (p.val == p.next.val) {
                p.next = p.next.next;
            } else {
                p = p.next;
            }
        }
        return head;
    }
}
