package p00;

/**
 * https://leetcode-cn.com/problems/palindrome-number/
 */
public class Problem0009 {

    public static void main(String[] args) {
        System.err.println(new Problem0009().isPalindrome(0));
        System.err.println(new Problem0009().isPalindrome(1));
        System.err.println(new Problem0009().isPalindrome(10));
        System.err.println(new Problem0009().isPalindrome(-1));
        System.err.println(new Problem0009().isPalindrome(121));
        System.err.println(new Problem0009().isPalindrome(1234554321));
        System.err.println(new Problem0009().isPalindrome(-1234554321));
    }

    /**
     * 思路：负数一定不回文，0一定回文，只需要判断正数，一位数一定回文，以0结尾的正数一定不回文
     * 利用整数除法和取模运算取出正数最左边和最右边回文对应的那一位的数，判断是否相等
     * 时间复杂度O(n)，n为位数空间O(1)
     */
    public boolean isPalindrome(int x) {
        if (x >= 0 && x <= 9) {
            return true;
        } else if (x < 0) {
            return false;
        } else if (x % 10 == 0) {
            return false;
        }
        // 求位数，只需要常数个变量的空间
        int length = 0;
        int copy = x;
        while (copy != 0) {
            copy = copy / 10;
            length++;
        }
        // 利用整数除法和取模运算取出正数左边和右边回文对应的那一位的数，判断是否相等
        for (int i = 0; i < (length >>> 1); i++) {
            int right = x / (int) Math.pow(10, length - i - 1) % 10;
            int left = x % (int) Math.pow(10, i + 1) / (int) Math.pow(10, i);
            if (right != left) {
                return false;
            }
        }
        return true;
    }
}
