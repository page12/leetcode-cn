package p00;

/**
 * https://leetcode-cn.com/problems/longest-common-prefix/
 */
public class Problem0014 {

    public static void main(String[] args) {
        System.err.println("fl".equals(new Problem0014().longestCommonPrefix(new String[]{"flower", "flow", "flight"})));
        System.err.println("".equals(new Problem0014().longestCommonPrefix(new String[]{"dog", "racecar", "car"})));
    }

    /**
     * 思路：找出最短的字符串
     * 用它的每个位置的字符去整个数组的字符串中判断对应下标是否一致，一致则继续，前缀追加这个字符，不一致则停止
     * 假设字符串数组长度为n，单个字符串长度为m，时间复杂度为O(n*m)，空间为O(1)
     */
    public String longestCommonPrefix(String[] strs) {
        // 题目有假设，不做校验
        int minLength = Integer.MAX_VALUE;
        String minLengthString = "";
        for (String str : strs) {
            if (str.length() < minLength) {
                minLength = str.length();
                minLengthString = str;
            }
        }
        for (int i = 0; i < minLength; i++) {
            char c = minLengthString.charAt(i);
            if (!checkStrs(c, i, strs)) {
                return minLengthString.substring(0, i);
            }
        }
        return minLengthString;
    }

    /**
     * 判断数组中所有字符串对应位置是否是相同字符
     */
    private boolean checkStrs(char c, int i, String[] strs) {
        for (String str : strs) {
            if (i >= str.length() || c != str.charAt(i)) {
                return false;
            }
        }
        return true;
    }
}
