package p00;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/generate-parentheses/
 */
public class Problem0022 {

    public static void main(String[] args) {
        Problem0022 p = new Problem0022();
        System.err.println(p.generateParenthesis(1));
        System.err.println(p.generateParenthesis(2));
        System.err.println(p.generateParenthesis(3));
    }

    /**
     * 思路：
     * 寻找括号字符串的限制条件，然后递归算法暴力生成
     * 一个有效的长度为2n的括号字符串起码满足下面几个条件
     * 1、有n个左括号，有n个右括号
     * 2、整个字符串最左边是左括号，也就是要以左括号打头；整个字符串最右边是右括号，也就是要以右括号结尾
     * 3、在任意一点处
     * 设它左边（包含该点）的左括号数为 LL，右括号数为 LR
     * 设它右边（不包含该点）的左括号数 RL，右括号数为 RR
     * 则有 LL >= LR，RL <= RR，LL + RL = n，LR + RR = n
     * 进一步可以推导出
     * 如果 LL == LR，则 RL == RR 右边必须是左括号开头
     * 如果 LL > LR，则 RL < RR 右边也可以以右括号开头
     * 重点是第3点
     * <br/>
     * 时间复杂度：不知道，这种递归太复杂了，不知道怎么分析
     * 空间复杂度：因为每次递归调用最多连续递归2n次就一定要返回，花费的栈空间就是2n，因此空间复杂度为O(n)
     */
    public List<String> generateParenthesis(int n) {
        // 题目有假设，不做校验
        List<String> result = new ArrayList<>();
        recurse('(', n - 1, n, new StringBuilder(), result);
        return result;
    }

    /**
     * 递归生成右边
     * current = 当前的括号
     * RL = 右边左括号数量
     * RR = 右边右括号数量
     * 满足 RL <= RR
     */
    private void recurse(char current,
                         int RL,
                         int RR,
                         StringBuilder prefix,
                         List<String> list) {
        prefix.append(current);
        if (RL == 0 && RR == 0) {
            list.add(prefix.toString());
        } else if (RL == 0) {
            recurse(')', RL, RR - 1, new StringBuilder(prefix), list);
        } else if (RL == RR) {
            recurse('(', RL - 1, RR, new StringBuilder(prefix), list);
        } else {
            // RL < RR
            recurse('(', RL - 1, RR, new StringBuilder(prefix), list);
            recurse(')', RL, RR - 1, new StringBuilder(prefix), list);
        }
    }
}
