package p00;

/**
 * https://leetcode-cn.com/problems/longest-palindromic-substring/
 */
public class Problem0005 {

    public static void main(String[] args) {
        Problem0005 p = new Problem0005();
        System.err.println(p.longestPalindrome("aaa"));
//        System.err.println(p.longestPalindrome("babad"));
//        System.err.println(p.longestPalindrome("cbbd"));
//        System.err.println(p.longestPalindrome("a"));
//        System.err.println(p.longestPalindrome("ac"));
    }

    /**
     * 思路：
     * 如果一个字符串是回文字符串（长度大于2），那么首尾各去除一个字符之后，还是回文字符串
     * 这样一直去除，直到剩下的字符串长度小于等于2，此时只有两种情况
     * 1、剩下一个任意字符
     * 2、剩下两个相等的字符
     * <br/>
     * 上面这个过程是可逆的，也就是可以通过上面的方法逆向操作，来不断生成回文字符串
     * 一样也有两种方式来循环生成
     * 1、一个任意的字符，首尾各添加一个字符x y，并且x == y
     * 2、两个连在一起的相同的字符，首尾各添加一个字符x y，并且x == y
     * 当不能再首尾添加时，如果当前的回文串比上次记录的长度要长，则把它记录下来
     * 最后记录的那个回文串，就是长度最大的
     * <br/>
     * 复杂度分析，设字符串长度为N：
     * 对于一个外部的i，内部的初始为一个字符的while循环的循环次数为 min(i, N -1- i)
     * 初始为两个字符的while循环的循环次数比上面少1
     * 内外相乘，总的循环次数小于 N^2 / 2
     * 因此时间复杂度为O(N^2)
     * 除了几个确定的变量，没有使用其他额外空间，所以空间复杂度为O(1)
     */
    public String longestPalindrome(String s) {
        // 题目假设s非空
        if (s.length() == 1) {
            return s;
        }
        int start = 0;
        int end = 0;
        // 通过一个下标，找出它能生成的最长的回文串
        // 对于两个连在一起的相同的字符，规定它们的下标分别为 i - 1 和 i
        // 因此可以跳过下标为0的字符
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == s.charAt(i - 1)) {
                // 初始为两个字符
                int k = 0;
                while (i + k + 1 < s.length() && i - k - 2 >= 0 && s.charAt(i + k + 1) == s.charAt(i - k - 2)) {
                    k++;
                }
                if (k * 2 + 2 > end - start + 1) {
                    start = i - k - 1;
                    end = i + k;
                }
            }
            // 初始为一个字符
            // 和上面的 初始为两个字符 的分支不是二选一，是都要执行
            int k = 0;
            while (i + k + 1 < s.length() && i - k - 1 >= 0 && s.charAt(i + k + 1) == s.charAt(i - k - 1)) {
                k++;
            }
            if (k * 2 + 1 > end - start + 1) {
                start = i - k;
                end = i + k;
            }

        }
        return s.substring(start, end + 1);
    }
}
