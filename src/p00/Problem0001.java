package p00;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/classic/problems/two-sum/
 */
public class Problem0001 {

    public static void main(String[] args) {
        System.err.println(Arrays.toString(new Problem0001().twoSum(new int[]{2, 7, 11, 15}, 9)));
        System.err.println(Arrays.toString(new Problem0001().twoSum(new int[]{3, 2, 4}, 6)));
        System.err.println(Arrays.toString(new Problem0001().twoSum(new int[]{3, 3}, 6)));
    }

    /**
     * 思路：要找出一个元素的配对元素，使得他们的和为target
     * 单次暴力查找一个配对元素需要遍历剩下的元素一次，时间复杂度为O(n)，总的时间复杂度为O(n^2),不符合条件
     * 利用hash表来节省查找时间，hash均匀时单次查找的时间趋近于常数（操作次数固定），时间视为O(1)，空间为O(n)
     * 总的时间复杂度为O(n)，空间复杂度为O(n)
     * 因为要使用原始下标，仅仅靠排序而不依赖别的是不行的
     */
    public int[] twoSum(int[] nums, int target) {
        // 题目有假设，这里不做校验
        // map = num -> 它的下标
        Map<Integer, Integer> map = new HashMap<>(nums.length);
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (map.containsKey(target - num)) {
                return new int[]{map.get(target - num), i};
            } else {
                map.put(num, i);
            }
        }
        return new int[]{};
    }
}
