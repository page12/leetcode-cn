package p00;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/unique-binary-search-trees-ii/
 */
public class Problem0095 {

    public static void main(String[] args) {
        Problem0095 p = new Problem0095();
        System.err.println(p.generateTrees(1).size() == 1);
        System.err.println(p.generateTrees(2).size() == 2);
        System.err.println(p.generateTrees(3).size() == 5);
        System.err.println(p.generateTrees(4).size() == 14);
        System.err.println(p.generateTrees(5).size() == 42);
    }

    /**
     * 思路：
     * 一、递归，自顶向下的思路
     * 根据二叉搜索树的性质
     * 可以知道，1 -> n 的二叉搜索树，每一个数i都可以是根节点
     * 但是要满足左子树全都小于i，右子树全都大于i
     * 也就是左子树是由 1 -> i-1 的二叉搜索树，右子树是 i+1 -> n 的二叉搜索树
     * 最终结果是其这个左子树和这个右子树的所有合并情况（左右各取一个来进行合并）
     * 这样就把问题分治为两个子问题，这两个子问题和原问题的分治方法一模一样
     * 递归函数可以额用recurse(left, right)来表示，left right 为生成的二叉搜索树中的左右边界，也就是最小值和最大值
     * <br/>
     * 二、动态规划，自底向上的思路
     * 对于节点为 1 -> n 的二叉搜索树
     * 因为n是最大的节点，所以n不能有right，从根到n节点的路径也不能有right
     * 这样可以推导出节点n只能有两种位置（把根节点当作普通节点看待，也可以算是一种）
     * 1、节点n是整棵树的根节点，其余的所有节点都在n的左子树上
     * 2、节点n不是整棵树的根节点，但是能够一直通过 root = root.right 来找到节点n，node.right = null 的就是节点n
     * 假设root为n-1时的根节点
     * 对于情况1，让 n.left = root即可
     * 对于情况2，在 root -> root.right -> root.right.right -> ... -> null 这一条right链上
     * 把节点n插入到某个下标（不能是0）之前，下标及其之后的整体变成n的左子树
     * 这种方法就是n-1个节点的树，变成n个节点的树的方法
     * 因为1个节点的树是已知的，从而可以求2个节点3个节点直到n个节点
     * 这就是利用自底向上的思路，它需要额外保存前一个的结果
     */
    public List<TreeNode> generateTrees(int n) {
        // 递归，自顶向下的思路
        return recurse(1, n);
    }

    /**
     * 递归生成所有二叉树
     * left right 为生成的二叉搜索树中的左右边界，也就是最小值和最大值
     */
    private List<TreeNode> recurse(int min, int max) {
        List<TreeNode> result = new ArrayList<>();
        if (min == max) {
            // 只有一个节点时，就一种情况
            result.add(new TreeNode(min));
            return result;
        } else if (min > max) {
            // 没有节点时，添加一个null节点用于后续left和right的赋值
            result.add(null);
            return result;
        }
        // 循环中i为根节点
        for (int i = min; i <= max; i++) {
            List<TreeNode> leftChildren = recurse(min, i - 1);
            List<TreeNode> rightChildren = recurse(i + 1, max);
            // 左右各取一个来进行合并
            for (TreeNode leftChild : leftChildren) {
                for (TreeNode rightChild : rightChildren) {
                    TreeNode root = new TreeNode(i);
                    root.left = leftChild;
                    root.right = rightChild;
                    result.add(root);
                }
            }
        }
        return result;
    }

    public List<TreeNode> generateTrees2(int n) {
        List<TreeNode> result = Collections.emptyList();
        int i = 1;
        while (i <= n) {
            result = generateNext(result, i);
            i++;
        }
        return result;
    }

    /**
     * 利用n-1的结果生成n的结果
     */
    private List<TreeNode> generateNext(List<TreeNode> list, int n) {
        List<TreeNode> result = new ArrayList<>();
        if (n == 1) {
            result.add(new TreeNode(1));
            return result;
        }
        for (TreeNode t : list) {
            // 下一次在right链中插入节点n的下标
            int lastInsertIdx = 0;
            TreeNode right = t;
            for (; ; ) {
                TreeNode copy = copyOf(t);
                TreeNode node = new TreeNode(n);
                if (lastInsertIdx == 0) {
                    // 上面说明中的情况1
                    node.left = copy;
                    result.add(node);
                } else {
                    // 上面说明中的情况2
                    int i = 1;
                    TreeNode p = copy;
                    while (i < lastInsertIdx) {
                        p = p.right;
                        i++;
                    }
                    node.left = p.right;
                    p.right = node;
                    result.add(copy);
                }
                lastInsertIdx++;
                if (right == null) {
                    break;
                }
                right = right.right;
            }
        }
        return result;
    }

    /**
     * 递归复制一棵树
     */
    private TreeNode copyOf(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode copy = new TreeNode(root.val);
        copy.left = copyOf(root.left);
        copy.right = copyOf(root.right);
        return copy;
    }
}