package p00;

/**
 * https://leetcode-cn.com/problems/jump-game/
 */
public class Problem0055 {

    public static void main(String[] args) {
        Problem0055 p = new Problem0055();
        System.err.println(p.canJump(new int[]{2, 3, 1, 1, 4}));
        System.err.println(p.canJump(new int[]{3, 2, 1, 0, 4}));
    }

    /**
     * 思路：
     * 和第45题基本一样，加一个判断逻辑就行
     * 利用贪心算法，记录当前能跳到的最远位置的下标farthest
     * 如果本次能跳到的最远位置的下标 >= length - 1，代表能够跳到终点
     * 如果本次能跳到的最远位置的下标 <= farthest，代表本次跳跃的范围不超过之前最大的跳跃范围，会一直跳不过去，不能跳到终点。这种情况，可以通过加一个循环条件 i <= farthest 来控制
     * 其他情况，继续循环即可
     */
    public boolean canJump(int[] nums) {
        // 题目假设了数组非空
        int count = 0;
        // 记录当前能跳到的最远位置的下标
        int farthest = 0;
        // 跳到最后一个元素上就结束，因此不需要处理最后一个元素
        for (int i = 0; i < nums.length - 1 && i <= farthest; i++) {
            farthest = Math.max(i + nums[i], farthest);
            if (farthest >= nums.length - 1) {
                return true;
            }
        }
        // 当数组只有一个0时，不会进入循环，因此这里还是要判断下
        return farthest >= nums.length - 1;
    }

    /**
     * 这个代码是从45题直接copy过来，只改动一点点
     * 对我个人而言，这种要更清楚一点，因为用了两层循环，能够知道跳了多少次
     */
    public boolean canJump2(int[] nums) {
        // 题目假设了数组非空
        int count = 0;
        // 记录当前能跳到的最远位置的下标
        int farthest = 0;
        // start end为下一次内部循环的左右边界
        int start = 0;
        int end = farthest;
        // 跳到最后一个元素上就结束，因此不需要管最后一个元素是多少
        while (start < nums.length - 1) {
            count++;
            for (int i = start; i <= end; i++) {
                farthest = Math.max(i + nums[i], farthest);
                if (farthest >= nums.length - 1) {
                    return true;
                }
            }
            if (farthest == end) {
                return false;
            }
            start = end + 1;
            end = farthest;
        }
        return true;
    }
}
