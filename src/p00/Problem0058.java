package p00;

/**
 * https://leetcode-cn.com/problems/length-of-last-word/
 */
public class Problem0058 {

    public static void main(String[] args) {
        System.err.println(new Problem0058().lengthOfLastWord("Hello World") == 5);
        System.err.println(new Problem0058().lengthOfLastWord(" Hello World ") == 5);
        System.err.println(new Problem0058().lengthOfLastWord(" ") == 0);
        System.err.println(new Problem0058().lengthOfLastWord("a") == 1);
        System.err.println(new Problem0058().lengthOfLastWord("a ") == 1);
        System.err.println(new Problem0058().lengthOfLastWord(" a ") == 1);
    }

    /**
     * 思路：很简单
     * 注意点：单词 是指仅由字母组成、不包含任何空格字符的最大子字符串，因此要去除尾部空格
     */
    public int lengthOfLastWord(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        boolean find = false;
        int result = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            char c = s.charAt(i);
            if (!find && ' ' != c) {
                find = true;
            }
            if (find && ' ' == c) {
                break;
            }
            if (find) {
                result++;
            }
        }
        return result;
    }
}
