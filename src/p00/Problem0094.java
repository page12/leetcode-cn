package p00;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/binary-tree-inorder-traversal/
 */
public class Problem0094 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(2);
        root.left = new TreeNode(3);
        root.left.left = new TreeNode(1);
        System.err.println(new Problem0094().inorderTraversal(root));
    }

    /**
     * 思路：递归比较简单，这里使用非递归方法
     * 非递归需要显式使用栈，入栈顺序跟递归算法传参的顺序一致
     * 中序是遍历完左叶子再遍历对应的根，此时左叶子对应的递归/迭代操作已经完成，根随即会出栈，然后再入栈右叶子
     * 整体顺序为：入根 -> 入左 -> 出左（遍历） -> 出根（遍历） -> 入右 -> 出右（遍历）
     * 因此可以得到中序遍历的出栈栈顺序就是遍历顺序（这里不严格），迭代算法就这一点和前序不一样
     */
    public List<Integer> inorderTraversal(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }
        List<Integer> result = new ArrayList<>();
        LinkedList<TreeNode> stack = new LinkedList<>();
        while (!stack.isEmpty() || root != null) {
            // 根入栈，以及left入栈，相当于递归算法中的 递归(left) 操作
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            // 下面两行代码：节点出栈并遍历
            // 因为每个节点都是根，因此这里就是递归算法中的 遍历(根)操作
            root = stack.pop();
            // 就这个遍历操作的地方和前序不一样，中序是出栈时就相当于遍历了
            result.add(root.val);
            // 处理right，这里相当于递归算法中的 递归(right) 操作
            root = root.right;
        }
        return result;
    }
}
