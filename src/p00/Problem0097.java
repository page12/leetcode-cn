package p00;

/**
 * https://leetcode-cn.com/problems/interleaving-string/
 */
public class Problem0097 {

    public static void main(String[] args) {
        Problem0097 p = new Problem0097();
        System.err.println(p.isInterleave("aabcc", "dbbca", "aadbbcbcac"));
        System.err.println(p.isInterleave("aabcc", "dbbca", "aadbbbaccc"));
        System.err.println(p.isInterleave("", "", ""));
    }

    /**
     * 思路：
     * 设s1长度为n，s2长度为m，则s3长度为n+m，否则比不可能是交错而来
     * 设f(i,j)=true表示s3是由s1的前n个字符和s2的前m个字符交错而来，下标从1开始
     * 因为交错后来自同一个字符串的字符的相对顺序不变
     * 所以s3的最后一个字符要么是s1的最后一个字符，要么是s2的最后一个字符
     * 当s1(i) = s3(i+j)时，最后一个字符可能是来自s1，此时f(i,j) = f(i-1,j)
     * 当s2(j) = s3(i+j)时，最后一个字符可能是来自s2，此时f(i,j) = f(i,j-1)
     * 总共就只有这两种情况，所以 f(i,j) = (s1(i) == s3(i+j) && f(i-1,j)) || (s2(j) == s3(i+j) && f(i,j-1))
     * <br/>
     * 推导出了递推公式，只需要知道初始值就可以自底向上了
     * 当i=j=0时，都是空字符串，此时返回true，也就是初始值f(0,0)=true
     * 当i=0, j!=0时，相当于两种情况变为一种情况，此时有 f(0,j) = s2(j) == s3(j) && f(0,j-1)
     * 当i!=0, j=0时，相当于两种情况变为一种情况，此时有 f(i,0) = s1(i) == s3(i) && f(i-1,0)
     * <br/>
     * 自底向上时，有 (n+1)*(m+1) 个中间结果，需要用一个二维数组保存，此时空间复杂度为O(n*m)
     * 通过递推公式可以知道，求解当前行的所有结果，只需要知道上一行的结果就够了，因此只用一行数组就行，滚动覆盖重复利用
     * s1和s2是可以交换的，所以数组长度为两者的最小值就可以了，此时空间复杂度为O(min(n, m))
     * 总共要求解 (n+1)*(m+1) 个中间结果，所以时间复杂度为O(n*m)
     */
    public boolean isInterleave(String s1, String s2, String s3) {
        // 题目假设字符串非空
        if (s1.length() + s2.length() != s3.length()) {
            return false;
        }
        // 字符串区分长度大小
        String smaller;
        String bigger;
        if (s1.length() > s2.length()) {
            smaller = s2;
            bigger = s1;
        } else {
            smaller = s1;
            bigger = s2;
        }
        if (smaller.length() == 0) {
            return bigger.equals(s3);
        }
        // 利用滚动数组
        // 内存循环开始前，滚动数组记录的是上一行的值
        // 内存循环开始后，滚动数组依次被当前行的值所覆盖
        // (i,j)是当前要求的值
        // (i-1,j)是还没有被覆盖的值，本次求值后会再覆盖它，它就是temp[j]
        // (i,j-1)是刚刚覆盖掉的值，也就是temp[j-1]
        // 下标为0的是空字符串的情况，所以长度要加1
        boolean[] temp = new boolean[smaller.length() + 1];
        for (int i = 0; i <= bigger.length(); i++) {
            for (int j = 0; j <= smaller.length(); j++) {
                // 上面分析的字符串的下标是1（因为用0表示空字符串了）
                // 所以这里用charAt要减1才行
                if (i == 0 && j == 0) {
                    temp[j] = true;
                } else if (i == 0) {
                    temp[j] = temp[j - 1] && smaller.charAt(j - 1) == s3.charAt(j - 1);
                } else if (j == 0) {
                    // j == 0代表是第一列，此时滚动数组的第一个值就是(i-1,j)
                    temp[j] = temp[j] && bigger.charAt(i - 1) == s3.charAt(i - 1);
                } else {
                    temp[j] = (temp[j - 1] && smaller.charAt(j - 1) == s3.charAt(i + j - 1))
                            || (temp[j] && bigger.charAt(i - 1) == s3.charAt(i + j - 1));
                }
            }
        }
        return temp[temp.length - 1];
    }
}
