package p00;

import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/valid-parentheses/
 */
public class Problem0020 {

    public static void main(String[] args) {
        System.err.println(new Problem0020().isValid("()"));
        System.err.println(new Problem0020().isValid("((()))"));
        System.err.println(new Problem0020().isValid("()[]{}"));
        System.err.println(new Problem0020().isValid("(]"));
        System.err.println(new Problem0020().isValid("([)]"));
        System.err.println(new Problem0020().isValid("{[]}"));
        System.err.println(new Problem0020().isValid("{[]}{[]}{[()()(({[]}))]}"));
    }

    /**
     * 思路：利用栈这个数据结构
     * 按字符串顺序入栈，每次入栈前判断栈顶元素和当前即将入栈的元素是否是匹配的括号
     * 匹配则栈顶元素弹出，当前即将入栈的元素不入栈
     * 不匹配则当前即将入栈的元素入栈
     * 最后栈空则字符串是有效的
     * 时间复杂度O(n)，空间也是O(n)
     */
    public boolean isValid(String s) {
        // 题目有假设，不做校验
        LinkedList<Character> stack = new LinkedList<>();
        for (int i = 0; i < s.length(); i++) {
            char current = s.charAt(i);
            if (!stack.isEmpty()) {
                char top = stack.peek();
                if ((top == '(' && current == ')') || (top == '[' && current == ']') || (top == '{' && current == '}')) {
                    stack.pop();
                } else {
                    stack.push(current);
                }
            } else {
                stack.push(current);
            }
        }
        return stack.isEmpty();
    }
}
