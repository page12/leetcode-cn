package p00;

/**
 * https://leetcode-cn.com/problems/jump-game-ii/
 */
public class Problem0045 {

    public static void main(String[] args) {
        Problem0045 p = new Problem0045();
        System.err.println(p.jump(new int[]{2, 3, 1, 1, 4}) == 2);
        System.err.println(p.jump(new int[]{2, 3, 0, 1, 4}) == 2);
    }

    /**
     * 思路：
     * 贪心算法
     * 记录当前能跳到的最远位置的下标
     * 然后遍历上次最远位置+1 到 当前最远位置中间的所有元素，并求本次能跳到的最远位置的下标
     * 如果本次能跳到的最远位置的下标 >= length，则结束
     * 此时的跳跃次数就是最终结果
     */
    public int jump(int[] nums) {
        // 题目假设了数组非空
        int count = 0;
        // 记录当前能跳到的最远位置的下标
        int farthest = 0;
        // start end为下一次内部循环的左右边界
        int start = 0;
        int end = farthest;
        // 跳到最后一个元素上就结束，因此不需要处理最后一个元素
        while (start < nums.length - 1) {
            count++;
            for (int i = start; i <= end; i++) {
                farthest = Math.max(i + nums[i], farthest);
                if (farthest >= nums.length - 1) {
                    return count;
                }
            }
            start = end + 1;
            end = farthest;
        }
        return count;
    }
}
