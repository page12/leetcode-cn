package p00;

/**
 * https://leetcode-cn.com/problems/search-insert-position/
 */
public class Problem0035 {

    public static void main(String[] args) {
//        System.err.println(new Problem0035().searchInsert(new int[]{1, 3, 5, 6}, 5));
//        System.err.println(new Problem0035().searchInsert(new int[]{1, 3, 5, 6}, 2));
//        System.err.println(new Problem0035().searchInsert(new int[]{1, 3, 5, 6}, 7));
//        System.err.println(new Problem0035().searchInsert(new int[]{1, 3, 5, 6}, 0));
//        System.err.println(new Problem0035().searchInsert(new int[]{1, 3}, 2));
//        System.err.println(new Problem0035().searchInsert(new int[]{1, 3}, 3));
        System.err.println(new Problem0035().searchInsert(new int[]{1, 3, 5, 6}, 7));
    }

    /**
     * 思路：二分查找，处理下找不到的情况就行
     */
    public int searchInsert(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        if (nums[0] >= target) {
            return 0;
        }
        if (nums[nums.length - 1] == target) {
            // 输入[1, 3] 3，预期输出为1，实际上输出为2也是符合的，题目没体现，成减1
            //  return nums.length;
            return nums.length - 1;
        }
        if (nums[nums.length - 1] < target) {
            return nums.length;
        }
        int start = 0;
        int end = nums.length;
        while (start <= end) {
            int searchIndex = start + ((end - start) >>> 1);
            if (nums[searchIndex] > target) {
                end = searchIndex - 1;
            } else if (nums[searchIndex] < target) {
                start = searchIndex + 1;
            } else {
                return searchIndex;
            }
        }
        if (nums[end] < target) {
            return end + 1;
        } else {
            return end;
        }
    }
}
