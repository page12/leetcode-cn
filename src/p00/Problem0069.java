package p00;

/**
 * https://leetcode-cn.com/problems/sqrtx/
 */
public class Problem0069 {

    public static void main(String[] args) {
        Problem0069 p = new Problem0069();
        for (int i = 0; i < 10000; i++) {
            if (p.mySqrt(i) != (int) Math.sqrt(i)) {
                System.err.println(i);
            }
        }
        System.err.println("test end");
    }

    /**
     * 思路：二分
     * n > 4时，n的平方根小于n/2，直接从[2,n/2]开始二分
     */
    public int mySqrt(int x) {
        // 题目有假设，不考虑负数
        if (x <= 1) {
            return x;
        }
        if (x == 2 || x == 3) {
            return 1;
        }
        if (x == 4) {
            return 2;
        }
        int start = 2;
        int end = x >>> 1;
        while (start <= end) {
            int mid = start + ((end - start) >>> 1);
            // 避免溢出，用除法判断
            int divide = x / mid;
            if (divide > mid) {
                start = mid + 1;
            } else if (divide < mid) {
                end = mid - 1;
            } else {
                return mid;
            }
        }
        // 整数除法结果小于等于浮点除法，所以跳出循环条件是start = end + 1，题目要的结果就是end
        return end;
    }
}
