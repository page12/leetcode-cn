package p00;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/merge-two-sorted-lists/
 */
public class Problem0021 {
    public static void main(String[] args) {

    }

    /**
     * 思路：简单的双指针
     * 把两个链表当前指针元素较小的那一个添加到合并链表的最后
     */
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        ListNode merge = null;
        ListNode p1 = l1;
        ListNode p2 = l2;
        while (p1 != null && p2 != null) {
            if (merge == null) {
                if (p1.val <= p2.val) {
                    merge = p1;
                    p1 = p1.next;
                } else {
                    merge = p2;
                    p2 = p2.next;
                }
            } else {
                if (p1.val <= p2.val) {
                    merge.next = p1;
                    merge = merge.next;
                    p1 = p1.next;
                } else {
                    merge.next = p2;
                    merge = merge.next;
                    p2 = p2.next;
                }
            }
        }
        if (p1 != null) {
            merge.next = p1;
        }
        if (p2 != null) {
            merge.next = p2;
        }
        return l1.val <= l2.val ? l1 : l2;
    }
}
