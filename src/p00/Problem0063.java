package p00;

/**
 * https://leetcode-cn.com/problems/unique-paths-ii/
 */
public class Problem0063 {

    public static void main(String[] args) {
        Problem0063 p = new Problem0063();
        System.err.println(p.uniquePathsWithObstacles(new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}) == 2);
        System.err.println(p.uniquePathsWithObstacles(new int[][]{{0, 1}, {0, 0}}) == 1);
    }

    /**
     * 思路：
     * 同第62题一样
     * 不同点是，如果一个点是障碍点，那么不能走到该点，也就不能通过该点往下或者往右
     * 此时可以把该点的中间结果设置为0
     * 最特殊的情况：因为所有路径都是从(0, 0)出发的，所以如果(0, 0)是障碍点，则一条路径都没有
     * 这样做时间复杂度O(n * m)，空间复杂度O(n * m)
     * <br/>
     * 也可以只用一行或者一列数组，此时空间复杂度降为 O(min(n, m))
     * 也可以用 obstacleGrid 数组（用负数表示障碍，正数表示中间结果，用完了还可以还原），此时空间复杂度为 O(1)
     * 这两种优化的代码就不写了
     */
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        // 题目有假设，不做校验
        if (obstacleGrid[0][0] == 1) {
            return 0;
        }
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        int[][] temp = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (obstacleGrid[i][j] == 1) {
                    temp[i][j] = 0;
                    continue;
                }
                if (i == 0 && j == 0) {
                    temp[i][j] = 1;
                } else if (i == 0) {
                    temp[i][j] = temp[i][j - 1];
                } else if (j == 0) {
                    temp[i][j] = temp[i - 1][j];
                } else {
                    temp[i][j] = temp[i - 1][j] + temp[i][j - 1];
                }
            }
        }
        return temp[m - 1][n - 1];
    }
}
