package p00;

/**
 * https://leetcode-cn.com/problems/implement-strstr/
 */
public class Problem0028 {

    public static void main(String[] args) {

    }

    /**
     * 这道题放在简单里面，意义不明
     * 如果是叫手写KMP算法，抱歉，不会，本身这个算法就需要时间去理解，不理解比困难还难
     * 暴力匹配就是个双循环，也没意义啊
     * 直接indexOf偷个懒
     */
    public int strStr(String haystack, String needle) {
        return haystack.indexOf(needle);
    }
}
