package p00;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/plus-one/
 */
public class Problem0066 {

    public static void main(String[] args) {
        System.err.println(Arrays.toString(new Problem0066().plusOne(new int[]{1, 2, 3})));
        System.err.println(Arrays.toString(new Problem0066().plusOne(new int[]{4, 3, 2, 1})));
        System.err.println(Arrays.toString(new Problem0066().plusOne(new int[]{9})));
        System.err.println(Arrays.toString(new Problem0066().plusOne(new int[]{9, 9, 9})));
        System.err.println(Arrays.toString(new Problem0066().plusOne(new int[]{1, 9})));
        System.err.println(Arrays.toString(new Problem0066().plusOne(new int[]{0})));
    }

    /**
     * 思路：比较简单，加完了考虑下进位就行
     * 特殊的进位9999这种，可以单独判断一次
     * 转成int或者long有可能溢出，这点需要知道
     */
    public int[] plusOne(int[] digits) {
        // 题目有假设，不做校验
        // 处理全都是9的特殊情况
        boolean allNine = true;
        for (int i = 0; i < digits.length; i++) {
            if (digits[i] != 9) {
                allNine = false;
                break;
            }
        }
        if (allNine) {
            // 数组是定长的，需要新建一个数组
            int[] result = new int[digits.length + 1];
            result[0] = 1;
            return result;
        }
        // 处理一般情况，直接在原数组上修改
        digits[digits.length - 1] = digits[digits.length - 1] + 1;
        // 进位判断
        for (int i = digits.length - 1; i >= 0; i--) {
            if (digits[i] == 10) {
                digits[i] = 0;
                digits[i - 1] = digits[i - 1] + 1;
            }
        }
        return digits;
    }
}
