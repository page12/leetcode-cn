package p00;

/**
 * https://leetcode-cn.com/problems/trapping-rain-water/
 */
public class Problem0042 {

    public static void main(String[] args) {
        Problem0042 p = new Problem0042();
        System.err.println(p.trap2(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}) == 6);
        System.err.println(p.trap2(new int[]{4, 2, 0, 3, 2, 5}) == 9);
        System.err.println(p.trap2(new int[]{2, 1, 0, 2}) == 3);
    }

    /**
     * 思路：
     * 1、寻找左右两边最大的高度
     * 收集满雨水后，一个点i上面收集的雨水等于它左右两边最大高度中较小的那个，减去它自身的高度
     * 设点i左边最大的高度为leftMax（不包含i点），点i右边最大的高度为rightMax（不包含i点）
     * 那么点i收集的雨水数量为 min(leftMax, rightMax) - height[i]
     * 最左边的点没有leftMax，最右边的点没有 rightMax，可以都记为0
     * 因此题目转换为求每一个点i的leftMax和rightMax
     * 求一个点i的leftMax和rightMax需要遍历一次，但是这里需要求所有的点，因此可以左右分别遍历一次就可以得到
     * 整个流程需要遍历三次，还额外需要两个长度为N的数组（可以通过合并来减少一次遍历，同时也减少一个数组）
     * 所以时间复杂度为O(N)，空间复杂度为O(N)
     * <br/>
     * 2、对方法1的优化
     * 方法1需要三次遍历
     * 实际上求leftMaxArray和最后一次遍历是同样的遍历，可以合并为一次，同时也可以不再需要leftMaxArray
     * 同样的，如果是反向遍历，则可以不需要rightMaxArray
     * 那么很可能能找到一种双向遍历法，把两个数组都省去
     * <br/>
     * 观察下方法1的代码
     * 点i收集的雨水数量为 min(leftMax, rightMax) - height[i]
     * 也就是如果提前知道 leftMax, rightMax 的大小关系的话，就只需要其中较小的一个就行
     * leftMaxArray满足单调不递减，即 i < j 时 leftMaxArray[i] <= leftMaxArray[i]
     * rightMaxArray满足单调不递增，即 i < j 时 rightMaxArray[i] >= leftMaxArray[i]
     * 那么一定存在一个点x使得
     * 当 i < x时 min(leftMax, rightMax) = leftMax
     * 当 i > x时 min(leftMax, rightMax) = rightMax
     * 进一步可以得到x就是最大高度max对应的点，此点上面不会有雨水，所以不用考虑它
     * 因此只需要求max对应的点，然后左边++遍历直到max，右边--遍历直到max，这样就可以省去两个数组
     * <br/>
     * 求max额外需要一次遍历，这一次遍历实际上是可以合并的
     * max = max(leftMax, rightMax) 或者 max = height[i]（只有一个最大值，下标为i）
     * 实际上我们也并不需要具体知道哪个点是max
     * 因为除了max自己那个点其余的都有 max >= leftMax && max >= rightMax
     * 当 leftMax >= rightMax，此时较小值为rightMax，并且leftMax可能是max，应该使rightMax继续变大
     * 当 leftMax <= rightMax，此时较小值为leftMax，并且rightMax可能是max，应该使leftMax继续变大
     * 当左右相交时，循环结束，此时也得到了真正的max = max(leftMax, rightMax)
     * <br/>
     * 通过上面的分析优化，最终只需要一次遍历，不需要额外数组
     * 时间复杂度为O(N)，空间复杂度为O(1)
     */
    public int trap(int[] height) {
        // 寻找左右两边最大的高度的代码
        // 题目有假设，不做校验
        if (height.length < 3) {
            return 0;
        }
        int sum = 0;
        // 先求每个点左右两边的最大值，分别用两个数组记录下来
        int[] leftMaxArray = new int[height.length];
        int[] rightMaxArray = new int[height.length];
        for (int i = 1; i < height.length; i++) {
            leftMaxArray[i] = Math.max(leftMaxArray[i - 1], height[i - 1]);
        }
        for (int i = height.length - 2; i >= 0; i--) {
            rightMaxArray[i] = Math.max(rightMaxArray[i + 1], height[i + 1]);
        }
        // 计算收集的雨水的总和
        for (int i = 0; i < height.length; i++) {
            int leftMax = leftMaxArray[i];
            int rightMax = rightMaxArray[i];
            if (leftMax > height[i] && rightMax > height[i]) {
                sum += Math.min(leftMax, rightMax) - height[i];
            }
        }
        return sum;
    }

    public int trap2(int[] height) {
        // 对方法1的优化
        // 题目有假设，不做校验
        if (height.length < 3) {
            return 0;
        }
        int sum = 0;
        int left = 1;
        int right = height.length - 2;
        int leftMax = height[0];
        int rightMax = height[height.length - 1];
        // 上一次循环结束时移动指针，下一次循环在计算移动后该点的值，所以这里要带等号
        while (left <= right) {
            if (leftMax >= rightMax) {
                rightMax = Math.max(rightMax, height[right]);
                if (leftMax > height[right] && rightMax > height[right]) {
                    // 因为leftMax是通过left求的，它不包括left本身
                    // 对于right而言，它的左边包括left，所以这里加了一次max操作
                    sum += Math.min(Math.max(leftMax, height[left]), rightMax) - height[right];
                }
                right--;
            } else {
                leftMax = Math.max(leftMax, height[left]);
                if (leftMax > height[left] && rightMax > height[left]) {
                    // 同上面分支的注释的原因一样
                    sum += Math.min(leftMax, Math.max(rightMax, height[right])) - height[left];
                }
                left++;
            }
        }
        return sum;
    }
}
