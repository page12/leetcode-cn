package p00;

/**
 * https://leetcode-cn.com/problems/maximum-subarray/
 */
public class Problem0053 {

    public static void main(String[] args) {
        System.err.println(new Problem0053().maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
        System.err.println(new Problem0053().maxSubArray(new int[]{1}));
        System.err.println(new Problem0053().maxSubArray(new int[]{0}));
        System.err.println(new Problem0053().maxSubArray(new int[]{-1}));
        System.err.println(new Problem0053().maxSubArray(new int[]{-3, -1, -2}));
    }

    /**
     * 思路：从左往右找序列，贪婪式累加增长序列（把右边的数添加的到该序列），通过下面的两个规则判断最后是否该序列是否需要继续累加增长还是直接放弃
     * 1、如果一段连续的序列的和小于0，那么该段连续序列当做前缀，会令整个序列的和变小，最终序列不能用该段序列当前缀，需要放弃
     * 2、反之，如果一段连续的序列的和大于0，那么该段连续序列当做前缀，会令整个序列的和变大
     * 基本流程：
     * 令最大和maxSum初始化成数组第一个数（maxSum不能初始化为0，要考虑数组全是负数的情况；sum会无条件累加一次，需要初始化为0）
     * 从左边第一个数开始，尝试把它后边的数累加起来，累加和记为sum，每次累加操作会产生一个新的和sum
     * a、如果sum大于已经存在的maxSum，则maxSum = sum（不用管该序列最终是否需要，只用管最大和是否变大）
     * b、如果sum是负数，代表一段连续的序列的和小于0，需要放弃该序列，并重置sum = 0，从下一个数开始继续这个累加操作（情况1）
     * c、如果sum是正数，继续累加下一个数（情况2），实际上什么不用做，继续循环就是
     */
    public int maxSubArray(int[] nums) {
        // 题目有假设，不做校验
        // 所有子序列和中最大的那一个，也就是结果
        int maxSum = nums[0];
        // 当前还在继续累加的序列的和
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            // 尝试累加
            sum += nums[i];
            // 如果sum大于已经存在的maxSum，则maxSum = sum
            if (sum > maxSum) {
                maxSum = sum;
            }
            // 如果sum < 0，代表一段连续的序列的和小于0，需要放弃该序列，并重置sum = 0
            if (sum < 0) {
                sum = 0;
            }
        }
        return maxSum;
    }
}
