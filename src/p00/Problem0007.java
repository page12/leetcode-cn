package p00;

/**
 * https://leetcode-cn.com/problems/reverse-integer/
 */
public class Problem0007 {
    public static void main(String[] args) {
        System.err.println(Integer.MAX_VALUE);
        System.err.println(new Problem0007().reverse(123456));
        System.err.println(new Problem0007().reverse(0));
        System.err.println(new Problem0007().reverse(-123456));
        System.err.println(new Problem0007().reverse(120));
        System.err.println(new Problem0007().reverse(-120));
        System.err.println(new Problem0007().reverse(Integer.MAX_VALUE));
        System.err.println(new Problem0007().reverse(Integer.MIN_VALUE));
        System.err.println(new Problem0007().reverse(1999999999));
        System.err.println(new Problem0007().reverse(2147483412));
    }

    /**
     * 思路：反转操作比较简单，负数按正数处理就行，注意负数范围大1个数字
     * 题目规定：假设环境不允许存储 64 位整数（有符号或无符号）
     * 因此需要判断反转后是否超过int范围，仅仅需要比较达到10位数的int
     * 可以同常数2^31 - 1比较前缀
     * 比如x最后一位是9，反转过去是9xxxxxxxxx，9 > 2因此溢出需要返回0
     * 比如x最后两位是23，反转过去是32xxxxxxxx，32 > 21因此溢出需要返回0
     * 时间复杂度O(n)，n为位数
     */
    public int reverse(int x) {
        if (x == 0) {
            return 0;
        } else if (x == Integer.MIN_VALUE) {
            return 0;
        } else if (x < 0) {
            return -reverse(-x);
        }

        int result = 0;
        // 可以用循环求位数，这样就不需要字符串，空间复杂度就是O(1)
        int length = String.valueOf(x).length();
        for (int i = 0; i < length; i++) {
            int lastNum = x % 10;
            x = x / 10;
            result = result * 10 + lastNum;
            // 判断是否溢出int
            // 注意点1：小于10位数不需要判断
            // 注意点2：幂运算不是^，这个符号在java中是异或运算
            if (length == 10 && result > (Integer.MAX_VALUE / Math.pow(10, 9 - i))) {
                return 0;
            }
        }
        return result;
    }
}
