package p00;

/**
 * https://leetcode-cn.com/problems/roman-to-integer/
 */
public class Problem0013 {

    public static void main(String[] args) {
        System.err.println(new Problem0013().romanToInt("III") == 3);
        System.err.println(new Problem0013().romanToInt("IV") == 4);
        System.err.println(new Problem0013().romanToInt("IX") == 9);
        System.err.println(new Problem0013().romanToInt("LVIII") == 58);
        System.err.println(new Problem0013().romanToInt("MCMXCIV") == 1994);
    }

    /**
     * 思路：从右往左读，累加，特殊处理IXC在左边的情况
     */
    public int romanToInt(String s) {
        // 题目有假设，不做校验
        int result = 0;
        int length = s.length();
        for (int i = 0; i < length; i++) {
            boolean specialLeft = false;
            char c = s.charAt(length - i - 1);
            switch (c) {
                case 'I':
                    result += 1;
                    break;
                case 'V':
                    result += 5;
                    if ('I' == getLeftChar(s, i)) {
                        result -= 1;
                        i++;
                    }
                    break;
                case 'X':
                    result += 10;
                    if ('I' == getLeftChar(s, i)) {
                        result -= 1;
                        i++;
                    }
                    break;
                case 'L':
                    result += 50;
                    if ('X' == getLeftChar(s, i)) {
                        result -= 10;
                        i++;
                    }
                    break;
                case 'C':
                    result += 100;
                    if ('X' == getLeftChar(s, i)) {
                        result -= 10;
                        i++;
                    }
                    break;
                case 'D':
                    result += 500;
                    if ('C' == getLeftChar(s, i)) {
                        result -= 100;
                        i++;
                    }
                    break;
                case 'M':
                    result += 1000;
                    if ('C' == getLeftChar(s, i)) {
                        result -= 100;
                        i++;
                    }
                    break;
                default:
                    // do nothing
            }
        }
        return result;
    }

    /**
     * 获取左边的字符，越界返回空格
     */
    private char getLeftChar(String s, int i) {
        int length = s.length();
        if (s.length() - i - 2 < 0) {
            return ' ';
        }
        return s.charAt(s.length() - i - 2);
    }
}
