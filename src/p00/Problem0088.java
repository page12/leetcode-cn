package p00;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/merge-sorted-array/
 */
public class Problem0088 {

    public static void main(String[] args) {
        Problem0088 p = new Problem0088();
        int[] nums1 = new int[]{2, 5, 6, 0, 0, 0};
        int[] nums2 = new int[]{1, 2, 3};
        p.merge(nums1, 3, nums2, 3);
        System.err.println(Arrays.toString(nums1));
    }

    /**
     * 思路：比较简单
     * 在nums1上合并，考虑到覆盖数据的问题，从右往左合并填充，特殊处理下nums2还有剩余元素没处理的情况
     */
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        // 题目有假设，不做校验
        int a = m;
        int b = n;
        while (a > 0 && b > 0) {
            int num1 = nums1[a - 1];
            int num2 = nums2[b - 1];
            if (num1 > num2) {
                nums1[a + b - 1] = num1;
                a--;
            } else {
                nums1[a + b - 1] = num2;
                b--;
            }
        }
        // nums2还没遍历完的情况，直接从左往右一次填充到头部
        for (int i = 0; i < b; i++) {
            nums1[i] = nums2[i];
        }
    }
}
