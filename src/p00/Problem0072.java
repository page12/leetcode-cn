package p00;

/**
 * https://leetcode-cn.com/problems/edit-distance/
 */
public class Problem0072 {

    public static void main(String[] args) {
        Problem0072 p = new Problem0072();
        System.err.println(p.minDistance("horse", "ros") == 3);
        System.err.println(p.minDistance("intention", "execution") == 5);
    }

    /**
     * 思路：
     * 先要明白两点
     * 1、一些操作是等价的
     * 对word1的删除，等价于对word2的添加
     * 对word2的添加，等价于对word1的删除
     * 对word1的修改，等价于对word2的修改
     * 因此总共就三种操作：对word1的添加，对word2的添加，对word1的修改
     * 2、操作都是一次性到位的，不会操作到同一个字符（增加x，然后又修改x，以及修改x，然后又修改同一个位置的字符）
     * 3、操作直接顺序是可以任意排列的，不影响最终结果，这个不好严格证明
     * <br/>
     * 空字符串是特殊情况
     * 空字符串与长度为N的字符串的编辑距离为N（删除次数 + 添加次数 = n）
     * 空字符与空字符不需要编辑，此时编辑距离为0
     * 为了能够表示空字符，设word1的长度为n+1，word2的长度为m+1
     * word1表示为word1(0,n)，word2表示为word(0,m)，编辑距离表示为 f(n, m)
     * <br/>
     * 根据三种操作，得到下面的四种递推公式（上面的三种，外加上一种特殊的修改）
     * f(n, m) = f(n-1, m) + 1，表明word1(0, n-1)要添加一个字符才能变成当前的状况，因为添加了一个字符，所以编辑距离要加1
     * f(n, m) = f(n, m-1) + 1，表明word2(0, m-1)要添加一个字符才能变成当前的状况，因为添加了一个字符，所以编辑距离要加1
     * f(n, m) = f(n-1, m-1) + 1，此时word1(n) != word2(m)，表明word1(0, n-1)与word2(0, m-1)都要添加一个相同字符才能变成当前情况
     * 注意，因为题目已经先有了n，这里要添加的字符本身就是存在的，所以都添加一个相同的字符等价于修改一个字符，此时只用了一次操作，所以编辑距离加1
     * f(n, m) = f(n-1, m-1)，此时word1(n) == word2(m)，表明原本需要修改的字符是一样的字符，也就不需要进行此操作，因此编辑距离不变
     * 四种情况是四选一的关系，要选择它们中最小的编辑距离，所以要用min操作
     * <br/>
     * 自底向上时，总共有 (n+1) * (m+1) 个中间结果
     * 求解这些中间结果总共需要 (n+1) * (m+1) 次，因此时间复杂度为O(n*m)
     * 全部保存这些中间结果需要一个二维数组保存这些结果，此时空间复杂度为O(n*m)
     * 因为二维数组中第一行和第一列的情况代表是有空字符串，此时结果都是已知的，不需要计算
     * 因此求下一行的数据只需要额外知道上一行的数据就行，这样可以只用一行数组，重复覆盖使用，此时空间复杂度为空间复杂度为O(min(n, m))
     * 这个优化点的代码就不写了
     */
    public int minDistance(String word1, String word2) {
        // 题目有假设，不做校验
        if (word1.length() == 0) {
            return word2.length();
        }
        if (word2.length() == 0) {
            return word1.length();
        }
        int n = word1.length();
        int m = word2.length();
        int[][] temp = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                if (i == 0) {
                    temp[i][j] = j;
                } else if (j == 0) {
                    temp[i][j] = i;
                } else if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                    temp[i][j] = Math.min(temp[i - 1][j] + 1, Math.min(temp[i][j - 1] + 1, temp[i - 1][j - 1]));
                } else {
                    temp[i][j] = Math.min(temp[i - 1][j] + 1, Math.min(temp[i][j - 1] + 1, temp[i - 1][j - 1] + 1));
                }
            }
        }
        return temp[n][m];
    }
}
