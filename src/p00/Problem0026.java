package p00;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/
 */
public class Problem0026 {

    public static void main(String[] args) {
        int[] array = new int[]{1, 1, 2};
        System.err.println(new Problem0026().removeDuplicates(array));
        System.err.println(Arrays.toString(array));

        array = new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        System.err.println(new Problem0026().removeDuplicates(array));
        System.err.println(Arrays.toString(array));
    }

    /**
     * 思路：简单的双指针
     * 一个是正常遍历的指针i一步步往前走，另一个是更新原数组的指针result
     * 遍历的指针碰见数字发生变化，就更新数组，并且result++
     */
    public int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int result = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[result] = nums[i];
                result++;
            }
        }
        return result;
    }
}
