package p00;

/**
 * https://leetcode-cn.com/problems/decode-ways/
 */
public class Problem0091 {

    public static void main(String[] args) {
        Problem0091 p = new Problem0091();
        System.err.println(p.numDecodings("12") == 2);
        System.err.println(p.numDecodings("226") == 3);
        System.err.println(p.numDecodings("0") == 0);
        System.err.println(p.numDecodings("06") == 0);
        System.err.println(p.numDecodings("33333333") == 1);
        System.err.println(p.numDecodings("2101") == 1);
        System.err.println(p.numDecodings("10011") == 0);
    }

    /**
     * 思路：
     * 首先要考虑编码是否是有效的，有两个规则
     * 1、以0开头的都是无效的
     * 2、包含 30 40 50 60 70 80 90 00 的，都是无效的
     * <br/>
     * 当不考虑编码是否合理时，最后是1位编码或者两位编码都可以，此时有
     * f(n) = f(n-1) + f(n-2)
     * 但是题目有限制条件：
     * 编码只能是1位或者两位数
     * 如果是1位数，不能是0
     * 如果是两位数，第一个不能是0，并且两个连起来小于等于26
     * <br/>
     * 加上限制条件后，递推公式变成下面这样
     * f(n) = f(n-2)，此时 s(n) == '0'
     * f(n) = f(n-1)，此时 s(n-1) == '0' 或者s(n-1)与s(n)组成的整数大于26
     * f(n) = f(n-1) + f(n-2)，此时是剩余情况
     * 初始值：
     * f(0) = 1，不合法的字符串前面已经排除
     * f(1) = 1，此时s(1) == '0'，或者s(0)与s(1)组成的字符串大于26
     * f(1) = 2，其余其余情况
     * <br/>
     * 使用自底向上的解法，用两个变量表示f(n-1)和f(n-2)
     * 此时时间复杂度为O(N)，空间复杂度为O(1)
     */
    public int numDecodings(String s) {
        // 长度为0时，没有解码方式
        // 以0开头时，不是有效的编码串
        if (s.length() == 0 || s.charAt(0) == '0') {
            return 0;
        }
        // 只有一个字符时，并且它不是0，则只有一种方式解码
        if (s.length() == 1) {
            return 1;
        }
        // 第二个字符是0，并且前两个大于26，比如30 40 50等等这些
        // 一开始两个字符就不能解码，因此返回0
        if (s.charAt(1) == '0' && convertToInt(s, 0) > 26) {
            return 0;
        }
        int minusTwo = 1;
        int minusOne = s.charAt(1) == '0' || convertToInt(s, 0) > 26 ? 1 : 2;
        if (s.length() == 2) {
            return minusOne;
        }
        int result = 0;
        for (int i = 2; i < s.length(); i++) {
            // 碰见 30 40 50等等这些，还包括 00 这种，都不能解码，直接返回0
            if (s.charAt(i) == '0' && (convertToInt(s, i - 1) > 26 || convertToInt(s, i - 1) == 0)) {
                return 0;
            }
            if (s.charAt(i) == '0') {
                result = minusTwo;
            } else if (s.charAt(i - 1) == '0' || convertToInt(s, i - 1) > 26) {
                result = minusOne;
            } else {
                result = minusOne + minusTwo;
            }
            minusTwo = minusOne;
            minusOne = result;
        }
        return result;
    }

    /**
     * i和i+1转换成数字
     */
    private int convertToInt(String s, int i) {
        return (s.charAt(i) - '0') * 10 + (s.charAt(i + 1) - '0');
    }
}
