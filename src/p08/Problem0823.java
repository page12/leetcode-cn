package p08;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/binary-trees-with-factors/
 */
public class Problem0823 {

    public static void main(String[] args) {
        Problem0823 p = new Problem0823();
        System.err.println(p.numFactoredBinaryTrees(new int[]{2, 4}) == 3);
        System.err.println(p.numFactoredBinaryTrees(new int[]{2, 4, 5, 10}) == 7);
    }

    /**
     * 思路：
     * 设dp[i]为以arr[i]为根节点树的种类
     * 对于任意 arr[x] = arr[i] * arr[j] 来说
     * dp[x] = sum(（左子树）dp[i]* （右子树）dp[j])，i,j 为满足乘法关系的任意一对值
     * 所以就是判断数组中有没有数能够构成乘法分解关系
     * 这里区分了左右，所以不用乘以2
     * 因为 i,j <= x，计算x时需要先计算出 i,j
     * 所以需要先排序
     */
    public int numFactoredBinaryTrees(int[] arr) {
        // 题目有假设，不做校验
        int mod = 1000000007;
        Arrays.sort(arr);
        // 使用long避免溢出
        long[] dp = new long[arr.length];
        Arrays.fill(dp, 1);
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            map.put(arr[i], i);
        }
        long sum = 0L;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <= i; j++) {
                if (arr[i] % arr[j] != 0) {
                    continue;
                }
                int divide = arr[i] / arr[j];
                if (!map.containsKey(divide)) {
                    continue;
                }
                dp[i] += dp[map.get(divide)] * dp[j];
            }
            sum += dp[i];
            sum = sum % mod;
        }
        return (int) sum;
    }
}
