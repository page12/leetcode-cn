package p08;

import common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/leaf-similar-trees/
 */
public class Problem0872 {

    public static void main(String[] args) {
        TreeNode root1 = new TreeNode(1);
        root1.left = new TreeNode(2);
        TreeNode root2 = new TreeNode(2);
        root2.left = new TreeNode(2);

        Problem0872 p = new Problem0872();
        System.err.println(p.leafSimilar(root1, root2));
    }

    /**
     * 思路：
     * 二叉树的深度优先（先序）遍历中，它的所有叶子节点是按从左至右的顺序访问的
     * 可以使用List保存遍历时的叶子节点
     * 设两棵树的节点数分别为N M
     * 递归的空间复杂度是O(N + M)，因为节点数为N的二叉树最多有 (N+1)/2 个叶子节点，此时是满二叉树，这个花费大于栈的花费
     * 递归的时间复杂度是O(N + M)
     */
    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        // 这是递归的代码
        // 题目假设树非空
        List<Integer> leaves1 = new ArrayList<>();
        List<Integer> leaves2 = new ArrayList<>();
        recurse(root1, leaves1);
        recurse(root2, leaves2);
        return leaves1.size() == leaves2.size() && leaves1.equals(leaves2);
    }

    /**
     * 递归
     */
    private void recurse(TreeNode root, List<Integer> leaves) {
        if (root == null) {
            return;
        }
        if (root.left == null && root.right == null) {
            leaves.add(root.val);
            return;
        }
        recurse(root.left, leaves);
        recurse(root.right, leaves);
    }
}
