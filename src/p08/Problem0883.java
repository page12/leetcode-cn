package p08;

/**
 * https://leetcode-cn.com/problems/projection-area-of-3d-shapes/
 */
public class Problem0883 {

    public static void main(String[] args) {
        Problem0883 p = new Problem0883();
        System.err.println(p.projectionArea(new int[][]{{2}}) == 5);
        System.err.println(p.projectionArea(new int[][]{{1, 2}, {3, 4}}) == 17);
    }

    /**
     * 思路：
     * 根据题目示例2中的图，可以得到
     * xy 平面的面积 = grid 这个二维数组中中非0元素的数量
     * xz 平面的面积 = grid 每一行的最大值相加的值
     * yz 平面的面积 = grid 每一列的最大值相加的值
     * 最终结果就是上面三个相加的值
     */
    public int projectionArea(int[][] grid) {
        // 题目有假设，不做校验
        int xy = 0;
        int xz = 0;
        int yz = 0;
        for (int i = 0; i < grid.length; i++) {
            int xMax = 0;
            for (int j = 0; j < grid[0].length; j++) {
                int v = grid[i][j];
                if (v > 0) {
                    xy++;
                    xMax = Math.max(xMax, v);
                }
            }
            xz += xMax;
        }
        if (xy == 0) {
            return 0;
        }
        for (int j = 0; j < grid[0].length; j++) {
            int yMax = 0;
            for (int i = 0; i < grid.length; i++) {
                int v = grid[i][j];
                yMax = Math.max(yMax, v);
            }
            yz += yMax;
        }
        return xy + yz + xz;
    }
}
