package p08;

/**
 * https://leetcode-cn.com/problems/transpose-matrix/
 */
public class Problem0867 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 最简单的方法就是直接操作
     */
    public int[][] transpose(int[][] matrix) {
        // 题目有假设，不做校验
        int r = matrix.length;
        int c = matrix[0].length;
        int[][] newMatrix = new int[c][r];
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                newMatrix[j][i] = matrix[i][j];
            }
        }
        return newMatrix;
    }
}
