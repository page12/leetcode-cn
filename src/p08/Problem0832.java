package p08;

/**
 * https://leetcode-cn.com/problems/flipping-an-image/
 */
public class Problem0832 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 直观的方式是进行两次遍历：先遍历处理水平翻转，再遍历处理反转图像
     * 现在看下能不能只用一次遍历
     * 因为图片里面只有0和1
     * 那么水平翻转后还是只有0和1
     * 那么反转图像后还是只有0和1
     * 也就是总共就四种情况
     * 设列宽为 c，列下标 i 对应的水平反转后的下标为 c - 1 - i，用a表示一行数据
     * <br/>
     * 则四种情况是：
     * 当 a[i] = 0, a[c-1-i] = 0 时，先水平翻转得到 (0, 0)，再反转图像得到 (1, 1)
     * 当 a[i] = 0, a[c-1-i] = 1 时，先水平翻转得到 (1, 0)，再反转图像得到 (0, 1)
     * 当 a[i] = 1, a[c-1-i] = 0 时，先水平翻转得到 (0, 1)，再反转图像得到 (1, 0)
     * 当 a[i] = 1, a[c-1-i] = 1 时，先水平翻转得到 (1, 1)，再反转图像得到 (0, 0)
     * <br/>
     * 总结下就是：
     * a[i] ^ a[c-1-i] == 1，则这两个点都不需要处理
     * a[i] ^ a[c-1-i] == 0，则a[i] = a[i]^1, a[c-1-i] = a[c-1-i]^1
     * 特殊情况
     * 当 a[i] 和 a[c-1-i] 就是同一个点时，异或结果为0，如果按上面对应的分支执行，则会异或两次，相当于没有操作
     * 因此需要额外处理一下，也就是额外异或一次
     */
    public int[][] flipAndInvertImage(int[][] image) {
        // 题目有假设，不做校验
        int r = image.length;
        int c = image[0].length;
        for (int i = 0; i < r; i++) {
            for (int j = 0; j <= (c - 1) / 2; j++) {
                if ((image[i][j] ^ image[i][c - 1 - j]) == 0) {
                    image[i][j] = image[i][j] ^ 1;
                    image[i][c - 1 - j] = image[i][c - 1 - j] ^ 1;
                }
                // 同一个点时，额外异或一次
                if (j == c - 1 - j) {
                    image[i][j] = image[i][j] ^ 1;
                }
            }
        }
        return image;
    }
}
