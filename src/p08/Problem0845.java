package p08;

/**
 * https://leetcode-cn.com/problems/longest-mountain-in-array/
 */
public class Problem0845 {

    public static void main(String[] args) {
        Problem0845 p = new Problem0845();
        System.err.println(p.longestMountain(new int[]{2, 1, 4, 7, 3, 2, 5}) == 5);
        System.err.println(p.longestMountain(new int[]{2, 2, 2}) == 0);
        System.err.println(p.longestMountain(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}) == 0);
        System.err.println(p.longestMountain(new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}) == 0);
        System.err.println(p.longestMountain(new int[]{0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1}) == 3);
    }

    /**
     * 思路：
     * 贪心算法，思路比较简单直观，直接把所有分支情况都列出来了
     * 增加一个状态 state = 1, 0, -1分别代表此时处理 递增，不可用，前面已经有递增的递减 这三种情况
     * state = 1 时：递增长度增加，如果相等则state = 0并且把距离重置为0，如果递减则 state = -1 同时长度增加
     * state = 0 时：递增则重新可用 state = 1，同时把距离重置为2
     * state = -1 时：递增state = 1并计算一次最大长度同时把距离重置为2，相等则state = 0并计算一次最大长度同时把距离重置为0，递减则距离增加
     * 遍历完后如果是 state = -1 的状态，则要再计算一次最大长度
     * 如果最大长度大于等于3则是最终结果，否则没有山脉返回0
     */
    public int longestMountain(int[] arr) {
        // 题目有假设，不做校验
        if (arr.length <= 2) {
            return 0;
        }
        int maxLength = 0;
        // 山脉至少有三个元素，所以从下标2开始
        int state = arr[1] > arr[0] ? 1 : 0;
        int currentLen = state == 1 ? 2 : 0;
        for (int i = 2; i < arr.length; i++) {
            if (state == 1) {
                if (arr[i] > arr[i - 1]) {
                    currentLen++;
                } else if (arr[i] == arr[i - 1]) {
                    currentLen = 0;
                    state = 0;
                } else {
                    currentLen++;
                    state = -1;
                }
            } else if (state == 0) {
                if (arr[i] > arr[i - 1]) {
                    state = 1;
                    currentLen = 2;
                }
            } else {
                if (arr[i] > arr[i - 1]) {
                    maxLength = Math.max(maxLength, currentLen);
                    state = 1;
                    currentLen = 2;
                } else if (arr[i] == arr[i - 1]) {
                    maxLength = Math.max(maxLength, currentLen);
                    state = 0;
                    currentLen = 0;
                } else {
                    currentLen++;
                }
            }
        }
        if (state == -1) {
            maxLength = Math.max(maxLength, currentLen);
        }
        // 山脉至少长度为3
        return maxLength >= 3 ? maxLength : 0;
    }
}
