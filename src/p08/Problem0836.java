package p08;

/**
 * https://leetcode-cn.com/problems/rectangle-overlap/
 */
public class Problem0836 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 设第一个矩形为(x1,y1,x2,y2)，第二个矩形为为(x3,y3,x4,y4)
     * 重叠时相交面积要大于0
     * 因此有面积为0的矩形时，肯定不重叠
     * 面积为0代表两个对角顶点共线，也就是 x1 == x2 || y1 == y2 || x3 == x4 || y3 == y4
     * <br/>
     * 当两个矩形面积都大于0时，重叠代表一个矩形的至少有顶点领一个矩形的内部
     * 根据题目意思，当面积大于0时，有 x1 < x2, y1 < y2, x3 < x4, y3 < y4
     * 顶点在内部等价于x轴y轴投影线段有重叠，利用数学知识，可以得到此情况等价于下面两个条件同时满足
     * max(x1, x3) < min(x2, x4)
     * max(y1, y3) < min(y2, y4)
     * <br/>
     * 综合上面两种情况，即得到最终判断逻辑
     */
    public boolean isRectangleOverlap(int[] rec1, int[] rec2) {
        // 题目有假设，不做校验
        if (rec1[0] == rec1[2] || rec1[1] == rec1[3]
                || rec2[0] == rec2[2] || rec2[1] == rec2[3]) {
            return false;
        }
        return Math.max(rec1[0], rec2[0]) < Math.min(rec1[2], rec2[2])
                && Math.max(rec1[1], rec2[1]) < Math.min(rec1[3], rec2[3]);
    }
}
