package p08;

/**
 * https://leetcode-cn.com/problems/buddy-strings/
 */
public class Problem0859 {

    public static void main(String[] args) {
        Problem0859 p = new Problem0859();
        System.err.println(p.buddyStrings("ab", "ba"));
        System.err.println(p.buddyStrings("ab", "ab"));
        System.err.println(p.buddyStrings("aa", "aa"));
        System.err.println(p.buddyStrings("aaaaaaabc", "aaaaaaacb"));
        System.err.println(p.buddyStrings("", "aa"));
        System.err.println(p.buddyStrings("ab", "ca"));
        System.err.println(p.buddyStrings("abab", "abac"));
    }

    /**
     * 思路：
     * 亲密字符串就是除了交换的两个字母外，剩下的部分都相等的字符串，所以它们长度一定要相等
     * <br/>
     * 题目示例2示例3表明，必须交换一次，也就是：
     * 如果两个字符串相等，但是构成的字符没有重复的，则一定不是亲密字符串
     * 如果两个字符串相等，但是构成的字符有重复的，则一定是亲密字符串
     * 题目假定了都是小写字符，因此长度大于26就一定有重复的，此时相等则一定是亲密字符串
     * 判断是否有重复字符，可以使用一个固定长度26的数组
     * 因为26小于32，所以还可以用一个int型的bitSet代替
     * <br/>
     * 对于不相等的字符串
     * 设交换的两个字母的下标分别为 x y，则有 s[x] = goal[y] && s[y] == goal[x]
     * 这个情况只允许出现一次，并且必须要出现一次，后续再遇到不一致的字母，则肯定不是亲密字符串
     * <br/>
     * 只需要同时遍历两个字符串一次，因此时间复杂度O(N)
     * 不需要其他额外空间，因此空间复杂度O(1)
     */
    public boolean buddyStrings(String s, String goal) {
        // 题目假设字符串非null
        if (s.length() != goal.length()) {
            return false;
        }
        if (s.equals(goal) && s.length() > 26) {
            return true;
        }
        int x = -1;
        int y = -1;
        int bitSet = 0;
        if (s.equals(goal)) {
            for (int i = 0; i < s.length(); i++) {
                // 判断是否有重复字符
                // 用int型的bitSet代替
                int bit = 1 << (s.charAt(i) - 'a');
                if ((bitSet & bit) != 0) {
                    return true;
                }
                bitSet |= bit;
            }
            return false;
        } else {
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) != goal.charAt(i)) {
                    if (x == -1) {
                        x = i;
                    } else if (y == -1) {
                        y = i;
                        if (s.charAt(x) != goal.charAt(y) || s.charAt(y) != goal.charAt(x)) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }
            // 不相等时一定有 x != -1
            // 所以这里只需要 y != -1 就可以确定是一定有一对字符能交换
            return y != -1;
        }
    }
}
