package p08;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/number-of-lines-to-write-string/
 */
public class Problem0806 {

    public static void main(String[] args) {
        Problem0806 p = new Problem0806();
        System.err.println(Arrays.toString(p.numberOfLines(new int[]{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                "abcdefghijklmnopqrstuvwxyz")));
        System.err.println(Arrays.toString(p.numberOfLines(new int[]{4, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10},
                "bbbcccdddaaa")));
    }

    /**
     * 思路：
     * 一次遍历，累加宽度sum
     * 如果第k行剩余宽度不够当前字符，则累加宽度先变为 k * 100再累加当前字符宽度
     * 当刚好第k行满了时，应该返回 [k - 1, 100]
     * 最后返回 (sum - 1) / 100 + 1 和 (sum - 1) % 100 + 1
     */
    public int[] numberOfLines(int[] widths, String s) {
        // 题目有假设，不做校验
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            int width = widths[s.charAt(i) - 'a'];
            int currentLineWidth = sum % 100;
            if (currentLineWidth + width > 100) {
                sum = sum + 100 - currentLineWidth;
            }
            sum += width;
        }
        return new int[]{(sum - 1) / 100 + 1, (sum - 1) % 100 + 1};
    }
}
