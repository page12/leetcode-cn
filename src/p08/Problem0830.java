package p08;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/positions-of-large-groups/
 */
public class Problem0830 {

    public static void main(String[] args) {
        Problem0830 p = new Problem0830();
        System.err.println(p.largeGroupPositions("abbxxxxzzy"));
        System.err.println(p.largeGroupPositions("abc"));
        System.err.println(p.largeGroupPositions("abcdddeeeeaabbbcd"));
        System.err.println(p.largeGroupPositions("aba"));

    }

    /**
     * 思路：
     * 用一个变量记录某个字符连续出现的长度 len，初始化为1
     * 从下标1开始遍历
     * 如果当前字符和前一个字符相等，则 len++
     * 如果不相等时，if len >= 3，则记录一次并令len = 1，否则 len = 1
     * 遍历完成时，最后一次的结果没有被记录，需要额外判断记录一次
     */
    public List<List<Integer>> largeGroupPositions(String s) {
        // 题目有假设，不做校验
        int len = 1;
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == s.charAt(i - 1)) {
                len++;
            } else {
                if (len >= 3) {
                    List<Integer> idxList = new ArrayList<>();
                    idxList.add(i - len);
                    idxList.add(i - 1);
                    result.add(idxList);
                }
                len = 1;
            }
        }
        if (len >= 3) {
            List<Integer> idxList = new ArrayList<>();
            idxList.add(s.length() - len);
            idxList.add(s.length() - 1);
            result.add(idxList);
        }
        return result;
    }
}
