package p08;

/**
 * https://leetcode-cn.com/problems/largest-sum-of-averages/
 */
public class Problem0813 {

    public static void main(String[] args) {
        Problem0813 p = new Problem0813();
        System.err.println(p.largestSumOfAverages(new int[]{9, 1, 2, 3, 9}, 3));
        System.err.println(p.largestSumOfAverages(new int[]{4, 1, 7, 5, 6, 2, 3}, 4));
    }

    /**
     * 思路：
     * 设dp(i,j)为数组[0,i]按题目描述的分成 j 组时的最大值，其中 i 为 [0,nums.length]，j 为 [0,k]
     * 设 avg(a,b) 为题目中分数的值，也就是 (nums[a] + ... + nums[b])/(b-a+1)
     * 设第 j-1 组的最后一个元素的下标为x，其中 0<= x < i
     * 对于任意x，分组后的值为
     * x的所有取值[0,i]中，最大的那一个就是 dp(i,j)
     * 也就是 dp(i,j) = max{ dp(x,j-1) + avg(x+1,i) }, 0<=x<=i
     * 特殊值:
     * j == 0 时，没有任何分组，因此dp(i,0) = 0
     * j > i+1 时，分组数量比元素个数还多，此时没意义，不用处理
     * <br/>
     * 求解 avg(a,b) 可以通过提前计算前缀和来加速
     * 设前缀和 sum(i) = 数组前i个元素的和
     * 则 avg(a,b) = (sum(b) - sum(a-1))/(b-a+1)
     */
    public double largestSumOfAverages(int[] nums, int k) {
        // 题目有假设，不做校验
        // 先求前缀和
        int[] sum = new int[nums.length + 1];
        sum[0] = 0;
        for (int i = 1; i <= nums.length; i++) {
            sum[i] = sum[i - 1] + nums[i - 1];
        }
        // 再求dp
        double[][] dp = new double[nums.length][k + 1];
        for (int i = 0; i < nums.length; i++) {
            for (int j = 1; j <= k; j++) {
                if (j == 1) {
                    dp[i][j] = sum[i + 1] * 1.0d / (i + 1);
                    continue;
                }
                if (j > i + 1) {
                    break;
                }
                double max = 0;
                for (int x = 0; x < i; x++) {
                    max = Math.max(max, dp[x][j - 1] + (sum[i + 1] - sum[x + 1]) * 1.0d / (i - x));
                }
                dp[i][j] = max;
            }
        }
        return dp[nums.length - 1][k];
    }
}
