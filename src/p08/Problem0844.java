package p08;

/**
 * https://leetcode-cn.com/problems/backspace-string-compare/
 */
public class Problem0844 {

    public static void main(String[] args) {
        Problem0844 p = new Problem0844();
        System.err.println(p.backspaceCompare("ab#c", "ad#c"));
        System.err.println(p.backspaceCompare("ab##", "c#d#"));
        System.err.println(p.backspaceCompare("a##c", "#a#c"));
        System.err.println(p.backspaceCompare("a#c", "b"));
        System.err.println(p.backspaceCompare("bbbextm", "bbb#extm"));
    }

    /**
     * 思路：
     * 双指针，从末尾开始比较
     * 用额外变量记录当前 # 的数量
     * 碰见一个 # 时加1
     * 碰见正常字符时，如果数量大于1，则抵消一个正常字符并且数量减1，同时指针左移一格
     * 这样做就只需要同时遍历两个字符串一次，不需要额外空间
     * 时间复杂度为O(N)，空间复杂度为O(1)
     */
    public boolean backspaceCompare(String s, String t) {
        // 题目有假设，不做校验
        int pS = s.length() - 1;
        int pT = t.length() - 1;
        int sharpCountS = 0;
        int sharpCountT = 0;
        // 这里使用或逻辑，就能处理最左边的 #
        while (pS >= 0 || pT >= 0) {
            // 有 # 或者没被抵消完时，向左遍历
            while (pS >= 0 && (s.charAt(pS) == '#' || sharpCountS > 0)) {
                if (s.charAt(pS) == '#') {
                    sharpCountS++;
                } else {
                    sharpCountS--;
                }
                pS--;
            }
            // 有 # 或者没被抵消完时，向左遍历
            while (pT >= 0 && (t.charAt(pT) == '#' || sharpCountT > 0)) {
                if (t.charAt(pT) == '#') {
                    sharpCountT++;
                } else {
                    sharpCountT--;
                }
                pT--;
            }
            if (pS == -1 && pT == -1) {
                // 都遍历到末尾
                return true;
            } else if (pS == -1 || pT == -1 || s.charAt(pS) != t.charAt(pT)) {
                // 只有一个抵消到末尾，或者未被抵消的字符不相等
                return false;
            } else {
                // 未被抵消的字符相等
                pS--;
                pT--;
            }
        }
        return true;
    }
}
