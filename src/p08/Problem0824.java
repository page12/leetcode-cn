package p08;

/**
 * https://leetcode-cn.com/problems/goat-latin/
 */
public class Problem0824 {

    public static void main(String[] args) {
        Problem0824 p = new Problem0824();
        System.err.println(p.toGoatLatin("I speak Goat Latin"));
        System.err.println(p.toGoatLatin("The quick brown fox jumped over the lazy dog"));
        System.err.println(p.toGoatLatin("Each word consists of lowercase and uppercase letters only"));
    }

    /**
     * 思路：
     * 遍历字符串sentence，以空格为分隔符处理
     * 处理过程比较简单，直接用字符操作时间空间性能比较好
     */
    public String toGoatLatin(String sentence) {
        // 题目有假设，不做校验
        int start = 0;
        int end = 0;
        int idx = 0;
        StringBuilder builder = new StringBuilder();
        while (start < sentence.length()) {
            if (sentence.charAt(start) == ' ') {
                builder.append(' ');
                start++;
                continue;
            }
            boolean startsWithVowel = isVowel(sentence.charAt(start));
            if (startsWithVowel) {
                builder.append(sentence.charAt(start));
            }
            idx++;
            end = start + 1;
            while (end < sentence.length() && sentence.charAt(end) != ' ') {
                builder.append(sentence.charAt(end));
                end++;
            }
            if (startsWithVowel) {
                builder.append("ma");
            } else {
                builder.append(sentence.charAt(start));
                builder.append("ma");
            }
            for (int i = 0; i < idx; i++) {
                builder.append('a');
            }
            if (end < sentence.length()) {
                builder.append(' ');
            }
            start = end + 1;
        }
        return builder.toString();
    }

    /**
     * 判断是否是元音字母
     */
    private boolean isVowel(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'
                || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U';
    }
}
