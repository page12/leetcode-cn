package p08;

import common.TreeNode;

import java.util.*;

/**
 * https://leetcode-cn.com/problems/all-possible-full-binary-trees/
 */
public class Problem0894 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 每个结点恰好有 0 或 2 个子结点
     * 当只有根节点这一层时，只有一个节点
     * 当有多层时，因为下一层的必定是上一层的子节点，所以下一层的节点数量肯定是偶数，总的节点数量肯定是奇数
     * 因此可以得到一个结论：n为偶数时，不存在这样的二叉树
     * <br/>
     * 题目描述的满二叉树，它的左子树和右子树也是这样的满二叉树，因此可以递归去生成
     * 循环令左子树的节点数为1,3,5,7...，则右子数的节点数 = n - 1 - 1,3,5,7...
     * 递归过程种可能会碰见相同的参数的情况，此时利用一个map缓存结果可以减少递归次数
     * <br/>
     * 上述递归过程种，对于n而言，在递归生成子树时，肯定会生成 n-2 n-4 ... 的子树
     * 题目要求的返回结果中只需要遍历，因此已经生成的子树不必复制一份，用指针引用子树的根节点即可
     */
    public List<TreeNode> allPossibleFBT(int n) {
        if ((n & 1) == 0) {
            return Collections.emptyList();
        }
        Map<Integer, List<TreeNode>> cache = new HashMap<>();
        return recurse(cache, n);
    }

    /**
     * 递归生成
     *
     * @param cache 递归参数及其结果的缓存
     * @param n     节点总数
     * @return 所有情况的树的根节点
     */
    private List<TreeNode> recurse(Map<Integer, List<TreeNode>> cache, int n) {
        if (cache.containsKey(n)) {
            return cache.get(n);
        }
        List<TreeNode> result = new ArrayList<>();
        if (n == 1) {
            result.add(new TreeNode(0));
            cache.put(n, result);
            return result;
        }
        for (int leftCount = 1; leftCount < n; leftCount += 2) {
            int rightCount = n - 1 - leftCount;
            List<TreeNode> leftChildren = recurse(cache, leftCount);
            List<TreeNode> rightChildren = recurse(cache, rightCount);
            for (TreeNode left : leftChildren) {
                for (TreeNode right : rightChildren) {
                    result.add(new TreeNode(0, left, right));
                }
            }
        }
        cache.put(n, result);
        return result;
    }
}
