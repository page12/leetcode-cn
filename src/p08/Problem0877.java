package p08;

/**
 * https://leetcode-cn.com/problems/stone-game/
 */
public class Problem0877 {

    public static void main(String[] args) {
        Problem0877 p = new Problem0877();
        System.err.println(p.stoneGame(new int[]{5, 3, 4, 5}));
    }

    /**
     * 思路：
     * 动态规划的思路和第488题一样
     * 看了先手必胜的推理过程，就懂一点
     * 这个题懂，碰见类似的别的题不会往这边想，想也想不出来
     * 还是动态规划好懂，碰见类似的别的题至少能想到是用动态规划，还能想出来
     */
    public boolean stoneGame(int[] piles) {
        // 题目有假设，不做校验
        int n = piles.length;
        // 可以使用滚动数组来优化
        int[][] dp = new int[n][n];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                if (i == j) {
                    dp[i][j] = piles[i];
                } else {
                    dp[i][j] = Math.max(piles[i] - dp[i + 1][j], piles[j] - dp[i][j - 1]);
                }
            }
        }
        return dp[0][n - 1] > 0;
    }
}
