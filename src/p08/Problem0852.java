package p08;

/**
 * https://leetcode-cn.com/problems/peak-index-in-a-mountain-array/
 */
public class Problem0852 {

    public static void main(String[] args) {
        Problem0852 p = new Problem0852();
        System.err.println(p.peakIndexInMountainArray(new int[]{0, 1, 0}) == 1);
        System.err.println(p.peakIndexInMountainArray(new int[]{0, 2, 1, 0}) == 1);
        System.err.println(p.peakIndexInMountainArray(new int[]{0, 10, 5, 2}) == 1);
        System.err.println(p.peakIndexInMountainArray(new int[]{3, 4, 5, 1}) == 2);
        System.err.println(p.peakIndexInMountainArray(new int[]{24, 69, 100, 99, 79, 78, 67, 36, 26, 19}) == 2);
    }

    /**
     * 思路：
     * 要找的那个下标是最大值对应的下标
     * 最大值左边的是升序，最大值右边的是降序
     * 因此可以利用二分查找来加速左右的搜索
     * 移动左轴的条件是 上一个 小于 当前 小于 下一个
     * 移动右轴的条件是 上一个 大于 当前 大于 下一个
     * 找到的添加是 上一个 小于 当前 大于 下一个
     * 题目假定了一定是山脉数组，长度大于等于3，所以不用管等于的情况，也不需要管边界情况，并且一定找得到
     */
    public int peakIndexInMountainArray(int[] arr) {
        // 题目有假设，不做校验
        int start = 0;
        int end = arr.length - 1;
        while (start <= end) {
            int mid = (end - start) / 2 + start;
            if (arr[mid - 1] < arr[mid] && arr[mid] < arr[mid + 1]) {
                start++;
            } else if (arr[mid - 1] > arr[mid] && arr[mid] > arr[mid + 1]) {
                end--;
            } else {
                return mid;
            }
        }
        // 题目有假设，因此这里的return不会被执行
        return -1;
    }
}
