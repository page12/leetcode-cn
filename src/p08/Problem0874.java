package p08;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/walking-robot-simulation/
 */
public class Problem0874 {

    public static void main(String[] args) {
        Problem0874 p = new Problem0874();
//        System.err.println(p.robotSim(new int[]{4, -1, 3}, new int[][]{}) == 25);
//        System.err.println(p.robotSim(new int[]{4, -1, 4, -2, 4}, new int[][]{{2, 4}}) == 65);
//        System.err.println(p.robotSim(new int[]{-2, 8, 3, 7, -1}, new int[][]{}));
//        System.err.println(p.robotSim(new int[]{4, -1, 4, -2, 4}, new int[][]{{2, 4}}));
        System.err.println(p.robotSim(new int[]{2, -1, 8, -1, 6}, new int[][]{}));
    }

    /**
     * 思路：
     * 暴力法
     * 用set存储障碍物的坐标
     * 因为java没有内置Pair或者Tuple2这种数据结构
     * 所以可以使用long来表示两个int，低32位是x坐标高32位是y坐标，这样就可以直接使用set
     * 执行行走命令时，一步步向前走，碰见障碍物就停止该条命令
     * 方向规定为 0代表x+，1代表y+，2代表x-，3代表y-
     */
    public int robotSim(int[] commands, int[][] obstacles) {
        // 题目有假设，不做校验
        Set<Long> obstacleSet = new HashSet<>();
        for (int i = 0; i < obstacles.length; i++) {
            long point = obstacles[i][0] + (((long) obstacles[i][1]) << 32);
            obstacleSet.add(point);
        }
        int x = 0;
        int y = 0;
        int direction = 1;
        // 距离的平方
        long maxDistanceSquare = 0;
        for (int i = 0; i < commands.length; i++) {
            if (commands[i] == -1) {
                direction = (direction - 1) % 4;
                if (direction == -1) {
                    direction = 3;
                }
            } else if (commands[i] == -2) {
                direction = (direction + 1) % 4;
            } else {
                for (int j = 0; j < commands[i]; j++) {
                    int currentX = x;
                    int currentY = y;
                    switch (direction) {
                        case 0:
                            currentX += 1;
                            break;
                        case 1:
                            currentY += 1;
                            break;
                        case 2:
                            currentX -= 1;
                            break;
                        case 3:
                            currentY -= 1;
                            break;
                        default:
                            // do nothing
                    }
                    if (obstacleSet.contains(currentX + (((long) currentY) << 32))) {
                        break;
                    }
                    x = currentX;
                    y = currentY;
                }
                maxDistanceSquare = Math.max(x * x + y * y, maxDistanceSquare);
            }
        }
        return (int) maxDistanceSquare;
    }
}
