package p08;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/most-common-word/
 */
public class Problem0819 {

    public static void main(String[] args) {
        Problem0819 p = new Problem0819();
        System.err.println(p.mostCommonWord("Bob hit a ball, the hit BALL flew far after it was hit.", new String[]{"hit"}));
    }

    /**
     * 思路：
     * 利用hash表统计次数，可以把banned也放入hash表，用一个特殊的次数标记，比如-1
     * 因为不知道具体有那些分隔符，所有需要通过遍历并使用substring来拆分paragraph
     */
    public String mostCommonWord(String paragraph, String[] banned) {
        // 题目有假设，不做校验
        Map<String, Integer> countMap = new HashMap<>();
        for (String ban : banned) {
            countMap.put(ban, -1);
        }
        int start = 0;
        int end = 0;
        int maxCount = 0;
        String maxCountString = "";
        paragraph = paragraph.toLowerCase();
        while (start < paragraph.length()) {
            if (Character.isLetter(paragraph.charAt(start))) {
                end = start + 1;
                while (end < paragraph.length() && Character.isLetter(paragraph.charAt(end))) {
                    end++;
                }
                String substring = paragraph.substring(start, end);
                int count = countMap.getOrDefault(substring, 0);
                if (count != -1) {
                    count = count + 1;
                    countMap.put(substring, count);
                    if (count > maxCount) {
                        maxCount = count;
                        maxCountString = substring;
                    }
                }
                start = end;
            }
            start++;
        }
        return maxCountString;
    }
}
