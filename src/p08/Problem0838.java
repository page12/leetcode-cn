package p08;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/push-dominoes/
 */
public class Problem0838 {

    public static void main(String[] args) {
        Problem0838 p = new Problem0838();
        System.err.println(p.pushDominoes(".L.R...LR..L.."));
        System.err.println(p.pushDominoes("RR.L"));
        System.err.println(p.pushDominoes("L.....RR.RL.....L.R."));
        System.err.println(p.pushDominoes("RR.R"));
    }

    /**
     * 思路：
     * 当一个点初始是LR最后的，最后还是LR
     * 当一个点初始不是L或者R时，设它
     * 离左边最近的R的距离为 lr
     * 离左边最近的L的距离为 ll
     * 离右边最近的L的距离为 rl
     * 离右边最近的L的距离为 rr
     * 为了方便处理，在计算距离时包含这个点本身，也就是点本身是LR时，对应的距离为1
     * 有下面三种状况
     * 当 rl[i] < rr[i] && (ll[i] < lr[i] || rl[i] < lr[i]) 时，最终状态是 L
     * 当 lr[i] < ll[i] && (rr[i] < rl[i] || lr[i] < rl[i]) 时，最终状态是 R
     * 其余情况，最终状态是 .
     * 转换为求字符串中每个点左右边最近LR的距离
     * 用四个数组存储，正反各遍历一次就可以了求出四个数组
     * 最后通过四个距离判断最终状态
     */
    public String pushDominoes(String dominoes) {
        // 题目有假设，不做校验
        char[] chars = dominoes.toCharArray();
        int n = chars.length;
        // 求每个点离左边最近的LR的距离，初始距离为无穷大，也就是Integer.MAX_VALUE
        int[] ll = new int[n];
        int[] lr = new int[n];
        Arrays.fill(ll, Integer.MAX_VALUE);
        Arrays.fill(lr, Integer.MAX_VALUE);
        ll[0] = chars[0] == 'L' ? 1 : Integer.MAX_VALUE;
        lr[0] = chars[0] == 'R' ? 1 : Integer.MAX_VALUE;
        for (int i = 1; i < n; i++) {
            if (chars[i] == '.') {
                ll[i] = ll[i - 1] == Integer.MAX_VALUE ? Integer.MAX_VALUE : ll[i - 1] + 1;
                lr[i] = lr[i - 1] == Integer.MAX_VALUE ? Integer.MAX_VALUE : lr[i - 1] + 1;
            } else if (chars[i] == 'L') {
                ll[i] = 1;
                lr[i] = lr[i - 1] == Integer.MAX_VALUE ? Integer.MAX_VALUE : lr[i - 1] + 1;
            } else {
                ll[i] = ll[i - 1] == Integer.MAX_VALUE ? Integer.MAX_VALUE : ll[i - 1] + 1;
                lr[i] = 1;
            }
        }
        // 求每个点离右边最近的LR的距离，初始距离为无穷大，也就是Integer.MAX_VALUE
        int[] rl = new int[n];
        int[] rr = new int[n];
        Arrays.fill(rl, Integer.MAX_VALUE);
        Arrays.fill(rr, Integer.MAX_VALUE);
        rl[n - 1] = chars[n - 1] == 'L' ? 1 : Integer.MAX_VALUE;
        rr[n - 1] = chars[n - 1] == 'R' ? 1 : Integer.MAX_VALUE;
        for (int i = n - 2; i >= 0; i--) {
            if (chars[i] == '.') {
                rl[i] = rl[i + 1] == Integer.MAX_VALUE ? Integer.MAX_VALUE : rl[i + 1] + 1;
                rr[i] = rr[i + 1] == Integer.MAX_VALUE ? Integer.MAX_VALUE : rr[i + 1] + 1;
            } else if (chars[i] == 'L') {
                rl[i] = 1;
                rr[i] = rr[i + 1] == Integer.MAX_VALUE ? Integer.MAX_VALUE : rr[i + 1] + 1;
            } else {
                rl[i] = rl[i + 1] == Integer.MAX_VALUE ? Integer.MAX_VALUE : rl[i + 1] + 1;
                rr[i] = 1;
            }
        }
        // 判断两个距离的大小，确定最终状态
        for (int i = 0; i < n; i++) {
            // 只有初始是.的点才会变化，初始是LR的结束还是LR
            if (chars[i] == '.') {
                if (rl[i] < rr[i] && (ll[i] < lr[i] || rl[i] < lr[i])) {
                    chars[i] = 'L';
                } else if (lr[i] < ll[i] && (rr[i] < rl[i] || lr[i] < rl[i])) {
                    chars[i] = 'R';
                }
            }
        }
        return new String(chars);
    }
}
