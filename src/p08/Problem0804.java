package p08;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/unique-morse-code-words/
 */
public class Problem0804 {

    public static void main(String[] args) {
        Problem0804 p = new Problem0804();
        System.err.println(p.uniqueMorseRepresentations(new String[]{"gin", "zen", "gig", "msg"}) == 2);
    }

    /**
     * 思路：
     * 利用HashSet保存编码后的字符串，能够自动去重
     */
    public int uniqueMorseRepresentations(String[] words) {
        String[] morse = new String[]{".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                "....", "..", ".---", "-.-", ".-..", "--", "-.",
                "---", ".--.", "--.-", ".-.", "...", "-",
                "..-", "...-", ".--", "-..-", "-.--", "--.."};
        Set<String> set = new HashSet<>();
        for (String word : words) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < word.length(); i++) {
                builder.append(morse[word.charAt(i) - 'a']);
            }
            set.add(builder.toString());
        }
        return set.size();
    }
}
