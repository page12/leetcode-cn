package p08;

/**
 * https://leetcode-cn.com/problems/lemonade-change/
 */
public class Problem0860 {

    public static void main(String[] args) {
        Problem0860 p = new Problem0860();
        System.err.println(p.lemonadeChange(new int[]{5, 5, 5, 10, 20}));
        System.err.println(p.lemonadeChange(new int[]{5, 5, 10}));
        System.err.println(p.lemonadeChange(new int[]{10, 10}));
        System.err.println(p.lemonadeChange(new int[]{5, 5, 10, 10, 20}));
    }

    /**
     * 思路：
     * 本题的找零情况就三种
     * 1、收20块，找回三个5块
     * 2、收20块，找回一个5块和一个10块
     * 3、收10块，找回一个5块
     * 因为5块的找零作用大于10块，所以20块应该优先用情况2找零
     * 可以用两个变量记录当前自己手上5块和10块的数量
     * 找零时判断下对于的数量是否大于等于就行
     * 只需要一次遍历，外加两个变量记录数量
     * 所以时间复杂度O(N)，空间复杂度O(1)
     */
    public boolean lemonadeChange(int[] bills) {
        // 题目有假设，不做校验
        if (bills.length == 0) {
            return true;
        }
        int fiveCount = 0;
        int tenCount = 0;
        for (int i = 0; i < bills.length; i++) {
            if (bills[i] == 10) {
                tenCount++;
                if (fiveCount >= 1) {
                    fiveCount--;
                } else {
                    return false;
                }
            } else if (bills[i] == 20) {
                // 20块收到了就不能用出去找零，所以不需要记录
                if (tenCount >= 1 && fiveCount >= 1) {
                    // 此时优先用10块找零
                    tenCount--;
                    fiveCount--;
                } else if (fiveCount >= 3) {
                    fiveCount -= 3;
                } else {
                    return false;
                }
            } else {
                fiveCount++;
            }
        }
        return true;
    }
}
