package p08;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/middle-of-the-linked-list/
 */
public class Problem0876 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 这题之前碰见过，经典的快慢双指针
     * 快指针一次走两步，慢指针一次走一步
     * 奇数长度时：
     * 快指针到末尾时，慢指针刚好是中间
     * 偶数长度时：
     * 如果允许快指针只走一步，则快指针到末尾时，慢指针是中间位置两个中后面的一个
     * 如果快指针必须一次走两步，则快指针不能继续往下走时，慢指针是中间位置两个中前面的一个
     * <br/>
     * 这题可以通过两次遍历来实现，也比较简单
     * 主要是记住快慢指针这种简单但是比较难想出来的技巧
     */
    public ListNode middleNode(ListNode head) {
        // 题目有假设，不做校验
        ListNode pFast = head;
        ListNode pSlow = head;
        while (pFast.next != null) {
            pSlow = pSlow.next;
            pFast = pFast.next;
            pFast = pFast.next == null ? pFast : pFast.next;
        }
        return pSlow;
    }
}
