package p08;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/subdomain-visit-count/
 */
public class Problem0811 {

    public static void main(String[] args) {
        Problem0811 p = new Problem0811();
        System.err.println(p.subdomainVisits(new String[]{"9001 discuss.leetcode.com"}));
        System.err.println(p.subdomainVisits(new String[]{"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"}));
    }

    /**
     * 思路：
     * 直接用hash表存储域名和次数
     * 域名字符串不断用 . 进行 indexOf 和 subString 就能得到所有域名
     */
    public List<String> subdomainVisits(String[] cpdomains) {
        // 题目有假设，不做校验
        Map<String, Integer> countMap = new HashMap<>();
        for (String cpdomain : cpdomains) {
            int idxOfSpace = cpdomain.indexOf(' ');
            String domain = cpdomain.substring(idxOfSpace + 1);
            Integer count = Integer.valueOf(cpdomain.substring(0, idxOfSpace));
            countMap.merge(domain, count, (oldValue, newValue) -> oldValue + count);
            int idx = domain.indexOf('.');
            while (idx > 0) {
                domain = domain.substring(idx + 1);
                countMap.merge(domain, count, (oldValue, newValue) -> oldValue + count);
                idx = domain.indexOf('.');
            }
        }
        List<String> result = new ArrayList<>(countMap.size());
        countMap.forEach((k, v) -> {
            result.add(v + " " + k);
        });
        return result;
    }
}
