package p08;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/length-of-longest-fibonacci-subsequence/
 */
public class Problem0873 {

    public static void main(String[] args) {
        Problem0873 p = new Problem0873();
        System.err.println(p.lenLongestFibSubseq(new int[]{1, 2, 3, 4, 5, 6, 7, 8}) == 5);
        System.err.println(p.lenLongestFibSubseq(new int[]{1, 3, 7, 11, 12, 14, 18}) == 3);
        System.err.println(p.lenLongestFibSubseq(new int[]{2, 4, 5, 6, 7, 8, 11, 13, 14, 15, 21, 22, 34}) == 5);
        System.err.println(p.lenLongestFibSubseq(new int[]{1, 3, 4, 7, 10, 11, 12, 18, 23, 35}) == 6);
        System.err.println(p.lenLongestFibSubseq(new int[]{2, 4, 7, 8, 9, 10, 14, 15, 18, 23, 32, 50}) == 5);
        System.err.println(p.lenLongestFibSubseq(new int[]{1, 3, 5}) == 0);
        System.err.println(p.lenLongestFibSubseq(new int[]{3, 4, 8, 11, 12, 15, 16, 20, 21, 22, 24, 26, 27, 28, 30, 39, 42, 43, 44, 45, 47, 50, 51, 53, 56, 58, 64, 72, 75, 78, 80, 81, 86, 103, 105, 107, 108, 116, 120, 123, 129, 130, 142, 156, 163, 171, 172, 188, 195, 204, 210, 228, 259, 278, 280, 304, 315, 327, 340, 370, 415, 449, 452, 550, 598, 674, 727, 732, 890, 1089, 1176, 1184, 1440, 1763, 1903, 1916, 2330, 2852, 3079, 3100, 3770, 4615, 4982, 5016, 6100, 7467, 8061, 8116, 9870, 12082, 13043, 13132, 15970, 19549, 21104, 21248, 31631, 34147, 34380, 51180, 55251, 55628, 90008, 145636}) == 19);

    }

    /**
     * 思路：
     * 暴力解法，因为一个斐波那契序列的前两个值必须手动指定，指定后，后续的都可以递推出来
     * 所以枚举所有的前两个值，并不断递推新的值
     * 如果新的值在数组中，则继续，否则就停止并记录一次最大长度
     * 判断新值是否在数组中，可以使用hash表快速判断
     */
    public int lenLongestFibSubseq(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            map.put(arr[i], i);
        }
        int maxLength = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                int minusTwo = arr[i];
                int minusOne = arr[j];
                int currentLength = 2;
                int fib = minusTwo + minusOne;
                while (map.containsKey(fib)) {
                    currentLength++;
                    minusTwo = minusOne;
                    minusOne = fib;
                    fib = minusTwo + minusOne;
                }
                if (currentLength > 2) {
                    maxLength = Math.max(maxLength, currentLength);
                }
            }
        }
        return maxLength;
    }
}
