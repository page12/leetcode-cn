package p08;

/**
 * https://leetcode-cn.com/problems/binary-gap/
 */
public class Problem0686 {

    public static void main(String[] args) {
        Problem0686 p = new Problem0686();
        System.err.println(p.binaryGap(22) == 2);
        System.err.println(p.binaryGap(5) == 2);
        System.err.println(p.binaryGap(6) == 1);
        System.err.println(p.binaryGap(8) == 0);
        System.err.println(p.binaryGap(1) == 0);
    }

    /**
     * 思路：
     * 循环移位，这个最直观
     * 记录下上一次1出现的位置，本次也是1时，和计算一次最大距离
     */
    public int binaryGap(int n) {
        int result = 0;
        int lastPos = 0;
        int pos = 1;
        int mask = 1;
        while (mask != 0) {
            if ((mask & n) != 0) {
                if (lastPos != 0) {
                    result = Math.max(result, pos - lastPos);
                }
                lastPos = pos;
            }
            mask = mask << 1;
            pos++;
        }
        return result;
    }
}
