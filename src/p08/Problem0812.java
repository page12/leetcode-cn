package p08;

/**
 * https://leetcode-cn.com/problems/largest-triangle-area/
 */
public class Problem0812 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 三角形面积通用计算公式（非底 * 高 / 2），我知道的就两个，海伦公式好和外积公式
     * 海伦公式计算边长需要开方，不方便进行计算
     * 二维向量的外积可以直接用坐标计算，更加方便
     * 二维向量的外积 = 向量a x 向量b = |a| * |b| * sin夹角 = 2 * S
     * 二维 向量a x 向量b 转换为坐标公式 = |xa * yb - xb * ya|
     * 所以 S = (xa * yb - xb * ya) / 2
     * 上面的公式需要三角形中一个点在原点
     * 当不在原点时，利用坐标差来计算，有
     * (xa - xc)*(yb - yc) - (xb - xc)*(ya - yc) = xa * yb + xb * yc + xc * ya - xa * yc - xb * ya - xc * yb
     * 综上，三个点的三角形面积为 |(xa * yb + xb * yc + xc * ya - xa * yc - xb * ya - xc * yb)| / 2
     */
    public double largestTriangleArea(int[][] points) {
        int length = points.length;
        double max = 0;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                for (int k = j + 1; k < length; k++) {
                    int s = points[i][0] * points[j][1] + points[j][0] * points[k][1] + points[k][0] * points[i][1]
                            - points[i][0] * points[k][1] - points[j][0] * points[i][1] - points[k][0] * points[j][1];
                    max = Math.max(Math.abs(s) / 2.0, max);
                }
            }
        }
        return max;
    }
}
