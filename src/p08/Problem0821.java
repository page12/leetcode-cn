package p08;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/shortest-distance-to-a-character/
 */
public class Problem0821 {

    public static void main(String[] args) {
        Problem0821 p = new Problem0821();
        System.err.println(Arrays.toString(p.shortestToChar("loveleetcode", 'e')));
    }

    /**
     * 思路：
     * 利用一个hash表
     * 先遍历一次s，计算左边最近的那一个c的距离，如果左边没有，则使用Integer.MAX_VALUE，使用hash表存储这个距离
     * 再遍历一次s，计算右边最近的那一个c的距离，如果小于之前存储的左距离，则更新，否则不更新
     * 两个对应的距离的较小值就是返回结果中的距离
     * 求单向距离可以用双指针简单实现，之前题目出现很多次了，左边和右边的区别就是遍历方向就行
     */
    public int[] shortestToChar(String s, char c) {
        // 题目有假设，不做校验
        int[] distance = new int[s.length()];
        // 求左边的最短距离
        int start = s.length() - 1;
        int end = s.length() - 1;
        while (start >= 0) {
            if (s.charAt(start) == c) {
                distance[start] = 0;
                start--;
                continue;
            }
            end = start - 1;
            while (end >= 0 && s.charAt(end) != c) {
                end--;
            }
            if (end >= 0) {
                // 左边有，距离依次减小
                for (int i = start; i >= end; i--) {
                    distance[i] = i - end;
                }
            } else {
                // 左边没有，距离设为最大值
                for (int i = start; i > end; i--) {
                    distance[i] = Integer.MAX_VALUE;
                }
            }
            start = end - 1;
        }
        // 求右边的最短距离
        start = 0;
        end = 0;
        while (start < s.length()) {
            if (s.charAt(start) == c) {
                distance[start] = 0;
                start++;
                continue;
            }
            end = start + 1;
            while (end < s.length() && s.charAt(end) != c) {
                end++;
            }
            if (end < s.length()) {
                // 右边有，距离依次减小
                for (int i = start; i <= end; i++) {
                    distance[i] = Math.min(end - i, distance[i]);
                }
            } else {
                // 这个分支可以不要，但还是放在这里，逻辑看起来更完整
                // 右边没有，距离设为最大值
                for (int i = start; i < end; i++) {
                    distance[i] = Math.min(Integer.MAX_VALUE, distance[i]);
                }
            }
            start = end + 1;
        }
        return distance;
    }
}
