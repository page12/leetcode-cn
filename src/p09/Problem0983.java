package p09;

/**
 * https://leetcode-cn.com/problems/minimum-cost-for-tickets/
 */
public class Problem0983 {

    public static void main(String[] args) {
        Problem0983 p = new Problem0983();
        System.err.println(p.mincostTickets(new int[]{1, 4, 6, 7, 8, 20}, new int[]{2, 7, 15}) == 11);
        System.err.println(p.mincostTickets(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 30, 31}, new int[]{2, 7, 15}) == 17);
        System.err.println(p.mincostTickets(new int[]{1, 2, 3, 4, 6, 8, 9, 10, 13, 14, 16, 17, 19, 21, 24, 26, 27, 28, 29}, new int[]{3, 14, 50}) == 50);
    }

    /**
     * 思路：
     * day[i]是旅行时间，则最迟在 day[i]，days[i]-7+1，days[i]-30+1 这一天买对应的 1天 7天 30天 的票
     * 设dp[i]为days[i-1]这一天结束旅行时对应的最小花费
     * 为了方便处理days[0]的情况，可以把dp数组多一个长度，并且规定dp[0] = 0
     * day[i]这一天结束旅行，则有三种状况：
     * 1、在 days[i] 这一天买1天的票，则有 dp[i] = dp[i-1] + cost[0]
     * 2、在 days[i]-7+1 这一天买7天的票，则有 dp[i] = dp[x] + cost[1]，x是 days[i]-7+1 对应的下标
     * 3、在 days[i]-30+1 这一天买30天的票，则有 dp[i] = dp[x] + cost[2]，x是 days[i]-30+1 对应的下标
     * 三种情况的最小值就是 dp[i] 的值
     * 因为 days[i]-7+1 和 days[i]-30+1 可能不在 days[] 中，为了票是有效的，需要找一个比它大的最小值代替，这就是x
     * 这个可以用到二分查找
     */
    public int mincostTickets(int[] days, int[] costs) {
        //
        // 这样就有 dp[0] = 0
        // 题目有假设，不做校验
        int[] dp = new int[days.length + 1];
        dp[0] = 0;
        for (int i = 1; i <= days.length; i++) {
            int day = days[i - 1];
            int a = dp[i - 1] + costs[0];
            int b = dp[search(days, day - 7 + 1)] + costs[1];
            int c = dp[search(days, day - 30 + 1)] + costs[2];
            dp[i] = Math.min(a, Math.min(b, c));
        }
//        System.err.println(Arrays.toString(dp));
        return dp[days.length];
    }

    /**
     * 二分查找买票时间
     * 如果找到就直接返回下标，找不到时返回比target大的最小值对应的下标，即循环结束时的 start
     */
    private int search(int[] days, int target) {
        int start = 0;
        int end = days.length;
        while (start <= end) {
            int mid = (end - start) / 2 + start;
            if (days[mid] == target) {
                return mid;
            } else if (days[mid] < target) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return start;
    }
}
