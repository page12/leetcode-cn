package p09;

/**
 * https://leetcode-cn.com/problems/longest-turbulent-subarray/
 */
public class Problem0978 {

    public static void main(String[] args) {
        Problem0978 p = new Problem0978();
        System.err.println(p.maxTurbulenceSize(new int[]{9, 4, 2, 10, 7, 8, 8, 1, 9}) == 5);
        System.err.println(p.maxTurbulenceSize(new int[]{4, 8, 12, 16}) == 2);
        System.err.println(p.maxTurbulenceSize(new int[]{100}) == 1);
    }

    /**
     * 思路：
     * 子数组因为必须连续再一起，已经判断了子数组前面的部分后，就可以不用再管它了，所以一般都是贪心算法可以求解
     * 子序列不需要连续，往往需要重复判断子序列前面的部分是否满足条件，此时就需要标准的动态规划了
     * 本题是子数组，所以贪心算法就可以了
     * 判断是否递增的条件也很简单，通过比较相邻的三个数，判断它们的两两比较结果的乘积是否是 -1 就行
     * 另外，还要特殊处理下比较结果是0的情况
     */
    public int maxTurbulenceSize(int[] arr) {
        // 题目有假设，不做校验
        if (arr.length == 1) {
            return 1;
        }
        int lastCmp = Integer.compare(arr[1], arr[0]);
        if (arr.length == 2) {
            return lastCmp == 0 ? 1 : 2;
        }
        int maxLength = lastCmp == 0 ? 1 : 2;
        int length = maxLength;
        for (int i = 2; i < arr.length; i++) {
            int cmp = Integer.compare(arr[i], arr[i - 1]);
            if ((lastCmp == 0 && cmp != 0) || lastCmp * cmp == -1) {
                length++;
            } else {
                maxLength = Math.max(maxLength, length);
                length = cmp == 0 ? 1 : 2;
            }
            lastCmp = cmp;
        }
        // 最后还要比较一次，用于处理数组末尾也满足条件的情况
        return Math.max(maxLength, length);
    }
}
