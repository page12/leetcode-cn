package p09;

import java.util.Deque;
import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/sum-of-subarray-minimums/
 */
public class Problem0907 {

    public static void main(String[] args) {
        Problem0907 p = new Problem0907();
//        System.err.println(p.sumSubarrayMins(new int[]{3, 1, 2, 4}) == 17);
//        System.err.println(p.sumSubarrayMins(new int[]{11, 81, 94, 43, 3}) == 444);
//        System.err.println(p.sumSubarrayMins(new int[]{1, 7, 5, 2, 2, 9, 3}) == 69);
        System.err.println(p.sumSubarrayMins(new int[]{1, 3, 2, 6, 9, 2, 4, 7}) == 89);
    }

    /**
     * 思路：
     * 暴力枚举所有子数组的区间
     * 计算区间[i，j]内的最小值通过一个简单的dp来加速算出
     * 不过这样会超时
     * <br/>
     * 通过观察，总算是找到了一个方法
     * 设 L[i] 为 arr[i] 左边第一个比它小或者相等的元素到它的距离
     * i == 0 时，没有左边，可以假设它的左边第一个比它小的元素下标为 -1
     * 设 R[i] 为 arr[i] 右边第一个比它小的元素到它的距离
     * i == arr.length-1 时，没有右边，可以假设它的右边第一个比它小的元素下标为 arr.length
     * 举个例子，设数组为
     * 1, 3, 2, 6, 9, 2, 4, 7    它的子数组的最小值分别为（x用于右对齐补全，idea自动格式化会删除前面的空格，不用管x）
     * 1, 1, 1, 1, 1, 1, 1, 1 => 以1为起点，竖向对应的数为终点的的子数组的最小值
     * x, 3, 2, 2, 2, 2, 2, 2 => 以3为起点，竖向对应的数为终点的的子数组的最小值
     * x, x, 2, 2, 2, 2, 2, 2 => 以2为起点，竖向对应的数为终点的的子数组的最小值
     * x, x, x, 6, 6, 2, 2, 2 => 以6为起点，竖向对应的数为终点的的子数组的最小值
     * x, x, x, x, 9, 2, 2, 2 => 以9为起点，竖向对应的数为终点的的子数组的最小值
     * x, x, x, x, x, 2, 2, 2 => 以2为起点，竖向对应的数为终点的的子数组的最小值
     * x, x, x, x, x, x, 4, 4 => 以4为起点，竖向对应的数为终点的的子数组的最小值
     * x, x, x, x, x, x, x, 7 => 以7为起点，竖向对应的数为终点的的子数组的最小值
     * 从左至右，把相同的最小值按竖向分割成矩形
     * 1 => 1 * 8
     * 3 => 1 * 1
     * 2 => 2 * 6
     * 6 => 1 * 2
     * 9 => 1 * 1
     * 2 => 3 * 3
     * 4 => 1 * 2
     * 7 => 1 * 1
     * 乘法的左边就是对应的数的 L[i]，右边就是对应的数的 R[i]
     * 这样最终结果 = sum(arr[i] * L[i] * R[i])，i从0到arr.length-1
     * 因此题目转换为求 L[i] 和 R[i]
     * 变成了典型的单调栈应用场景
     */
    public int sumSubarrayMins(int[] arr) {
        // 题目有假设，不做校验
        int mod = 1000000007;
        Deque<Integer> stack = new LinkedList<>();
        // 设 L[i] 为 arr[i] 左边第一个比它小或者相等的元素到它的距离
        // L[0] 没有左边，假设它的左边比它小的下标为 -1
        int[] L = new int[arr.length];
        stack.push(-1);
        int i = 0;
        // 从左至右遍历，一直要保证 栈顶 -> 栈底 满足 >= 关系
        while (i < arr.length) {
            int idx = stack.peek();
            if (idx == -1 || arr[i] >= arr[idx]) {
                L[i] = i - idx;
                stack.push(i);
                i++;
            } else {
                stack.pop();
            }
        }
//        System.err.println(Arrays.toString(L));
        // 设 R[i] 为 arr[i] 右边第一个比它小的元素到它的距离
        // R[arr.length-1] 没有右边，假设它的左边比它小的下标为 arr.length
        int[] R = new int[arr.length];
        stack.clear();
        stack.push(arr.length);
        i = arr.length - 1;
        // 从右至左遍历，一直要保证 栈顶 -> 栈底 满足 > 关系
        while (i >= 0) {
            int idx = stack.peek();
            if (idx == arr.length || arr[i] > arr[idx]) {
                R[i] = -(i - idx);
                stack.push(i);
                i--;
            } else {
                stack.pop();
            }
        }
//        System.err.println(Arrays.toString(R));
        // 使用long避免乘法溢出
        long sum = 0;
        for (i = 0; i < arr.length; i++) {
            // 不能直接写成 sum += arr[i] * L[i] * R[i]
            // 要一开始就变成long类型，不能先int乘法（这一步可能会溢出）再通过long加法自动转型
            // 这个地方提交错了好几次，每次都是那种错了都不知道哪里错了的测试用例，一屏幕都是数字
            sum += 1L * arr[i] * L[i] * R[i];
        }
        // 因为用了long类型，可以最后才执行一次mod操作
        sum %= mod;
        return (int) sum;
    }

//    public int sumSubarrayMins(int[] arr) {
//        // 暴力算法，会超时
//        // 题目有假设，不做校验
//        int mod = 1000000007;
//        // 使用dp求[i,j]范围内的最小值
//        int[] dp = new int[arr.length];
//        int sum = 0;
//        // 暴力枚举所有子数组的范围
//        // i 为子数组起点下标
//        // j 为子数组终点下标
//        for (int i = 0; i < arr.length; i++) {
//            for (int j = i; j < arr.length; j++) {
//                if (i == j) {
//                    dp[j] = arr[j];
//                } else {
//                    dp[j] = Math.min(dp[j - 1], arr[j]);
//                }
//                sum += dp[j];
//                if (sum >= mod) {
//                    sum = sum % mod;
//                }
//            }
//        }
//        return sum;
//    }
}
