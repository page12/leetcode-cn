package p06;

import java.util.Arrays;
import java.util.Comparator;

/**
 * https://leetcode-cn.com/problems/maximum-length-of-pair-chain/
 */
public class Problem0646 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 和435题很像，可以用动态规划，也可以用贪心算法
     */
    public int findLongestChain(int[][] pairs) {
        // 题目有假设，不做校验
        Arrays.sort(pairs, Comparator.comparingInt(a -> a[1]));
        int maxLength = 1;
        int r = pairs[0][1];
        for (int i = 1; i < pairs.length; i++) {
            if (pairs[i][0] > r) {
                r = pairs[i][1];
                maxLength++;
            }
        }
        return maxLength;
    }
}
