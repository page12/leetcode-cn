package p06;

/**
 * https://leetcode-cn.com/problems/maximum-average-subarray-i/
 */
public class Problem0643 {

    public static void main(String[] args) {
        Problem0643 p = new Problem0643();
        System.err.println(p.findMaxAverage(new int[]{1, 12, -5, -6, 50, 3}, 4));
    }

    /**
     * 思路：
     * 长度为 k 的连续子数组只有 n - k + 1 个
     * 设下标从1开始，则这 n - k + 1 个分别是
     * [1, k], [2, k + 1], ... , [n - k + 1, n]
     * 顺序遍历一次求这 n - k + 1 个之中的最大和，最后一步再平均即可得到最终结果
     * <br/>
     * 求时如果每次都直接使用 nums[i] + nums[i + 1] + nums[i + 2] + ... + nums[i + k - 1]，则变成双重循环，速度会很慢
     * 对比下[1, k] 和 [2, k + 1]，它只有两个数字不同，少了一个然后多了一个
     * 因此可以利用前一次的结果快速求当前的结果，这样就避免了双循环，降低了时间复杂度
     * 总的时间复杂度为O(N)，空间复杂度为O(1)
     */
    public double findMaxAverage(int[] nums, int k) {
        // 题目有假设，不做校验
        int sum = 0;
        for (int i = 0; i < k; i++) {
            sum += nums[i];
        }
        int maxSum = sum;
        for (int i = 1; i < nums.length - k + 1; i++) {
            sum = sum - nums[i - 1] + nums[i + k - 1];
            maxSum = Math.max(sum, maxSum);
        }
        return maxSum * 1.0d / k;
    }
}
