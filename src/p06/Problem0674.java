package p06;

/**
 * https://leetcode-cn.com/problems/longest-continuous-increasing-subsequence/
 */
public class Problem0674 {

    public static void main(String[] args) {
        Problem0674 p = new Problem0674();
        System.err.println(p.findLengthOfLCIS(new int[]{1, 3, 5, 4, 7}) == 3);
        System.err.println(p.findLengthOfLCIS(new int[]{2, 2, 2, 2, 2}) == 1);
        System.err.println(p.findLengthOfLCIS(new int[]{1, 3, 5, 7}) == 4);
    }

    /**
     * 思路：
     * 简单的贪心算法
     * 遍历数组
     * 如果当前是递增的，则长度加1
     * 否则，计算一次最大长度
     * 遍历完成后再计算一次最大长度，就得到结果
     */
    public int findLengthOfLCIS(int[] nums) {
        // 题目有假设，不做校验
        if (nums.length == 1) {
            return 1;
        }
        int maxLength = 1;
        int lastNum = nums[0];
        int currentLength = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > lastNum) {
                currentLength++;
            } else {
                maxLength = Math.max(currentLength, maxLength);
                currentLength = 1;
            }
            lastNum = nums[i];
        }
        return Math.max(currentLength, maxLength);
    }
}
