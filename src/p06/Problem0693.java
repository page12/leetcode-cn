package p06;

/**
 * https://leetcode-cn.com/problems/binary-number-with-alternating-bits/
 */
public class Problem0693 {

    public static void main(String[] args) {
        Problem0693 p = new Problem0693();
        System.err.println(p.hasAlternatingBits(5));
        System.err.println(p.hasAlternatingBits(7));
        System.err.println(p.hasAlternatingBits(11));
        System.err.println(p.hasAlternatingBits(10));
        System.err.println(p.hasAlternatingBits(3));
    }

    /**
     * 思路：
     * 对于一个二进制，如果1和0交叉出现，那么只有两种情况，设最左边的那一位为第一位
     * a、奇数位都是1，类似 0101 = 5 这种，以01为循环节，对应奇数n
     * b、偶数位都是1，类似 1010 = A（十六进制） 这种，以10为循环节，对应偶数n
     * 所以可以通过移位来判断，每次把n左移两位，判断对应的两位是否是01或者10
     */
    public boolean hasAlternatingBits(int n) {
        // 通过奇数偶数判断循环节用01还是10
        int loopPart = (n & 1) == 1 ? 1 : 2;
        // loopPart是最右边两位，它的掩码是3
        int loopPartMask = 3;
        while (n != 0) {
            if ((loopPartMask & n) != loopPart) {
                return false;
            }
            n >>= 2;
        }
        return true;
    }
}
