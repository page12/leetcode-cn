package p06;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/partition-to-k-equal-sum-subsets/
 */
public class Problem0698 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 和第473题基本一样
     * 那一题是分成四个相等的，也就是k=4
     * 这一题就是一般情况，也就是正k边形
     * 对473题的代码稍微改进一下就行
     */
    public boolean canPartitionKSubsets(int[] nums, int k) {
        // 题目有假设，不做校验
        Arrays.sort(nums);
        int sum = 0;
        for (int i : nums) {
            sum += i;
        }
        if (sum % k != 0) {
            return false;
        }
        int length = sum / k;
        boolean[] used = new boolean[nums.length];
        Deque<Integer> stack = new LinkedList<>();
        return recurse(0, nums, used, stack, length, nums.length - 1, k);
    }

    /**
     * 递归处理
     *
     * @param remainingLength 当前要拼凑的边的剩余长度
     * @param matchsticks     原数组
     * @param used            使用情况数组
     * @param stack           一个栈，记录已经被使用的所有元素
     * @param length          边长，用于拼凑好一条边后，重新设置remainingLength
     * @param maxIndex        总是从剩余的最大的那一个数字开始去尝试，这个就是那个最大数字的下标
     * @param k               总共要拼凑的边的数量
     * @return
     */
    private boolean recurse(int remainingLength, int[] matchsticks, boolean[] used, Deque<Integer> stack, int length, int maxIndex, int k) {
        if (remainingLength == 0) {
            if (getHandledCount(stack, length) == k) {
                return true;
            } else {
                remainingLength = length;
                maxIndex = matchsticks.length - 1;
            }
        }
        for (int i = maxIndex; i >= 0; i--) {
            if (used[i] || matchsticks[i] > remainingLength) {
                continue;
            }
            stack.push(matchsticks[i]);
            used[i] = true;
            boolean result = recurse(remainingLength - matchsticks[i], matchsticks, used, stack, length, i - 1, k);
            if (result) {
                return true;
            } else {
                stack.pop();
                used[i] = false;
            }
        }
        return false;
    }

    /**
     * 计算已经拼凑好了的边的数量
     */
    private int getHandledCount(Deque<Integer> stack, int length) {
        int sum = 0;
        for (int i : stack) {
            sum += i;
        }
        return sum / length;
    }
}
