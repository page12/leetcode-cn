package p06;

/**
 * https://leetcode-cn.com/problems/number-of-longest-increasing-subsequence/
 */
public class Problem0673 {

    public static void main(String[] args) {
        Problem0673 p = new Problem0673();
        System.err.println(p.findNumberOfLIS(new int[]{1, 3, 5, 4, 7}) == 2);
        System.err.println(p.findNumberOfLIS(new int[]{2, 2, 2, 2, 2}) == 5);
        System.err.println(p.findNumberOfLIS(new int[]{1, 4, 3, 2}) == 3);
        System.err.println(p.findNumberOfLIS(new int[]{2, 1}) == 2);
    }

    /**
     * 思路：
     * 动态规划的思路和第300题很像
     * 那一题是求最长长度，本题是求最长长度的出现了多少次
     * 设len[i]是是以下标i结尾的最长递增子序列的长度，初始值全为1
     * 设dp[i]是以下标i结尾的最长子序列的数量，初始值全为1
     * 求len[i]和第300题一样
     * 内层循环中
     * 当len[i]变大时，表示产生了新的最长子序列，此时还只发现了一种递增，此时dp[i] = dp[j]
     * 当len[i]不变时，表示还可以通过此时下标j到下标i来递增，此时dp[i] += dp[j]
     * 最后通过两个数组，求最大长度对应的下标在dp中的累加和，即得到最终结果
     */
    public int findNumberOfLIS(int[] nums) {
        // 题目有假设，不做校验
        // 整个数组最长递增子序列的长度
        int maxLength = 1;
        // len[i]是是以下标i结尾的最长递增子序列的长度
        int[] len = new int[nums.length];
        // dp[i]是以下标i结尾的最长子序列的数量
        int[] dp = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            // 最少长度为1，最少数量也为1
            len[i] = 1;
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    if (len[j] + 1 > len[i]) {
                        len[i] = len[j] + 1;
                        dp[i] = dp[j];
                    } else if (len[j] + 1 == len[i]) {
                        dp[i] += dp[j];
                    }
                }
            }
            maxLength = Math.max(len[i], maxLength);
        }
        // 求最终数量
        int result = 0;
        for (int i = 0; i < len.length; i++) {
            if (len[i] == maxLength) {
                result += dp[i];
            }
        }
        return result;
    }
}
