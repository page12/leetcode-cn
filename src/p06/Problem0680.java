package p06;

/**
 * https://leetcode-cn.com/problems/valid-palindrome-ii/
 */
public class Problem0680 {

    public static void main(String[] args) {
        Problem0680 p = new Problem0680();
        System.err.println(p.validPalindrome("aba"));
        System.err.println(p.validPalindrome("abca"));
    }

    /**
     * 思路：
     * 左右指针相向而行，判断各自的字符是否相等
     * 如果不相等，则代表左右字符有一个能删除再判断剩下的部分是否回文
     * 左右哪一个都可以删除，因此最多需要判断两次
     */
    public boolean validPalindrome(String s) {
        // 题目假设了非空字符串
        if (s.length() <= 2) {
            return true;
        }
        int left = 0;
        int right = s.length() - 1;
        while (left < right) {
            if (s.charAt(left) != s.charAt(right)) {
                return validPalindrome(s, left, right - 1) || validPalindrome(s, left + 1, right);
            }
            left++;
            right--;
        }
        return true;
    }

    /**
     * 部分字符串判断是否回文
     */
    private boolean validPalindrome(String s, int left, int right) {
        while (left < right) {
            if (s.charAt(left) != s.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
