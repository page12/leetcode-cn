package p06;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/set-mismatch/
 */
public class Problem0645 {

    public static void main(String[] args) {
        Problem0645 p = new Problem0645();
        int[] array = new int[]{1, 2, 2, 4};
        System.err.println(Arrays.toString(p.findErrorNums(array)));
        System.err.println(Arrays.toString(array));
        System.err.println(Arrays.toString(p.findErrorNums(new int[]{1, 1})));
        System.err.println(Arrays.toString(p.findErrorNums(new int[]{2, 2})));
    }

    /**
     * 思路：
     * 本题和第448题基本一样，利用一个长度为n的数组当作hash表即可
     * 第448题只需要找到丢失的数字，因此可以用true/false当作hash表的value
     * 本题还需要找到重复的数字，因此需要改为次数当作hash表的value
     * 时间复杂度O(N)，空间复杂度O(N)
     * <br/>
     * 同样的，如果允许修改原数组，把原数组当作hash表，则可以利用O(1)空间完成
     * 此时，为了能够还原回之前的数组，可以采用加法运算
     * 每次加n代表次数 + 1，次数等于整除以n的结果，最后还原时根据次数减n（用取模运算也可以）即可
     * 时间复杂度O(N)，空间复杂度O(1)
     */
    public int[] findErrorNums(int[] nums) {
        // 这是新建hash表的解法
        // 题目有假设，不做校验
        // 利用数组当hash表，记录各个元素出现的次数
        int[] hash = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            hash[nums[i] - 1] += 1;
        }
        int[] result = new int[2];
        // 题目规定了有一个元素出现两次，另一个元素出现零次
        for (int i = 0; i < hash.length; i++) {
            if (hash[i] == 0) {
                result[1] = i + 1;
            } else if (hash[i] == 2) {
                result[0] = i + 1;
            }
        }
        return result;
    }

    public int[] findErrorNums2(int[] nums) {
        // 这是直接用原数组当hash表的解法
        // 题目有假设，不做校验
        // 利用数组当hash表，记录各个元素出现的次数，每次加n代表次数 + 1
        // 因为数组元素进行了加n操作，所以当作下标时要取模
        for (int i = 0; i < nums.length; i++) {
            nums[(nums[i] - 1) % nums.length] += nums.length;
        }
        int[] result = new int[2];
        // 题目规定了有一个元素出现两次，另一个元素出现一次
        // 出现两次的元素，因为加了2n，所以是唯一大于2n的元素对应的(下标+1)
        // 出现零次的元素，因为没有加n，所以是唯一小于等于n的元素对应的(下标+1)
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] <= nums.length) {
                result[1] = i + 1;
            } else if (nums[i] > 2 * nums.length) {
                result[0] = i + 1;
            }
            // 还原数组
            // 本题没必要，还浪费执行时间
            // 但是实际编程中这样很有必要
            nums[i] = (nums[i] - 1) % nums.length + 1;
        }
        return result;
    }
}
