package p06;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/second-minimum-node-in-a-binary-tree/
 */
public class Problem0671 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 直接递归，递归过程中进行判断
     * 唯一能利用的判断条件就是 root.val = min(root.left.val, root.right.val)
     * 利用这个能得到整棵树的根节点一定是最小值min，那么第二小的值就是整棵树中不等于min的最小值
     * 记leftMin2 = 左子树终不等于min的最小值，rightMin2 = 右子树终不等于min的最小值
     * 因为root.left.val是右子树的最小值，root.right.val是右子树的最小值
     * 所以如果 leftMin2 < root.right.val，则leftMin2是整棵树第二小的值，此时算是提前找到（先左子树递归）
     * 如果leftMin2 rightMin2都存在，则它们中间最小的那个就是第二小的值
     * 如果都不存在，则整棵树也不存在
     * 只有一个存在，则它就是第二小的值
     */
    public int findSecondMinimumValue(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) {
            return -1;
        }
        int leftMin2 = dfs(root.left, root.val);
        if (leftMin2 != -1 && leftMin2 < root.right.val) {
            // 在左子树中提前找到
            return leftMin2;
        }
        int rightMin2 = dfs(root.right, root.val);
        if (leftMin2 == -1 && rightMin2 == -1) {
            return -1;
        }
        if (leftMin2 == -1) {
            return rightMin2;
        }
        if (rightMin2 == -1) {
            return leftMin2;
        }
        return Math.min(leftMin2, rightMin2);
    }

    /**
     * 深度优先递归min2这个值，返回-1时代表没找到
     */
    private int dfs(TreeNode root, int min) {
        // 题目假设了每个节点的子节点数量只能为 2 或 0
        // 子节点为0
        if (root.left == null && root.right == null) {
            if (root.val == min) {
                return -1;
            } else {
                return root.val;
            }
        }
        // 子节点为2
        int leftMin2 = dfs(root.left, min);
        int rightMin2 = dfs(root.right, min);
        if (leftMin2 == -1) {
            return rightMin2;
        }
        if (rightMin2 == -1) {
            return leftMin2;
        }
        return Math.min(leftMin2, rightMin2);
    }
}
