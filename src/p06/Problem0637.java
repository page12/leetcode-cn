package p06;

import common.TreeNode;

import java.util.*;

/**
 * https://leetcode-cn.com/problems/average-of-levels-in-binary-tree/
 */
public class Problem0637 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 利用广度优先BFS，非常简单直观
     */
    public List<Double> averageOfLevels(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }
        List<Double> result = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        result.add(root.val + 0.0d);
        // 上一层的数量
        int count = 1;
        double sum = 0.0d;
        while (!queue.isEmpty()) {
            TreeNode t = queue.poll();
            count--;
            if (t.left != null) {
                queue.add(t.left);
                sum += t.left.val;
            }
            if (t.right != null) {
                queue.add(t.right);
                sum += t.right.val;
            }
            if (count == 0 && !queue.isEmpty()) {
                count = queue.size();
                result.add(sum * 1.0d / count);
                sum = 0;
            }
        }
        return result;
    }
}
