package p06;

import common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/two-sum-iv-input-is-a-bst/
 */
public class Problem0653 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 利用二叉搜索树的中序序列是有序的这一个性质
     * 先求出中序序列
     * 再双指针遍历中序序列查找是否有两数之和等于指定的数
     * <br/>
     * 中序序列需要O(N)空间，大于递归空间复杂度，所以总的空间复杂度为O(N)
     * 要遍历一次树 + 遍历一次中序序列，所以总的时间复杂度为O(N)
     */
    public boolean findTarget(TreeNode root, int k) {
        if (root == null) {
            return false;
        }
        // 先求中序序列
        List<Integer> list = new ArrayList<>();
        fillInorderList(root, list);
        // 退化为有序数组的两数之和
        int left = 0;
        int right = list.size() - 1;
        while (left < right) {
            int currentSum = list.get(left) + list.get(right);
            if (currentSum > k) {
                right--;
            } else if (currentSum < k) {
                left++;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * 求中序序列
     */
    private void fillInorderList(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        fillInorderList(root.left, list);
        list.add(root.val);
        fillInorderList(root.right, list);
    }
}
