package p06;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/construct-string-from-binary-tree/
 */
public class Problem0606 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 参考先序遍历的递归代码，添加判断逻辑即可
     * 只有如下四种情况：
     * 左右都没有
     * 只有右
     * 只有左
     * 左右都有
     */
    public String tree2str(TreeNode root) {
        if (root == null) {
            return "()";
        }
        StringBuilder builder = new StringBuilder();
        dfs(root, builder);
        return builder.toString();
    }

    /**
     * 调用时要满足root != null
     */
    private void dfs(TreeNode root, StringBuilder builder) {
        builder.append(root.val);
        // 左右都没有时，什么都不用操作
        if (root.left == null && root.right != null) {
            // 只有右
            builder.append("()(");
            dfs(root.right, builder);
            builder.append(")");
        } else if (root.left != null && root.right == null) {
            // 只有左
            builder.append("(");
            dfs(root.left, builder);
            builder.append(")");
        } else if (root.left != null && root.right != null) {
            // 左右都有
            builder.append("(");
            dfs(root.left, builder);
            builder.append(")");
            builder.append("(");
            dfs(root.right, builder);
            builder.append(")");
        }
    }
}
