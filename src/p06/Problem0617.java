package p06;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/merge-two-binary-trees/
 */
public class Problem0617 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 利用递归，合并 = 合并根节点 + 合并两个左子树 + 合并两个右子树
     */
    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        if (root1 == null && root2 == null) {
            return null;
        }
        if (root1 == null) {
            return root2;
        }
        if (root2 == null) {
            return root1;
        }
        TreeNode root = mergeNode(root1, root2);
        root.left = mergeTrees(root1.left, root2.left);
        root.right = mergeTrees(root1.right, root2.right);
        return root;
    }

    /**
     * 合并两个节点
     */
    private TreeNode mergeNode(TreeNode a, TreeNode b) {
        if (a == null && b == null) {
            return null;
        }
        if (a == null) {
            return new TreeNode(b.val);
        }
        if (b == null) {
            return new TreeNode(a.val);
        }
        return new TreeNode(a.val + b.val);
    }
}
