package p06;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/degree-of-an-array/
 */
public class Problem0697 {

    public static void main(String[] args) {
        Problem0697 p = new Problem0697();
//        System.err.println(p.findShortestSubArray(new int[]{1, 2, 2, 3, 1}) == 2);
//        System.err.println(p.findShortestSubArray(new int[]{1, 2, 2, 3, 1, 4, 2}) == 6);
        System.err.println(p.findShortestSubArray(new int[]{2, 1}) == 1);
    }

    /**
     * 思路：
     * 一个直观的想法是，先遍历一次原数组，用hash表统计次数，并记录最大次数
     * 然后遍历一次hash表求出次数最大的那个元素，也就是数组的度对应的元素
     * 知道度对应的元素后，再遍历一次原数组，就能得到一个最小长度（记录第一次出现的下标和最后一次出现的下标，下标之差加1，即是最小长度）
     * 如果有多个度对应的元素，就多次遍历原数组，分别得到一个最小长度
     * 所有长度中最小的那个就是最终结果
     * <br/>
     * 上面这个想法虽然简单直观，但是一眼就能看出优化方式
     * 那就是遍历次数太多了，每次遍历的逻辑都是独立的不互相影响的，因此可以合并为一次遍历
     * 需要遍历求得的数据有三种，分别是：次数，第一次出现的下标，最后一次出现的下标
     * 可以通过简单封装一个类来实现
     * <br/>
     * 设数组长度为N
     * 则hash表基础大小额外花费N的空间
     * 封装数据大小为最多花费kN的空间（k为变成类时体积变大的倍数，一种编程语言中是固定的，本题封装3个数据，可以简单理解为3），所以总的空间复杂度为O(N)
     * 整个流程需要遍历一次数组，然后遍历一次hash表，所以总的时间复杂度为O(N)
     */
    public int findShortestSubArray(int[] nums) {
        // 题目假设数组非空
        Map<Integer, CountAndIndex> map = new HashMap<>();
        int maxCount = 0;
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                CountAndIndex countAndIndex = map.get(nums[i]);
                countAndIndex.count++;
                countAndIndex.endIndex = i;
                maxCount = Math.max(countAndIndex.count, maxCount);
            } else {
                map.put(nums[i], new CountAndIndex(1, i, i));
                maxCount = Math.max(1, maxCount);
            }
        }
        int minLength = nums.length;
        for (Map.Entry<Integer, CountAndIndex> entry : map.entrySet()) {
            CountAndIndex countAndIndex = entry.getValue();
            if (maxCount == countAndIndex.count) {
                minLength = Math.min(minLength, countAndIndex.endIndex - countAndIndex.startIndex + 1);
            }
        }
        return minLength;
    }

    /**
     * 写一个类封装 次数，第一次出现的下标，最后一次出现的下标 这三个数据
     */
    private static class CountAndIndex {
        int count;
        int startIndex;
        int endIndex;

        CountAndIndex(int count, int startIndex, int endIndex) {
            this.count = count;
            this.startIndex = startIndex;
            this.endIndex = endIndex;
        }
    }
}
