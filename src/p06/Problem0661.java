package p06;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/image-smoother/
 */
public class Problem0661 {

    public static void main(String[] args) {
        Problem0661 p = new Problem0661();
        System.err.println(Arrays.toString(p.imageSmoother(new int[][]{{1, 1, 1}, {1, 0, 1}, {1, 1, 1}})));
        System.err.println(Arrays.toString(p.imageSmoother2(new int[][]{{100, 200, 100}, {200, 50, 200}, {100, 200, 100}})));
    }

    /**
     * 思路：
     * 求平均灰度的过程比较简单
     * 主要问题是不能修改img中的数据
     * 因此最直观的方式是新创建一个二维数组来保存平均灰度，这需要和img同等大小的额外空间
     * <br/>
     * 如果允许把img直接当作返回对象，那么就还有一种不创建新二维数组的解法
     * 题目规定了整数范围为0-255，对cv有了解的朋友应该知道，这就是八位灰阶图
     * 它一般只用8位，也就是一个字节来表示一个像素点
     * 本题的是元素数据是int，它有四个字节，剩下的3个字节都没用
     * 因此可以用剩下的3个字节存储平均灰度，这样就不需要额外空间了
     */
    public int[][] imageSmoother(int[][] img) {
        // 使用额外的二维数组的算法
        int r = img.length;
        int c = img[0].length;
        if (r == 1 && c == 1) {
            return img;
        }
        int[][] newImg = new int[r][c];
        for (int x = 0; x < r; x++) {
            for (int y = 0; y < c; y++) {
                newImg[x][y] = getGrayScale(img, x, y);
            }
        }
        return newImg;
    }

    /**
     * 计算一个点的平均灰度
     */
    private int getGrayScale(int[][] img, int x, int y) {
        int sum = 0;
        int count = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && i < img.length && j >= 0 && j < img[0].length) {
                    sum += img[i][j];
                    count++;
                }
            }
        }
        return sum / count;
    }

    public int[][] imageSmoother2(int[][] img) {
        // 直接修改原二维数组的解法
        int r = img.length;
        int c = img[0].length;
        if (r == 1 && c == 1) {
            return img;
        }
        for (int x = 0; x < r; x++) {
            for (int y = 0; y < c; y++) {
                // 使用第二个字节的8位来存储灰度
                img[x][y] += getGrayScaleLow8Bit(img, x, y) << 8;
            }
        }
        // 替换掉原来的img
        for (int x = 0; x < r; x++) {
            for (int y = 0; y < c; y++) {
                // 移位覆盖掉最低8位的数据
                img[x][y] >>= 8;
            }
        }
        return img;
    }

    /**
     * 计算一个点的平均灰度，只用最低8位的数据
     */
    private int getGrayScaleLow8Bit(int[][] img, int x, int y) {
        int sum = 0;
        int count = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && i < img.length && j >= 0 && j < img[0].length) {
                    // 最低8位是原来的灰度
                    sum += img[i][j] & 0xff;
                    count++;
                }
            }
        }
        return sum / count;
    }
}
