package p06;

/**
 * https://leetcode-cn.com/problems/count-binary-substrings/
 */
public class Problem0696 {

    public static void main(String[] args) {
        Problem0696 p = new Problem0696();
        System.err.println(p.countBinarySubstrings("00110011") == 6);
        System.err.println(p.countBinarySubstrings("10101") == 4);
    }

    /**
     * 思路：
     * 把字符串转换为0或者1连续出现的次数的数组
     * 比如把 00111000110000 转换为 [2, 3, 3, 2, 4]
     * 这个数组的由来为：一开始0连续出现了两次，后面是1连续出现了两次，以此类推得到这个数组
     * <br/>
     * 对于[2, 3, 3, 2, 4]，求次数就简单多了，从左边第二个开始到最后一个，每个次数和它前面的一个次数构成题目中的子串
     * 一开始是[2, 3]，它有两种，分别是11和22（1和2是出现的次数，无关这个是字符0还是字符1）
     * 然后是[3, 3]，它有三种 11 22 33
     * 等等情况
     * 可以归纳出：两个次数countA和countB之间的结果是min(countA, countB)
     * 把这个min(countA, countB)累加起来就可以得到最终结果
     * <br/>
     * 因为是从左至右求，每次求只需要当前的次数和后面紧跟的次数，所以不要一次性把次数全求出来，利用两个变量记录就可以
     * 这样就只需要一次循环了
     * 时间复杂度是O(N)，空间复杂度是O(1)
     */
    public int countBinarySubstrings(String s) {
        // 题目有假设，不做校验
        int result = 0;
        // countA是左边的那个次数，countB是右边的那个次数
        // 一开始是没有countA的，所以先初始化为0
        int countA = 0;
        int countB = 1;
        char lastChar = s.charAt(0);
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == lastChar) {
                countB++;
            } else if (countA == 0) {
                // 表示还没有得到最前面两个次数
                // 这个分支只会执行一次
                countA = countB;
                countB = 1;
            } else {
                result += Math.min(countA, countB);
                countA = countB;
                countB = 1;
            }
            lastChar = s.charAt(i);
        }
        // 循环跳出时，最后一次累加没做，所以循环外面要累加一次
        result += Math.min(countA, countB);
        return result;
    }
}
