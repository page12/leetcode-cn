package p06;

/**
 * https://leetcode-cn.com/problems/maximum-product-of-three-numbers/
 */
public class Problem0628 {

    public static void main(String[] args) {
        Problem0628 p = new Problem0628();
        System.err.println(p.maximumProduct(new int[]{1, 2, 3}) == 6);
        System.err.println(p.maximumProduct(new int[]{1, 2, 3, 4}) == 24);
        System.err.println(p.maximumProduct(new int[]{-1, -2, -3}) == -6);
    }

    /**
     * 思路：
     * 题目的示例3有表明负数的情况，所以本题主要考虑负数如何处理
     * a、当有负数又有正数时，最多只能选取两个负数
     * 此时这两个负数是绝对值最大的两个负数，也就是所有数中最小的两个数，剩下的正数是所有数中最大的那个数
     * 如果不选负数，则情况和只有正数时一样
     * b、当只有正数时，最大的三个数就是选取的三个数
     * c、当只有负数时，乘积一定为负数，所以要选取绝对值最小的三个负数，也就是所有数中最大的三个数
     * 综合上面的三种情况，只需要取最大的三个数，以及最小的两个数，就能都满足
     * 因为三个两个都是常数，所以可以一次性遍历求出
     */
    public int maximumProduct(int[] nums) {
        // 题目有假设，不做校验
        // 题目设定了范围，因此可以这样初始化
        int max1 = Integer.MIN_VALUE, max2 = Integer.MIN_VALUE, max3 = Integer.MIN_VALUE;
        int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE;
        for (int num : nums) {
            // 求最大的三个数
            if (num > max1) {
                max3 = max2;
                max2 = max1;
                max1 = num;
            } else if (num > max2) {
                max3 = max2;
                max2 = num;
            } else if (num > max3) {
                max3 = num;
            }
            // 求最小的两个数
            if (num < min1) {
                min2 = min1;
                min1 = num;
            } else if (num < min2) {
                min2 = num;
            }
        }
        return Math.max(max1 * max2 * max3, max1 * min1 * min2);
    }
}
