package p06;

/**
 * https://leetcode-cn.com/problems/can-place-flowers/
 */
public class Problem0605 {

    public static void main(String[] args) {
        Problem0605 p = new Problem0605();
        System.err.println(p.canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 1));
        System.err.println(p.canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 2));
        System.err.println(p.canPlaceFlowers(new int[]{1, 0, 0, 0, 0, 1}, 2));
        System.err.println(p.canPlaceFlowers(new int[]{1, 0, 0, 0}, 1));
        System.err.println(p.canPlaceFlowers(new int[]{1, 0, 0, 0, 0}, 2));
    }

    /**
     * 思路：简单的双指针
     * start指针从左边起找到第一个能种花的位置（当前是0 && 左边越界或者为0 && 右边越界或者为0）
     * end指针从start指针右边起找到第一个已经种花的位置（当前是1）
     * 如果end到了右边界，则 (end - start + 1) / 2  就是当前区间能种花的数量
     * 如果end没到右边界，则 (end - start) / 2  就是当前区间能种花的数量
     * start然后继续从end右边起，继续循环，直到end或者start越界
     * 统计下种花数量的总和，再判断下就得到结果
     * 总的时间复杂度O(N)，空间复杂度O(1)
     */
    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        // 题目有假设，不做校验
        int start = 0;
        int end = 0;
        int sum = 0;
        while (start < flowerbed.length) {
            if (flowerbed[start] == 0
                    && (start == 0 || flowerbed[start - 1] == 0)
                    && (start == flowerbed.length - 1 || flowerbed[start + 1] == 0)) {
                end = start + 1;
                while (end < flowerbed.length && flowerbed[end] != 1) {
                    end++;
                }
                if (end == flowerbed.length) {
                    sum += (end - start + 1) / 2;
                    break;
                } else {
                    sum += (end - start) / 2;
                    start = end + 1;
                }
            } else {
                start++;
            }
        }
        return sum >= n;
    }
}
