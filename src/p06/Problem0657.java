package p06;

/**
 * https://leetcode-cn.com/problems/robot-return-to-origin/
 */
public class Problem0657 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 用两个数记录坐标(x, y)
     * L = x - 1
     * R = x + 1
     * U = y + 1
     * D = y - 1
     * 最后判断(x，y) == (0, 0)
     * 要能回到远点必须移动偶数次，所以奇数长度的moves一定不能
     */
    public boolean judgeCircle(String moves) {
        if (moves == null || moves.length() == 0) {
            return true;
        }
        if ((moves.length() & 1) == 1) {
            return false;
        }
        int x = 0;
        int y = 0;
        for (int i = 0; i < moves.length(); i++) {
            char c = moves.charAt(i);
            switch (c) {
                case 'L':
                    x = x - 1;
                    break;
                case 'R':
                    x = x + 1;
                    break;
                case 'U':
                    y = y + 1;
                    break;
                case 'D':
                    y = y - 1;
                    break;
                default:
                    // do nothing
            }
        }
        return x == 0 && y == 0;
    }

}
