package p06;

import common.Employee;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/employee-importance/
 */
public class Problem0690 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 这个题实际上就是N叉数的节点求和，也就是N叉树的遍历
     * 因为都是用id关联的，所以先要用个hash表映射下
     */
    public int getImportance(List<Employee> employees, int id) {
        Map<Integer, Employee> employeeMap = new HashMap<>();
        Employee specifyEmployee = null;
        for (Employee employee : employees) {
            employeeMap.put(employee.id, employee);
            if (employee.id == id) {
                specifyEmployee = employee;
            }
        }
        return dfs(specifyEmployee, employeeMap);
    }

    /**
     * dfs递归求和
     */
    private int dfs(Employee employee, Map<Integer, Employee> employeeMap) {
        if (employee == null) {
            return 0;
        }
        int importance = employee.importance;
        if (employee.subordinates != null && employee.subordinates.size() != 0) {
            for (int subordinateId : employee.subordinates) {
                importance += dfs(employeeMap.get(subordinateId), employeeMap);
            }
        }
        return importance;
    }
}
