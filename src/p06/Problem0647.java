package p06;

/**
 * https://leetcode-cn.com/problems/palindromic-substrings/
 */
public class Problem0647 {

    public static void main(String[] args) {
        Problem0647 p = new Problem0647();
        System.err.println(p.countSubstrings("abc") == 3);
        System.err.println(p.countSubstrings("aaa") == 6);
        System.err.println(p.countSubstrings("xkjkqlajprjwefilxgpdpebieswu"));
    }

    /**
     * 思路：
     * 枚举所有可能的回文串中心，并尝试进行扩展
     * 扩展即是左右两边各添加一个相同的字符
     * 扩展一次，就新得到一个回文串
     * 回文串中心有可能是单个字符，也可能是两个连续字符（超过两个的都可以由单个或者两个扩展而来）
     * 当单个字符作为回文中心时，因为单个字符本身就是回文的，此时不需要额外判断
     * 当两个连续字符作为回文中心时，它们必须是一样的字符才是回文中心，否则不需要再继续扩展
     * 总的回文串数量，就是扩展次数，加上回文中心的数量
     */
    public int countSubstrings(String s) {
        int result = 0;
        int n = s.length();
        // 单个字符当作回文中心，有n种情况
        for (int i = 0; i <= n - 1; i++) {
            result++;
            int left = i - 1;
            int right = i + 1;
            // 内部循环进行扩展
            while (left >= 0 && right <= n - 1 && s.charAt(left) == s.charAt(right)) {
                left--;
                right++;
                result++;
            }
        }
        // 两个连续字符当作回文中心，有n-1种情况
        for (int i = 0; i <= n - 2; i++) {
            // 判断下两个连续字符是否回文，也就是是否相等
            if (s.charAt(i) != s.charAt(i + 1)) {
                continue;
            }
            result++;
            int left = i - 1;
            int right = i + 2;
            // 内部循环进行扩展
            while (left >= 0 && right <= n - 1 && s.charAt(left) == s.charAt(right)) {
                left--;
                right++;
                result++;
            }
        }
        return result;
    }
}
