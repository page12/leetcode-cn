package p06;

import java.util.Deque;
import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/valid-parenthesis-string/
 */
public class Problem0678 {

    public static void main(String[] args) {
        Problem0678 p = new Problem0678();
        System.err.println(p.checkValidString("()"));
        System.err.println(p.checkValidString("(*)"));
        System.err.println(p.checkValidString("(*))"));
        System.err.println(p.checkValidString("((((()(()()()*()(((((*)()*(**(())))))(())()())(((())())())))))))(((((())*)))()))(()((*()*(*)))(*)()"));
        System.err.println(p.checkValidString("(((((*)))**"));
    }

    /**
     * 思路：
     * 利用两个栈
     * 栈A用来匹配括号
     * 栈B用来记录*号下标
     * 遍历字符串
     * 如果当前是*号，则把下标入栈B，栈B从顶到底是递减的
     * 如果当前是右扩号，栈A不为空并且栈A顶部是左括号，则栈A出栈一次
     * 其余情况，则把下标入栈A
     * <br/>
     * 遍历完后，如果栈A为空，则代表匹配，返回true
     * 否则，查看栈A顶部的下标对应的字符
     * 如果是左括号，判断是否有 topA < topB，如果是，则AB都出栈一次，如果不是，则表示这个左括号右边不可能有右括号了，直接返回false
     * 如果是右括号，判断是否有 topA > topB，如果是，则AB都出栈一次，如果不是，只有B出栈一次
     * 栈A或者栈B为空，表示结束了*号的匹配
     * 最后栈A为空，则匹配，否则不匹配
     */
    public boolean checkValidString(String s) {
        Deque<Integer> stackA = new LinkedList<>();
        Deque<Integer> stackB = new LinkedList<>();
        // 先跳过*号去匹配
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '*') {
                stackB.push(i);
            } else if (c == ')' && !stackA.isEmpty() && s.charAt(stackA.peek()) == '(') {
                stackA.pop();
            } else {
                stackA.push(i);
            }
        }
        // 寻找满足条件的*号来匹配剩下的括号
        while (!stackB.isEmpty() && !stackA.isEmpty()) {
            int topA = stackA.peek();
            int topB = stackB.peek();
            if (s.charAt(topA) == '(') {
                if (topB > topA) {
                    stackA.pop();
                    stackB.pop();
                } else {
                    return false;
                }
            } else {
                if (topB < topA) {
                    stackA.pop();
                    stackB.pop();
                } else {
                    stackB.pop();
                }
            }
        }
        return stackA.isEmpty();
    }
}
