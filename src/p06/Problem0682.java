package p06;

import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/baseball-game/
 */
public class Problem0682 {

    public static void main(String[] args) {
        Problem0682 p = new Problem0682();
        System.err.println(p.calPoints(new String[]{"5", "2", "C", "D", "+"}) == 30);
        System.err.println(p.calPoints(new String[]{"5", "-2", "4", "C", "D", "9", "+", "+"}) == 27);
        System.err.println(p.calPoints(new String[]{"1"}) == 1);
    }

    /**
     * 思路：
     * 因为有C这种操作，所以很容易联想到栈
     * 剩下的就很简单了
     */
    public int calPoints(String[] ops) {
        // 题目有假设，不做校验
        LinkedList<Integer> stack = new LinkedList<>();
        for (String op : ops) {
            switch (op) {
                case "C":
                    stack.pop();
                    break;
                case "D":
                    stack.push(stack.peek() * 2);
                    break;
                case "+":
                    Integer top = stack.pop();
                    Integer secondTop = stack.peek();
                    stack.push(top);
                    stack.push(top + secondTop);
                    break;
                default:
                    // 都视为整数，题目有假设，这里不考虑异常情况
                    stack.push(Integer.valueOf(op));
            }
        }
        int sum = 0;
        for (Integer i : stack) {
            sum += i;
        }
        return sum;
    }
}
