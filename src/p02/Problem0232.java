package p02;

import java.util.Deque;
import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/implement-queue-using-stacks/
 */
public class Problem0232 {

    public static void main(String[] args) {

    }

    /**
     * 思路：某序列一次性连续入栈，然后一次性连续出栈，入的顺序和出的顺序会反过来
     * 对出栈的序列再次进行一次性连续入栈和一次性连续出栈，又会恢复成某序列初始的顺序，即最开始入的顺序和最终出的顺序一致，变成了队列的效果
     * 所以可以额外使用一个栈，出队列时把上一次连续入栈的元素一次性全部出栈，再重复压入这个额外的栈，额外的栈的出栈顺序就是入队 or 出队的顺序
     */
    private static class MyQueue {

        /**
         * 存储一次性连续入队元素的stack，入队元素先入这个栈（java中的栈一般都是使用Deque）
         */
        Deque<Integer> inStack;
        /**
         * 存储一次性连续出队元素的stack，要出队元素先入这个栈，再出队（java中的栈一般都是使用Deque）
         */
        Deque<Integer> outStack;

        /**
         * Initialize your data structure here.
         */
        public MyQueue() {
            inStack = new LinkedList<>();
            outStack = new LinkedList<>();
        }

        /**
         * Push element x to the back of queue.
         */
        public void push(int x) {
            inStack.push(x);
        }

        /**
         * Removes the element from in front of queue and returns that element.
         */
        public int pop() {
            // 拆箱可能会NPE，不管这个问题
            if (outStack.isEmpty()) {
                while (!inStack.isEmpty()) {
                    outStack.push(inStack.pop());
                }
            }
            return outStack.pop();
        }

        /**
         * Get the front element.
         */
        public int peek() {
            // peek个pop很像，因此仿照pop实现
            // 拆箱可能会NPE，不管这个问题
            if (outStack.isEmpty()) {
                while (!inStack.isEmpty()) {
                    outStack.push(inStack.pop());
                }
            }
            return outStack.peek();
        }

        /**
         * Returns whether the queue is empty.
         */
        public boolean empty() {
            return inStack.isEmpty() && outStack.isEmpty();
        }
    }
}
