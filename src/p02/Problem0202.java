package p02;

/**
 * https://leetcode-cn.com/problems/happy-number/
 */
public class Problem0202 {

    public static void main(String[] args) {
        Problem0202 p = new Problem0202();
        System.err.println(p.isHappy(2));
    }

    /**
     * 思路：如果一个数是快乐数，那么非常容易判定，循环直到等于1就是
     * 终点如果不是快乐数，该如何判定？
     * 题目中给了一个不是快乐数的例子，这个数是2
     * 对2进行快乐操作，结果依次是4 -> 16 -> 37 -> 58 -> 89 -> 145 -> 42 -> 20 -> 4 -> 16 -> 37 -> ...
     * 可以看到结果形成了4 -> 16 -> 37 -> 58 -> 89 -> 145 -> 42 -> 20 -> 4的无限循环，所以2不是快乐数
     * 那么对11进行快乐操作，因为 11 -> 2 -> 4，所以11也不是快乐数
     * 因此可以得出结论：不是快乐数 等价于 进行快乐操作时会得到无限循环的结果
     * 如何验证循环了呢？可以参考第141题环形链表的判断方法
     * 利用快慢双指针，快指针一次执行两次快乐操作，慢指针一次执行一次快乐操作
     * 如果快指针的结果等于慢指针的结果，代表发生了结果的无限循环，此时返回false，表示不是快乐数
     * 如果快指针的结果等于1，代表是快乐数，返回true
     */

    public boolean isHappy(int n) {
        // 特殊处理n == 1 or n == 10, 100, 1000等等的特殊情况
        if (n == 1 || happy(n) == 1) {
            return true;
        }
        int fast = n;
        int slow = n;
        while (fast != 1) {
            // 快指针一次执行两次快乐操作
            fast = happy(fast);
            fast = happy(fast);
            // 慢指针一次执行一次快乐操作
            slow = happy(slow);
            // 快指针的结果等于慢指针的结果，代表发生了结果的无限循环
            if (fast == slow) {
                return false;
            }
        }
        return true;
    }

    /**
     * 快乐操作：将该数替换为它每个位置上的数字的平方和
     */
    private int happy(int n) {
        int sum = 0;
        while (n != 0) {
            int digit = n % 10;
            sum += digit * digit;
            n = n / 10;
        }
        return sum;
    }
}
