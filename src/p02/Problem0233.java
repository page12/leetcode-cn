package p02;

/**
 * https://leetcode-cn.com/problems/number-of-digit-one/
 */
public class Problem0233 {

    public static void main(String[] args) {
        Problem0233 p = new Problem0233();
        System.err.println(p.countDigitOne(13) == 6);
        System.err.println(p.countDigitOne(99) == 20);
        System.err.println(p.countDigitOne(100) == 21);
        System.err.println(p.countDigitOne(0) == 0);
        System.err.println(p.countDigitOne(1) == 1);
        System.err.println(p.countDigitOne(1000) == 301);
    }

    /**
     * 思路：
     * 首先注意一点：题目中数字1的个数，如果有两个1是算两次的，比如11,，以此类推
     * 其次：求的是所有小于等于数字的1的个数的和，比如13，符合条件的有 1,10,11,12,13 这里面所有的1加起来有6个，所以结果是6
     * <br/>
     * 先观察数学规律
     * 对于0 -> 9,0 -> 99,0 -> 999这种，它们包含的数字1的个数是有规律的
     * 设位数为x，x位数最大值为 10^x - 1
     * 0 -> 10^x - 1 总共有 10^x 个数
     * 如果第i位是1，因为其余各位可以是0-9的任意数，它们总共有10^(i-1)个不同的数，所以第i位的1的个数为 10^(x-1)
     * i有n种不同的取值，所以所有的1的个数为 x * 10^(x-1)
     * 对于11这，它第1位是1，第二位也是1，但是每次计算个数时，只计算了其中一位的1的个数，因此上面的结论没有问题
     * 把这个公式记为 f(x) = x * 10^(x-1)
     * <br/>
     * 对于数字n，举个例子它是 68375，,是个5位数
     * 可以它划分成两个区间 10000 -> 68375 以及 0 -> 9999
     * 0 -> 9999 这个区间套用上面的公式可以直接算出，结果是 f(4)
     * 对于 10000 -> 68375 这个区间，可以进一步划分
     * 10000 -> 19999，其中1的个数为 10000 + f(4)
     * 20000 -> 29999，其中1的个数为 f(4)
     * 30000 -> 39999，其中1的个数为 f(4)
     * 40000 -> 49999，其中1的个数为 f(4)
     * 50000 -> 59999，其中1的个数为 f(4)
     * 60000 -> 63875，其中1的个数为 3875 中1的个数，也就是 countDigitOne(3875)
     * 对于 3875 中1的个数，可以递归求解，当然也可以用循环
     * <br/>
     * 特殊的情况，如果最右边的位是1，比如 13875
     * 则区间划分为 0 -> 9999 和 10000 -> 13875
     * 0 -> 9999 = f(4)
     * 10000 -> 13875 = 3875 + 1 + countDigitOne(3875)
     * <br/>
     * 根据上面这个例子的思路，就能得到最终算法
     * 如果从左往右算，就是递归
     * 如果从右往左算，就是动态规划
     * 在计算3875时，只需要先计算875就行，也就是只需要一个已经算好的结果，所以这个动态规划只需要一个变量存储结果
     * 下面的就是动态规划的代码
     */
    public int countDigitOne(int n) {
        int sum = 0;
        // 右边的数，比如68375它右边的数依次是 5, 75, 375 8375
        // 因为是从最右边开始处理
        // 所以它们的结果就是处理完它们之后的sum的值，只要不断执行 sum += xxxx 就相当于利用了已经算好的结果
        int right = 0;
        // x是位数
        int x = 1;
        while (n != 0) {
            int digit = n % 10;
            if (digit != 0) {
                int fxMinusOne = f(x - 1);
                if (digit == 1) {
                    sum += fxMinusOne + right + 1;
                } else {
                    sum += digit * fxMinusOne + Math.pow(10, x - 1);
                }
            }
            right += (int) (digit * Math.pow(10, x - 1));
            x++;
            n = n / 10;
        }
        return sum;
    }

    /**
     * 计算上面的公式f(x) = x * 10^(x-1)
     * 也就是0 -> (10^x - 1) 中1的个数
     */
    private int f(int x) {
        return (int) (x * Math.pow(10, x - 1));
    }
}
