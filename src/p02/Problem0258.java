package p02;

/**
 * https://leetcode-cn.com/problems/add-digits/
 */
public class Problem0258 {

    public static void main(String[] args) {

    }

    /**
     * 思路：不要循环和递归的O(1)解法，肯定就是总结数学规律，利用数学公式
     * 对于一位数或者两位数，可以很简单的总结出公式为 result = num % 9（num不是9的倍数） or result = 9（num是9的倍数）
     * 下面看一个例子：
     * 假设abcd为十进制数字，那么有abcd = 1000a + 100b + 10c + d 题目首先要计算 a + b + c + d
     * abcd = 1000a + 100b + 10c + d = 999a + 99b + 99c + a + b + c + d，其中a + b + c + d是一位数或者两位数
     * 那么abcd % 9 = (a + b + c + d) % 9，此时可以套用一位数或者两位数的公式
     * 对于更多位数，那么它进行按位相加时，一定能减少位数，最终变成一位数或者两位数，然后套用上面的公式
     */
    public int addDigits(int num) {
        if (num == 0) {
            return 0;
        }
        int result = num % 9;
        return result == 0 ? 9 : result;
        // 或者直接下面的写法
        // 下面的写法，验证起来很简单，但是在不知道的情况下正向推导总结起来不容易，还是上面的写法容易理解
        // return (num - 1) % 9 + 1
    }
}
