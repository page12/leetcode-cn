package p02;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * https://leetcode-cn.com/problems/isomorphic-strings/
 */
public class Problem0205 {

    public static void main(String[] args) {
        Problem0205 p = new Problem0205();
        System.err.println(p.isIsomorphic("egg", "add"));
        System.err.println(p.isIsomorphic("foo", "bar"));
        System.err.println(p.isIsomorphic("paper", "title"));
        System.err.println(p.isIsomorphic("badc", "bada"));
    }

    /**
     * 思路：利用hash表，比较简单
     * 注意点：映射关系是双向的且是一一对应的，不允许出现 a -> a && c -> a的情况
     */
    public boolean isIsomorphic(String s, String t) {
        // 不判断空字符串和不符合格式的字符串
        Map<Character, Character> mapS2T = new HashMap<>(26);
        Map<Character, Character> mapT2S = new HashMap<>(26);
        for (int i = 0; i <= s.length() - 1; i++) {
            char charS = s.charAt(i);
            char charT = t.charAt(i);
            if (!checkMapping(mapS2T, charS, charT) || !checkMapping(mapT2S, charT, charS)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 映射并校验
     */
    private boolean checkMapping(Map<Character, Character> mapS2T, char charS, char charT) {
        // key不存在时返回null，这里要使用包装类避免NPE
        Character charC = mapS2T.put(charS, charT);
        // 之前没映射过，或者映射的是同一个字符，则返回true
        return charC == null || Objects.equals(charT, charC);
    }
}
