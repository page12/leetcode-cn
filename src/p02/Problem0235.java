package p02;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-search-tree/
 */
public class Problem0235 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 最直观的方法就是求p和q的遍历路径
     * 因为是二叉搜索树，所以遍历起来速度很快，每一步都可以确定向左还是向右
     * 得到p和q的遍历路径后，对比一下就可以得到结果
     * 时间复杂度和空间复杂度都为树的高度（最好情况下为O(logn)，最坏情况下为O(n)）
     * <p>
     * 进一步优化：
     * 利用二叉搜索树的性质，可以确定p和q是在根节点的左边还是右边，假设p.val <= q.val
     * if (x > root) 则 x 在右边，反之在左边（题目假设没有相等的数值）
     * 如果p在root左边，q在root右边，则root就是最近的公共祖先（p <= root <= q）
     * 如果p和q都在左边，则对root.left递归执行此操作（p,q <= root）
     * 如果p和q都在左边，则对root.right递归执行此操作(p,q >= root）
     * 这种优化算法不需要存储全部的遍历路径，所以可以节省空间，时间复杂度依然是树的高度（最好情况下为O(logn)，最坏情况下为O(n)）
     * 下面的代码是优化算法的代码
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // 题目有假设，不做校验
        int leftVal = Math.min(p.val, q.val);
        int rightVal = Math.max(p.val, q.val);
        if (leftVal <= root.val && root.val <= rightVal) {
            return root;
        } else if (rightVal <= root.val) {
            return lowestCommonAncestor(root.left, p, q);
        } else {
            return lowestCommonAncestor(root.right, p, q);
        }
    }
}
