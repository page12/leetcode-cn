package p02;

/**
 * https://leetcode-cn.com/problems/missing-number/
 */
public class Problem0268 {

    public static void main(String[] args) {
        Problem0268 p = new Problem0268();
        System.err.println(p.missingNumber(new int[]{3, 0, 1}) == 2);
        System.err.println(p.missingNumber(new int[]{0, 1}) == 2);
        System.err.println(p.missingNumber(new int[]{9, 6, 4, 2, 3, 5, 7, 0, 1}) == 8);
        System.err.println(p.missingNumber(new int[]{0}) == 1);
    }

    /**
     * 思路：[0, n]这个整数区间内仅仅少了一个整数，设该区间为A
     * 那么[0, n]和A的差集就是少了的那个元素
     * 题目转化为了如何求差集
     * 求差集的操作就是互相抵消掉对应的元素，简单的遍历查找再抵消太慢了
     * 可以通过互为逆运算的两种运算来进行抵消，比如加法和减法，乘法和除法
     * 任意数字s先加一个元素，再减去同样的元素，结果仍然是s
     * 再根据加法的交换律，可以先对[0, n]执行所有的加法，再对区间A执行所有的减法
     * 如果两个数组相同，则结果是0，否则结果就是那个唯一的差值
     * 这种算法的时间复杂度都是O(N)，空间复杂度都是O(1)
     * <br/>
     * 实际上使用任何可以互逆，并且满足交换律的运算都可以（加法和减法是最简单的，乘法和除法要考虑溢出的时）
     * 一个满足条件的特殊的运算就是异或运算，它和它自己互为逆运算，即满足
     * 如果a ^ b = c，那么c ^ b = a, c ^ a = b
     * 并且异或运算满足交换律
     * <br/>
     * 上面的方法可以推广到任意数组a和b，b仅仅比a少一个元素（多一个元素的情况也等价），其余的元素都一样的情况，基本思路都一样
     * 首先令s = 0，先对a累计进行某种操作，得到结果s
     * 在对b中累计进行对应的逆操作，最后的结果s就是差值的那一个元素
     * 时间复杂度都是O(N)，空间复杂度都是O(1)
     * <br/>
     * 还有其他直观但相对不实用的方式，排序然后遍历，碰见跳跃点就找到了结果，代码就不贴了
     */
    public int missingNumber(int[] nums) {
        // 题目有假设，不做校验
        int n = nums.length;
        // 计算n*(n+1)/2，如果考虑乘法溢出，需要使用long
        // 题目有假设，这里就不考虑了
        int sum = n * (n + 1) / 2;
        // 遍历数组进行逆操作，也就是累减操作
        for (int i = 0; i < n; i++) {
            sum -= nums[i];
        }
        return sum;
    }
}
