package p02;

/**
 * https://leetcode-cn.com/problems/maximal-square/
 */
public class Problem0221 {

    public static void main(String[] args) {
        Problem0221 p = new Problem0221();
        System.err.println(p.maximalSquare(new char[][]{{'1', '0', '1', '0', '0'},
                {'1', '0', '1', '1', '1'},
                {'1', '1', '1', '1', '1'},
                {'1', '0', '0', '1', '0'}}));
    }

    /**
     * 思路：
     * 用两个二维数组分别记录一个点左边的连续1的个数，和上面的连续1的个数（都是以该点为起点计算连续）设这两个数组为x[][] y[][]
     * 设点的左边为(i,j)，用 f(i,j) 表示以 (i,j) 为右下顶点的最大正方形的边长，则有两种情况
     * 1、如果 (i,j) 不是某个正方形的右下顶点，则 f(i,j) = 0
     * 2、如果 (i,j) 是某个正方形的右下顶点，且边长大于1，则 (i-1,j-1) 也是某个正方形的右下顶点
     * 反过来说，如果 (i-1,j-1) 是某个正方形的右下顶点
     * 则只要x[i][j] > 0或者y[i][j] > 0， (i,j) 就是某一个正方形的右下顶点
     * 此时它的最大边长 f(i,j) = min{ f(i-1,j-1) + 1, x[i][j], y[i][j] }
     * 很明显，当 f(i-1,j-1) = 0时，也能套用上面的公式
     * <br/>
     * 这样再遍历一次二维数组，就能得到所有的 f(i,j)
     * 再额外用一个变量记录所有点的 f(i,j) 中的最大值，这个最大值就是最终结果
     */
    public int maximalSquare(char[][] matrix) {
        int n = matrix.length;
        int m = matrix[0].length;
        // 求x
        int[][] x = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (j == 0) {
                    x[i][j] = matrix[i][j] == '1' ? 1 : 0;
                } else {
                    x[i][j] = matrix[i][j] == '1' ? x[i][j - 1] + 1 : 0;
                }
            }
        }
        // 求y
        int[][] y = new int[n][m];
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                if (i == 0) {
                    y[i][j] = matrix[i][j] == '1' ? 1 : 0;
                } else {
                    y[i][j] = matrix[i][j] == '1' ? y[i - 1][j] + 1 : 0;
                }
            }
        }
        // 最大边长
        int max = 0;
        // 求dp中间结果，这一步可以使用滚动数组优化
        int[][] dp = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = x[i][j] >= 1 ? 1 : 0;
                } else {
                    dp[i][j] = Math.min(dp[i - 1][j - 1] + 1, Math.min(x[i][j], y[i][j]));
                }
                max = Math.max(dp[i][j], max);
            }
        }
        return max * max;
    }
}
