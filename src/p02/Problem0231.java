package p02;

/**
 * https://leetcode-cn.com/problems/power-of-two/
 */
public class Problem0231 {

    public static void main(String[] args) {

    }

    /**
     * 思路：2的幂的二进制表示中只有一个1，利用这一点判断二进制中1的个数
     * 注意：因为2的幂都是正数，所以负数（-2^31）需要排除
     * 第191题是求二进制中1的个数，这题跟那边一样的算法
     */
    public boolean isPowerOfTwo(int n) {
        return n > 0 && Integer.bitCount(n) == 1;
    }
}
