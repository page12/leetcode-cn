package p02;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/contains-duplicate/
 */
public class Problem0217 {

    public static void main(String[] args) {
        Problem0217 p = new Problem0217();
        System.err.println(p.containsDuplicate(new int[]{1, 2, 3, 1}));
        System.err.println(p.containsDuplicate(new int[]{1, 2, 3, 4}));
        System.err.println(p.containsDuplicate(new int[]{1, 1, 1, 3, 3, 4, 3, 2, 4, 2}));
    }

    /**
     * 思路：使用hash表是最直接的思路，时间复杂度为O(N)，空间为O(N)
     * 其他方式：排序然后遍历查找，时间空间复杂度就是对应的排序算法的复杂度
     */
    public boolean containsDuplicate(int[] nums) {
        if (nums == null || nums.length <= 1) {
            return false;
        }
        Set<Integer> set = new HashSet<>(nums.length);
        int i = 0;
        while (i < nums.length && set.add(nums[i])) {
            i++;
        }
        return i != nums.length;
    }
}
