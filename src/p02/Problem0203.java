package p02;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/remove-linked-list-elements/
 */
public class Problem0203 {

    public static void main(String[] args) {

    }

    /**
     * 思路：基本的链表删除操作
     * 因为本题的链表没有虚拟节点当做head，所以删除的节点就是head时需要特殊判断下，并重新给head赋值，令head = head.next，这个逻辑写起来还有点麻烦
     * 对于单链表来说，添加一个虚拟节点当做head可以让各种操作更加一致化，少写很多if-else
     * 这里选择添加一个虚拟节点当做head，让代码更简单
     */
    public ListNode removeElements(ListNode head, int val) {
        ListNode dummyNode = new ListNode(0, head);
        ListNode p = dummyNode;
        while (p.next != null) {
            if (p.next.val == val) {
                p.next = p.next.next;
            } else {
                p = p.next;
            }
        }
        return dummyNode.next;
    }
}
