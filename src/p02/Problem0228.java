package p02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/summary-ranges/
 */
public class Problem0228 {

    public static void main(String[] args) {
        Problem0228 p = new Problem0228();
        System.err.println(p.summaryRanges(new int[]{0, 1, 2, 4, 5, 7}));
        System.err.println(p.summaryRanges(new int[]{0, 2, 3, 4, 6, 8, 9}));
        System.err.println(p.summaryRanges(new int[]{}));
        System.err.println(p.summaryRanges(new int[]{-1}));
        System.err.println(p.summaryRanges(new int[]{0}));
    }

    /**
     * 思路：很简单，直接用两个数标记区间的下限和上限就行
     * 相邻两个元素之间的差值等于1就还可以扩大区间
     * 否则就区间中断，需要重置区间上下限
     */
    public List<String> summaryRanges(int[] nums) {
        if (nums == null || nums.length == 0) {
            return Collections.emptyList();
        }
        List<String> result = new ArrayList<>();
        int left = 0;
        int right = 0;
        for (int i = 1; i < nums.length; i++) {
            // 无重复元素，不需要考虑差值为0的情况
            if (nums[i] - nums[i - 1] == 1) {
                right++;
            } else {
                if (left == right) {
                    result.add(String.valueOf(nums[left]));
                } else {
                    result.add(nums[left] + "->" + nums[right]);
                }
                right = i;
                left = i;
            }
        }
        // 添加最后一个区间
        if (left == right) {
            result.add(String.valueOf(nums[left]));
        } else {
            result.add(nums[left] + "->" + nums[right]);
        }
        return result;
    }
}
