package p02;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * https://leetcode-cn.com/problems/word-pattern/
 */
public class Problem0290 {

    public static void main(String[] args) {
        Problem0290 p = new Problem0290();
        System.err.println(p.wordPattern("abba", "dog cat cat dog"));
        System.err.println(p.wordPattern("abba", "dog cat cat fish"));
        System.err.println(p.wordPattern("aaaa", "dog cat cat dog"));
        System.err.println(p.wordPattern("abba", "dog dog dog dog"));
        System.err.println(p.wordPattern("aaaa", "dog dog dog dog"));
    }

    /**
     * 思路：用hash表存储映射关系，映射关系是双向一对一的，并且是有顺序的，不允许出现多个映射成一个的情况，不允许错位映射
     * 假设N为pattern的长度，M为s的长度
     * 分割s需要O(N)时间，需要O(N)的额外空间存储分割后的数组
     * 两个hash表都需要O(M)的空间
     * 遍历映射需要O(M)的时间
     * 所以总的时间复杂度为O(N+M)，总的空间复杂度为O(N+M)
     */
    public boolean wordPattern(String pattern, String s) {
        // 题目有假设，不做校验
        // 可以手动遍历一次去分割，这里直接简单用split
        String[] array = s.trim().split(" ");
        if (pattern.length() != array.length) {
            return false;
        }
        Map<Character, String> pattern2ArrayMap = new HashMap<>();
        Map<String, Character> array2PatternMap = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            String str = array[i];
            char c = pattern.charAt(i);
            // 按顺序对比映射关系
            String strInMap = pattern2ArrayMap.get(c);
            Character cInMap = array2PatternMap.get(str);
            if (strInMap == null && cInMap == null) {
                pattern2ArrayMap.put(c, str);
                array2PatternMap.put(str, c);
            } else if (!Objects.equals(str, strInMap) || !Objects.equals(c, cInMap)) {
                return false;
            }
        }
        return true;
    }
}
