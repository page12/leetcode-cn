package p02;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/move-zeroes/
 */
public class Problem0283 {

    public static void main(String[] args) {
        Problem0283 p = new Problem0283();
        int[] nums = new int[]{0, 1, 0, 3, 12};
        p.moveZeroes(nums);
        System.err.println(Arrays.toString(nums));
    }

    /**
     * 思路：双指针，一个指针zero标记最左边的0，一个指针nonZero标记zero右边第一个非0的数
     * 初始化zero = 0，nonZero = 1
     * 从左往右遍历
     * 先找到zero，然后从nonZero（一定要满足nonZero > zero）的位置开始找第一个非0的数
     * 找到后交换数字，然后zero = zero + 1，nonZero = nonZero + 1继续循环
     * zero或者nonZero到数组最右边就是遍历完成
     * 时间复杂度O(N)，空间复杂度O(1)
     */
    public void moveZeroes(int[] nums) {
        if (nums == null || nums.length <= 1) {
            return;
        }
        int zero = 0;
        int nonZero = 0;
        while (zero < nums.length) {
            if (nums[zero] == 0) {
                // 找到zero右边第一个非0的数
                if (nonZero <= zero) {
                    nonZero = zero + 1;
                }
                while (nonZero < nums.length && nums[nonZero] == 0) {
                    nonZero++;
                }
                // 越界，直接结束整个循环
                if (nonZero >= nums.length) {
                    break;
                }
                // 交换
                nums[zero] = nums[nonZero];
                nums[nonZero] = 0;
                nonZero = nonZero + 1;
            }
            zero++;
        }
    }
}
