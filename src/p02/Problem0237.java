package p02;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/delete-node-in-a-linked-list/
 */
public class Problem0237 {

    public static void main(String[] args) {

    }

    /**
     * 思路：删除一个链表节点（非头节点），必须要知道它的前驱节点
     * 所以一个方法就是遍历链表寻找到node的前驱节点，但是因为没有头节点，此方法行不通
     * 反过来思考，知道node，只执行一次删除操作，则我们只能删除它的后继节点（题目有假设一定有后继节点）
     * 此时如果保存它的后继节点的全部属性，并赋值给node，则相当于是直接删除了node，此时不需要遍历，时间复杂度为O(1)
     * 下面的代码就是不需要遍历的O(1)的代码
     */
    public void deleteNode(ListNode node) {
        // 题目有假设，不做校验
        ListNode next = node.next;
        // 删除后继节点
        node.next = node.next.next;
        // 复制后继节点的值
        node.val = next.val;
    }
}
