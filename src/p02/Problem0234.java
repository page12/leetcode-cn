package p02;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/palindrome-linked-list/
 */
public class Problem0234 {

    public static void main(String[] args) {

    }

    /**
     * 思路：根据回文的性质，前一半的反转和后一半是一样的
     * 利用这个性质，可以得到O(n)时间复杂度和O(1)空间复杂度的解法
     * 题目所给的链表是单向的，所以要找到前一半需要遍历一次链表得到长度
     * 不过这个方法会更改链表，如果有需要的话，可以再反转一次前一半把链表还原
     * 下面的是不还原链表的代码
     * 反转链表是第206题，可以参考
     */
    public boolean isPalindrome(ListNode head) {
        if (head == null) {
            return false;
        }
        if (head.next == null) {
            return true;
        }
        // 求链表长度
        ListNode p = head;
        int length = 0;
        while (p != null) {
            length++;
            p = p.next;
        }
        // 反转链表的前一半
        ListNode leftReverse = head;
        p = leftReverse.next;
        leftReverse.next = null;
        // 前一半的反转初始化的长度为1，所以从2开始
        for (int i = 2; i <= length / 2; i++) {
            ListNode node = p;
            p = p.next;
            // addFirst
            node.next = leftReverse;
            leftReverse = node;
        }
        ListNode pLeftReverse = leftReverse;
        // 反转完前一半后p是后一半的头结点
        // 判断下length是奇数还是偶数
        ListNode pRight = (length & 1) == 1 ? p.next : p;
        // 对比前一半的反转和后一半是否一致
        while (pLeftReverse != null) {
            if (pLeftReverse.val != pRight.val) {
                return false;
            }
            pLeftReverse = pLeftReverse.next;
            pRight = pRight.next;
        }
        return true;
    }
}
