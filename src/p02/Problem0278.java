package p02;

/**
 * https://leetcode-cn.com/problems/first-bad-version/
 */
public class Problem0278 {

    public static void main(String[] args) {

    }

    /**
     * 思路：一个版本是错误的，则后面所有的版本都是错误的
     * 把正确版本视为0=false，错误的版本视为1=true，则整个数组都是单调递增的
     * 因此可以使用二分搜索
     * 结果点满足它的前一个是正确的（如果有），它本身是是错误的，它后面是错误的（如果有），用false true true表示
     * 如果true true true，则往左查找
     * 如果false false false，则往右查找
     * 对于false false true这种情况，可以视为提前找到
     * 因为是单调递增的，所以只有这四种情况
     */
    public int firstBadVersion(int n) {
        // 题目有假设，不做校验
        int start = 1;
        int end = n;
        int mid = 0;
        while (start <= end) {
            mid = (end - start) / 2 + start;
            // 利用单调性可以提前判断current和next
            // 前一个版本，如果没有就是视为false，正确的版本
            boolean prev = mid != 1 && isBadVersion(mid - 1);
            // 当前版本
            boolean current = prev || isBadVersion(mid);
            // 后一个版本，如果没有就是视为true，错误的版本题目
            // 假设一定有一个错误版本，所以可以这样假设
            boolean next = prev || mid == n || isBadVersion(mid + 1);

            // 所有的四种情况的判断
            if (!prev && current && next) {
                // false true true的情况
                return mid;
            } else if (prev && current && next) {
                // true true true的情况
                end = mid - 1;
            } else if (!prev && !current && !next) {
                // false false false的情况
                start = mid + 1;
            } else {
                // false false true的情况，提前找到
                return mid + 1;
            }
        }
        // 题目假设一定有一个错误的版本
        return mid;
    }

    /**
     * 这个方法是题目内置的，不需要编写
     * 这里写出来只是为了编译通过
     */
    private boolean isBadVersion(int version) {
        return false;
    }
}
