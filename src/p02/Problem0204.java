package p02;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/count-primes/
 */
public class Problem0204 {

    public static void main(String[] args) {
        Problem0204 p = new Problem0204();
        System.err.println(p.countPrimes(13));
    }

    /**
     * 思路：
     * 先了解判断一个数是不是质数的简单直观的方法——试除法（质数是数论的核心，高端判断方法一堆）
     * 用[2, n-1]的数去除n，能被整除就不是质数，所有都不能被整除则是质数
     * 上面的方法范围太大了，可以优化为 [2, sqrt(n)向下取整]
     * 这个很好证明，大于1的自然数不是质数就是合数，假设n是合数，那么一定有这种表示 n = a * b
     * 如果 a in [2, sqrt(n)向下取整]，根据正数乘法的性质一定有 b in [sqrt(n)向下取整, n - 1]，a的范围和b的范围可以双向互推
     * 也就是如果一个数能被 a in [2, sqrt(n)向下取整] 整除，那么它也一定能被 b in [sqrt(n)向下取整, n - 1]，反之亦然
     * <p>
     * 根据上面的说明，一个简单的方法就是遍历[2,n]，用上面的方法逐个判断是否是质数
     * 判断一个数是不是质数的时间复杂度为 O(sqrt(n))，因此总的时间复杂度为O(n * sqrt(n))
     * 只需要常数个变量维护循环状态，因此空间复杂度为O(1)
     * <p>
     * 这个简单的算法太慢了，分析下它的过程可以知道，中间有太多元素实际上没必要用 sqrt(n) 的时间去判断
     * 因为一个数的大于等于2倍的倍数肯定不是质数，也就是所有的偶数都不是质数，所有3的倍数都不是质数，所有5的倍数都不是质数等等
     * 在得到一个数是质数后，它的任意倍数都可以直接标记为不是质数，提前记录下来后续就不需要判断了
     * 这种利用倍数直接筛选的方法就是埃氏筛选法，它需要额外的O(n)的空间用于提前记录不是质数的数
     * 时间复杂度为O(nloglogn)（网上抄的，证明不会）
     * 埃氏筛很好进一步优化，因为偶数除了2都不是质数，所以只需要判断[2, sqrt(n)向下取整]内的奇数就可以了
     * 下面的代码是只针对奇数的埃氏筛
     */
    public int countPrimes(int n) {
        // 写下面的循环时都是当小于等于n理解的，提交发现这里对不上，直接偷懒这里使用n = n - 1，不用改下面的几个地方
        n = n - 1;
        if (n <= 1) {
            return 0;
        }
        // 只用判断奇数
        int size = (n - 1) / 2;
        // 初始化用于提前记录是不是质数的数组
        boolean flagArray[] = new boolean[size];
        Arrays.fill(flagArray, true);
        int sqrt = (int) Math.sqrt(n);
        // 只判断奇数时，跳过了第一个质数2，所以初始化为1
        int result = 1;
        for (int i = 3; i <= sqrt; i += 2) {
            if (flagArray[(i / 2) - 1]) {
                // 从i*i开始提前判断标记，在(i, i*i)直接的i的倍数，一定会被比i小的质数提前标记
                // 因为只判断奇数，所以每次循环令j += i * 2
                for (int j = i * i; j <= n; j += i * 2) {
                    flagArray[(j / 2) - 1] = false;
                }
            }
        }
        for (boolean flag : flagArray) {
            if (flag) {
                result = result + 1;
            }
        }
        return result;
    }
}
