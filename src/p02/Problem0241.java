package p02;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/different-ways-to-add-parentheses/
 */
public class Problem0241 {

    public static void main(String[] args) {
        Problem0241 p = new Problem0241();
        System.err.println(p.diffWaysToCompute("2-1-1"));
        System.err.println(p.diffWaysToCompute("2*3-4*5"));
    }

    /**
     * 思路：
     * 先把字符串拆分成数字列表和符号列表，数字列表比符号列表长度大1
     * 添加括号就是对列表进行分割
     * 利用分治法，递归求解所有可能的分割情况
     * <br/>
     * 每一次分割，就相当于把分割后的左右部分分别加上括号（只有一个数字时，括号加不加无所谓）
     * 比如 abcd 可以先分割为 (a)(bcd)，再对bcd进行分割
     * 看题目的示例，不允许出现一层括号包裹三个数，比如 a+(b-c-d) 这种情况，因此最后的分割结果是一个或者两个数字
     * 当前长度范围 n <= 2 时，不允许再分割了，也不用往下递归，这样所有的分割情况都是递归路径的叶子节点
     * 当前长度范围 n > 2 时，可以进行分割，分割为两段时，有 n - 1 种分割方法，从左至右循环就可以得到所有分割情况
     * 最后把分割后左右两边的结果用双重循环组合一下就行
     */
    public List<Integer> diffWaysToCompute(String expression) {
        List<Integer> numbers = new ArrayList<>();
        List<Character> ops = new ArrayList<>();
        split(expression, numbers, ops);
        return recurse(numbers, ops, 0, numbers.size() - 1);
    }

    /**
     * 递归求解所有可能情况
     *
     * @param numbers 数字列表
     * @param ops     符号列表
     * @param start   列表的可用范围，闭区间，因为没有真正物理拆分，用下标记录逻辑拆分
     */
    private List<Integer> recurse(List<Integer> numbers, List<Character> ops, int start, int end) {
        List<Integer> result = new ArrayList<>();
        // 拆分后只剩下一个数
        if (start == end) {
            result.add(numbers.get(end));
            return result;
        }
        // 拆分后剩下两个数
        if (start + 1 == end) {
            result.add(calculate(numbers.get(start), numbers.get(end), ops.get(start)));
            return result;
        }
        // 从左至右循环分割
        for (int i = start; i <= end - 1; i++) {
            List<Integer> leftResult = recurse(numbers, ops, start, i);
            List<Integer> rightResult = recurse(numbers, ops, i + 1, end);
            char op = ops.get(i);
            // 组装最后计算结果
            for (int left : leftResult) {
                for (int right : rightResult) {
                    result.add(calculate(left, right, op));
                }
            }
        }
        return result;
    }

    /**
     * 拆分成数字列表和符号列表
     */
    private void split(String s, List<Integer> numbers, List<Character> ops) {
        int startOfNumber = -1;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= '0' && c <= '9' && startOfNumber == -1) {
                startOfNumber = i;
            } else if (c == '+' || c == '-' || c == '*') {
                ops.add(c);
                numbers.add(Integer.valueOf(s.substring(startOfNumber, i)));
                startOfNumber = -1;
            }
        }
        numbers.add(Integer.valueOf(s.substring(startOfNumber)));
    }

    /**
     * 两个操作数进行运算
     */
    private int calculate(int left, int right, char op) {
        switch (op) {
            case '+':
                return left + right;
            case '-':
                return left - right;
            case '*':
                return left * right;
            default:
                throw new IllegalArgumentException("wrong op: " + op);
        }
    }
}
