package p02;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/valid-anagram/
 */
public class Problem0242 {

    public static void main(String[] args) {
        Problem0242 p = new Problem0242();
        System.err.println(p.isAnagram("anagram", "nagaram"));
        System.err.println(p.isAnagram("rat", "car"));
    }

    /**
     * 思路：两个字符串互为异构字符串，就是它们的组成字符串的所有字符的个数都对应相等
     * 因此最直接的思路就是使用hash表记录字符串中每个字符的数量然后比较
     * 此方法需要O(M)的空间，其中M为字符集的大小，时间复杂度为O(N)
     * <p>
     * 另一种方法是排序
     * 排序完后是相同字符串则是异构字符串，时间复杂度和空间复杂度为对应的排序算法的复杂度
     * 注意：很多语言中字符串都是不变的数据，对字符串中字符进行排序时需要先复制一份出来，这需要额外花费O(N)的时间和空间
     * <p>
     * 具体使用那种算法，可以根据实际情况考虑
     * 下面的代码是使用hash表的代码
     */
    public boolean isAnagram(String s, String t) {
        // 只用一个map，遍历s时计数加1，遍历t时计数减1，最后统计hash表的计数是否全为0
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i <= s.length() - 1; i++) {
            map.merge(s.charAt(i), 1, (oldValue, newValue) -> oldValue + 1);
        }
        for (int i = 0; i <= t.length() - 1; i++) {
            map.merge(t.charAt(i), -1, (oldValue, newValue) -> oldValue - 1);
        }
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() != 0) {
                return false;
            }
        }
        return true;
    }
}
