package p02;

/**
 * https://leetcode-cn.com/problems/house-robber-ii/
 */
public class Problem0213 {

    public static void main(String[] args) {
        Problem0213 p = new Problem0213();
        System.err.println(p.rob(new int[]{2, 3, 2}) == 3);
        System.err.println(p.rob(new int[]{1, 2, 3, 1}) == 4);
        System.err.println(p.rob(new int[]{0}) == 0);
    }

    /**
     * 思路：
     * 和第198题基本一模一样
     * 这题有个限制
     * 如果出发点是0，则数组的右边界是n-2
     * 如果出发点是1，则数组的右边界是n-1
     * 把198题的逻辑稍微改下，用这两个边界当作参数，分别执行一次就行
     * 其中较大的结果就是最终结果
     */
    public int rob(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }
        return Math.max(rob(nums, 0, nums.length - 2), rob(nums, 1, nums.length - 1));
    }

    public int rob(int[] nums, int start, int end) {
        if (nums.length - 1 == 1) {
            return nums[start];
        }
        if (nums.length - 1 == 2) {
            return Math.max(nums[start], nums[start + 1]);
        }
        if (nums.length - 1 == 3) {
            return Math.max(nums[start] + nums[start + 2], nums[start + 1]);
        }
        int minusThree = nums[start];
        int minusTwo = nums[start + 1];
        int minusOne = nums[start] + nums[start + 2];
        for (int i = start + 3; i <= end; i++) {
            int current = Math.max(minusThree, minusTwo) + nums[i];
            minusThree = minusTwo;
            minusTwo = minusOne;
            minusOne = current;
        }
        return Math.max(minusTwo, minusOne);
    }
}
