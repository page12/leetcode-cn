package p02;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * https://leetcode-cn.com/problems/binary-tree-paths/
 */
public class Problem0257 {

    public static void main(String[] args) {

    }

    /**
     * 思路：DFS递归allPaths = (root + leftPath) 并集 (left + leftPath)
     */
    public List<String> binaryTreePaths(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }
        List<StringBuilder> allPaths = new ArrayList<>();
        addAllPath(root, allPaths, new StringBuilder());
        return allPaths.stream().map(StringBuilder::toString).collect(Collectors.toList());
    }

    /**
     * DFS递归寻找所有path并添加
     */
    private void addAllPath(TreeNode root, List<StringBuilder> allPaths, StringBuilder prefix) {
        // 提前判断，可以减少减少一次递归，也就减少一次字符串复制
        if (root.left == null && root.right == null) {
            prefix.append(root.val);
            allPaths.add(prefix);
        } else {
            prefix.append(root.val).append("->");
            if (root.left != null) {
                // prefix是引用，这里要new一个
                addAllPath(root.left, allPaths, new StringBuilder(prefix));
            }
            if (root.right != null) {
                // prefix是引用，这里要new一个
                addAllPath(root.right, allPaths, new StringBuilder(prefix));
            }
        }
    }
}
