package p02;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/contains-duplicate-ii/
 */
public class Problem0219 {

    public static void main(String[] args) {
        Problem0219 p = new Problem0219();
        System.err.println(p.containsNearbyDuplicate(new int[]{1, 2, 3, 1}, 3));
        System.err.println(p.containsNearbyDuplicate(new int[]{1, 0, 1, 1}, 1));
        System.err.println(p.containsNearbyDuplicate(new int[]{1, 2, 3, 1, 2, 3}, 2));
    }

    /**
     * 因为需要判断相等的两个数的下标差值是否小于等于k，所以不能修改原数组，也就不能使用排序
     * 最直观的方法就是使用hash表，维护一个min(n,k)大小的hash表，其中的元素为范围[i+1,i+k]内的元素
     * 遍历数组并不断维护更新这个hash表即可，不在范围[i+1,i+k]内的元素要删除
     * 时间复杂度为O(n)，空间为O(min(n,k))
     */
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        if (nums == null || nums.length <= 1 || k < 1) {
            return false;
        }
        Set<Integer> set = new HashSet<>(Math.min(nums.length, k));
        for (int i = 0; i < nums.length; i++) {
            if (set.contains(nums[i])) {
                return true;
            }
            if (set.size() >= k) {
                set.remove(nums[i - k]);
            }
            if (!set.add(nums[i])) {
                return true;
            }
        }
        return false;
    }
}
