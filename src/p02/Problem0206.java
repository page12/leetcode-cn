package p02;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/reverse-linked-list/
 */
public class Problem0206 {

    public static void main(String[] args) {

    }

    /**
     * 思路：比较简单
     * 反转链表的操作等价于不断的使原链表的后一个节点添加为新链表的头节点
     * 这样就转换成遍历链表去执行addFirst操作
     * 其他方式：用递归，或者显式用栈去实现，不过链表的递归深度就是链表长度，因此不怎么推荐
     */
    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        // 初始化新链表，头节点是原来链表的头节点
        ListNode p = head.next;
        head.next = null;
        // 循环执行addFirst操作
        while (p != null) {
            ListNode node = p;
            p = p.next;
            // addFirst
            node.next = head;
            head = node;
        }
        return head;
    }
}
