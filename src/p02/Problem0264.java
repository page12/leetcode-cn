package p02;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/ugly-number-ii/
 */
public class Problem0264 {

    public static void main(String[] args) {
        Problem0264 p = new Problem0264();
        System.err.println(p.nthUglyNumber(10) == 12);
        System.err.println(p.nthUglyNumber(1) == 1);
    }

    /**
     * 思路：
     * 设n是丑数，则 n*2 n*3 n*5 也是丑数
     * 并且因为丑数只能包含235这三个因数
     * 所以从第一个丑数1开始，通过不断对前面的丑数乘235就能得到后面所有的丑数
     * 示例为：
     * 一开始只有1，对它乘235得到 1,2,3,5
     * 第2个是2，对它乘235得到 1,2,3,5,4,6,10
     * 第3个是3，对它乘235得到 1,2,3,5,4,6,10,9,15（数字6重复出现了）
     * 第4个是5，对它乘235得到 1,2,3,5,4,6,10,9,15,25（数字10和数字15重复出现了）
     * 第5个是4，对它乘235得到 1,2,3,5,4,6,10,9,15,25,8,20（数字12重复出现了）
     * 第6个是6...
     * <br/>
     * 上面的过程能生成所有丑数，但是已经生成的丑数不是排序的
     * 如果在生成过程中添加排序操作，就能得到有序生成的丑数
     * 这个排序要保证第i个去执行乘235操作的数是第i小的，每次都只需要知道最小元素
     * 因此可以使用堆排序中的最小堆，它并不用保证所有的都是有序，但是堆顶的一定是最小的，重新获取最小元素的操作也只需要O(logN)的时间
     * 使用最小堆的过程为，每次把堆顶的数（也就是第i个数）乘235后把这三个乘积加入堆，然后堆顶的数出堆，下次继续取堆顶的数重复这个操作
     * 因为最小堆并不能很好的执行去重操作，所以还需要另外一个哈希表来执行去重操作
     */
    public int nthUglyNumber(int n) {
        // 使用最小堆
        // java中优先级队列 PriorityQueue 就是最小堆
        PriorityQueue<Long> minHeap = new PriorityQueue<>();
        minHeap.offer(1L);
        // 防止乘法溢出
        Set<Long> set = new HashSet<>();
        for (int i = 1; i < n; i++) {
            // 不用管这个unboxing
            long min = minHeap.poll();
            System.err.println(min);
            if (!set.contains(min * 2)) {
                minHeap.offer(min * 2);
                set.add(min * 2);
            }
            if (!set.contains(min * 3)) {
                minHeap.offer(min * 3);
                set.add(min * 3);
            }
            if (!set.contains(min * 5)) {
                minHeap.offer(min * 5);
                set.add(min * 5);
            }
        }
        // 不用管这个unboxing
        return minHeap.poll().intValue();
    }

    /**
     * 对堆最小堆方法的优化
     * 使用最小堆时，一股脑把乘235后的乘积全都添加到堆中去了，然后再进行一次堆调整来得到堆顶的最小值
     * 实际上要添加的元素就3个，根据上面使用最小堆的流程，实际上我们只需要这三个乘积中最小的那一个，因此可以手动执行堆调整过程
     * 设通过丑数乘235得到的数字的数组分别为 ugly2[] ugly3[] ugly5[]
     * 从下标0开始往右移动，丑数分别是三个数组中的未被移动过的区间的最小值
     * 因此使用三个指针p2,p3,p5就可以了
     * 当最小值是 ugly2[] 中产生的时，p2右移一格，同时生成下一个待比较最小值的数 x*2，并添加到数组中，其中x为上一个计算时用的丑数，三个数组分别不同
     * 对于ugly3[] ugly5[] 同理，下一个待比较的最小值的数为 p3*3 和 p5*5
     * 这样，当一个指针不需要移动时，不需要产生它后面的数，因为当前的数还没有被计算成最小值，后面的数肯定不会被计算成最小值
     * 相当于按需生成了，节省了一部分空间
     * 因为计算x时需要知道所有已经计算好丑数，所以需要一个数组存储x
     * 根据上面的推导，知道 x = ugly2[] + ugly3[] + ugly5[] 三个数组的并集
     * 所以综合起来只需要一个数组就行数组长度为n+1
     */
    public int nthUglyNumber2(int n) {
        int[] uglyNums = new int[n];
        uglyNums[0] = 1;
        int p2 = 0;
        int p3 = 0;
        int p5 = 0;
        for (int i = 1; i < n; i++) {
            int ugly = Math.min(Math.min(uglyNums[p2] * 2, uglyNums[p3] * 3), uglyNums[p5] * 5);
            if (ugly == uglyNums[p2] * 2) {
                p2++;
            }
            if (ugly == uglyNums[p3] * 3) {
                p3++;
            }
            if (ugly == uglyNums[p5] * 5) {
                p5++;
            }
            uglyNums[i] = ugly;
        }
        return uglyNums[n - 1];
    }
}
