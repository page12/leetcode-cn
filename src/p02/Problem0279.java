package p02;

/**
 * https://leetcode-cn.com/problems/perfect-squares/
 */
public class Problem0279 {

    public static void main(String[] args) {
        Problem0279 p = new Problem0279();
        System.err.println(p.numSquares(12) == 3);
        System.err.println(p.numSquares(13) == 2);
    }

    /**
     * 思路：
     * 设所求结果为f(n)，则有
     * f(n) = 1，n为完全平方数
     * f(n) = min{ f(n-1) + f(1), f(n-2) + f(2), ... }，n不是完全平方数
     * 当 n-1 n-2 也不是完全平方数时，f(n-1) = f(n-2) + f(1)
     * 因为f(1) = 1，f(2) = 2，此时有 f(n-1) + f(1) = f(n-2) + f(2)，也就是这时候min操作中有很多相同的数
     * 假设i是完全平方数，则 f(n) = f(n-i) + f(i) = f(n-i) + 1 >= f(n-i+1) + f(i-1)
     * 所以可以只用 i = 完全平方数 来进行min操作
     * 使用自底向上的方式时，从1开始，额外需要一个长度为n的数组来保存f(i)
     */
    public int numSquares(int n) {
        int[] numSquares = new int[n];
        numSquares[0] = 1;
        for (int i = 2; i <= n; i++) {
            int min = Integer.MAX_VALUE;
            for (int j = 1; j * j <= i; j++) {
                if (j * j == i) {
                    min = 1;
                    break;
                }
                min = Math.min(numSquares[j * j - 1] + numSquares[i - j * j - 1], min);
            }
            numSquares[i - 1] = min;
        }
        return numSquares[n - 1];
    }
}
