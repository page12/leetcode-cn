package p02;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/invert-binary-tree/
 */
public class Problem0226 {

    public static void main(String[] args) {

    }

    /**
     * 思路：递归操作，二叉树的递归操作额外需要树的深度个栈空间，平均需要O(logN)，最坏情况变成单链表需要O(N)空间
     * 如果使用迭代代替递归，可以使用先序遍历的通用模板来修改
     */
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        if (root.left != null) {
            invertTree(root.left);
        }
        if (root.right != null) {
            invertTree(root.right);
        }
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;
        return root;
    }
}
