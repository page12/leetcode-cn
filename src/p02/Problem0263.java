package p02;

/**
 * https://leetcode-cn.com/problems/ugly-number/
 */
public class Problem0263 {

    public static void main(String[] args) {
        Problem0263 p = new Problem0263();
        System.err.println(p.isUgly(6));
        System.err.println(p.isUgly(8));
        System.err.println(p.isUgly(14));
        System.err.println(p.isUgly(1));
    }

    /**
     * 思路：丑数的质因子只有2, 3, 5，对丑数连续整除2, 3, 5，最后的结果一定是1，不是1则不是丑数
     * 因为java整数除法的原因，实际使用上面的方法时需要修改下，要先使用mod运算判断是否能整除
     */
    public boolean isUgly(int n) {
        if (n <= 0) {
            return false;
        }
        if (n == 1) {
            return true;
        }
        while (n % 2 == 0) {
            n = n / 2;
        }
        while (n % 3 == 0) {
            n = n / 3;
        }
        while (n % 5 == 0) {
            n = n / 5;
        }
        return n == 1;
    }
}
