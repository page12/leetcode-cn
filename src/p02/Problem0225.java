package p02;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 */
public class Problem0225 {

    public static void main(String[] args) {

    }

    /**
     * 思路：栈（先进后出）和队列（先进先出）可以理解成是顺序相反的两种数据结构
     * 利用队列实现栈，主要的一点就是要思考如何颠倒元素
     * 如果队列只有一个元素 n，再入队一个元素n + 1，如何用两个队列实现n + 1先入队？
     * 方法很简单先把n + 1入队另一个空队列，再把包含元素n的队列入队到包含n + 1的队列，这样就实现了颠倒元素，实现了有两个元素的栈
     * 有三个元素的，因为栈每次只能添加一个元素，再这之前的元素都已经整体颠倒顺序了，可以视为一个整体，这样就可以套用两个元素是的操作
     */
    private static class MyStack {

        /**
         * 包含所有元素的queue
         */
        Queue<Integer> contentQueue;
        /**
         * 辅助queue，用于入栈时颠倒元素在队列中的排列顺序，保证入栈的元素是队列头部元素
         */
        Queue<Integer> assistantQueue;

        /**
         * Initialize your data structure here.
         */
        public MyStack() {
            contentQueue = new LinkedList<>();
            assistantQueue = new LinkedList<>();
        }

        /**
         * Push element x onto stack.
         */
        public void push(int x) {
            if (contentQueue.size() == 0) {
                contentQueue.add(x);
            } else {
                assistantQueue.add(x);
                assistantQueue.addAll(contentQueue);
                // 此时因为contentQueue的所有元素都包含在了assistantQueue中，所以要清空contentQueue，再交换队列
                contentQueue.clear();
                Queue<Integer> temp = contentQueue;
                contentQueue = assistantQueue;
                assistantQueue = temp;
            }
        }

        /**
         * Removes the element on top of the stack and returns that element.
         */
        public int pop() {
            // 拆箱可能会NPE，不管这个问题
            return contentQueue.poll();
        }

        /**
         * Get the top element.
         */
        public int top() {
            // 拆箱可能会NPE，不管这个问题
            return contentQueue.peek();
        }

        /**
         * Returns whether the stack is empty.
         */
        public boolean empty() {
            return contentQueue.isEmpty();
        }
    }
}
