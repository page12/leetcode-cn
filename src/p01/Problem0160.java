package p01;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/intersection-of-two-linked-lists/
 */
public class Problem0160 {

    public static void main(String[] args) {

    }

    /**
     * 思路：两个链表的交点，如果有，则按照链表顺序，它应该是第一个满足 pA == pB 的地方
     * 同时交点后面的每个节点都满足 pA == pB
     * 使用O(1)空间的话，则只能遍历链表
     * 此时需要构造一个方法使得正常遍历链表（A和B一次都走一格）时A和B能遍历到同一个元素上去，也就是遍历路程相等
     * 设A的长度为la，B的长度为lb，A和B的公共长度为lc
     * 那么如果有交点，交点与headA的距离为la - lc，与headB的距离为lb - lc
     * 要使得遍历路程相等，只需要分别额外遍历一次B和A，此时遍历到交点时pA的路程为 la + (lb - lc), pB的路程为 lb + (la - lc)，两者相等
     * 按照如下顺序遍历，pA遍历完一次A后，从headB开始遍历，pB遍历完一次B后，从headA开始遍历，第一个使得pA == pB的点就是交点
     * 注意点：pA == pB 时两者都不能是null，这种情况是没有交点的（可以视为交点无穷远）
     */
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) {
            return null;
        }
        ListNode pA = headA;
        ListNode pB = headB;
        // 按照思路中的遍历方式
        // 当pA == null && pB == null时，两者遍历的距离相等，都遍历到另一个链表的尾部
        // 此时循环需要结束，返回null表示链表不想交
        while (pA != null || pB != null) {
            if (pA == null) {
                pA = headB;
            }
            if (pB == null) {
                pB = headA;
            }
            if (pA == pB) {
                return pA;
            }
            pA = pA.next;
            pB = pB.next;
        }
        return null;
    }
}
