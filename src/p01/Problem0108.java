package p01;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/convert-sorted-array-to-binary-search-tree/
 */
public class Problem0108 {

    public static void main(String[] args) {

    }

    /**
     * 思路：二叉搜索树的中序遍历就是有序数组的顺序
     * 因为要求是平衡树，可以利用二分方式来确定根节点是有序数组中哪个元素，不断递归分治就行
     */
    public TreeNode sortedArrayToBST(int[] nums) {
        int length = nums.length;
        int mid = (length - 1) >>> 1;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = buildBST(nums, 0, mid - 1);
        root.right = buildBST(nums, mid + 1, length - 1);
        return root;
    }

    private TreeNode buildBST(int[] nums, int start, int end) {
        if (start > end) {
            return null;
        }
        int mid = start + ((end - start) >>> 1);
        TreeNode root = new TreeNode(nums[mid]);
        root.left = buildBST(nums, start, mid - 1);
        root.right = buildBST(nums, mid + 1, end);
        return root;
    }
}
