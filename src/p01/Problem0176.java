package p01;

/**
 * https://leetcode-cn.com/problems/second-highest-salary/
 */
public class Problem0176 {

    // 第二高就是剩下的小于最高的记录中的最高的，用两次MAX就可以轻松得到
    // SELECT MAX(Salary) AS SecondHighestSalary
    // FROM Employee
    // WHERE Salary < (SELECT MAX(Salary) FROM Employee)
}
