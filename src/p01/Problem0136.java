package p01;

/**
 * https://leetcode-cn.com/problems/single-number/
 */
public class Problem0136 {

    public static void main(String[] args) {
        System.err.println(new Problem0136().singleNumber(new int[]{2, 2, 1}) == 1);
        System.err.println(new Problem0136().singleNumber(new int[]{4, 1, 2, 1, 2}) == 4);
        System.err.println(new Problem0136().singleNumber(new int[]{1, 2, 3, 3, 2, 1, -5}) == -5);
    }

    /**
     * 思路：利用异或的性质
     * a ^ a = 0
     * a ^ 0 = a
     * 那么a ^ a ^ b ^ b ^ c = c
     * 题目中给定数组中只有一个数字是出现一次的，视为上面的c；剩下的数字都出现了两次，视为上面的a和b
     */
    public int singleNumber(int[] nums) {
        // 题目有假设，不做校验
        int result = 0;
        for (int num : nums) {
            result ^= num;
        }
        return result;
    }
}
