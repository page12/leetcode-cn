package p01;

/**
 * https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-ii/
 */
public class Problem0122 {

    public static void main(String[] args) {
//        System.err.println(new Problem0122().maxProfit(new int[]{70, 80, 0, 50, -20, 40}));
        System.err.println(new Problem0122().maxProfit(new int[]{5, 2, 3, 2, 6, 6, 2, 9, 1, 0, 7, 4, 5, 0}));
    }

    /**
     * 思路：比第121题好理解多了
     * 从左往右遍历，求每个极小值与其紧跟的那一个极大值的差值，这就是一次买入卖出
     * 考虑全家单调的情况，此时不能买入卖出，利润是0
     * 所以极大值极小值要加特殊判断：极大值不能是下标0，极小值不能是最后一个下标
     * 把所有买入卖出累加起来就是结果
     */
    public int maxProfit(int[] prices) {
        // 题目有假设，不做校验
        if (prices.length == 1) {
            return 0;
        }
        int sum = 0;
        int max = 0;
        boolean findMax = false;
        int min = 0;
        boolean findMin = false;
        for (int i = 0; i < prices.length; i++) {
            // 极小值
            if (i == 0 && prices[i] <= prices[i + 1]) {
                min = prices[i];
                findMin = true;
            } else if (i != 0 && i != prices.length - 1 && prices[i] < prices[i + 1] && prices[i] <= prices[i - 1]) {
                min = prices[i];
                findMin = true;
            }
            // 极大值
            if (i == prices.length - 1 && prices[i] >= prices[i - 1]) {
                max = prices[i];
                findMax = true;
            } else if (i != 0 && i != prices.length - 1 && prices[i] >= prices[i + 1] && prices[i] > prices[i - 1]) {
                max = prices[i];
                findMax = true;
            }
            if (findMax && findMin) {
                findMax = false;
                findMin = false;
                sum += (max - min);
            }
        }
        return sum;
    }
}
