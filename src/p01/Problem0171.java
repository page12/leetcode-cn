package p01;

/**
 * https://leetcode-cn.com/problems/excel-sheet-column-number/
 */
public class Problem0171 {

    public static void main(String[] args) {
        Problem0171 p = new Problem0171();
        System.err.println(p.titleToNumber("A") == 1);
        System.err.println(p.titleToNumber("Z") == 26);
        System.err.println(p.titleToNumber("AA") == 27);
        System.err.println(p.titleToNumber("AB") == 28);
        System.err.println(p.titleToNumber("AZ") == 52);
        System.err.println(p.titleToNumber("ZY") == 701);
        System.err.println(p.titleToNumber("FXSHRXW") == 2147483647);
    }

    /**
     * 思路：很简单，x进制转10进制是通用流程，直接套用
     */
    public int titleToNumber(String columnTitle) {
        // 不考虑columnTitle为空，以及不符合格式的情况
        int result = 0;
        for (int i = 0; i < columnTitle.length(); i++) {
            result = result * 26 + (columnTitle.charAt(i) - 'A' + 1);
        }
        return result;
    }
}
