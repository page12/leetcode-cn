package p01;

/**
 * https://leetcode-cn.com/problems/majority-element/
 */
public class Problem0169 {

    public static void main(String[] args) {
        Problem0169 p = new Problem0169();
        System.err.println(p.majorityElement(new int[]{3, 2, 3}) == 3);
        System.err.println(p.majorityElement(new int[]{2, 2, 1, 1, 1, 2, 2}) == 2);
        System.err.println(p.majorityElement(new int[]{1, 2, 1, 2, 1, 3, 3, 3, 3, 3, 3}) == 3);
    }

    /**
     * 思路：之前知道摩尔投票算法求绝对众数（出现次数大于⌊n/2⌋，题目所求的结果），这是最消耗最小的算法，时间复杂度为O(n)，空间为O(1)
     * 初始化元素m = nums[0]，票数t = 1
     * 遍历从下标1开始nums
     * 如果m = nums[i]，则t++，否则t--
     * 如果t == 0，则更换m并重置票数，令m = nums[i], t = 1
     * 遍历完成后的m就是结果
     * 这个算法简单理解就是不同的数字之间一换一，最后留下的数字就是绝对众数
     * 注意，摩尔投票算法只适用于绝对众数，不是这种条件也会返回值，但是没意义
     * 其他思路：维护hash表，时间复杂度为O(n)，空间为O(n)；排序后直接返回中间的数，时间复杂度为O(nlogn)，空间为O(1)
     */
    public int majorityElement(int[] nums) {
        // 空数组不考虑，没有多数元素的数组也不考虑
        int m = nums[0];
        int t = 1;
        for (int i = 1; i < nums.length; i++) {
            if (m == nums[i]) {
                t++;
            } else {
                t--;
            }
            if (t == 0) {
                m = nums[i];
                t = 1;
            }
        }
        return m;
    }
}
