package p01;

import java.util.*;

/**
 * https://leetcode-cn.com/problems/palindrome-partitioning/
 */
public class Problem0131 {

    public static void main(String[] args) {
        Problem0131 p = new Problem0131();
        System.err.println(p.partition("aab"));
        System.err.println(p.partition("a"));
        System.err.println(p.partition("bb"));
        System.err.println(p.partition("aaa"));
    }

    /**
     * 思路：递归分割，判断每一部分是不是回文串
     * 对于长度为n的字符串
     * 按照(1,n-1) (2,n-2),(n-1,1)先分割一次
     * 每次分割结果中，如果有长度大于1的，再递归分割
     * 递归过程中判断回文串时，会有很多一样的字符串需要判断，因此需要缓存，或者提前计算出这些结果
     * 如果使用双指针法判断回文串，判断一次需要O(n)的时间
     * 整个字符串的子串共有 C(n,2) = n(n-1)/2 种，这样全部子串就需要O(n^3)的时间
     * 这里面会有很多重复的判断
     * 对于字符串 s(i,j)，如果它是回文串且长度大于1，则去除首尾两个字符后s(i+1,j-1)也是回文串
     * 长度为1一定是回文串
     * 因此递推公式为
     * f(i,j) = true，此时i = j
     * f(i,j) = f(i+1,j-1) && s(i) = s(j)，此时j-i > 1
     * f(i,j) = s(i) == s(j)，此时j-i = 1
     * 利用这个递推公式，可以在O(n^2)的时间求出所有子串是否是回文串的结果
     * 保存这些结果需要额外的O(n^2)的空间
     * <br/>
     * 直接暴力递归的逻辑比较简单，它在每次递归求出后面部分的所有情况的列表，然后把第一部分逐个添加到这些列表中去
     * 设后面部分长度为n，它最多有2^(n-1)种分割方法（区分分割顺序），这样逐个添加的次数也就变成了2^(n-1)次，整体速度会很慢
     * 可以使用带回溯的递归，就是用一个栈来保存已经处理好的子问题的结果
     * 当不能再继续递归下去时，使用当前栈的数据，然后弹出栈顶数据，接着递归正常回溯
     */
    public List<List<String>> partition(String s) {
        // 题目有假设，不做校验
        // 满足j >= i，只需要求对角线及其上面的就行
        // 从右下开始，按照从右至左，从下至上的顺序求
        // 因为递推公式就是这样的，需要先知道左下的元素
        boolean[][] dpTable = new boolean[s.length()][s.length()];
        for (int i = s.length() - 1; i >= 0; i--) {
            for (int j = s.length() - 1; j >= i; j--) {
                if (i == j) {
                    dpTable[i][j] = true;
                } else if (j - i == 1) {
                    dpTable[i][j] = s.charAt(i) == s.charAt(j);
                } else {
                    dpTable[i][j] = dpTable[i + 1][j - 1] && s.charAt(i) == s.charAt(j);
                }
            }
        }
        List<List<String>> result = new ArrayList<>();
        recurse(s, 0, dpTable, new LinkedList<>(), result);
        return result;
    }

    /**
     * @param s        原字符串
     * @param splitIdx 第一个分割点，[0, splitIdx]范围是分割后的第一个部分
     * @param dpTable  判断是否是回文串的二维数组，提前算好了
     * @param stack    记录当前递归路径中所有分割好的部分，用于回溯
     * @param result   最终结果
     */
    private void recurse(String s, int splitIdx, boolean[][] dpTable, Deque<String> stack, List<List<String>> result) {
        // 分割后，后续部分什么都没有的情况，也就是整体不分割的情况，此时就算是已经分割完毕
        // 没有后续部分，也不能再分割，也就不能再递归
        // 此时相当于是当前递归栈到了顶点，整个栈中保存的全都是已经分割好的部分
        // 因为已经分割完毕，所以此时栈的情况就是一种结果，要把它复制保存下来
        if (s.length() == splitIdx) {
            // 栈（在list头部操作的）是颠倒顺序的，所以这里要反转一次
            // 直接再list尾部操作的话，就不需要反转
            List<String> list = new ArrayList<>(stack);
            Collections.reverse(list);
            result.add(list);
        }
        for (int i = splitIdx; i < s.length(); i++) {
            // 不是回文串时，只需要判断第一部分
            // 后面的部分一定有能分割成全是回文串的方法（全不分成一个）
            if (!dpTable[splitIdx][i]) {
                continue;
            }
            String first = s.substring(splitIdx, i + 1);
            // 栈中保存已经分割好的部分
            stack.push(first);
            recurse(s, i + 1, dpTable, stack, result);
            // 当上面的递归函数返回到这里时，相当于已经处理好了一种情况
            // 这里出栈代表回溯一次
            stack.pop();
        }
    }
}
