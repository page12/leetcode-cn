package p01;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/
 */
public class Problem0104 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.right = new TreeNode(2);
        root.right = new TreeNode(3);
        System.err.println(new Problem0104().maxDepth(root));
    }

    /**
     * 思路：分治法，左右子树的深度中较大的那个加1就是最大深度
     * 其他方法：BFS时记录层数
     */
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftDepth = maxDepth(root.left);
        int rightDepth = maxDepth(root.right);
        return Math.max(leftDepth, rightDepth) + 1;
    }
}
