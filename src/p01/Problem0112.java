package p01;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/path-sum/
 */
public class Problem0112 {

    public static void main(String[] args) {

    }

    /**
     * 思路：用DFS递归分治，最直观的思路
     */
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return false;
        }
        int val = root.val;
        // 叶子节点，判断是否符合
        if (root.left == null && root.right == null && val == targetSum) {
            return true;
        } else {
            return hasPathSum(root.left, targetSum - val) || hasPathSum(root.right, targetSum - val);
        }
    }
}
