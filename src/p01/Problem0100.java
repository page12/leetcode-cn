package p01;

import common.TreeNode;

import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/same-tree/
 */
public class Problem0100 {

    public static void main(String[] args) {
        TreeNode p = new TreeNode(1);
        p.left = new TreeNode(2);
        p.right = new TreeNode(3);

        TreeNode q = new TreeNode(1);
        q.left = new TreeNode(3);
        q.right = new TreeNode(2);

        System.err.println(new Problem0100().isSameTree(p, q));
    }

    /**
     * 思路：BFS，二叉树的后继节点最多只有左右两个，可以代码里面写死
     * 入队前判断入队的节点是否相等，不相等就不是相同的树，直接返回false
     * 遍历完了就是相同的树，返回true
     * 递归比较简单，这里不写
     */
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null || p.val != q.val) {
            return false;
        }
        LinkedList<TreeNode> queue1 = new LinkedList<>();
        queue1.addFirst(p);
        LinkedList<TreeNode> queue2 = new LinkedList<>();
        queue2.addFirst(q);
        while (!queue1.isEmpty() && !queue2.isEmpty()) {
            // 利用二叉树性质，这里可以少一层循环，直接写死left和right
            TreeNode treeNode1 = queue1.removeLast();
            TreeNode treeNode2 = queue2.removeLast();
            // left
            TreeNode pLeft = treeNode1.left;
            TreeNode qLeft = treeNode2.left;
            if (pLeft != null && qLeft != null) {
                if (pLeft.val != qLeft.val) {
                    return false;
                }
                queue1.addFirst(pLeft);
                queue2.addFirst(qLeft);
            } else if (pLeft != qLeft) {
                return false;
            }
            // right
            TreeNode pRight = treeNode1.right;
            TreeNode qRight = treeNode2.right;
            if (pRight != null && qRight != null) {
                if (pRight.val != qRight.val) {
                    return false;
                }
                queue1.addFirst(pRight);
                queue2.addFirst(qRight);
            } else if (pRight != qRight) {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断节点是否相等
     */
    private boolean isEqual(TreeNode p, TreeNode q) {
        return p == q || (p != null && q != null && p.val == q.val);
    }
}
