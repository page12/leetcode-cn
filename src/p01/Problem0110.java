package p01;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/balanced-binary-tree/
 */
public class Problem0110 {

    public static void main(String[] args) {

    }

    /**
     * 思路：利用求二叉树最大深度的递归算法，改进一下
     * 平衡二叉树的任何一棵子树都是平衡二叉树
     * 如果左右子树的深度差大于1，则整棵树都不平衡
     * 此时可以返回一个特殊的深度，快速结束递归，相当于把判断逻辑也提前递归分治了
     */
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }
        int leftDepth = maxDepth(root.left);
        if (leftDepth == -1) {
            return false;
        }
        int rightDepth = maxDepth(root.right);
        if (rightDepth == -1) {
            return false;
        }
        return Math.abs(leftDepth - rightDepth) <= 1;
    }

    /**
     * 求二叉树的最大深度，改进了一些
     */
    private int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftDepth = maxDepth(root.left);
        if (leftDepth == -1) {
            return -1;
        }
        int rightDepth = maxDepth(root.right);
        if (rightDepth == -1) {
            return -1;
        }
        // 左右子树深度差大于1，则root不平衡，此时返回-1可以快速结束递归
        if (Math.abs(leftDepth - rightDepth) > 1) {
            return -1;
        }
        return Math.max(leftDepth, rightDepth) + 1;
    }
}
