package p01;

/**
 * https://leetcode-cn.com/problems/maximum-product-subarray/
 */
public class Problem0152 {

    public static void main(String[] args) {
        Problem0152 p = new Problem0152();
        System.err.println(p.maxProduct(new int[]{2, 3, -2, 4}) == 6);
        System.err.println(p.maxProduct(new int[]{-2, 0, -1}) == 0);
        System.err.println(p.maxProduct(new int[]{3, -1, 4}) == 4);
        System.err.println(p.maxProduct(new int[]{-2, -3, 7}) == 42);
    }

    /**
     * 思路：
     * 贪心算法
     * 用三个变量记录三个值，分别是
     * 以当前元素为终点的某一段连续序列的连续最大乘积 maxSeq > 0，否则在下一次累乘前要重置为1（根据乘法性质，可以重置为1）
     * 以当前元素为终点的某一段连续序列的连续最小乘积 minSeq < 0，否则在下一次累乘前要重置为1（根据乘法性质，可以重置为1）
     * 整个数组的最大连续乘积，也就是最终结果 result
     * <br/>
     * 算法基本流程为，遍历数组，设当前元素为nums[i]
     * 如果当前元素大于0，乘以正数之后符号不变，此时不需要其他操作
     * 如果当前元素小于0，根据乘法性质，可以交换 maxSeq 和 minSeq，再执行maxSeq *= nums[i]，minSeq *= nums[i]
     * 如果当前元素为0，则 maxSeq 和 minSeq 都等于0，表示这一段连续序列不能再继续扩张下去，相当于重新开始新的序列
     * <br/>
     * 可以把判断 maxSeq > 0 minSeq < 0 的条件放在循环最后面，这样就只用写nums[i]为负数的分支
     */
    public int maxProduct(int[] nums) {
        // 题目有假设，不做校验
        int maxSeq = 1;
        int minSeq = 1;
        int result = nums[0];
        for (int i = 0; i < nums.length; i++) {
            maxSeq *= nums[i];
            minSeq *= nums[i];
            if (nums[i] < 0) {
                int temp = maxSeq;
                maxSeq = minSeq;
                minSeq = temp;
            }
            result = Math.max(maxSeq, result);
            // 不符合规定时重置为1
            if (maxSeq <= 0) {
                maxSeq = 1;
            }
            if (minSeq >= 0) {
                minSeq = 1;
            }
        }
        return result;
    }
}
