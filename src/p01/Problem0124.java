package p01;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/binary-tree-maximum-path-sum/
 */
public class Problem0124 {

    public static void main(String[] args) {
        Problem0124 p = new Problem0124();
//        TreeNode root = new TreeNode(-10);
//        root.left = new TreeNode(9);
//        root.right = new TreeNode(20);
//        root.right.left = new TreeNode(15);
//        root.right.right = new TreeNode(7);
//        System.err.println(p.maxPathSum(root));

        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(-2);
        root.right = new TreeNode(-3);
        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);
        root.right.left = new TreeNode(-2);
        root.left.left.left = new TreeNode(-1);
        System.err.println(p.maxPathSum(root));
    }

    /**
     * 思路：
     * 对于任意节点x
     * 设上一层选择它作为路径中的一员时，最大路径长度会增加length
     * 对于节点x，如果左右两边的length都为负数，则x.length = x.val
     * 否则，选择左右两边length较大的那一个添加在路径中x.length = x.val + max(x.left.length, x.val + x.right.length)
     * 综合起来就是 x.length = max(x.val, x.val + left.length, x.val + right.length)
     * <br/>
     * 设以x为根节点的树的最大路径和为max，那么max是下面六种情况的最大值
     * 1、最大路径形状是 x左 + x + x右，此时 max = x.val + x.left.length + x.right.length
     * 2、最大路径形状是 x左 + x，此时 max = x.val + x.left.length + x.right.length
     * 3、最大路径形状是 x + x右，此时 max = x.val + x.left.length + x.right.length
     * 4、最大路径只包含x这一个节点，此时 max = x.val
     * 5、最大路径在x的左子树中，此时 max = x.left.max
     * 6、最大路径在x的右子树中，此时 max = x.right.max
     * <br/>
     * 上面的x.left.length和x.left.max可以用递归求得，因为一次要求两个值，所以可以用一个类包装下
     * 令 x = root，此时的递归结果就是整棵树的最大路径和
     */
    public int maxPathSum(TreeNode root) {
        // 题目有假设，不做校验
        Result result = dfs(root);
        return result.max;
    }

    /**
     * 递归求Result
     */
    private Result dfs(TreeNode root) {
        int length;
        int max;
        if (root.left == null && root.right == null) {
            length = root.val;
            max = root.val;
        } else if (root.right == null) {
            Result left = dfs(root.left);
            length = Math.max(root.val + left.length, root.val);
            max = Math.max(length, left.max);
        } else if (root.left == null) {
            Result right = dfs(root.right);
            length = Math.max(root.val + right.length, root.val);
            max = Math.max(length, right.max);
        } else {
            Result left = dfs(root.left);
            Result right = dfs(root.right);
            length = max(root.val, root.val + left.length, root.val + right.length);
            max = max(
                    Math.max(root.val, root.val + left.length + right.length),
                    Math.max(root.val + left.length, root.val + right.length),
                    Math.max(left.max, right.max)
            );
        }
        return new Result(length, max);
    }

    /**
     * 求三个数最大值
     */
    private static int max(int a, int b, int c) {
        return Math.max(a, Math.max(b, c));
    }

    /**
     * 求四个数最大值
     */
    private static int max(int a, int b, int c, int d) {
        return Math.max(Math.max(a, b), Math.max(c, d));
    }

    private static class Result {
        // 上一层选择此节点作为路径时，路径长度增加值
        int length;
        // 一棵树下的最大路径和
        int max;

        Result(int length, int max) {
            this.length = length;
            this.max = max;
        }
    }
}
