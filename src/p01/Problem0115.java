package p01;

/**
 * https://leetcode-cn.com/problems/distinct-subsequences/
 */
public class Problem0115 {

    public static void main(String[] args) {
        Problem0115 p = new Problem0115();
        System.err.println(p.numDistinct("rabbbit", "rabbit"));
        System.err.println(p.numDistinct("babgbag", "bag"));
    }

    /**
     * 思路：
     * 这个题的递归公式很好推导
     * 设s长度为n，t长度为m，f(i,j)表示s的前i个字符和t的前j个字符的结果，下标从1开始，且n >= m（n < m一定不包含，直接返回0）
     * 那么有下面两种情况
     * 当s(i) = t(j)时，f(i,j) = f(i-1,j-1)
     * 当s(i) != t(j)时，因为t(j)还没被匹配，但是它一定要被匹配，所以只能删除s(i)，所以此时有 f(i,j) = f(i-1,j)
     * <br/>
     * 上面的递推公式看着就很奇怪，因为没有加法，导致次数不能增加
     * 研究了半天，总数是找到原因了
     * 题目规定了s = rabbbit和 t = rabbit的结果是3，也就删除三个b中的任何一个，都算一种单独的情况
     * 上面的公式的意思是比较删除后的结果，不管哪种方式删除，最终都得到rabbit，此时再比较，肯定最多只能得到1
     * 为了区别不同的删除方式，需要添加一种情况
     * 也就是当s(i) = t(j)时，还需要比较s(i-1)和t(j)，因为当s(i-1)的子序列中包含t(j)时，s(i)的肯定也包含t(j)，这种情况的数量要被加进去
     * 一个直接的例子就是abcdd和abcd
     * <br/>
     * 最终的递推公式为
     * 当s(i) = t(j)时，f(i,j) = f(i-1,j-1) + f(i-1,j)
     * 当s(i) != t(j)时，f(i,j) = f(i-1,j)
     * <br/>
     * 再考虑一些特殊值
     * 当i=j=0时，两个都是空串，相等，因此是1，有f(0,0) = 1
     * 当i=0, j!=0时，表示s的长度小于t的长度，此时一定不包含，因此值为0，有f(0,j) = 0
     * 当i!=0, j=0时，表示t是空串，此时只有一种删除方式，那就是把s全删光，因此值为1，有f(i,0) = 1
     * 自底向上，需要一个二维数组保存(n+1)*(m+1)个中间结果，时间空间复杂度都为O(n*m)
     * 可以使用滚动数组来优化空间复杂度为O(m)，下面的代码是使用滚动数组的代码
     */
    public int numDistinct(String s, String t) {
        // 题目有假设，不做校验
        if (s.length() < t.length()) {
            return 0;
        }
        if (s.equals(t)) {
            return 1;
        }
        // 下标为0的是空字符串的情况，所以长度要加1
        int[] temp = new int[t.length() + 1];
        int diagonal = 0;
        for (int i = 0; i <= s.length(); i++) {
            for (int j = 0; j <= t.length(); j++) {
                // 上面分析的字符串的下标是1（因为用0表示空字符串了）
                // 所以这里用charAt要减1才行
                if (i == 0 && j == 0) {
                    temp[j] = 1;
                } else if (i == 0) {
                    temp[j] = 0;
                } else if (j == 0) {
                    // 注意这里更新对角的值
                    int nextDiagonal = temp[j];
                    temp[j] = 1;
                    diagonal = nextDiagonal;
                } else if (s.charAt(i - 1) == t.charAt(j - 1)) {
                    // 注意这里更新对角的值
                    int nextDiagonal = temp[j];
                    temp[j] = temp[j] + diagonal;
                    diagonal = nextDiagonal;
                } else {
                    // 注意这里更新对角的值
                    // temp[j] = temp[j]
                    diagonal = temp[j];
                }
            }
        }
        return temp[temp.length - 1];
    }
}
