package p01;

/**
 * https://leetcode-cn.com/problems/dungeon-game/
 */
public class Problem0174 {

    public static void main(String[] args) {
        Problem0174 p = new Problem0174();
        System.err.println(p.calculateMinimumHP(new int[][]{{-2, -3, 3}, {-5, -10, 1}, {10, 30, -5}}));
    }

    /**
     * 思路：
     * 有一个重要规则，那就是任何时候HP至少都要有1点
     * 终点处的最小生命值为1，这是处理完终点的点数之后的
     * 所以进入终点房间时最小生命值 = 1 减去 终点房间的点数
     * 如果计算出的最小生命值 <= 0，则最少值要重置为1
     * <br>
     * 正常的动态规划都是知道初始0的值，现在的情况是只有终点的值是已知的
     * 所以可以反过来推导，利用进入下一个房间时的最小生命值，反推出进入当前房间时的最小生命值
     * 很容易能得到下面的公式：
     * 进入当前房间时的最小生命值 = 进入下一个房间时的最小生命值 减去 当前房间的点数
     * 如果当前房间时的最小生命值 <= 0，则最少值要重置为1
     * <br>
     * 因为有两条路可以走，所以计算两条路时最后要用min操作取较小的那一个
     * 设进入当前房间时的最小生命值为dp[i][j]，转换为坐标后公式如下
     * dp[i][j] = Math.min(dp[i][j + 1] - dungeon[i][j], dp[i + 1][j] - dungeon[i][j])
     * 边界时只有一条路，所以不需要min操作
     * 起点dp[n-1][m-1] = 1 - dungeon[i][j]
     * 如果计算完任意一点后 dp[i][j] <= 0 要重置其为1
     */
    public int calculateMinimumHP(int[][] dungeon) {
        // 题目有假设，不做校验
        int n = dungeon.length;
        int m = dungeon[0].length;
        int[][] dp = new int[n][m];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = m - 1; j >= 0; j--) {
                if (i == n - 1 && j == m - 1) {
                    dp[i][j] = 1 - dungeon[i][j];
                } else if (i == n - 1) {
                    dp[i][j] = dp[i][j + 1] - dungeon[i][j];
                } else if (j == m - 1) {
                    dp[i][j] = dp[i + 1][j] - dungeon[i][j];
                } else {
                    dp[i][j] = Math.min(dp[i][j + 1] - dungeon[i][j], dp[i + 1][j] - dungeon[i][j]);
                }
                // 最小生命值为1，任何时候都不能小于1
                if (dp[i][j] <= 0) {
                    dp[i][j] = 1;
                }
            }
//            System.err.println(Arrays.toString(dp[i]));
        }
        return dp[0][0];
    }
}
