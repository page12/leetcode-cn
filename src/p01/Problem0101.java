package p01;

import common.TreeNode;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/symmetric-tree/
 */
public class Problem0101 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);

        System.err.println(new Problem0101().isSymmetric(root));
    }

    /**
     * 思路：BFS，二叉树的后继节点最多只有左右两个，可以代码里面写死
     * 某一层全部入队后，判断该层是否对称
     * 注意，二叉树有左右之分，因此null节点也被加入到队列了，要特殊判断下
     * 如果使用递归算法，需要交换左右来判断，即p的左边和q的右边比较是否相等，p的右边和q的左边比较是否相等
     */
    public boolean isSymmetric(TreeNode root) {
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            // 当前层的节点的数量
            int currentLayerLength = queue.size();
            // 循环下一层所有节点
            while (currentLayerLength-- > 0) {
                TreeNode node = queue.removeLast();
                if (node != null) {
                    queue.addFirst(node.left);
                    queue.addFirst(node.right);
                }
            }
            // 判断某一层是否对称
            if (!isLayerSymmetric(queue)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断某一层是否对称
     */
    private boolean isLayerSymmetric(LinkedList<TreeNode> queue) {
        if (queue.isEmpty()) {
            return true;
        }
        Iterator<TreeNode> iter = queue.iterator();
        Iterator<TreeNode> descIter = queue.descendingIterator();
        while (iter.hasNext()) {
            TreeNode p = iter.next();
            TreeNode q = descIter.next();
            if (!(p == q || (p != null && q != null && p.val == q.val))) {
                return false;
            }
        }
        return true;
    }
}
