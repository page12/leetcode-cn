package p01;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/binary-tree-postorder-traversal/
 */
public class Problem0145 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(3);
        System.err.println(new Problem0145().postorderTraversal(root));

        root = new TreeNode(3);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(1);
        System.err.println(new Problem0145().postorderTraversal(root));

        root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.right = new TreeNode(4);
        root.right = new TreeNode(3);
        root.right.left = new TreeNode(5);
        root.right.right = new TreeNode(6);
        System.err.println(new Problem0145().postorderTraversal(root));
    }

    /**
     * 下面的代码是参考前序中序，跟着入栈出栈图调出来的，迭代方式的后序算法比前序和中序难理解
     * 使用递归好理解，或者改进前序（根左右变成根右左），然后反转结果也可以得到一样的结果
     */
    public List<Integer> postorderTraversal(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }
        List<Integer> result = new ArrayList<>();
        LinkedList<TreeNode> stack = new LinkedList<>();
        // 添加指针来判断是否遍历了右叶子
        TreeNode lastTraversalNode = null;
        while (!stack.isEmpty() || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.peek();
            if (root.right != null && root.right != lastTraversalNode) {
                root = root.right;
            } else {
                stack.pop();
                result.add(root.val);
                lastTraversalNode = root;
                // 后序遍历的过程中在遍历完左叶子和右叶子时都会返回根节点
                // 这里设置为null避免再度进入遍历左叶子的while循环
                root = null;
            }
        }
        return result;
    }
}
