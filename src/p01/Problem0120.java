package p01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/triangle/
 */
public class Problem0120 {

    public static void main(String[] args) {
        Problem0120 p = new Problem0120();
        List<List<Integer>> triangle = new ArrayList<>();
        triangle.add(Arrays.asList(2));
        triangle.add(Arrays.asList(3, 4));
        triangle.add(Arrays.asList(6, 5, 7));
        triangle.add(Arrays.asList(4, 1, 8, 3));
        System.err.println(p.minimumTotal(triangle));
    }

    /**
     * 思路：
     * 这一看就是动态规划
     * 当一个点向下走时，有左右两种选择
     * 走左边时，在数组中的下标不变
     * 走右边时，在数组中的下标加1
     * 设最短路径时第x层下标为i，最短路径为f(x,i)，下标从0开始，则有下面三种情况
     * 当i是左边界下标时，说明向下走时选择的是左边，此时f(x,i) = f(x-1,i) + triangle[x][i]
     * 当i是右边界下标时，说明向下走时选择的是右边，此时f(x,i) = f(x-1,i-1) + triangle[x][i]
     * 当i不是边界下标是，走两条路径中较短的那一条，此时f(n) = min{ f(x-1,i) + triangle[x][i], f(x-1,i-1) + triangle[x][i] }
     * 初始值f(0,0) = 顶点的值
     * 利用自底向上即可到达最后一行的n个点时，各自的最短路径
     * 因为最后一行所有点都可以是终点，因此还要再筛选它们中的最小值，这个最小值就是最终结果
     */
    public int minimumTotal(List<List<Integer>> triangle) {
        // 题目有假设，不做校验
        // 使用滚动数组优化
        int[] temp = new int[triangle.size()];
        // 左对齐后对角的值，读取f(x-1,i-1)时会用到它，记得更新
        int diagonal = 0;
        for (int x = 0; x < triangle.size(); x++) {
            List<Integer> line = triangle.get(x);
            for (int i = 0; i < line.size(); i++) {
                int current = line.get(i);
                if (x == 0) {
                    temp[i] = line.get(0);
                } else if (i == 0) {
                    diagonal = temp[i];
                    temp[i] = temp[i] + current;
                } else if (i == line.size() - 1) {
                    temp[i] = diagonal + current;
                } else {
                    int nextDiagonal = temp[i];
                    temp[i] = Math.min(temp[i] + current, diagonal + current);
                    diagonal = nextDiagonal;
                }
            }
//            System.err.println(Arrays.toString(temp));
        }
        // 最后一行所有点都可以是终点，因此还要再筛选它们中的最小值
        int result = Integer.MAX_VALUE;
        for (int i = 0; i < temp.length; i++) {
            result = Math.min(temp[i], result);
        }
        return result;
    }
}
