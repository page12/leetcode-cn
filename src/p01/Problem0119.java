package p01;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/pascals-triangle-ii/
 */
public class Problem0119 {

    public static void main(String[] args) {

    }

    /**
     * 思路：同第118题，注意行数比第118题小1，是从0开始的
     */
    public List<Integer> getRow(int rowIndex) {
        if (rowIndex == 0) {
            List<Integer> row = new ArrayList<>();
            row.add(1);
            return row;
        } else if (rowIndex == 1) {
            List<Integer> row = new ArrayList<>();
            row.add(1);
            row.add(1);
            return row;
        }
        List<Integer> last = new ArrayList<>(rowIndex + 1);
        last.add(1);
        last.add(1);
        for (int i = 2; i <= rowIndex; i++) {
            last = getCurrentByLast(last);
        }
        return last;
    }

    /**
     * 根据上一行的结果求当前行的结果
     */
    private List<Integer> getCurrentByLast(List<Integer> last) {
        List<Integer> current = new ArrayList<>(last.size() + 1);
        current.add(1);
        for (int i = 1; i < last.size(); i++) {
            current.add(last.get(i - 1) + last.get(i));
        }
        current.add(1);
        return current;
    }
}
