package p01;

/**
 * https://leetcode-cn.com/problems/excel-sheet-column-title/
 */
public class Problem0168 {

    public static void main(String[] args) {
        Problem0168 p = new Problem0168();
        System.err.println(p.convertToTitle(1).equals("A"));
        System.err.println(p.convertToTitle(26).equals("Z"));
        System.err.println(p.convertToTitle(27).equals("AA"));
        System.err.println(p.convertToTitle(28).equals("AB"));
        System.err.println(p.convertToTitle(52).equals("AZ"));
        System.err.println(p.convertToTitle(701).equals("ZY"));
        System.err.println(p.convertToTitle(2147483647).equals("FXSHRXW"));
    }

    /**
     * 思路：循环整除操作，余数是当前位的
     * 需要注意一点：结果里面是没有0的，用的是Z = 26代替，因此需要特殊处理没有余数为0的情况，此时商数要减1
     */
    public String convertToTitle(int columnNumber) {
        // 题目有假设，columnNumber >= 1
        StringBuilder builder = new StringBuilder();
        while (columnNumber != 0) {
            int remainder = columnNumber % 26;
            builder.append(getChar((remainder)));
            columnNumber = columnNumber / 26;
            // 余数为0，用Z = 26代替，商数减1代表减去了Z = 26
            if (remainder == 0) {
                columnNumber--;
            }
        }
        return builder.reverse().toString();
    }

    /**
     * 0 - 25 转换成 A - Z
     */
    private char getChar(int n) {
        if (n == 0) {
            return 'Z';
        }
        return (char) ('A' + n - 1);
    }
}
