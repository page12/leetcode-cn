package p01;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/binary-tree-preorder-traversal/
 */
public class Problem0144 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(3);
        System.err.println(new Problem0144().preorderTraversal(root));

        root = new TreeNode(3);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(1);
        System.err.println(new Problem0144().preorderTraversal(root));
    }

    /**
     * 思路：跟着递归算法的入栈出栈图来写，参考了中序算法第94题，和那题的代码就一行代码位置的差别
     * 注意前序因为是先根，每个节点都是根，所以节点入栈时就算是遍历了
     * 整体顺序为：入根（遍历） -> 入左（遍历） -> 出左 -> 出根 -> 入右（遍历） -> 出右
     * 因此可以得到前序遍历的入栈顺序就是遍历顺序（这里不严格），迭代算法就这一点和中序不一样
     */
    public List<Integer> preorderTraversal(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }
        List<Integer> result = new ArrayList<>();
        LinkedList<TreeNode> stack = new LinkedList<>();
        while (!stack.isEmpty() || root != null) {
            while (root != null) {
                stack.push(root);
                // 就这个遍历操作的地方和中序不一样，前序是入栈时就相当于遍历了
                result.add(root.val);
                root = root.left;
            }
            root = stack.pop();
            root = root.right;
        }
        return result;
    }
}
