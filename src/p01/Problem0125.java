package p01;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/valid-palindrome/
 */
public class Problem0125 {

    public static void main(String[] args) {
        System.err.println(new Problem0125().isPalindrome("A man, a plan, a canal: Panama"));
        System.err.println(new Problem0125().isPalindrome("race a car"));
        System.err.println(new Problem0125().isPalindrome("    "));
        System.err.println(new Problem0125().isPalindrome(""));
        System.err.println(new Problem0125().isPalindrome("a"));
    }

    /**
     * 思路：因为只考虑字母和数字字符，可以忽略字母的大小写，所以可以开辟空间来存储处理后的字符串，再判断其是否回文
     * 时间复杂度O(n)，空间复杂度O(n)，n为字符串长度
     */
    public boolean isPalindrome(String s) {
        // 题目有假设，不考虑null
        if (s.length() == 0) {
            return true;
        }
        List<Character> chars = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if ('0' <= c && c <= '9') {
                chars.add(c);
            } else if ('A' <= c && c <= 'Z') {
                // 大写转小写
                chars.add((char) (c + 32));
            } else if ('a' <= c && c <= 'z') {
                chars.add((char) (c));
            }
        }
        if (chars.isEmpty()) {
            return true;
        }
        // 判断是否回文
        for (int i = 0; i < chars.size() >>> 1; i++) {
            if (chars.get(i) != chars.get(chars.size() - 1 - i)) {
                return false;
            }
        }
        return true;
    }
}
