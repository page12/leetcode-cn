package p01;

/**
 * https://leetcode-cn.com/problems/number-of-1-bits/
 */
public class Problem0191 {

    public static void main(String[] args) {

    }

    /**
     * 思路：Java内置这个方法Integer.bitCount(int i)，方法的实现是直接套用 Hacker's Delight 这本书，是效率最高的方法
     * 不过这个方法比较难懂，看那本书的上还有其他的方法实现bitCount，一样难懂
     * 其他思路：一个是循环逐位判断（这种方法比较直观简单，代码不写了），一个是 n & (n-1)（下面的hammingWeight2）
     */
    public int hammingWeight(int n) {
        // 内置方法Integer.bitCount(n);
        // HD, Figure 5-2
        n = n - ((n >>> 1) & 0x55555555);
        n = (n & 0x33333333) + ((n >>> 2) & 0x33333333);
        n = (n + (n >>> 4)) & 0x0f0f0f0f;
        n = n + (n >>> 8);
        n = n + (n >>> 16);
        return n & 0x3f;
    }

    /**
     * 下面是n & (n-1)算法
     * 设n最右边的bit=1的位为第x位（从右开始，起始为1）
     * 那么n - 1相当于把x位变为0，小于x的所有位变为1
     * 根据上面的规定n的x位为1，小于x的所有位为0
     * 所以n & (n-1)的结果就是n的二进制中小于等于x的所有位全部变为0
     * n的x位为1，n & (n-1)的结果等价于把n的最右边bit=1的位（第x位）变为0
     * 循环这个过程不断令n = n & (n-1)，直到n == 0，循环次数就是bit=1的次数
     */
    public int hammingWeight2(int n) {
        int bitCount = 0;
        while (n != 0) {
            n = n & (n - 1);
            bitCount++;
        }
        return bitCount;
    }
}
