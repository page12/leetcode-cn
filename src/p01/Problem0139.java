package p01;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/word-break/
 */
public class Problem0139 {

    public static void main(String[] args) {
        Problem0139 p = new Problem0139();
        System.err.println(p.wordBreak("leetcode", Arrays.asList("leet", "code")));
        System.err.println(p.wordBreak("applepenapple", Arrays.asList("apple", "pen")));
        System.err.println(p.wordBreak("catsandog", Arrays.asList("cats", "dog", "sand", "and", "cat")));
    }

    /**
     * 思路：
     * 设分割次数最多的结果为 (A1,A2,A3,An)，其中A为字典中的单词
     * 那么对分割后的单词组进行拆分，比如 (A1,A2,A3) 和 (An)，则An是单词表中的单词，(A1,A2,A3)是可以被拆分的
     * 设 (A1,A2,A3) = s(0,i)，字符串长度为n，则(An) = s(i+1, n-1)
     * 设字符串的前n个字符能够拆分等价于 f(n-1) = true
     * 则 f(n-1) = f(i) && wordDict.contains(s(i+1, n-1))
     * f(i)还可以继续用这种方式拆分
     */
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> set = new HashSet<>(wordDict);
        boolean[] dpTable = new boolean[s.length()];
        for (int i = 0; i < s.length(); i++) {
            for (int j = i; j < s.length(); j++) {
                // 代表前面j个字符可拆分，可以不管它们了
                if (dpTable[j]) {
                    continue;
                }
                if (i == 0) {
                    dpTable[j] = set.contains(s.substring(i, j + 1));
                } else {
                    dpTable[j] = dpTable[i - 1] && set.contains(s.substring(i, j + 1));
                }
            }
            // 有一种拆分方式就返回true
            if (dpTable[s.length() - 1]) {
                return true;
            }
        }
        return false;
    }
}
