package p01;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/
 */
public class Problem0167 {

    public static void main(String[] args) {
        Problem0167 p = new Problem0167();
        System.err.println(Arrays.toString(p.twoSum(new int[]{2, 7, 11, 15}, 9)));
        System.err.println(Arrays.toString(p.twoSum(new int[]{2, 3, 4}, 6)));
        System.err.println(Arrays.toString(p.twoSum(new int[]{-1, 0}, -1)));
    }

    /**
     * 思路：简单的收尾双指针
     * 双指针的和小于target时，head++
     * 大于target时，tail++
     * 等于时，直接返回结果
     */
    public int[] twoSum(int[] numbers, int target) {
        int[] result = new int[]{-1, -1};
        int head = 0;
        int tail = numbers.length - 1;
        while (head != tail) {
            int currentSum = numbers[head] + numbers[tail];
            if (currentSum > target) {
                tail--;
            } else if (currentSum < target) {
                head++;
            } else {
                result[0] = head + 1;
                result[1] = tail + 1;
                return result;
            }
        }
        return result;
    }
}
