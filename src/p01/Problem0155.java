package p01;

import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/min-stack/
 */
public class Problem0155 {

    public static void main(String[] args) {

    }

    /**
     * 思路：对栈元素进行再次封装，入栈时同时此时的最小值，因为栈只能访问栈顶元素，所以只需要每次同栈顶元素比较大小
     * 其他思路：利用第二个栈minStack来实现getMin(),元素入栈时，比较它和minStack栈顶元素,如果大于则同时入栈minStack，否则不操作minStack
     * 两者思路空间花费都差不多，不计算其他开销都额外需要O(N)空间
     * 下面代码图简单直接用LinkedList的栈操作了
     */
    static class MinStack {

        LinkedList<Node> stack;

        /**
         * initialize your data structure here.
         */
        public MinStack() {
            stack = new LinkedList<>();
        }

        public void push(int val) {
            if (stack.isEmpty()) {
                stack.push(new Node(val, val));
            } else {
                Node top = stack.peek();
                stack.push(new Node(val, Math.min(val, top.currentMin)));
            }
        }

        public void pop() {
            stack.pop();
        }

        public int top() {
            Node top = stack.peek();
            // stack空时自动拆箱会NPE，题目不考虑
            return top == null ? null : top.val;
        }

        public int getMin() {
            Node top = stack.peek();
            // stack空时自动拆箱会NPE，题目不考虑
            return top == null ? null : top.currentMin;
        }

        private static class Node {
            int val;
            int currentMin;

            Node(int val, int currentMin) {
                this.val = val;
                this.currentMin = currentMin;
            }
        }
    }
}
