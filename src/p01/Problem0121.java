package p01;

/**
 * https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock/
 */
public class Problem0121 {

    public static void main(String[] args) {
        System.err.println(new Problem0121().maxProfit(new int[]{9, 5, 7, 4, 2, 4, 1, 6, 4}));
        System.err.println(new Problem0121().maxProfit(new int[]{5, 4, 3, 2, 1}));
        System.err.println(new Problem0121().maxProfit(new int[]{70, 80, 0, 50, -20, 40}));
    }

    /**
     * 思路：想了好一会
     * 几个要点：
     * 1、卖出时间 > 买入时间
     * 2、买入金额 < 卖出金额
     * 3、为了使利益最大化，买入点是一段区间内的极小值，卖出点是一段区间内的极大值
     * 4、为了使利益最大化，买入的金额一定是卖出时间之前最小的那一个金额
     * 按顺序找到所有极大值和极小值
     * 先取第一个极小值min与其紧跟的那一个极大值max的差值，利润 = max - min
     * 如果这不是最大利润，那么只有如下情况：
     * 要么最大利润的极小值小于min，要么最大利润的极大值大于max，要么两者都有
     * 其余情况不许更改max和min
     * 更改max和min后计算一次利润，如果比之前计算的利润大，就保持这个新的利润
     * 这样迭代完成后保存的值的就是最大的利润，也就是要求的结果
     * 特别要注意第1点，如果极小值的下标大于极大值的下标，代表买入时间大于卖出时间，此时极大值无效，要重置max
     */
    public int maxProfit(int[] prices) {
        // 题目有假设，不做校验
        if (prices.length == 1) {
            return 0;
        }
        // 初始化成题目范围外的数值，第一次赋值时不要进行额外判断
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        int min = Integer.MAX_VALUE;
        int minIndex = -1;
        int result = 0;
        for (int i = 0; i < prices.length; i++) {
            // 发现极小值
            if ((i == 0 && prices[i] <= prices[i + 1])
                    || (i != 0 && i != prices.length - 1 && prices[i] < prices[i + 1] && prices[i] <= prices[i - 1])) {
                if (min > prices[i]) {
                    min = prices[i];
                    minIndex = i;
                }
            }
            // 发现极大值
            if ((i == prices.length - 1 && prices[i] >= prices[i - 1])
                    || (i != 0 && i != prices.length - 1 && prices[i] >= prices[i + 1] && prices[i] > prices[i - 1])) {
                if (max < prices[i]) {
                    max = prices[i];
                    maxIndex = i;
                }
            }
            // minIndex > maxIndex时不符合要求
            if (minIndex < maxIndex) {
                if (result < (max - min)) {
                    result = max - min;
                }
            } else {
                max = Integer.MIN_VALUE;
                maxIndex = -1;
            }
        }
        return result;
    }
}
