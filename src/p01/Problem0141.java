package p01;

import common.ListNode;

/**
 * https://leetcode-cn.com/problems/linked-list-cycle/
 */
public class Problem0141 {

    public static void main(String[] args) {

    }

    /**
     * 思路：快慢双指针遍历法
     * 快指针pFast每次走两步，慢指针pSlow每次走一步
     * 如果没环，那么快指针一自己先到达终点 pFast = null
     * 如果有环，那么它一定能够在某个地方遇到慢指针 pFast = pSlow
     */
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        ListNode pFast = head;
        ListNode pSlow = head;
        while (pFast != null) {
            // 快指针一次走两步
            pFast = pFast.next;
            if (pFast == null) {
                break;
            }
            pFast = pFast.next;
            // 慢指针一次走一步
            pSlow = pSlow.next;
            // 指针相遇即代表有环
            if (pFast == pSlow) {
                return true;
            }
        }
        return false;
    }
}
