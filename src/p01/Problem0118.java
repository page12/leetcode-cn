package p01;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/pascals-triangle/
 */
public class Problem0118 {

    public static void main(String[] args) {
        System.err.println(new Problem0118().generate(6));
    }

    /**
     * 思路：比较简单，杨辉三角第n层有n个元素，利用上层可以很直接求出下层
     * 其他的，比如直接组合数公式也行
     */
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 1; i <= numRows; i++) {
            if (i == 1) {
                List<Integer> row = new ArrayList<>();
                row.add(1);
                result.add(row);
            } else if (i == 2) {
                List<Integer> row = new ArrayList<>();
                row.add(1);
                row.add(1);
                result.add(row);
            } else {
                result.add(getCurrentByLast(result.get(i - 2)));
            }
        }
        return result;
    }

    /**
     * 根据上一行的结果求当前行的结果
     */
    private List<Integer> getCurrentByLast(List<Integer> last) {
        List<Integer> current = new ArrayList<>(last.size() + 1);
        current.add(1);
        for (int i = 1; i < last.size(); i++) {
            current.add(last.get(i - 1) + last.get(i));
        }
        current.add(1);
        return current;
    }
}
