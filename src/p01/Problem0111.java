package p01;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/minimum-depth-of-binary-tree/
 */
public class Problem0111 {

    public static void main(String[] args) {

    }

    /**
     * 思路：和最大深度类似
     * 重要区别是需要特殊处理左右子树深度为0（没有左子树或者没有右子树的情况）
     * 因为对于数字0和其他正数用min比较会得到0，此时实际上没有子树，也就不应该把0当做深度
     * 最大深度用max，在某次比较重一定会丢掉这个没有实际意义的0，所以不需要特殊处理
     */
    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftDepth = minDepth(root.left);
        int rightDepth = minDepth(root.right);
        // 没有左子树时返回右子树的高度
        if (leftDepth == 0) {
            leftDepth = rightDepth;
        }
        // 没有右子树时返回左子树的高度
        if (rightDepth == 0) {
            rightDepth = leftDepth;
        }
        return Math.min(leftDepth, rightDepth) + 1;
    }
}
