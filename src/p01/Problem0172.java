package p01;

/**
 * https://leetcode-cn.com/problems/factorial-trailing-zeroes/
 */
public class Problem0172 {

    public static void main(String[] args) {

    }

    /**
     * 思路：2! = 2，根据阶乘性质，从2开始的阶乘都是偶数
     * 因为2 * 5 = 10，质数里面也只有2*5会在末尾产生0
     * 根据这点性质，把n的阶乘进行质因数分解表示，统计其中有多少个2*5就行
     * 例如10! = 1*2*3*4*5*6*7*8*9*10 = 1*2*3*(2*2)*5*(2*3)*7*(2*2*2)*(3*3)*(2*5)，这其中有两个2*5（根据乘法交换律）
     * 由质因数分解表示形式可知，2*5的个数等于min(2的个数，5的个数)
     * 根据阶乘性质，每隔一位就会乘以一次偶数，因此2的个数大于5的个数（结论是对的，没给出严格数学证明）
     * 所以我们只需要统计质因数分解表示形式中5的个数，也就是只需要计算其中5的倍数的质因数分解中总共有多少个5
     * <p>
     * 根据上面的分析，一个简单的算法就是（下面说明中的log5(i)表示以5为底的对数）
     * int sum = 0;
     * for i in {5, 10, 15, 20} {sum += log5(i)向下取整}
     * return sum;
     * 这是个O(n)的算法（n/5还是线性）
     * 因为一些编程语言不直接支持log5操作，计算log5会有误差，此方法不是特别实用
     * <p>
     * 要进一步优化到O(logn)，则需要观察5的倍数中5的个数出现的规律（不是质因数分解，只关注乘数因子5），如下
     * 5 = 5
     * 10 = 5 * 2
     * 15 = 5 * 3
     * 20 = 5 * 4
     * 25 = 5 * 5
     * 30 = 5 * 6
     * ...
     * 50 = 5 * 5 * 2
     * ...
     * ...
     * 120 = 5 * 24
     * 125 = 5 * 5 * 5
     * 130 = 5 * 26
     * ...
     * 观察上面的等式的右边（为了对齐左边补了空格），用*划分列
     * 则第一列的5的个数为 n/5，第二列的5的个数为 n/5，第三列的5的个数为 n/125
     * 类推出第i列的5的个数为 n/(5^i)（这里是指数操作，不是位运算操作）
     * 总共的5的个数 = n/5 + n/5 + n/125 + ... + n/(5^i)
     * 上面公式中i = log5(n) 向下取整
     * 对数计算不方便，可以用它的逆运算指数运算计算i，也就是求一个整数i，使得5^i是小于等于n的最大正整数
     * 整个循环累加的次数是log5(n)，根据换底公式时间复杂度是O(logn)
     */
    public int trailingZeroes(int n) {
        int x = 5;
        int result = 0;
        // 循环次数就是 log5(n) 向下取整
        while (x <= n) {
            result += n / x;
            x = x * 5;
        }
        return result;
    }
}
