package p01;

/**
 * https://leetcode-cn.com/problems/reverse-bits/
 */
public class Problem0190 {

    public static void main(String[] args) {

    }

    /**
     * 思路：Java内置这个方法Integer.reverse(int i)，方法的实现是直接套用 Hacker's Delight 这本书，是效率最高的方法
     * 不过这个方法比较难懂，看那本书的上还有其他的方法实现reverse，一样难懂
     * 其他思路：跟颠倒字符串一样，循环一半长度逐个交换，这个最容易懂
     */
    public int reverseBits(int n) {
        // 内置方法Integer.reverse(n)
        // HD, Figure 7-1
        n = (n & 0x55555555) << 1 | (n >>> 1) & 0x55555555;
        n = (n & 0x33333333) << 2 | (n >>> 2) & 0x33333333;
        n = (n & 0x0f0f0f0f) << 4 | (n >>> 4) & 0x0f0f0f0f;
        n = (n << 24) | ((n & 0xff00) << 8) |
                ((n >>> 8) & 0xff00) | (n >>> 24);
        return n;
    }
}
