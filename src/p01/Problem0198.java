package p01;

/**
 * https://leetcode-cn.com/problems/house-robber/
 */
public class Problem0198 {

    public static void main(String[] args) {
        Problem0198 p = new Problem0198();
        System.err.println(p.rob(new int[]{1, 2, 3, 1}) == 4);
        System.err.println(p.rob(new int[]{2, 7, 9, 3, 1}) == 12);
    }

    /**
     * 思路：
     * 不能走相邻的点，因此，只能间隔1个点或者间隔2个点走
     * 为什么不能间隔3个点？
     * 因为间隔3个点可以中间再加一步，变成间隔1个点，再间隔一个点，这样就多了一次累加
     * 设下标为i，数组长度为n，下标从0开始
     * 设跳到i后的最大金额为f(i)，很容易得到下面的递推公式
     * f(i) = max{f(i-2), f(i-3)} + nums[i]
     * 初始点f(0) = nums[0]，f(1) = nums[1]
     * 当跳到 n-2 这个点时，不能再跳到 n-1 了，这表明 n-2 是另外一个终点
     * 因此最终结果是 max{ f(n-2), f(n-1) }
     * 这个动态规划只有一个参数，需要额外保存前面三个的值（公式里面没有f(i-1)，但是计算i+1时需要它）
     * 因此用自底向上的循环的方式可以得到O(N)时间复杂度 + O(1)空间复杂度的算法
     */
    public int rob(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }
        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        if (nums.length == 3) {
            return Math.max(nums[0] + nums[2], nums[1]);
        }
        int minusThree = nums[0];
        int minusTwo = nums[1];
        int minusOne = nums[0] + nums[2];
        for (int i = 3; i < nums.length; i++) {
            int current = Math.max(minusThree, minusTwo) + nums[i];
            minusThree = minusTwo;
            minusTwo = minusOne;
            minusOne = current;
        }
        return Math.max(minusTwo, minusOne);
    }
}
