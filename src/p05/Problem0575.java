package p05;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/distribute-candies/
 */
public class Problem0575 {

    public static void main(String[] args) {
        Problem0575 p = new Problem0575();
        System.err.println(p.distributeCandies(new int[]{1, 1, 2, 2, 3, 3}) == 3);
        System.err.println(p.distributeCandies(new int[]{1, 1, 2, 3}) == 2);
    }

    /**
     * 思路：不要想太复杂了，实际情况很简单
     * 假设n个糖果中总共有m个种类，其中n是偶数
     * 妹妹一定分得n/2个糖果
     * 要使得这n/2中糖果种类最大
     * 就要考虑m的大小
     * 如果 m >= n/2，则可以把 n/2 种糖果各分1个给妹妹，所以妹妹分得的最大种类就是 n/2
     * 如果 m < n/2，则可以先把m种糖果各分1个给妹妹，再分 n/2 - m 个给妹妹，这后分的n/2 - m一定是重复种类的糖果，所以最大种类就是 m
     * 因此最终结果就是 m >= n/2 ? n/2 : m = min(n/2, m)
     * 题目转化为了求m，也就是求n个数中右多少种不同的数
     * 利用hash表进行去重处理能够很快得到m
     * 这么做时间复杂度为O(N)，空间复杂度也为O(N)
     */
    public int distributeCandies(int[] candyType) {
        // 题目有假设数组长度大于等于2
        // 利用hashSet求m
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < candyType.length; i++) {
            set.add(candyType[i]);
        }
        int n = candyType.length;
        int m = set.size();
        return Math.min(n / 2, m);
    }
}
