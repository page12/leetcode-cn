package p05;

/**
 * https://leetcode-cn.com/problems/reshape-the-matrix/
 */
public class Problem0556 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 先判断能不能reshape
     * 如果可以，则需要进行坐标转换，即把newMat的坐标转换为mat的坐标
     */
    public int[][] matrixReshape(int[][] mat, int r, int c) {
        int originRowNum = mat.length;
        int originColumnNum = mat[0].length;
        int total = originRowNum * originColumnNum;
        if (r * c != total) {
            return mat;
        }
        int[][] newMat = new int[r][c];
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                // 坐标转换
                int currentIdx = c * i + j + 1;
                int oi = (currentIdx - 1) / originColumnNum;
                int oj = (currentIdx - 1) % originColumnNum;
                newMat[i][j] = mat[oi][oj];
            }
        }
        return newMat;
    }
}
