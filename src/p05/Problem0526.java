package p05;

/**
 * https://leetcode-cn.com/problems/beautiful-arrangement/
 */
public class Problem0526 {

    public static void main(String[] args) {
        Problem0526 p = new Problem0526();
//        System.err.println(p.countArrangement(2) == 2);
        System.err.println(p.countArrangement(3));
    }

    /**
     * 思路：
     * 利用一个带回溯的递归，同时另一个数组used[]记录是否被使用
     * 向下递归时，标记下已经被使用的元素
     * 回溯时，重新标记为未被使用
     * 设排列后的数组为 a = int[n+1]，下标0的元素可以不管
     * 这数组只在尾部操作，因此可以当作回溯用的栈
     * 判断一个元素是否能被使用
     * 据题目的两个条件即可
     * 两个条件是
     * a[i] % i == 0
     * i % a[i] == 0
     * 本题中只需要知道所有排列情况，不需要知道排列内容，因此数组a可以和used数组合并成一个数组
     */
    public int countArrangement(int n) {
        // 题目有假设，不做校验
        boolean[] used = new boolean[n + 1];
        return recurse(used, 1, n);
    }

    /**
     * 递归求解
     *
     * @param used 记录某个数字是否被使用
     * @param i    当前是第i个数
     * @param n    排列的最大长度
     * @return 排列数量
     */
    private int recurse(boolean[] used, int i, int n) {
        if (i > n) {
            // 此处表示已经完成了一种排列情况
            return 1;
        }
        int result = 0;
        for (int num = 1; num <= n; num++) {
            if (used[num] || (num % i != 0 && i % num != 0)) {
                continue;
            }
            // 向下递归时标记为已使用
            used[num] = true;
            result += recurse(used, i + 1, n);
            // 回溯时标记为未使用
            used[num] = false;
        }
        return result;
    }
}
