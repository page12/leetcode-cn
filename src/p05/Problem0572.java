package p05;

import common.TreeNode;

import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/subtree-of-another-tree/
 */
public class Problem0572 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 二叉树的问题基本都可以用递归去判断
     * subRoot是root的子树，有三种情况
     * 1、subRoot和root完全一样
     * 2、subRoot是root.left的子树
     * 3、subRoot是root.right的子树
     * 因此问题转换为怎样判断完全一样，这个也可以用递归去判断
     * 树s和树t一样，表示它们满足这三个条件
     * 1、s和t的根节点的值一样
     * 2、s的左子树和t的左子树一样
     * 3、s的右子树和根的右子树一样
     * <br/>
     * 两个递归是嵌套的，和嵌套循环类似，所以时间复杂度应该相乘，为O(N * M)，N M为两棵树的节点总数空间复杂度应该相加
     * 空间复杂度取决于栈的深度，因为两棵树任意一颗都会遍历到底，而且只要有一棵树遍历到底就会进行出栈操作，所以空间复杂度为两棵树深度的最大值
     * <br/>
     * 另一个思路是按照先序遍历（深度优先）的方式遍历两棵树
     * 得到它们的遍历序列sRoot和sSubRoot
     * 如果sRoot.contains(sSubRoot)则代表是子树
     * 但是这种方法是错误的！
     * 它不能区别示例2这种情况：sRoot = 341205，sSubRoot = 412，都有412，但是其中的2这个节点是有区别的
     * 出现错误是因为，先序遍历序列中右边相邻的节点可能是左边相邻的节点的子节点，也可能不是，看不出两个2的区别
     * 这两个2的区别是：s的2有左子树，t的2没有左子树
     * 推广为一般情况就是，它们的左右子树有差别
     * 因此只要先序遍历序列中能体现每个节点左右子树，就能够区分这两个节点
     * <br/>
     * 一个简单的方法就是改变每个节点在遍历序列的toString()方法
     * 例如，把t的2这个节点的toString改为TreeNode(2, null, null)，对应的4这个节点的toString()改为TreeNode(4, 1, 2)
     * <br/>
     * 这种简单的改toString()的方法，适用本题的情况，但是并不能区分树中所有的节点
     * 比如这种树：1是根节点，每个1的左是2，每个1的右是3，每个2的左是1，每个3的右是1
     * 这里面的除了叶子节点之外，123的toString()都是一样的，区分不了单个节点
     * 本题的subRoot是一棵树，它的这种toString()的先序遍历序列一定包含null，就不算是单个节点了，而应该视为整体来区分
     * <br/>
     * 改toString()方法，只需要分别遍历两棵树各一次，然后是字符串查找，所以时间复杂度为O(N + M)，N M为两棵树的节点总数
     * 需要额外空间来存储遍历序列的字符串，这需要kN + kM，k为toString时字符串变长的的倍数，所以空间复杂度为O(N + M)
     * <br/>
     * 虽然时间复杂度比递归低（递归是乘积），但是提交起来递归要快很多
     * 可能有两个原因：1、递归算法简单，编译器和CPU好优化递归代码；2、测试用例情况覆盖不全
     */
    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        // 这是递归思路的代码
        // 虽然题目假设了root subRoot非空，但是因为会递归调用，所以还是要判断下
        // 这里判断null还能提前结束递归
        if (root == null) {
            return false;
        }
        if (subRoot == null) {
            return true;
        }
        return isSameTree(root, subRoot) || isSubtree(root.left, subRoot) || isSubtree(root.right, subRoot);
    }

    /**
     * 判断是否是相同的tree
     */
    private boolean isSameTree(TreeNode s, TreeNode t) {
        if (s == null && t == null) {
            return true;
        }
        if (s == null || t == null) {
            return false;
        }
        if (s.val != t.val) {
            return false;
        }
        return isSameTree(s.left, t.left) && isSameTree(s.right, t.right);
    }

    public boolean isSubtree2(TreeNode root, TreeNode subRoot) {
        // 利用先序遍历序列，改toString的思路的代码
        // 题目有假设非空
        return preOrderString(root).contains(preOrderString(subRoot));
    }

    /**
     * 求先序遍历序列
     */
    private String preOrderString(TreeNode root) {
        StringBuilder builder = new StringBuilder();
        LinkedList<TreeNode> stack = new LinkedList<>();
        while (!stack.isEmpty() || root != null) {
            while (root != null) {
                append(root, builder);
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            root = root.right;
        }
        return builder.toString();
    }

    /**
     * 改toString()的内容并添加到先序遍历序列中去，node != null，
     */
    private void append(TreeNode node, StringBuilder builder) {
        String leftVal = node.left == null ? null : String.valueOf(node.left.val);
        String rightVal = node.right == null ? null : String.valueOf(node.right.val);
        builder.append("TreeNode(")
                .append(node.val)
                .append(",")
                .append(leftVal)
                .append(",")
                .append(rightVal)
                .append(")");
    }
}
