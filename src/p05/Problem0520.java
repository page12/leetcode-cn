package p05;

/**
 * https://leetcode-cn.com/problems/detect-capital/
 */
public class Problem0520 {

    public static void main(String[] args) {
        Problem0520 p = new Problem0520();
        System.err.println(p.detectCapitalUse("USA"));
        System.err.println(p.detectCapitalUse("leetcode"));
        System.err.println(p.detectCapitalUse("Google"));
        System.err.println(p.detectCapitalUse("FlaG"));
        System.err.println(p.detectCapitalUse("aB"));
        System.err.println(p.detectCapitalUse("a"));
        System.err.println(p.detectCapitalUse("B"));
    }

    /**
     * 思路：比较简单，一次遍历就能实现
     * 记录下前一个字母和当前字母的大小写
     * 循环遍历比较下，如果大小写不同则不是正确的用法
     * 特殊情况：如果第一个字母是大写，则要跳过第一个字母和第二个字母的比较
     */
    public boolean detectCapitalUse(String word) {
        // 题目有假设，不做校验
        // 第一个字母是否大写
        boolean firstUpper = Character.isUpperCase(word.charAt(0));
        // 前一个字母是否大写
        boolean prevUpper = firstUpper;
        // 当前前字母是否大写
        boolean currentUpper;
        for (int i = 1; i < word.length(); i++) {
            currentUpper = Character.isUpperCase(word.charAt(i));
            // 如果第一个字母是大写，则跳过第一个字母和第二个字母的比较
            if ((!firstUpper || i != 1) && prevUpper ^ currentUpper) {
                return false;
            }
            prevUpper = currentUpper;
        }
        return true;
    }
}
