package p05;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/minimum-index-sum-of-two-lists/
 */
public class Problem0599 {

    public static void main(String[] args) {
        Problem0599 p = new Problem0599();
        System.err.println(Arrays.toString(p.findRestaurant(new String[]{"Shogun", "Tapioca Express", "Burger King", "KFC"},
                new String[]{"Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"})));
        System.err.println(Arrays.toString(p.findRestaurant(new String[]{"Shogun", "Tapioca Express", "Burger King", "KFC"},
                new String[]{"KFC", "Shogun", "Burger King"})));
        System.err.println(Arrays.toString(p.findRestaurant(new String[]{"Shogun", "Piatti", "Tapioca Express", "Burger King", "KFC"},
                new String[]{"Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"})));
    }

    /**
     * 思路：
     * 利用hash表计算下标和
     * 先遍历list1，hash表的key为餐馆名字，value为list1下标加1再取反，取反变成负数用于标识这个餐馆不是共同喜爱的餐馆
     * 再遍历list2，如果餐馆已经在hash表中，则下标取反（之前的下标是负数存放）加上list2的下标，这个遍历能同时得到最小下标和以及最小下标和出现的次数
     * 再遍历一次hash表就能得到最后结果
     * 时间复杂度O(N + M)，空间复杂度O(min(N, M))
     */
    public String[] findRestaurant(String[] list1, String[] list2) {
        String[] smaller;
        String[] bigger;
        if (list1.length < list2.length) {
            smaller = list1;
            bigger = list2;
        } else {
            smaller = list2;
            bigger = list1;
        }
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < smaller.length; i++) {
            map.put(smaller[i], -(i + 1));
        }
        // 求最小下标和以及次数
        // 题目有假定范围，所以可以这样初始化min，否则使用包装类更好
        int min = Integer.MAX_VALUE;
        int minCount = 0;
        for (int i = 0; i < bigger.length; i++) {
            if (map.containsKey(bigger[i])) {
                int sum = -map.get(bigger[i]) + i;
                map.put(bigger[i], sum);
                if (sum == min) {
                    minCount++;
                } else if (sum < min) {
                    min = sum;
                    minCount = 1;
                }
            }
        }
        String[] result = new String[minCount];
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (min == entry.getValue()) {
                result[--minCount] = entry.getKey();
            }
        }
        return result;
    }
}
