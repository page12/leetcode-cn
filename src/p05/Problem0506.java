package p05;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/relative-ranks/
 */
public class Problem0506 {

    public static void main(String[] args) {
        Problem0506 p = new Problem0506();
        System.err.println(Arrays.toString(p.findRelativeRanks(new int[]{5, 4, 3, 2, 1})));
        System.err.println(Arrays.toString(p.findRelativeRanks(new int[]{80, 100, 90, 60, 70})));
    }

    /**
     * 思路：要求每个元素的排名，这既需要排序，又需要查找，并且是每个元素都要查找
     * 因此查找不应该用二分查找，它总的查找时间为O(NlogN)
     * 对于有序数组，直接用hash表存储每个元素的排名（score元素的值 -> score元素的排名），总的查找时间为O(N)，需要额外O(N)的空间
     * <br/>
     * 现在就是要考虑排序算法
     * 一般的在原数组上排序的算法，都和hash表不相干
     * 但是有一种排序过程中需要创建特殊数组的排序算法，它就是计数排序
     * <br/>
     * 计数排序创建的counter数组
     * 它的下标就是对应的score的元素的排名（如果有，并且是升序排名，相同的元素只占用一个排名，并且通过这个排名可以反推出对应的score的元素的值）
     * 它的元素值为对应的score的元素的次数
     * 本题元素值唯一，且只需要相对顺序
     * 因此稍微改进下就可以了变成需要的hash表，把counter数组的值改为score元素对应的下标即可
     * <br/>
     * 时间复杂度为O(N)，空间复杂度为O(数据集的大小)，本题中数据集的大小为 max - min + 1
     * 由于数据集的大小是一个不确定的值，有时候会非常浪费空间，这也是限制计数排序的最大问题
     * 当数据集较小时，比如对百分制的整数成绩进行排名，数据集为0到100的整数
     * 此时计数排序是非常实用的排序算法，比其他大多数排序算法都快
     */
    public String[] findRelativeRanks(int[] score) {
        // 题目有假设，不做校验
        // 找到最大元素和最小元素
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < score.length; i++) {
            if (score[i] > max) {
                max = score[i];
            }
            if (score[i] < min) {
                min = score[i];
            }
        }
        // 初始化hashArray为-1，-1下标代表对应的score中的元素不存在
        int[] hashArray = new int[max - min + 1];
        Arrays.fill(hashArray, -1);
        // 对于每个存在的元素，hashArray的值记录为其在score中的下标
        for (int i = 0; i < score.length; i++) {
            hashArray[score[i] - min] = i;
        }
        String[] result = new String[score.length];
        // 遍历hashArray，得到所有元素的排名，并把排名转换为需要的形式
        int rank = 1;
        for (int i = hashArray.length - 1; i >= 0; i--) {
            if (hashArray[i] < 0) {
                continue;
            }
            switch (rank) {
                case 1:
                    result[hashArray[i]] = "Gold Medal";
                    break;
                case 2:
                    result[hashArray[i]] = "Silver Medal";
                    break;
                case 3:
                    result[hashArray[i]] = "Bronze Medal";
                    break;
                default:
                    result[hashArray[i]] = String.valueOf(rank);
            }
            rank++;
        }
        return result;
    }
}
