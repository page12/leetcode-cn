package p05;

import common.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/n-ary-tree-preorder-traversal/
 */
public class Problem0589 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 方法1：递归
     * 递归法很简单，不用多说
     * </br>
     * 方法2：迭代
     * 利用两个栈实现，完全仿照递归的栈的状态
     * 参考第590题
     * </br>
     * 方法3：另一种迭代
     * 这是一种优化的迭代，和递归流程差别很大
     * 先入栈root，再一次性把children全部入栈（这是和递归的主要区别，递归可不会一次入栈所有children）
     * 入栈会颠倒一次children的顺序，为了保证读取到的顺序不变，所以需要逆序入栈
     * 为了保证整体的出栈顺序等于遍历顺序，可以在循环外把整个树的根节点入栈
     */
    public List<Integer> preorder(Node root) {
        // 这是递归代码
        List<Integer> visitList = new ArrayList<>();
        dfs(root, visitList);
        return visitList;
    }

    private void dfs(Node root, List<Integer> visitList) {
        if (root == null) {
            return;
        }
        visitList.add(root.val);
        if (root.children != null && root.children.size() != 0) {
            for (Node node : root.children) {
                dfs(node, visitList);
            }
        }
    }

    public List<Integer> preorder2(Node root) {
        // 利用两个栈的迭代法
        if (root == null) {
            return Collections.emptyList();
        }
        List<Integer> visitList = new ArrayList<>();
        // 存储节点的栈
        LinkedList<Node> stack = new LinkedList<>();
        // 用于存储栈顶节点在其所在的children中的下标的栈
        LinkedList<Integer> idxStack = new LinkedList<>();
        // 根节点在循环外先入栈
        stack.push(root);
        // 入栈即遍历
        visitList.add(root.val);
        // 栈顶节点在其所在的children中的下标
        // 注意，整个树的根节点没有对应的nextBrotherIdx
        int topIdx = 0;
        while (!stack.isEmpty()) {
            root = stack.peek();
            if (root.children != null && root.children.size() != 0 && topIdx < root.children.size()) {
                // 还有没遍历完的children，继续深入
                idxStack.push(topIdx);
                stack.push(root.children.get(topIdx));
                // 入栈即遍历
                visitList.add(root.children.get(topIdx).val);
                // 深入时nextBrotherIdx变为下一层的序号，因此要重置为0
                topIdx = 0;
            } else {
                // 没有children
                // 出栈节点
                root = stack.pop();
                if (!idxStack.isEmpty()) {
                    // 出栈当前节点的下标
                    int currentIdx = idxStack.pop();
                    // 更新下一个兄弟的下标
                    topIdx = currentIdx + 1;
                }
            }
        }
        return visitList;
    }

    public List<Integer> preorder3(Node root) {
        // 这是显式用一个栈的迭代法代码
        if (root == null) {
            return Collections.emptyList();
        }
        List<Integer> visitList = new ArrayList<>();
        LinkedList<Node> stack = new LinkedList<>();
        // 根节点在循环外先入栈，这样能保证出栈顺序就是遍历顺序
        stack.push(root);
        while (!stack.isEmpty()) {
            root = stack.pop();
            visitList.add(root.val);
            if (root.children != null && root.children.size() != 0) {
                // 逆序入栈
                for (int i = root.children.size() - 1; i >= 0; i--) {
                    stack.push(root.children.get(i));
                }
            }
        }
        return visitList;
    }
}
