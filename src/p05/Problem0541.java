package p05;

/**
 * https://leetcode-cn.com/problems/reverse-string-ii/
 */
public class Problem0541 {

    public static void main(String[] args) {
        Problem0541 p = new Problem0541();
        System.err.println(p.reverseStr("abcdefg", 2));
    }

    /**
     * 思路：分段操作
     * 每2k分一段，一段内的前k个执行反转操作，后k个执行复制操作
     * 因为java的字符串有toCharArray操作，这个操作会复制一次字符串，所以可以省掉复制操作
     */
    public String reverseStr(String s, int k) {
        // 题目有假设，不做校验
        char[] chars = s.toCharArray();
        for (int i = 0; 2 * k * i < s.length(); i++) {
            // 反转[2k * i, 2k * i + 2k)区间内的字符串
            int reverseStart = 2 * k * i;
            int reverseEnd = Math.min(reverseStart + k, s.length());
            for (int j = reverseStart; j <= (reverseStart + reverseEnd) / 2; j++) {
                int swapIdx = reverseEnd + reverseStart - 1 - j;
                chars[j] = s.charAt(swapIdx);
                chars[swapIdx] = s.charAt(j);
            }
        }
        return new String(chars);
    }
}
