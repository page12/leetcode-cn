package p05;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/diameter-of-binary-tree/
 */
public class Problem0543 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 当一颗二叉树的直接穿过根节点（路径里面包含根节点）时，直径 = 左子树深度 + 右子树深度
     * 当不穿过根节点时，直径 = max(左子树直径, 右子树直径)
     * 所以直径 = max(左子树深度 + 右子树深度, max(左子树直径, 右子树直径))
     */
    public int diameterOfBinaryTree(TreeNode root) {
        return dfs(root).diameter;
    }

    /**
     * 深度优先（先序）遍历求深度和直径
     */
    private DepthAndDiameter dfs(TreeNode root) {
        if (root == null) {
            return new DepthAndDiameter(0, 0);
        }
        DepthAndDiameter left = dfs(root.left);
        DepthAndDiameter right = dfs(root.right);
        int depth = Math.max(left.depth, right.depth) + 1;
        int diameter = Math.max(left.depth + right.depth, Math.max(left.diameter, right.diameter));
        return new DepthAndDiameter(depth, diameter);
    }

    /**
     * java没有内置Tuple，这里手动写个类，用于一次性返回深度和直径
     */
    private static class DepthAndDiameter {
        int depth;
        int diameter;

        DepthAndDiameter(int depth, int diameter) {
            this.depth = depth;
            this.diameter = diameter;
        }
    }
}
