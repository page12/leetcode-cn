package p05;

/**
 * https://leetcode-cn.com/problems/delete-operation-for-two-strings/
 */
public class Problem0583 {

    public static void main(String[] args) {
        Problem0583 p = new Problem0583();
        System.err.println(p.minDistance2("sea", "eat") == 2);
        System.err.println(p.minDistance2("leetcode", "etco") == 4);
        System.err.println(p.minDistance2("a", "ab") == 1);
    }

    /**
     * 思路：
     * 设word1 = s，word2 = t，dp[i][j]为s的前i个字符和t的前j个字符执行删除操作的最少次数，0 <= i,j <= 各自的长度
     * 当 s(i) = t(j) 时，这两个字符都不用删除，此时  dp[i - 1][j - 1]
     * 当 s(i) != t(j) 时，有两种操作方式
     * 1、删除s(i)，此时 dp[i][j] = dp[i-1][j] + 1
     * 2、删除t(j)，此时 dp[i][j] = dp[i][j-1] + 1
     * 两种操作方式中次数较少的那一个就是最少次数，因此 dp[i][j] = min{ dp[i-1][j] + 1, dp[i][j-1] + 1 }
     * 特殊值：
     * i = 0 时，要全部删完，此时次数是 j
     * j = 0 时，要全部删完，此时次数是 i
     * <br/>
     * 二维dp状态可以使用滚动数组优化为一维，不过要特殊处理下
     * 先要区分字符串长度，外层循环是较长的，内层是较短的
     * 要新增一个变量用来保存左上对角的值dp[i - 1][j - 1]，在当前值被覆盖之前更新
     * 详见下面的第二种方法
     */
    public int minDistance(String word1, String word2) {
        // 题目有假设，不做校验
        int m = word1.length();
        int n = word2.length();
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0) {
                    dp[i][j] = j;
                } else if (j == 0) {
                    dp[i][j] = i;
                } else {
                    dp[i][j] = word1.charAt(i - 1) == word2.charAt(j - 1)
                            ? dp[i - 1][j - 1]
                            : Math.min(dp[i - 1][j] + 1, dp[i][j - 1] + 1);
                }
            }
        }
        return dp[m][n];
    }

    public int minDistance2(String word1, String word2) {
        // 题目有假设，不做校验
        // s是较长的那个字符串，对应下标i
        // t是较短的那个字符串，对应下标j
        String s;
        String t;
        if (word1.length() > word2.length()) {
            s = word1;
            t = word2;
        } else {
            s = word2;
            t = word1;
        }
        int[] dp = new int[t.length() + 1];
        // 用来保存左上对角的值dp[i - 1][j - 1]，在当前值被覆盖之前更新
        int diagonal = 0;
        for (int i = 0; i <= s.length(); i++) {
            for (int j = 0; j <= t.length(); j++) {
                if (i == 0) {
                    dp[j] = j;
                } else if (j == 0) {
                    diagonal = dp[j];
                    dp[j] = i;
                } else if (s.charAt(i - 1) == t.charAt(j - 1)) {
                    int temp = dp[j];
                    dp[j] = diagonal;
                    diagonal = temp;
                } else {
                    diagonal = dp[j];
                    dp[j] = Math.min(dp[j] + 1, dp[j - 1] + 1);
                }
            }
        }
        return dp[dp.length - 1];
    }
}
