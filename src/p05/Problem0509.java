package p05;

/**
 * https://leetcode-cn.com/problems/fibonacci-number/
 */
public class Problem0509 {

    public static void main(String[] args) {
        Problem0509 p = new Problem0509();
        for (int i = 0; i <= 30; i++) {
            System.err.println("fib(" + i + ") = " + p.fib(i));
        }
    }

    /**
     * 思路：求斐波那契数列常用的三种方式
     * 1、直接递归，因为大多数编程语言不能优化尾递归，所以递归过程中会重复计算很多次，这种方法非常慢
     * 2、改为迭代，从0开始求到n，缓存f(n-1)和f(n-2)，这种方法是线性时间，速度达标
     * 3、利用各种数学性质快速求解（除了通项公式，别的我也不会）
     * 下面的代码是迭代的方式
     */
    public int fib(int n) {
        // 注意题目规定了f(0） = 0
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        int minusOne = 1;
        int minusTwo = 0;
        int current = 0;
        for (int i = 2; i <= n; i++) {
            current = minusOne + minusTwo;
            minusTwo = minusOne;
            minusOne = current;
        }
        return current;
    }
}
