package p05;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/binary-tree-tilt/
 */
public class Problem0563 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.left.left = new TreeNode(4);
        root.left.left.left.left = new TreeNode(5);
        Problem0563 p = new Problem0563();
        System.err.println(p.findTilt(root));
    }

    /**
     * 思路：
     * 要求坡度，就需要先求树的节点总和，然后根据节点总和求各个节点的坡度，最后把坡度之和累加就是结果
     * 求当前节点总和的时候已经可以计算出当前节点的坡度，也能够同时累加坡度之和
     * 所以如果封装多个一起返回的数据，则可以一次遍历就计算出结果
     * 当然，也可以使用全局变量来代替数据封装
     * 总的复杂度和二叉树的深度优先遍历相同
     */
    public int findTilt(TreeNode root) {
        return dfs(root).currentSumOfSlope;
    }

    private SumAndSlope dfs(TreeNode root) {
        if (root == null) {
            return new SumAndSlope(0, 0, 0);
        }
        SumAndSlope left = dfs(root.left);
        SumAndSlope right = dfs(root.right);
        int sum = root.val + left.sum + right.sum;
        int slope = Math.abs(left.sum - right.sum);
        int currentSumOfSlope = slope + left.currentSumOfSlope + right.currentSumOfSlope;
        return new SumAndSlope(sum, slope, currentSumOfSlope);
    }

    /**
     * java没有内置Tuple，这里手动写个类，用于封装多个一起返回的数据
     */
    private static class SumAndSlope {
        // 当前节点以及它下面的子节点的总和
        int sum;
        // 当前节点的坡度
        int slope;
        // 当前递归过程中的坡度总和，放在这里避免使用全局变量
        int currentSumOfSlope;

        SumAndSlope(int sum, int slope, int currentSumOfSlope) {
            this.sum = sum;
            this.slope = slope;
            this.currentSumOfSlope = currentSumOfSlope;
        }
    }
}
