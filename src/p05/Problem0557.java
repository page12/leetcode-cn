package p05;

/**
 * https://leetcode-cn.com/problems/reverse-words-in-a-string-iii/
 */
public class Problem0557 {

    public static void main(String[] args) {
        Problem0557 p = new Problem0557();
        System.err.println(p.reverseWords("Let's take LeetCode contest"));
    }

    /**
     * 思路：很简单的双指针
     * 左指针标记单词的第一个字符，右指针标记单词的最后一个字符，然后反转这个单词
     * 然后左指针 = 右指针 + 1寻找之后第一个非空格字符，
     * 接着右指针从左指针处开始找单词的最后一个字符（右边字符是空格或者右边界）
     * 一次循环就可以，不需要其他额外变量
     * 时间复杂度O(N)，空间复杂度O(1)
     */
    public String reverseWords(String s) {
        if (s == null) {
            return null;
        }
        if (s.length() <= 1) {
            return s;
        }
        char[] chars = s.toCharArray();
        int left = 0;
        int right = 0;
        while (left < chars.length) {
            if (chars[left] != ' ') {
                right = left;
                while (right < chars.length - 1 && chars[right + 1] != ' ') {
                    right++;
                }
                reverseWord(chars, left, right);
                if (right == chars.length - 1) {
                    break;
                }
                left = right + 1;
            }
            left++;
        }
        return new String(chars);
    }

    /**
     * 反转单词
     */
    private void reverseWord(char[] chars, int left, int right) {
        while (left < right) {
            char temp = chars[left];
            chars[left] = chars[right];
            chars[right] = temp;
            left++;
            right--;
        }
    }
}