package p05;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/array-partition-i/
 */
public class Problem0561 {

    public static void main(String[] args) {
        Problem0561 p = new Problem0561();
        System.err.println(p.arrayPairSum(new int[]{1, 4, 3, 2}) == 4);
        System.err.println(p.arrayPairSum(new int[]{6, 2, 6, 5, 1, 2}) == 9);
    }

    /**
     * 思路：
     * 先排序
     * 然后最大的跟第二大的一组，剩下的2n-2个元素中也按照这样的方式分组，就能使得最后的结果最大
     * 这个很好证明
     * <br/>
     * 对于1 -> 2n个从小到大排序的数（1 -> 2n为下标），设F(n)为分成n组后的min操作的和
     * 上面的原理等价于 F(n) = F(n - 1) + min(nums[2n-1], nums[2n])
     * 因为是升序排列，nums[2n]是最大的数，nums[2n-1]是第二大的数，所以 F(n) = F(n - 1) + nums[2n-1]
     * 还有其他两种情况需要比较（下面的a一定满足a <= nums[2n-1] <= nums[2n]）：
     * 1、一个数a与nums[2n-1]交换分组
     * 因为nums[2n-1]大于剩下的n-1组中的任意一个数，所以n-1组的结果F(n-1)不变，但是后面的变成min(a, nums[2n]) = a <= nums[2n-1]，总的结果不变或者变小
     * 2、一个数a与nums[2n]交换分组
     * 因为nums[2n]大于剩下的n-1组中的任意一个数，所以n-1组的结果F(n-1)不变，但是后面的变成min(a, nums[2n-1]) = a <= nums[2n-1]，总的结果不变或者变小
     * 因此F(n) = F(n - 1) + min(nums[2n-1], nums[2n])这种分组情况是结果最大的情况
     * <br/>
     * 对数组进行排序不影响怎么分组，排序数组和不排序数组的最后分组结果是一样的，所以先排序的思路可行
     * 按照升序排序时，上面的方法简化为 最终结果 = 所有奇数位下标的和（下标从1开始，如果从0开始则是偶数位）
     * 总的流程只需要先排序然后遍历所有奇数位求和，所以总的复杂度就是排序算法的复杂度
     */
    public int arrayPairSum(int[] nums) {
        // 题目有假设，不用做校验
        Arrays.sort(nums);
        int result = 0;
        for (int i = 0; i < nums.length; i += 2) {
            result += nums[i];
        }
        return result;
    }
}
