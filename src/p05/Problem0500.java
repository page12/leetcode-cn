package p05;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/keyboard-row/
 */
public class Problem0500 {

    public static void main(String[] args) {
//        int[] lineOfChar = new int[26];
//        for (int i = 0; i < 26; i++) {
//            String s = String.valueOf((char) ('a' + i));
//            if ("qwertyuiop".contains(s)) {
//                lineOfChar[i] = 1;
//            } else if ("asdfghjkl".contains(s)) {
//                lineOfChar[i] = 2;
//            } else {
//                lineOfChar[i] = 3;
//            }
//        }
//        System.err.println(Arrays.toString(lineOfChar));

        Problem0500 p = new Problem0500();
        System.err.println(Arrays.toString(p.findWords(new String[]{"Hello", "Alaska", "Dad", "Peace"})));
        System.err.println(Arrays.toString(p.findWords(new String[]{"omk"})));
        System.err.println(Arrays.toString(p.findWords(new String[]{"adsdf", "sfd"})));
    }

    /**
     * 思路：利用hash表，记录每个字符是第几行的，然后把单词转换成数字就可以判断了
     */
    public String[] findWords(String[] words) {
        // 题目有假设，不做校验
        // 26个字母的hash表可以用数组代替
        int[] lineOfChar = new int[]{2, 3, 3, 2, 1, 2, 2, 2, 1, 2, 2, 2, 3, 3, 1, 1, 1, 1, 2, 1, 1, 3, 1, 3, 1, 3};
        List<String> result = new ArrayList<>();
        for (String w : words) {
            if (isSameLine(w, lineOfChar)) {
                result.add(w);
            }
        }
        return result.toArray(new String[0]);
    }

    /**
     * 判断是不是同一行字母组成的单词
     */
    private boolean isSameLine(String w, int[] lineOfChar) {
        // 题目有假设，不做校验
        int lastLine = lineOfChar[toUpperCase(w.charAt(0)) - 'A'];
        for (int i = 1; i < w.length(); i++) {
            char c = toUpperCase(w.charAt(i));
            if (lastLine != lineOfChar[c - 'A']) {
                return false;
            }
        }
        return true;
    }

    /**
     * 小写转大小
     */
    private char toUpperCase(char c) {
        return c >= 'a' ? (char) (c - 32) : c;
    }
}
