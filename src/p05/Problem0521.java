package p05;

/**
 * https://leetcode-cn.com/problems/longest-uncommon-subsequence-i/
 */
public class Problem0521 {

    public static void main(String[] args) {
        Problem0521 p = new Problem0521();
        System.err.println(p.findLUSlength("aba", "cdc") == 3);
        System.err.println(p.findLUSlength("aaa", "bbb") == 3);
        System.err.println(p.findLUSlength("aaa", "aaa") == -1);
    }

    /**
     * 这题没描述清楚！！！
     * 看不懂就直接去看评论或者答案吧，我看了之后才知道原来定义如此简单
     * 首先是特殊子序列的定义：设有一字符串s，s如果把a中的任意个（包括0个）字母删除后能得到s，则s是a的一个特殊子序列
     * 然后是最长特殊子序列的定义：最长特殊子序列就是所有s中长度最大的那一个
     * 因此可以得到一个重要的结论：a是a自己的最长特殊序列（题目就说了a是a的子序列，这跟个垃圾产品经理说话透露信息的方式差不多）
     * <br/>
     * 回到题目，a的最长特殊子序列，b是b的最长特殊子序列
     * 如果要满足 独有 这个条件，则需要a != b
     * a == b时，所有子序列都不独有，此时返回-1
     * 当 a.length > b.length时，a就是a的特殊子序列，但不是b的特殊子序列，因此此时最长的特殊子序列就是啊
     * a.length < b.length时，因为a和b可以交换，所以逻辑和a.length > b.length一样
     */
    public int findLUSlength(String a, String b) {
        return a.equals(b) ? -1 : Math.max(a.length(), b.length());
    }
}
