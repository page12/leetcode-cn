package p05;

/**
 * https://leetcode-cn.com/problems/longest-palindromic-subsequence/
 */
public class Problem0516 {

    public static void main(String[] args) {
        Problem0516 p = new Problem0516();
        System.err.println(p.longestPalindromeSubseq("bbbab") == 4);
        System.err.println(p.longestPalindromeSubseq("cbbd") == 2);
    }

    /**
     * 思路：
     * 一个回文串，首尾两个字母相同
     * 减去首尾两个字母后，剩下的要么是空串，要么还是回文串，可以把空串也当作回文串
     * 如果一个字符串不是回文串，那么它减去头部一个字符，或者减去尾部一个字符，都有可能是回文串，此时不能一次性减两个
     * 利用上面的结论，很容易得到递推公式
     * 设dp[i][j]表示 [i,j] 区间内最长回文子序列的长度，i <= j <= s.length，则题目最终结果是dp[0][s.length-1]
     * 当 s(i) == s(j) 时 dp[i][j] = dp[i+1][j-1] + 2
     * 当 s(i) != s(j) 时 dp[i][j] = max{ dp[i][j-1], dp[i+1][j] }
     * 特殊情况
     * i = j 时，只有一个字母，肯定是回文串，此时长度为1
     * i = j-1 时，区间内只有两个字符，此时如果 s(i) == s(j) 则 dp[i+1][j-1] = 2，否则 = 1
     * 根据递推公式，求dp[i][j]需要它左、左下、下边三个地方的dp值
     * 因此可以额从下往上，从左往右来循环
     * 也可以使用滚动数组优化空间使用，因为需要左以及左下的dp值，所以还需要要另一个单独的变量先记录左下的值
     */
    public int longestPalindromeSubseq(String s) {
        // 题目有假设，不做校验
        if (s.length() == 1) {
            return 1;
        }
        int n = s.length();
        // 也可以使用滚动数组优化空间使用
        // 因为需要左以及左下的dp值，所以还需要要另一个单独的变量先记录左下的值
        int[][] dp = new int[n][n];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                boolean eq = s.charAt(i) == s.charAt(j);
                if (i == j) {
                    dp[i][j] = 1;
                } else if (i == j - 1) {
                    dp[i][j] = eq ? 2 : 1;
                } else {
                    dp[i][j] = eq ? dp[i + 1][j - 1] + 2 : Math.max(dp[i][j - 1], dp[i + 1][j]);
                }
            }
        }
        return dp[0][n - 1];
    }
}
