package p05;

import common.TreeNode;

import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/minimum-absolute-difference-in-bst/
 */
public class Problem0530 {

    public static void main(String[] args) {

    }

    /**
     * 思路：利用二叉搜索树的中序遍历是有序的这个性质即可
     * 求一个有序数组的两个元素的最小的绝对值，利用变量记录上一个值和上一个最小绝对值，只需要顺序遍历一次就可以了，不需要回溯
     * 所以本题也不需要先求出所有中序遍历序列，在中序遍历过程中加入绝对值比较逻辑即可
     */
    public int getMinimumDifference(TreeNode root) {
        // 题目假设了树中至少有两个节点
        int minAbs = Integer.MAX_VALUE;
        // 因为要顺序计算绝对值，所以要初始化成二叉搜索树最右边的那个节点的val，这需要循环一次
        // 所以这里使用包装类，这样就能够在迭代中初始化
        Integer lastVal = null;
        LinkedList<TreeNode> stack = new LinkedList<>();
        while (!stack.isEmpty() || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if (lastVal == null) {
                lastVal = root.val;
            } else {
                int currentVal = root.val;
                int abs = Math.abs(currentVal - lastVal);
                if (abs < minAbs) {
                    minAbs = abs;
                }
                lastVal = currentVal;
            }
            root = root.right;
        }
        return minAbs;
    }
}
