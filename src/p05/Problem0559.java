package p05;

import common.Node;

/**
 * https://leetcode-cn.com/problems/maximum-depth-of-n-ary-tree/
 */
public class Problem0559 {

    public static void main(String[] args) {

    }

    /**
     * 思路：和二叉树一样，深度优先遍历
     * 深度 = max(所有子树的深度) + 1
     */
    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }
        if (root.children == null || root.children.size() == 0) {
            return 1;
        }
        int maxDepth = 0;
        for (Node node : root.children) {
            int depth = maxDepth(node);
            maxDepth = Math.max(depth, maxDepth);
        }
        return maxDepth + 1;
    }
}
