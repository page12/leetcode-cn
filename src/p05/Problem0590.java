package p05;

import common.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/n-ary-tree-postorder-traversal/
 */
public class Problem0590 {

    public static void main(String[] args) {
        Node n1 = new Node(1, new ArrayList<>());
        Node n2 = new Node(2, new ArrayList<>());
        Node n3 = new Node(3, new ArrayList<>());
        Node n4 = new Node(4, new ArrayList<>());
        Node n5 = new Node(5, new ArrayList<>());
        Node n6 = new Node(6, new ArrayList<>());
        Node n7 = new Node(7, new ArrayList<>());
        n1.children.add(n2);
        n1.children.add(n3);
        n1.children.add(n4);
        n3.children.add(n5);
        n3.children.add(n6);
        n3.children.add(n7);

        Problem0590 p = new Problem0590();
        System.err.println(p.postorder2(n1));
    }

    /**
     * 思路：同第589题差不多
     * 方法1：递归
     * 递归法很简单，相对于先序遍历交换下遍历当前节点代码和遍历其children的代码的顺序就行，不用多说
     * </br>
     * 方法2：真迭代（和递归的栈的状态一致）
     * 迭代法的难点在于 先遍历children，再遍历root，但是入栈顺序是先root再children
     * 出栈回溯时，如果当前回溯到的节点有children，不能再走之前的逻辑（把children逆序入栈），而是要遍历它自己的下一个兄弟
     * 为了要知道当前节点节点的下一个兄弟是谁，可以利用另外一个栈来记录
     * 可以记录当前节点的在它的所有兄弟中的下标，当回溯到此节点时，下标 + 1就是它的下一个兄弟
     * 如果下标超出范围了，则下一个兄弟不存在，此时应该继续出栈回溯
     * 整棵树的根节点没有兄弟，也无法找到所在兄弟数组，所以不需要记录它在所有兄弟中的下标
     * </br>
     * 之所以称其为真迭代，是因为它的栈的状态和递归算法栈的状态是一样的，是完全仿照递归流程来实现的迭代
     * 下面的解题思路3，已经看不出递归算法的逻辑了
     * <br/>
     * 如果前序也利用两个栈的迭代法，则会发现此时先序和后序的代码结构基本一样，因为这个方法就是完全仿照递归来说实现的
     * </br>
     * 方法3：假迭代
     * 和二叉树一样，也可以利用先序遍历改造下（children逆序入栈改为正序入栈），得到一个遍历序列，然后颠倒序列的方法
     * 而且它并不是真的迭代，因为不能按需迭代
     * 这种方法比较取巧，并不推荐
     */
    public List<Integer> postorder(Node root) {
        // 这是递归代码
        List<Integer> visitList = new ArrayList<>();
        dfs(root, visitList);
        return visitList;
    }

    private void dfs(Node root, List<Integer> visitList) {
        if (root == null) {
            return;
        }
        if (root.children != null && root.children.size() != 0) {
            for (Node node : root.children) {
                dfs(node, visitList);
            }
        }
        visitList.add(root.val);
    }

    public List<Integer> postorder2(Node root) {
        // 利用两个栈的迭代法
        if (root == null) {
            return Collections.emptyList();
        }
        List<Integer> visitList = new ArrayList<>();
        // 存储节点的栈
        LinkedList<Node> stack = new LinkedList<>();
        // 用于存储栈顶节点在其所在的children中的下标的栈
        LinkedList<Integer> idxStack = new LinkedList<>();
        // 根节点在循环外先入栈
        stack.push(root);
        // 栈顶节点在其所在的children中的下标
        // 注意，整个树的根节点没有对应的nextBrotherIdx
        int topIdx = 0;
        while (!stack.isEmpty()) {
            root = stack.peek();
            if (root.children != null && root.children.size() != 0 && topIdx < root.children.size()) {
                // 还有没遍历完的children，继续深入
                idxStack.push(topIdx);
                stack.push(root.children.get(topIdx));
                // 深入时nextBrotherIdx变为下一层的序号，因此要重置为0
                topIdx = 0;
            } else {
                // 没有children
                // 出栈节点并遍历
                root = stack.pop();
                if (!idxStack.isEmpty()) {
                    // 出栈当前节点的下标
                    int currentIdx = idxStack.pop();
                    // 更新下一个兄弟的下标
                    topIdx = currentIdx + 1;
                }
                // 遍历
                visitList.add(root.val);
            }
        }
        return visitList;
    }
}
