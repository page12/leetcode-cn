package p05;

/**
 * https://leetcode-cn.com/problems/base-7/
 */
public class Problem0504 {

    public static void main(String[] args) {
        Problem0504 p = new Problem0504();
        System.err.println(p.convertToBase7(0));
        System.err.println(p.convertToBase7(2));
        System.err.println(p.convertToBase7(7));
        System.err.println(p.convertToBase7(10));
        System.err.println(p.convertToBase7(49));
        System.err.println(p.convertToBase7(-50));
    }

    /**
     * 思路：进制转换都是同一个思路，循环执行 % 和 / 即可
     */
    public String convertToBase7(int num) {
        if (num == 0) {
            return "0";
        }
        boolean negative = num < 0;
        if (negative) {
            num = -num;
        }
        StringBuilder builder = new StringBuilder();
        while (num != 0) {
            int digit = num % 7;
            builder.append(digit);
            num = num / 7;
        }
        if (negative) {
            builder.append('-');
        }
        return builder.reverse().toString();
    }
}
