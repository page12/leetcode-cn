package p05;

/**
 * https://leetcode-cn.com/problems/range-addition-ii/
 */
public class Problem0598 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 1、创建m*n的二维数组，每次操作都直接更改这个二维数组，最后遍历求最大数对应的次数
     * 这种方法需要额外O(m * n)的空间，每次更改操作需要O(m * n)的时间，总共需要O(m * n * k)的时间复杂度，k为ops.length
     * 2、计算交集区域
     * 这里的交集区域是特殊的区域
     * 因为每次操作都是以(0,0)为左上顶点的矩形来进行操作的
     * 交集区域最终也一定是以(0,0)为左上顶点的矩形
     * 这个矩形的上边长最终等于操作区域的最小上边长
     * 这个矩形的左边长最终等于操作区域的最小左边长
     * 这样做时间复杂度为O(k)，k为ops.length，空间复杂度为O(1)，比方法1快很多
     */
    public int maxCount(int m, int n, int[][] ops) {
        // 计算交集区域的代码
        // 题目有假设，不做校验
        for (int i = 0; i < ops.length; i++) {
            m = Math.min(m, ops[i][0]);
            n = Math.min(n, ops[i][1]);
        }
        return m * n;
    }
}
