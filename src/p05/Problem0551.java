package p05;

/**
 * https://leetcode-cn.com/problems/student-attendance-record-i/
 */
public class Problem0551 {

    public static void main(String[] args) {
        Problem0551 p = new Problem0551();
        System.err.println(p.checkRecord("PPALLP"));
        System.err.println(p.checkRecord("PPALLL"));
    }

    /**
     * 思路：
     * 要判断是否是A超标，可以用一个标志位 aFlag
     * aFlag = true表示当前已经有了一个A，因此如果再遇见A，就return false
     * 要判断是否是L超标，可以检测是否是三个连续的L
     * 整个只需要一次遍历就行，除了标志位外也不再需要其他额外空间
     * 所以时间复杂度O(N)，空间复杂度为O(1)
     */
    public boolean checkRecord(String s) {
        if (s == null || s.length() <= 1) {
            return true;
        }
        boolean aFlag = false;
        boolean lFlag = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if ('A' == c) {
                if (aFlag) {
                    return false;
                } else {
                    aFlag = true;
                }
            } else if ('L' == c && i >= 2 && 'L' == s.charAt(i - 1) && 'L' == s.charAt(i - 2)) {
                return false;
            }
        }
        return true;
    }
}
