package p05;

/**
 * https://leetcode-cn.com/problems/coin-change-2/
 */
public class Problem0518 {

    public static void main(String[] args) {
        Problem0518 p = new Problem0518();
        System.err.println(p.change2(5, new int[]{1, 2, 5}));
        System.err.println(p.change2(3, new int[]{2}));
    }

    /**
     * 思路：
     * 设 c in coins，dp[i] 为金额 i 的所有方式，i 为 [0,amount] 中的所有值
     * 则任意金额 i 的分割方式为 c + (i - c)，对于 c 的每一种取值，方式总数为 dp[i-c]
     * 因此可以得到 dp[i] = sum(dp[i-c]) c in coins
     * 但是这个是有问题的，它计算的是排列数而非组合数
     * 举例：amount = 5, coins = {1, 2, 5}
     * 3 = 2 + 1 = 1 + 2
     * 也就是 dp[3] = dp[1] + dp[2]
     * 计算 dp[1] 就是 2 + (1) 的情况，最后一次使用 2 来拼凑
     * 计算 dp[2] 就是 1 + (2) = 1 + (2 + 0) 的情况，最后一次用1来拼凑，之前使用过了2，所以重复出现了 1 + 2 这种组合
     * <br/>
     * 组合数只关注有没有被使用，不关心第几次被使用
     * 因此还需要加一个状态记录当前硬币是否被使用，这样就变成二维的了
     * 重新定义 dp[i][j] 表示用[0,i]这个区间内的硬币，组成 j 金额的组合数
     * 只关心是否被使用，使用1次和使用多次都算是被使用，因此只有两种情况：
     * 1、使用了第i枚硬币时，总的组合数等于[0,i]这个区间内的硬币组成金额 j-coins[i] 的组合数量，因此有 dp[i][j] = dp[i][j-coins[i]]
     * 因为使用1次第i枚硬币后还可以继续使用，因此等号右边的区间还是 [0,i]
     * 2、没有使用第i枚硬币时，总的组合数等于[0,i-1]这个区间内的硬币组成金额 j 的组合数量，因此有 dp[i][j] = dp[i-1][j]
     * 两种情况是并集，因此有 dp[i][j] = dp[i][j-coins[i]] + dp[i-1][j]
     * 特殊情况：
     * dp[0][0] 是金额为0，此时只有一枚硬币都不使用这一种情况，因此 dp[0][0] = 1
     * i = 0 && j != 0 时，没有硬币，但是金额大于0，不可能组合出来，因此 dp[i][j>0] = 0
     * i != 0 && j == 0 时，金额为0，此时只有一枚硬币都不使用这一种情况，因此 dp[i>0][0] = 1
     * j < coins[i] 时，情况1没有意义，此时只有情况2，也就是 dp[i][j] = dp[i-1][j]
     * <br/>
     * 如果区分使用次数，则和完全背包问题很像
     * 求组合数时，按上面的dp定义，不需要区分使用次数，因为使用一次后的剩余情况 dp[i][j-coins[i]] 包含了再继续使用同一枚硬币的的情况
     * 因此不需要关心次数
     * <br/>
     * 二维数组可以通过滚动数组优化成一维数组
     * 这时候对比看下两种代码，就能知道为什么要把 coins 的循环放在外面
     */
    public int change(int amount, int[] coins) {
        // 题目有假设，不做校验
        int[][] dp = new int[coins.length + 1][amount + 1];
        // 第一行只需要处理一个值
        dp[0][0] = 1;
        for (int i = 1; i <= coins.length; i++) {
            int coin = coins[i - 1];
            for (int j = 0; j <= amount; j++) {
                if (j == 0) {
                    dp[i][j] = 1;
                } else if (j < coin) {
                    dp[i][j] = dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i][j - coin] + dp[i - 1][j];
                }
            }
        }
        return dp[coins.length][amount];
    }

    public int change2(int amount, int[] coins) {
        // 题目有假设，不做校验
        // 使用滚动数组优化后
        // 下面的代码有些多余，但是放在这里能更方便对比一维和二维的代码
        // 便于理解为什么要把 coins 的循环放在外面
        int[] dp = new int[amount + 1];
        for (int coin : coins) {
            for (int j = 0; j <= amount; j++) {
                if (j == 0) {
                    dp[j] = 1;
                } else if (j < coin) {
                    // 使用滚动数组优化时，二维的 dp[i - 1][j] 就是一维的 dp[j]
                    dp[j] = dp[j];
                } else {
                    // 使用滚动数组优化时，二维的 dp[i - 1][j] 就是一维的 dp[j]
                    dp[j] = dp[j] + dp[j - coin];
                }
            }
        }
        return dp[amount];
    }
}
