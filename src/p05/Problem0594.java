package p05;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/longest-harmonious-subsequence/
 */
public class Problem0594 {

    public static void main(String[] args) {
        Problem0594 p = new Problem0594();
        System.err.println(p.findLHS(new int[]{1, 3, 2, 2, 5, 2, 3, 7}) == 5);
        System.err.println(p.findLHS(new int[]{1, 2, 3, 4}) == 2);
        System.err.println(p.findLHS(new int[]{1, 1, 1, 1}) == 0);
    }

    /**
     * 思路：
     * 和谐子序列中的所有数都是整数，并且它们的最大值和最小值相差为1
     * 所以和谐子序列中一定只有两种数字，设其分别为a和b，并且有a = b - 1
     * 和谐子序列的长度等于 a的次数 + b的次数 = a的次数 + (a+1)的次数（a+1不存在时，长度直接记为0）
     * 因此本题就转换为求数组中各种数字的次数，再对各种数字及其次数遍历一次，求得 a的次数 + (a+1)的次数 的最大值
     * 利用hash表是最方便的求次数并且支持遍历的方法
     * 时间复杂度O(N)，空间复杂度O(N)
     */
    public int findLHS(int[] nums) {
        // 题目有假设nums.length >= 1
        // 先利用hash表求各种数字的次数
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.merge(num, 1, (oldValue, newValue) -> oldValue + 1);
        }
        int maxLength = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int num = entry.getKey();
            int count = entry.getValue();
            int length;
            if (!map.containsKey(num + 1)) {
                length = 0;
            } else {
                length = count + map.get(num + 1);
            }
            maxLength = Math.max(length, maxLength);
        }
        return maxLength;
    }
}
