package p05;

/**
 * https://leetcode-cn.com/problems/perfect-number/
 */
public class Problem0507 {

    public static void main(String[] args) {
        Problem0507 p = new Problem0507();
        System.err.println(p.checkPerfectNumber(6));
        System.err.println(p.checkPerfectNumber(496));
        System.err.println(p.checkPerfectNumber(8128));
        System.err.println(p.checkPerfectNumber(2));
        System.err.println(p.checkPerfectNumber(1));
    }

    /**
     * 思路：暴力循环，求出所有正因子
     * 利用乘法交换律，可以把区间缩小为[1, sqrt(n)]
     * 因为正因子不包含自身，所以可以进一步把区间缩小为[2, sqrt(n)]
     * 计算因子和时，再把1加进去
     * 特殊情况：小于等于1的数，都不是完美数
     */
    public boolean checkPerfectNumber(int num) {
        if (num <= 1) {
            return false;
        }
        int sqrt = (int) Math.sqrt(num);
        // 一定有正因子1，并且不包含自身，所以循环从2开始并且sum初始化为1
        int sum = 1;
        for (int i = 2; i <= sqrt; i++) {
            if (num % i == 0) {
                sum += i;
                int divide = num / i;
                if (divide != i) {
                    sum += divide;
                }
            }
        }
        return sum == num;
    }
}
