package p05;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/01-matrix/
 */
public class Problem0542 {

    public static void main(String[] args) {
        Problem0542 p = new Problem0542();
        int[][] result = p.updateMatrix(new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}});
        for (int[] a : result) {
            System.err.println(Arrays.toString(a));
        }
    }

    /**
     * 思路：
     * 当一个点是0，它本身的距离是0
     * 同时它可以确定上下左右四个点的距离：点是0，则距离是0，否则是1
     * 如果一个点是1，则它的距离等于上下左右四个点的最小距离再加1
     * 上下左右四个点必须提前算出来，这样不好处理
     * 可以换成四次处理
     * 依次计算距离左上，左下，右上，右下的最近的0的距离
     * 正上、正下、正左、正右的会有重复计算，不过因为是取最小值，所以不影响最终结果
     */
    public int[][] updateMatrix(int[][] mat) {
        int m = mat.length;
        int n = mat[0].length;
        int[][] result = new int[m][n];
        // 先把距离初始化为-1，因为0是有意义的值
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (mat[i][j] == 0) {
                    result[i][j] = 0;
                } else {
                    // 初始化成一个较大的值，避免加法溢出就行
                    // 最大距离为 m+n-2
                    result[i][j] = Integer.MAX_VALUE - m - n;
                }
            }
        }
        // 计算距离左上的最近的0的距离
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i - 1 >= 0) {
                    result[i][j] = Math.min(result[i][j], result[i - 1][j] + 1);
                }
                if (j - 1 >= 0) {
                    result[i][j] = Math.min(result[i][j], result[i][j - 1] + 1);
                }
            }
        }
        // 计算距离左下的最近的0的距离
        for (int i = m - 1; i >= 0; i--) {
            for (int j = 0; j < n; j++) {
                if (i + 1 < m) {
                    result[i][j] = Math.min(result[i][j], result[i + 1][j] + 1);
                }
                if (j - 1 >= 0) {
                    result[i][j] = Math.min(result[i][j], result[i][j - 1] + 1);
                }
            }
        }
        // 计算距离右上的最近的0的距离
        for (int i = 0; i < m; i++) {
            for (int j = n - 1; j >= 0; j--) {
                if (i - 1 >= 0) {
                    result[i][j] = Math.min(result[i][j], result[i - 1][j] + 1);
                }
                if (j + 1 < n) {
                    result[i][j] = Math.min(result[i][j], result[i][j + 1] + 1);
                }
            }
        }
        // 计算距离右下的最近的0的距离
        for (int i = m - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                if (i + 1 < m) {
                    result[i][j] = Math.min(result[i][j], result[i + 1][j] + 1);
                }
                if (j + 1 < n) {
                    result[i][j] = Math.min(result[i][j], result[i][j + 1] + 1);
                }
            }
        }
        return result;
    }
}
