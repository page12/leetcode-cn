package p07;

/**
 * https://leetcode-cn.com/problems/minimum-ascii-delete-sum-for-two-strings/
 */
public class Problem0712 {

    public static void main(String[] args) {
        Problem0712 p = new Problem0712();
        System.err.println(p.minimumDeleteSum("sea", "eat") == 231);
        System.err.println(p.minimumDeleteSum2("sea", "eat") == 231);
    }

    /**
     * 思路：
     * 和第583题基本一样
     * 那一题求的是次数，cost固定是1
     * 本题cost是被删除的字符串的ascii码值
     * <br/>
     * 设s1 = s，s2 = t，dp[i][j]为s的前i个字符和t的前j个字符执行删除操作的最少cost，0 <= i,j <= 各自的长度
     * 当 s(i) = t(j) 时，这两个字符都不用删除，此时  dp[i - 1][j - 1]
     * 当 s(i) != t(j) 时，有两种操作方式
     * 1、删除s(i)，此时 dp[i][j] = dp[i-1][j] + s(i)
     * 2、删除t(j)，此时 dp[i][j] = dp[i][j-1] + t(j)
     * 两种操作方式中cost较少的那一个就是最少次数，因此 dp[i][j] = min{ dp[i-1][j] + s(i), dp[i][j-1] + t(j) }
     * 特殊值：
     * i = 0 && j = 0 时，两个都是空字符串，次数dp[i][j] = 0
     * i = 0 时，要把t(0,j)全部删完，此时次数是 t(0,j) 的 ascii 码值之和
     * j = 0 时，要把s(0,i)全部删完，此时次数是 s(0,i) 的 ascii 码值之和
     * <br/>
     * 二维dp状态可以使用滚动数组优化为一维，不过要特殊处理下
     * 先要区分字符串长度，外层循环是较长的，内层是较短的
     * 要新增一个变量用来保存左上对角的值dp[i - 1][j - 1]，在当前值被覆盖之前更新
     * 详见下面的第二种方法
     */
    public int minimumDeleteSum(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = 0;
                } else if (i == 0) {
                    dp[i][j] = dp[i][j - 1] + s2.charAt(j - 1);
                } else if (j == 0) {
                    dp[i][j] = dp[i - 1][j] + s1.charAt(i - 1);
                } else {
                    dp[i][j] = s1.charAt(i - 1) == s2.charAt(j - 1)
                            ? dp[i - 1][j - 1]
                            : Math.min(dp[i - 1][j] + s1.charAt(i - 1), dp[i][j - 1] + s2.charAt(j - 1));
                }
            }
        }
        return dp[m][n];
    }

    public int minimumDeleteSum2(String s1, String s2) {
        // 题目有假设，不做校验
        // s是较长的那个字符串，对应下标i
        // t是较短的那个字符串，对应下标j
        String s;
        String t;
        if (s1.length() > s2.length()) {
            s = s1;
            t = s2;
        } else {
            s = s2;
            t = s1;
        }
        int[] dp = new int[t.length() + 1];
        // 用来保存左上对角的值dp[i - 1][j - 1]，在当前值被覆盖之前更新
        int diagonal = 0;
        for (int i = 0; i <= s.length(); i++) {
            for (int j = 0; j <= t.length(); j++) {
                if (i == 0 && j == 0) {
                    dp[j] = 0;
                } else if (i == 0) {
                    dp[j] = dp[j - 1] + t.charAt(j - 1);
                } else if (j == 0) {
                    diagonal = dp[j];
                    dp[j] += s.charAt(i - 1);
                } else if (s.charAt(i - 1) == t.charAt(j - 1)) {
                    int temp = dp[j];
                    dp[j] = diagonal;
                    diagonal = temp;
                } else {
                    diagonal = dp[j];
                    dp[j] = Math.min(dp[j] + s.charAt(i - 1), dp[j - 1] + t.charAt(j - 1));
                }
            }
        }
        return dp[dp.length - 1];
    }
}
