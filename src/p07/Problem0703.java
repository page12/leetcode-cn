package p07;

import java.util.PriorityQueue;

/**
 * https://leetcode-cn.com/problems/kth-largest-element-in-a-stream/
 */
public class Problem0703 {

    public static void main(String[] args) {
        KthLargest kthLargest = new KthLargest(3, new int[]{4, 5, 8, 2});
        System.err.println(kthLargest.add(3) == 4);
        System.err.println(kthLargest.add(5) == 5);
        System.err.println(kthLargest.add(10) == 5);
        System.err.println(kthLargest.add(9) == 8);
        System.err.println(kthLargest.add(4) == 8);
//        KthLargest kthLargest = new KthLargest(1, new int[]{});
//        System.err.println(kthLargest.add(-3) == -3);
//        System.err.println(kthLargest.add(-2) == -2);
//        System.err.println(kthLargest.add(-4) == -2);
//        System.err.println(kthLargest.add(0) == 0);
//        System.err.println(kthLargest.add(4) == 4);
    }

    /**
     * 思路：
     * 这个题目就是要维护一个长度为k的优先级队列，它能够在O(1)时间内返回它的最小值
     * 每次添加元素时
     * 如果大于优先级队列的最小值，则需要先把最小值出队，再入队元素，并返回此时的优先级队列的最小值
     * 如果小于等于优先级队列的最小值，则不改变第k大的元素，此时上面都不用做，直接返回最小值就行
     * <br/>
     * java内置有优先级队列PriorityQueue
     * 它是使用最小堆来实现的
     * 如果要手动实现最小堆，请参考堆排序的实现，或者研究PriorityQueue源码
     */
    private static class KthLargest {

        private PriorityQueue<Integer> priorityQueue;
        private int maxSize;

        public KthLargest(int k, int[] nums) {
            priorityQueue = new PriorityQueue<>(k);
            maxSize = k;
            for (int num : nums) {
                this.add(num);
            }
        }

        public int add(int val) {
            // 数据不足k个时，先入队
            if (priorityQueue.size() < maxSize) {
                priorityQueue.add(val);
                // 题目假设了返回时一定有k个元素
                return priorityQueue.peek();
            } else {
                int min = priorityQueue.peek();
                if (min >= val) {
                    return min;
                } else {
                    priorityQueue.poll();
                    priorityQueue.add(val);
                    return priorityQueue.peek();
                }
            }
        }
    }
}
