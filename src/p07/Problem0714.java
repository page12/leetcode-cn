package p07;

/**
 * https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/
 */
public class Problem0714 {

    public static void main(String[] args) {
        Problem0714 p = new Problem0714();
        System.err.println(p.maxProfit2(new int[]{1, 3, 2, 8, 4, 9}, 2) == 8);
        System.err.println(p.maxProfit2(new int[]{1, 3, 7, 5, 10, 3}, 3) == 6);
    }

    /**
     * 思路：
     * 设dp[i][k]为截止第i天两种状态的收益最大收益
     * k=0,1分别代表当天操作过后是持有股票的状态还是不持有股票的状态
     * 买入的时候计算手续费
     * 如果当天买入，则前一天是不持有股票的状态，因此有 dp[i][0] = dp[i-1][1] - prices[i] - fee
     * 如果当天不操作，则就是前一天的值，因此有 dp[i][0,1] = dp[i-1][0,1]
     * 如果当天卖出，则前一天是持有股票的状态，因此有 dp[i][1] = dp[i-1][0] + prices[i]
     * 综合一下，取较大值，得到
     * dp[i][0] = max{ dp[i-1][1] - prices[i] - fee, dp[i-1][0] }
     * dp[i][1] = max{ dp[i-1][0] + prices[i] , dp[i-1][1] }
     * 特殊值：第一天只能进行买入操作，因此 dp[0][0] = -prices[0] dp[0][1] = 0
     * 最后一天时，不持有股票肯定比持有股票收益高，因此最终结果为 dp[prices.length-1][1]
     * <br/>
     * 因为dp[i][0,1]只需要dp[i-1][0,1]的值，所以可以用滚动数组优化空间复杂度，降到O(1)的空间复杂度
     * 官方题解的贪心算法，不太好理解
     */
    public int maxProfit(int[] prices, int fee) {
        // 动态规划解法
        // 题目有假设，不做校验
        int[][] dp = new int[prices.length][2];
        dp[0][0] = -prices[0] - fee;
        dp[0][1] = 0;
        for (int i = 1; i < prices.length; i++) {
            dp[i][0] = Math.max(dp[i - 1][1] - prices[i] - fee, dp[i - 1][0]);
            dp[i][1] = Math.max(dp[i - 1][0] + prices[i], dp[i - 1][1]);
        }
        return dp[prices.length - 1][1];
    }

    public int maxProfit2(int[] prices, int fee) {
        // 滚动数组优化
        // 题目有假设，不做校验
        int dp0 = -prices[0] - fee;
        int dp1 = 0;
        for (int i = 1; i < prices.length; i++) {
            int tempDp0 = dp0;
            dp0 = Math.max(dp1 - prices[i] - fee, tempDp0);
            dp1 = Math.max(tempDp0 + prices[i], dp1);
        }
        return dp1;
    }
}
