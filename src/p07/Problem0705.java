package p07;

/**
 * https://leetcode-cn.com/problems/design-hashset/
 */
public class Problem0705 {

    public static void main(String[] args) {

    }

    /**
     * 思路：参考第706题
     * HashSet就是HashMap退化成key和value一样的情况
     */
    private static class MyHashSet {

        private HashNode[] array;

        /**
         * Initialize your data structure here.
         */
        public MyHashSet() {
            array = new HashNode[16];
        }

        public void add(int key) {
            HashNode hashNode = array[key & 0xf];
            while (hashNode != null) {
                if (hashNode.key == key) {
                    return;
                }
                hashNode = hashNode.next;
            }
            HashNode newHashNode = new HashNode(key, key, array[key & 0xf]);
            array[key & 0xf] = newHashNode;
        }

        public void remove(int key) {
            HashNode hashNode = array[key & 0xf];
            if (hashNode == null) {
                return;
            }
            // 删除的是头节点
            if (hashNode.key == key) {
                array[key & 0xf] = hashNode.next;
                return;
            }
            // 删除的不是头节点
            while (hashNode.next != null) {
                if (hashNode.next.key == key) {
                    hashNode.next = hashNode.next.next;
                    return;
                }
                hashNode = hashNode.next;
            }
        }

        /**
         * Returns true if this set contains the specified element
         */
        public boolean contains(int key) {
            HashNode hashNode = array[key & 0xf];
            while (hashNode != null) {
                if (hashNode.key == key) {
                    return true;
                }
                hashNode = hashNode.next;
            }
            return false;
        }

        /**
         * hash节点
         */
        private static class HashNode {
            int key;
            int value;
            HashNode next;

            HashNode(int key, int value, HashNode next) {
                this.key = key;
                this.value = value;
                this.next = next;
            }
        }
    }
}
