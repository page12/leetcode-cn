package p07;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/self-dividing-numbers/
 */
public class Problem0728 {

    public static void main(String[] args) {
        Problem0728 p = new Problem0728();
        System.err.println(p.selfDividingNumbers(1, 22));
    }

    /**
     * 思路：
     * 没什么好思路，直接暴力求解
     */
    public List<Integer> selfDividingNumbers(int left, int right) {
        // 题目有假设，不做校验
        List<Integer> result = new ArrayList<>();
        for (int i = left; i <= right; i++) {
            if (isSelfDividingNumber(i)) {
                result.add(i);
            }
        }
        return result;
    }

    /**
     * 判断一个数是不是自除数
     */
    private boolean isSelfDividingNumber(int n) {
        int temp = n;
        while (temp != 0) {
            int digit = temp % 10;
            // 包含0或者不被整除都不是自除数
            if (digit == 0 || n % digit != 0) {
                return false;
            }
            temp = temp / 10;
        }
        return true;
    }
}
