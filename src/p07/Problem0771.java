package p07;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/jewels-and-stones/
 */
public class Problem0771 {

    public static void main(String[] args) {
        Problem0771 p = new Problem0771();
        System.err.println(p.numJewelsInStones("aA", "aAAbbbb") == 3);
        System.err.println(p.numJewelsInStones("z", "ZZ") == 0);
    }

    /**
     * 思路：
     * 把J变成字符的HashSet，这样判断S中的字符是不是宝石就只需要常数时间了
     * 题目没有规定是英文字母，所以用HashSet
     * 如果规定了是英文字母，则可以使用固定长度52的数组
     */
    public int numJewelsInStones(String jewels, String stones) {
        if (jewels == null || jewels.length() == 0 || stones == null || stones.length() == 0) {
            return 0;
        }
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < jewels.length(); i++) {
            set.add(jewels.charAt(i));
        }
        int result = 0;
        for (int i = 0; i < stones.length(); i++) {
            if (set.contains(stones.charAt(i))) {
                result++;
            }
        }
        return result;
    }
}
