package p07;

/**
 * https://leetcode-cn.com/problems/binary-search/
 */
public class Problem0704 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 本题就是叫你写二分查找
     * 如果从序号小的题开始做，到现在应该写了好几次二分查找了，包括一些它的变型
     */
    public int search(int[] nums, int target) {
        int start = 0;
        int end = nums.length - 1;
        while (start <= end) {
            // 这样做避免 start + end直接溢出
            int mid = (end - start) / 2 + start;
            if (nums[mid] > target) {
                end = mid - 1;
            } else if (nums[mid] < target) {
                start = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
