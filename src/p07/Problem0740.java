package p07;

import java.util.Map;
import java.util.TreeMap;

/**
 * https://leetcode-cn.com/problems/delete-and-earn/
 */
public class Problem0740 {

    public static void main(String[] args) {
        Problem0740 p = new Problem0740();
        System.err.println(p.deleteAndEarn2(new int[]{3, 4, 2}) == 6);
        System.err.println(p.deleteAndEarn2(new int[]{2, 2, 3, 3, 3, 4}) == 9);
        System.err.println(p.deleteAndEarn2(new int[]{3, 1}) == 4);
        System.err.println(p.deleteAndEarn2(new int[]{1, 6, 3, 3, 8, 4, 8, 10, 1, 3}) == 43);
    }

    /**
     * 思路：
     * 选择 nums[i] 后，全部的 nums[i]-1 和 nums[i] + 1 都会被删除
     * 可以再次选择 nums[i]， 此时不会有别的元素被删除
     * 因此选择 nums[i]，可以一次性全部选择
     * 可以先把数组按大小排序，并统计对应数字的次数，设去重后的排序数组为n[]，对应的次数数组为t[]
     * 设在dp[i]为数组n[]中[0,i]区间内的最大点数
     * 如果选择下标 i 的元素，则
     * 当 n[i-1] == n[i]-1 时，则n[i] n[i-1] 二选一，选了n[i]的话可以就是与[0,i-2]搭配，因此 dp[i] = max{ dp[i-1], dp[i-2] + n[i]*t[i] }
     * 当 n[i-1] != n[i]-1 时，则可以继续选择n[i]，因此 dp[i] = dp[i-1] + n[i]*t[i]
     * 特殊值：
     * dp[0] = 0
     * dp[1] = n[0] * t[0]
     * 如果n[1] == n[0] + 1，则 dp[1] = max{ dp[0], n[1]*t[1] }，否则 dp[1] = n[1]*t[1] + dp[0]
     * 如果最后两个数满足加1关系，则dp数组最后两个值中的较大值是最终结果，否则则dp的最后一个值是最终结果，可以使用max来获取最终结果
     * <br/>
     * 因为只需要前面三个dp值，所以也可以省去数组，直接写三个变量，下面代码没写这种优化
     * <br/>
     * 看了官方题解，才知道，这就是 打家劫舍4
     * 利用计数排序，就变成了和第198题几乎一样的题，那一题也是不能取相邻的元素
     * 小偷的故事还在继续，只是换了个名字
     */
    public int deleteAndEarn(int[] nums) {
        // 题目有假设，不做校验
        // 使用TreeMap，兼顾计数和排序
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for (int num : nums) {
            map.merge(num, 1, (oldValue, newValue) -> oldValue + 1);
        }
        int[] n = new int[map.size()];
        int[] t = new int[map.size()];
        int idx = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            n[idx] = entry.getKey();
            t[idx] = entry.getValue();
            idx++;
        }
        if (n.length == 1) {
            return n[0] * t[0];
        }
        // 可以用三个变量代替数组来优化空间复杂度
        int[] dp = new int[n.length];
        dp[0] = n[0] * t[0];
        dp[1] = n[1] == n[0] + 1 ? Math.max(dp[0], n[1] * t[1]) : n[1] * t[1] + dp[0];
        for (int i = 2; i < dp.length; i++) {
            dp[i] = n[i - 1] == n[i] - 1
                    ? Math.max(dp[i - 1], dp[i - 2] + n[i] * t[i])
                    : dp[i - 1] + n[i] * t[i];
        }
        return Math.max(dp[dp.length - 2], dp[dp.length - 1]);
    }

    public int deleteAndEarn2(int[] nums) {
        // 题目有假设，不做校验
        int max = 0;
        for (int num : nums) {
            max = Math.max(num, max);
        }
        int[] count = new int[max];
        // 下面的是第198题代码稍微修改下得来的
        for (int num : nums) {
            count[num - 1] += 1;
        }
        if (count.length == 1) {
            return count[0];
        }
        if (count.length == 2) {
            return Math.max(count[0], count[1] * 2);
        }
        if (count.length == 3) {
            return Math.max(count[0] + count[2] * 3, count[1] * 2);
        }
        int minusThree = count[0];
        int minusTwo = count[1] * 2;
        int minusOne = count[0] + count[2] * 3;
        for (int i = 3; i < count.length; i++) {
            int current = Math.max(minusThree, minusTwo) + count[i] * (i + 1);
            minusThree = minusTwo;
            minusTwo = minusOne;
            minusOne = current;
        }
        return Math.max(minusTwo, minusOne);
    }
}
