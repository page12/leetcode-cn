package p07;

/**
 * https://leetcode-cn.com/problems/min-cost-climbing-stairs/
 */
public class Problem0746 {

    public static void main(String[] args) {
        Problem0746 p = new Problem0746();
        System.err.println(p.minCostClimbingStairs(new int[]{10, 15, 20}) == 15);
        System.err.println(p.minCostClimbingStairs(new int[]{1, 100, 1, 1, 1, 100, 1, 1, 100, 1}) == 6);
    }

    /**
     * 思路：
     * 这是一个简单的动态规划
     * 设数组长度为n，到楼梯顶的最小总花费为 f(n)
     * 到楼梯顶有两种方法：n - 2 上两个阶梯，以及，n - 1上一个阶梯
     * 那么就有 f(n) = min{f(n-2) + cost[n-2], f(n-1) + cost[n-1]}
     * 初始特殊值为
     * f(0) = 0，表示从第0阶梯开始
     * f(1) = 0，表示从第1阶梯开始
     * 依次遍历即可以得到f(2) f(3) ... f(n)
     * 只需要遍历数组一次，也不需要其他额外空间，所以时间复杂度为O(n)，空间复杂度为O(1)
     */
    public int minCostClimbingStairs(int[] cost) {
        // 题目有假设，不做校验
        int minusTwo = 0;
        int minusOne = 0;
        int minCurrentCost = 0;
        for (int i = 2; i <= cost.length; i++) {
            minCurrentCost = Math.min(minusTwo + cost[i - 2], minusOne + cost[i - 1]);
            minusTwo = minusOne;
            minusOne = minCurrentCost;
        }
        return minCurrentCost;
    }
}
