package p07;

/**
 * https://leetcode-cn.com/problems/flood-fill/
 */
public class Problem0733 {

    public static void main(String[] args) {
        Problem0733 p = new Problem0733();
        System.err.println(p.floodFill(new int[][]{{0, 0, 0}, {0, 1, 1}}, 1, 1, 1));
    }

    /**
     * 思路：
     * 一个点的上下左右要处理，上下左右这四个点的上下左右也要处理
     * 很明显，这就是递归
     * 注意一点，因为上下左右是所有方位，所以会绕回最开始那个点，此时如果那个点的颜色等于新颜色，就由会开始一次，形成无限递归
     * 因此需要加个判断，当不改变一开始的点的颜色时，不需要递归
     */
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        // 题目有假设，不做校验
        int oldColor = image[sr][sc];
        if (oldColor == newColor) {
            // 不改变一开始的点的颜色时，不需要递归，否则可能会无限递归
            return image;
        }
        recurse(sr, sc, oldColor, newColor, image);
        return image;
    }

    /**
     * 递归
     */
    private void recurse(int r, int c, int oldColor, int newColor, int[][] image) {
        // 超出边界
        if (r < 0 || r >= image.length || c < 0 || c >= image[0].length) {
            return;
        }
        if (oldColor != image[r][c]) {
            return;
        }
        image[r][c] = newColor;
        recurse(r - 1, c, oldColor, newColor, image);
        recurse(r + 1, c, oldColor, newColor, image);
        recurse(r, c - 1, oldColor, newColor, image);
        recurse(r, c + 1, oldColor, newColor, image);
    }
}
