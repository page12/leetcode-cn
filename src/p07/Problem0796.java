package p07;

/**
 * https://leetcode-cn.com/problems/rotate-string/
 */
public class Problem0796 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 移位不改变字符串长度，所以当长度不相等时，一定不是移位的
     * s + s中包含了任意一种移位后的情况
     * 利用这一点，可以很简单的判断是否是移位
     */
    public boolean rotateString(String s, String goal) {
        return s.length() == goal.length() && (s + s).contains(goal);
    }
}
