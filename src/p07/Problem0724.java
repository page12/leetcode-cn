package p07;

/**
 * https://leetcode-cn.com/problems/find-pivot-index/
 */
public class Problem0724 {

    public static void main(String[] args) {
        Problem0724 p = new Problem0724();
        System.err.println(p.pivotIndex2(new int[]{1, 7, 3, 6, 5, 6}) == 3);
        System.err.println(p.pivotIndex2(new int[]{1, 2, 3}) == -1);
        System.err.println(p.pivotIndex2(new int[]{2, 1, -1}) == 0);
        System.err.println(p.pivotIndex2(new int[]{-1, -1, 0, 1, 1, 0}) == 5);
    }

    /**
     * 思路：
     * 一开始想用双指针，最后发现无法处理下标是0或者nums.length - 1的情况
     * 准备写暴力算法
     * 暴力算法就是，遍历数组元素，计算下它左边和右边的和，看是否相等
     * 它需要遍历一次数组，每次计算左右两边的和又要整体遍历一次，所以时间复杂度为O(N^2)
     * 不需要其他额外空间，所以空间复杂度为O(1)
     * <br/>
     * 在写暴力算法的途中发现了可以优化的地方
     * 记左边和为 L(i) = nums[0] + nums[1] + ... + nums[i - 1]
     * 记右边和为 R(i) = nums[i + 1] + nums[i + 2] + ... + nums[nums.length - 1]
     * 那么可以推出
     * L(i = L(i - 1) + nums[i - 1]
     * R(i) = R(i - 1) - nums[i]
     * 设整个数组的和为S，那么有
     * L(0) = 0
     * R(0) = S - num[0]
     * 再配合上面的递推公式，知道 L(i - 1) 和 R(i - 1) 时，可以通过加减1个元素就直接得到 L(i) 和 R(i)
     * 这样计算左右两边的和就可以用O(1)的时间，能够将时间复杂度降为O(N)
     */
    public int pivotIndex(int[] nums) {
        // 这是暴力算法
        // 题目有假设，不做校验
        for (int i = 0; i < nums.length; i++) {
            int L = sum(nums, 0, i - 1);
            int R = sum(nums, i + 1, nums.length - 1);
            if (L == R) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 区间求和
     */
    private int sum(int[] nums, int start, int end) {
        int sum = 0;
        for (int i = start; i <= end; i++) {
            sum += nums[i];
        }
        return sum;
    }

    public int pivotIndex2(int[] nums) {
        // // 这是暴力算法的优化算法
        // 题目有假设，不做校验
        // 先求L R 初始值
        int L = 0;
        int R = 0;
        for (int i = 0; i < nums.length; i++) {
            R += nums[i];
        }
        R = R - nums[0];
        // 先判断 i == 0
        if (L == R) {
            return 0;
        }
        // 再遍历判断
        for (int i = 1; i < nums.length; i++) {
            L += nums[i - 1];
            R -= nums[i];
            if (L == R) {
                return i;
            }
        }
        return -1;
    }
}
