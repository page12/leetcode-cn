package p07;

/**
 * https://leetcode-cn.com/problems/design-hashmap/
 */
public class Problem0706 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 直接仿照java HashMap 的思路，它使用的是链地址法
     * 其中的两个两个重点这里就不体现了，这两个重点是 扩容 与 链查找优化（链表转红黑树）
     * 为了简单，这里直接使用key作为hash值（java的Integer的hashCode就是直接返回本身的value）
     */
    private static class MyHashMap {

        private HashNode[] array;

        /**
         * Initialize your data structure here.
         */
        public MyHashMap() {
            array = new HashNode[16];
        }

        /**
         * value will always be non-negative.
         */
        public void put(int key, int value) {
            HashNode hashNode = array[key & 0xf];
            while (hashNode != null) {
                if (hashNode.key == key) {
                    hashNode.value = value;
                    return;
                }
                hashNode = hashNode.next;
            }
            HashNode newHashNode = new HashNode(key, value, array[key & 0xf]);
            array[key & 0xf] = newHashNode;
        }

        /**
         * Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
         */
        public int get(int key) {
            HashNode hashNode = array[key & 0xf];
            while (hashNode != null) {
                if (hashNode.key == key) {
                    return hashNode.value;
                }
                hashNode = hashNode.next;
            }
            return -1;
        }

        /**
         * Removes the mapping of the specified value key if this map contains a mapping for the key
         */
        public void remove(int key) {
            HashNode hashNode = array[key & 0xf];
            if (hashNode == null) {
                return;
            }
            // 删除的是头节点
            if (hashNode.key == key) {
                array[key & 0xf] = hashNode.next;
                return;
            }
            // 删除的不是头节点
            while (hashNode.next != null) {
                if (hashNode.next.key == key) {
                    hashNode.next = hashNode.next.next;
                    return;
                }
                hashNode = hashNode.next;
            }
        }

        /**
         * hash节点
         */
        private static class HashNode {
            int key;
            int value;
            HashNode next;

            HashNode(int key, int value, HashNode next) {
                this.key = key;
                this.value = value;
                this.next = next;
            }
        }
    }
}
