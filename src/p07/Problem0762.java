package p07;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode-cn.com/problems/prime-number-of-set-bits-in-binary-representation/
 */
public class Problem0762 {

    public static void main(String[] args) {
        Problem0762 p = new Problem0762();
        System.err.println(p.countPrimeSetBits(6, 10) == 4);
        System.err.println(p.countPrimeSetBits(10, 15) == 5);
    }

    /**
     * 思路：
     * 总共两点
     * 1、求二进制表示中位为1的个数，这个第191题就是，java中有内置API Integer.bitCount(int n)
     * 2、int最多32位，32以内的质数就 2 3 5 7 11 13 17 19 23 29 31 这11个，是固定的
     */
    public int countPrimeSetBits(int left, int right) {
        Set<Integer> primeSet = new HashSet<>(Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31));
        int count = 0;
        for (int i = left; i <= right; i++) {
            if (primeSet.contains(Integer.bitCount(i))) {
                count++;
            }
        }
        return count;
    }
}
