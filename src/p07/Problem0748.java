package p07;

/**
 * https://leetcode-cn.com/problems/shortest-completing-word/
 */
public class Problem0748 {

    public static void main(String[] args) {
        Problem0748 p = new Problem0748();
        System.err.println(p.shortestCompletingWord("1s3 PSt", new String[]{"step", "steps", "stripe", "stepple"}));
        System.err.println(p.shortestCompletingWord("1s3 456", new String[]{"looks", "pest", "stew", "show"}));
        System.err.println(p.shortestCompletingWord("Ah71752", new String[]{"suggest", "letter", "of", "husband", "easy", "education", "drug", "prevent", "writer", "old"}));
        System.err.println(p.shortestCompletingWord("OgEu755", new String[]{"enough", "these", "play", "wide", "wonder", "box", "arrive", "money", "tax", "thus"}));
        System.err.println(p.shortestCompletingWord("iMSlpe4", new String[]{"claim", "consumer", "student", "camera", "public", "never", "wonder", "simple", "thought", "use"}));
    }

    /**
     * 思路：
     * 利用hash表存储字符出现的次数，英文字母的hash表可以使用固定长度数组，只有小写字母时是26
     * 补全词的每个字符出现次数要大于牌照中对应字符出现的次数，如果没出现，视为次数是0
     * 最短的补全词就是长度最小的补全词
     * 从做至右遍历，min 操作不要带等号，则最短的补全词就是长度相同的取最左边的哪一个
     * <br/>
     * 设words数组的长度为 N，字符串平均长度为 k（包括words和licensePlate），字符集大小为 C
     * 求一次hash表时间消耗为 k，空间消耗为 C
     * 两个hash表比较一次消耗时间为 C，消耗常数个空间
     * 整个流程需要求 N + 1 次hash表，再比较 N 次hash表
     * 所以总的时间复杂度为O(N * k + N * C)
     * 总的空间复杂度为O(N * C)
     * 一些说明：
     * 1、字符集大小本题固定为 C = 26
     * 2、实际上循环中创建的hash表可以重复利用，因此总的只需要两个hash表就行，这个简单改下代码就可以实现
     */
    public String shortestCompletingWord(String licensePlate, String[] words) {
        int[] lpCharCountArray = getCharCountArray(licensePlate);
        String minWord = "";
        int minLength = Integer.MAX_VALUE;
        for (String word : words) {
            int[] wordCharCountArray = getCharCountArray(word);
            if (checkCharCount(wordCharCountArray, lpCharCountArray) && word.length() < minLength) {
                minLength = word.length();
                minWord = word;
            }
        }
        return minWord;
    }

    /**
     * 返回一个存储字符对应数字的26长度hash表数组
     */
    private int[] getCharCountArray(String s) {
        int[] countArray = new int[26];
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (65 <= c && c <= 90) {
                // 大写字母
                countArray[c + 32 - 'a'] += 1;
            } else if (97 <= c && c <= 122) {
                // 小写字母
                countArray[c - 'a'] += 1;
            }
        }
        return countArray;
    }

    /**
     * 判断：x中每个字符出现次数要大于y中对应字符出现的次数
     */
    private boolean checkCharCount(int[] x, int[] y) {
        for (int i = 0; i < 26; i++) {
            if (x[i] < y[i]) {
                return false;
            }
        }
        return true;
    }
}
