package p07;

/**
 * https://leetcode-cn.com/problems/champagne-tower/
 */
public class Problem0799 {

    public static void main(String[] args) {
        Problem0799 p = new Problem0799();
        System.err.println(p.champagneTower(1, 1, 1));
        System.err.println(p.champagneTower(2, 1, 1));
        System.err.println(p.champagneTower(100000009, 33, 17));
    }

    /**
     * 思路：
     * 一次性全倒在第一杯，然后一层一层循环处理溢出的情况
     * 设下标为 (i,j)
     * 整体是三角形，进行左对齐
     * 如果(i,j)溢出了，则下一层的(i+1,j)和(i+1,j+1)都会接收到溢出的一半
     * 可以先假定(0,0)有poured杯，然后去一层一层地循环去处理溢出的情况
     * 用一个二维数组记录状态，最后下标处就是结果
     */
    public double champagneTower(int poured, int query_row, int query_glass) {
        // 题目有假设，不做校验
        // 下标从0开始，所以这里变成要加1
        // 只需要处理到 query_row 行，为了处理这一行的溢出，需要再加一行
        // 因此这里总共加2
        double[][] state = new double[query_row + 2][query_row + 2];
        state[0][0] = poured;
        for (int i = 0; i <= query_row; i++) {
            for (int j = 0; j <= query_row; j++) {
                if (state[i][j] > 1) {
                    double halfOverflow = (state[i][j] - 1) / 2;
                    state[i][j] = 1;
                    // 这里要用加，因为当前杯子最多接收上一层两个杯子的溢出
                    state[i + 1][j] += halfOverflow;
                    state[i + 1][j + 1] += halfOverflow;
                }
            }
        }
        return state[query_row][query_glass];
    }
}
