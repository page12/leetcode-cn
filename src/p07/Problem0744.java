package p07;

/**
 * https://leetcode-cn.com/problems/find-smallest-letter-greater-than-target/
 */
public class Problem0744 {

    public static void main(String[] args) {
        Problem0744 p = new Problem0744();
        System.err.println(p.nextGreatestLetter(new char[]{'a', 'b'}, 'z') == 'a');
        System.err.println(p.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'a') == 'c');
        System.err.println(p.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'c') == 'f');
        System.err.println(p.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'd') == 'f');
        System.err.println(p.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'g') == 'j');
        System.err.println(p.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'j') == 'c');
        System.err.println(p.nextGreatestLetter(new char[]{'c', 'f', 'j'}, 'k') == 'c');
        System.err.println(p.nextGreatestLetter(new char[]{'e', 'e', 'e', 'e', 'e', 'e', 'n', 'n', 'n', 'n'}, 'e') == 'n');
    }

    /**
     * 思路：
     * 有序数组的查找，一般都能转换为二分查找
     * 本题要找的是大于它的最小值，根据二分查找
     * 如果找到等于target的值，因为会有重复的字母出现，所以此时不能直接返回letters[mid + 1]，而是要视为小于target的情况执行 start = mid + 1
     * 如果找不到等于target的值，则本题转换为寻找插入位置，同第35题一样，即
     * 当letters[end] > target，插入位置为end，也就是返回 letters[end]
     * 当letters[end] < target，插入位置为 end + 1，也就是返回 letters[end + 1]
     * 当letters[end] == target，因为要比target大，所以要用end左边的值，也就是返回 letters[end + 1]
     * 当极端情况（大于最大或者小于最小）时，返回两个边界值
     * <br/>
     * 本题需要注意一点，那就是 在比较时，字母是依序循环出现的
     * 例如：target = 'z' 并且 letters = ['a', 'b']，此时返回 'a'
     * 也就是 target >= max(letters) 时，直接返回下标为0的那个字母就行
     */
    public char nextGreatestLetter(char[] letters, char target) {
        // 题目有假设，不做校验
        // 极端情况处理
        if (target < letters[0] || target >= letters[letters.length - 1]) {
            return letters[0];
        }
        int start = 0;
        int end = letters.length - 1;
        while (start <= end) {
            int mid = (end - start) / 2 + start;
            if (letters[mid] > target) {
                end = mid - 1;
            } else if (letters[mid] <= target) {
                start = mid + 1;
            }
        }
        // 处理没有和target相等的字母时的情况
        if (letters[end] <= target) {
            return letters[end + 1];
        }
        return letters[end];
    }
}
