package p07;

/**
 * https://leetcode-cn.com/problems/to-lower-case/
 */
public class Problem0709 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 利用ASCII表，进行数值加减来转换大小写
     * 26大写字母 + 32 = 对应的26小写字母
     * 大写字母范围为 [65, 90]
     * 小写字母范围为[97, 122]
     */
    public String toLowerCase(String s) {
        // 题目假设s非空
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (65 <= chars[i] && chars[i] <= 90) {
                chars[i] += 32;
            }
        }
        return new String(chars);
    }
}
