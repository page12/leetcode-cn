package p07;

/**
 * https://leetcode-cn.com/problems/1-bit-and-2-bit-characters/
 */
public class Problem0717 {

    public static void main(String[] args) {
        Problem0717 p = new Problem0717();
        System.err.println(p.isOneBitCharacter(new int[]{1, 0, 0}));
        System.err.println(p.isOneBitCharacter(new int[]{1, 1, 1, 0}));
    }

    /**
     * 思路：
     * 碰见0，代表是一个0
     * 碰见1，代表是10或者11，应该跳过下一个bit
     * 如果能遍历到最后一个0，则最后一个字符必定是0代表的字符
     */
    public boolean isOneBitCharacter(int[] bits) {
        // 题目假设bits非空
        int i = 0;
        while (i < bits.length - 1) {
            if (bits[i] == 0) {
                i += 1;
            } else {
                i += 2;
            }
        }
        return i == bits.length - 1 && bits[i] == 0;
    }
}
