package p07;

/**
 * https://leetcode-cn.com/problems/largest-number-at-least-twice-of-others/
 */
public class Problem0747 {

    public static void main(String[] args) {
        Problem0747 p = new Problem0747();
        System.err.println(p.dominantIndex(new int[]{3, 6, 1, 0}) == 1);
        System.err.println(p.dominantIndex(new int[]{1, 2, 3, 4}) == -1);
        System.err.println(p.dominantIndex(new int[]{1}) == 0);
    }

    /**
     * 思路：
     * 通过两次遍历可以很简单求出来，第一次遍历求最大值，第二次遍历判断是否大于两倍
     * 现在思考如何只用一次遍历，发现还是很简单的
     * 设所有数中最大的数为 max，第二大的数为 max2
     * 如果存在题目所说的数，
     * 则有 max >= 剩下的所有数 * 2，
     * 即 max >= max(剩下的所有数) * 2
     * 即 max >= max2 * 2
     * 反过来如果不存在这样的数，那么一定有 max <= max2 * 2
     * 所以本题就转换为求 max 和 max2 并比较 max >= max2 * 2
     */
    public int dominantIndex(int[] nums) {
        // 题目有假设，不做校验
        int maxIdx = -1;
        int max = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max) {
                max2 = max;
                max = nums[i];
                maxIdx = i;
            } else if (nums[i] > max2) {
                max2 = nums[i];
            }
        }
        return max >= max2 * 2 ? maxIdx : -1;
    }
}
