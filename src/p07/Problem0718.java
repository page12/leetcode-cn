package p07;

/**
 * https://leetcode-cn.com/problems/maximum-length-of-repeated-subarray/
 */
public class Problem0718 {

    public static void main(String[] args) {
        Problem0718 p = new Problem0718();
        System.err.println(p.findLength3(new int[]{1, 2, 3, 2, 1}, new int[]{3, 2, 1, 4, 7}));
        System.err.println(p.findLength3(new int[]{0, 1, 1, 1, 1}, new int[]{1, 0, 1, 0, 1}));
        System.err.println(p.findLength3(new int[]{0, 0, 0, 0, 0, 0, 1, 0, 0, 0}, new int[]{0, 0, 0, 0, 0, 0, 0, 1, 0, 0}));
    }

    /**
     * 思路：
     * 注意，子数组要求连续在一起，中间不能跳跃
     * 暴力算法 = 枚举所有的 i j 以它们开头，计算公共子数组的长度，然后得到最大长度，这种方法会超时
     * 如果设 dp[i][j] 为以 i j 结尾的最长子数组长度
     * 则当 nums1[i] = nums2[j] 时，为了确定最长长度
     * 还需要往回遍历一次，看一下nums1[0,i-1]和nuns2[0,j-1]从末尾开始有多少个相同的
     * 这样就和暴力算法差不多了，因此这个dp定义有问题
     * 上述dp中花时间的地方是 往回遍历确定结尾相同的长度，也就是相同后缀长度 这一个步骤
     * 这个求相同后缀长度的，可以用动态规划来解决
     * 设 dp[i][j] 为以 i j 结尾的相同后缀最大长度，则
     * 当 nums1[i] = nums2[j] 时，dp[i][j] = dp[i-1][j-1] + 1
     * 当 nums1[i] != nums2[j] 时，dp[i][j] = 0
     * 对于所有 i j 最大的相同后缀长度就是子数组长度
     * 特殊值：当 i = 0 或者 j = 0 时，有一个空串，此时后缀长度为0
     * <br/>
     * 求 dp[i][j] 时只需要左上对角的值，可以使用滚动数组优化来降低空间复杂度，额外用一个变量保存左上对角的值
     * 详见下面的findLength2
     * <br/>
     * 滑动窗口
     * 暴力算法中，计算以 i j 开头的最大长度时，会包含一次 计算以 i+x j+x 开头的最大长度，x是满足长度的任意值
     * 后面又会计算 i=i+x j=j+x 这就导致了重复计算
     * 因此后面不需要处理 i+x j+x 开头的最大长度
     * 实际上的情况只有 i == 0 && j 任意 和 j = 0 && i 任意（中间 i == 0 && j == 0 重复一次）这两个大情况
     * 这两个大情况相当于是把其中一个数组固定不动，另一个数组进行移位
     * 左移和右移可以通过左对齐和右对齐来等效掉，因此往一个方向移位就行
     * 这里采用两个数组左对齐 + 左移的方式
     * nums1和nums2左对齐，先把nums1左移，然后复位，再把nums2左移，这样就操作完成了
     * 整个过程相当于把两个数组相向滑动，从相离到相加再到相离的过程
     * 不用真实进行移位，用下标标记下起始位置就可以了
     */
    public int findLength(int[] nums1, int[] nums2) {
        // 题目有假设，不做校验
        int m = nums1.length;
        int n = nums2.length;
        int maxLength = 0;
        // i j 当作原数组下标使用时要减1，因为dp包含0个元素的情况
        int[][] dp = new int[m + 1][n + 1];
        // 当 i = 0 或者 j = 0 时，有一个空串，此时后缀长度为0，因此从1开始循环
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                dp[i][j] = nums1[i - 1] == nums2[j - 1]
                        ? dp[i - 1][j - 1] + 1
                        : 0;
                maxLength = Math.max(maxLength, dp[i][j]);
            }
        }
        return maxLength;
    }

    public int findLength2(int[] nums1, int[] nums2) {
        // 滚动数组
        // 题目有假设，不做校验
        // s是较长的那个数组，对应下标i
        // t是较短的那个数组，对应下标j
        int[] s;
        int[] t;
        if (nums1.length > nums2.length) {
            s = nums1;
            t = nums2;
        } else {
            s = nums2;
            t = nums1;
        }
        int maxLength = 0;
        // 左上对角的值
        int diagonal = 0;
        int[] dp = new int[t.length + 1];
        for (int i = 1; i <= s.length; i++) {
            for (int j = 1; j <= t.length; j++) {
                diagonal = j == 1 ? 0 : diagonal;
                int temp = dp[j];
                dp[j] = s[i - 1] == t[j - 1] ? diagonal + 1 : 0;
                diagonal = temp;
                maxLength = Math.max(maxLength, dp[j]);
            }
        }
        return maxLength;
    }

    public int findLength3(int[] nums1, int[] nums2) {
        // 滑动窗口
        // 题目有假设，不做校验
        // s是较长的那个数组，对应下标i
        // t是较短的那个数组，对应下标j
        int[] s;
        int[] t;
        if (nums1.length > nums2.length) {
            s = nums1;
            t = nums2;
        } else {
            s = nums2;
            t = nums1;
        }
        int maxLength = 0;
        // 左对齐，把nums1左移
        for (int i = 0; i < s.length; i++) {
            maxLength = Math.max(maxLength, getCommonLength(nums1, i, nums2, 0));
        }
        // 左对齐，把nums2左移
        for (int j = 0; j < t.length; j++) {
            maxLength = Math.max(maxLength, getCommonLength(nums1, 0, nums2, j));
        }
        return maxLength;
    }

    /**
     * 移位后求以 i j 左对齐开头的公共部分的长度
     */
    private int getCommonLength(int[] s, int i, int[] t, int j) {
        int maxLength = 0;
        int currentLength = 0;
        while (i < s.length && j < t.length) {
            if (s[i] == t[j]) {
                currentLength++;
            } else {
                maxLength = Math.max(maxLength, currentLength);
                currentLength = 0;
            }
            i++;
            j++;
        }
        // 如果全部相同，则循环内没有执行最后一次max，因此这里要补上一次
        return Math.max(maxLength, currentLength);
    }
}
