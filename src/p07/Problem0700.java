package p07;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/search-in-a-binary-search-tree/
 */
public class Problem0700 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 本题就是二叉搜索树的查找，一点都没改
     * 二叉搜索树的查找一般不用递归
     * 利用它的大小性质(左 < 中 < 右)能够确定target是在左子树上还是右子树上，因此不用回溯，也就不用递归了
     * 这种查找的时间复杂度为树的高度(平均时间复杂度为O(logN)，最坏情况树退化为链表时O(N))，空间复杂度为O(1)
     */
    public TreeNode searchBST(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        while (root != null) {
            if (root.val == val) {
                return root;
            } else if (root.val > val) {
                root = root.left;
            } else {
                root = root.right;
            }
        }
        return root;
    }
}
