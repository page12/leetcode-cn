package p04;

import java.util.Arrays;
import java.util.Comparator;

/**
 * https://leetcode-cn.com/problems/non-overlapping-intervals/
 */
public class Problem0435 {

    public static void main(String[] args) {
        Problem0435 p = new Problem0435();
        System.err.println(p.eraseOverlapIntervals2(new int[][]{{0, 2}, {1, 3}, {1, 3}, {2, 4}, {3, 5}, {3, 5}, {4, 6}}));
    }

    /**
     * 思路：
     * 和第300题第354题很像
     * 先按区间左值进行升序排序
     * 然后求最长递增子序列
     * 本题的递增 等价于 当前区间左值 >= 被比较的区间的右值，也就是没有交集并且在左边
     * 设f(i)为排序后以第i个区间为结尾的最长递增区间的长度
     * 则f(i) = max{f(x)} + 1，其中x代指i左边所有没有交集的区间的下标
     * 如果i左边不存在这样的区间，则 f(i) = 1，因此 f(0) = 1
     * 利用这个递推公式，很容易写出自底向上的动态规划的代码
     * 这样的动态规划求的是最长递增长度，最终结果 = intervals.length 减去 最长递增长度
     */
    public int eraseOverlapIntervals(int[][] intervals) {
        // 题目有假设，不做校验
        if (intervals.length <= 1) {
            return 0;
        }
        // 先按区间左值进行升序排序
        Arrays.sort(intervals, Comparator.comparingInt(a -> a[0]));
        int maxLength = 1;
        int[] dp = new int[intervals.length];
        for (int i = 0; i < intervals.length; i++) {
            int max = 1;
            for (int j = 0; j < i; j++) {
                if (intervals[i][0] >= intervals[j][1]) {
                    max = Math.max(max, dp[j] + 1);
                }
            }
            dp[i] = max;
            maxLength = Math.max(maxLength, max);
        }
        return intervals.length - maxLength;
    }

    /**
     * 思路：
     * 可以利用贪心算法求最长递增子序列的长度，递增的定义和上面动态规划中定义的相同
     * 任选一个区间时[lx,rx]，可以拼接在它后面组成递增子序列的区间[l, r]必须满足 l >= rx
     * 因此可以选择所有区间中 r 最小的那一个当作第一个区间，设其为rx
     * 这样不是唯一的选择（所有的可选择的区间，都是在下一个区间的左边），但是每次都能是最优的选择
     * 不是唯一选择时，设其余选择的r = ry
     * 因为选择后续期间是用的是 l >= rx，此时 l >= ry => l > rx，因此可以使用rx替换ry
     * 选择完第一个区间后，后续的区间也可以用这种方法进行筛选
     * 为了方便对r进行比较，可以按r进行升序排序
     */
    public int eraseOverlapIntervals2(int[][] intervals) {
        // 题目有假设，不做校验
        if (intervals.length <= 1) {
            return 0;
        }
        // 先按区间右值进行升序排序
        Arrays.sort(intervals, Comparator.comparingInt(a -> a[1]));
        int maxLength = 1;
        int r = intervals[0][1];
        // 因为是按 r 进行升序排序的
        // 所以第一个满足intervals[i][0]的是所有相同左值中r最小的
        for (int i = 0; i < intervals.length; i++) {
            if (intervals[i][0] >= r) {
                r = intervals[i][1];
                maxLength++;
            }
        }
        return intervals.length - maxLength;
    }
}