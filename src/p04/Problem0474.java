package p04;

/**
 * https://leetcode-cn.com/problems/ones-and-zeroes/
 */
public class Problem0474 {

    public static void main(String[] args) {
        Problem0474 p = new Problem0474();
        System.err.println(p.findMaxForm(new String[]{"10", "0001", "111001", "1", "0"}, 5, 3));
        System.err.println(p.findMaxForm(new String[]{"10", "0", "1"}, 1, 1));
    }

    /**
     * 思路：
     * 这就是二维费用的01背包问题
     * 二维费用是指选择一个元素后，会产生两种成本，本题中的成本就是选中的字符串的0和1的数量
     * 设dp[i][j][k]表示在[0,i]这些元素中，0的个数限制为j，1的个数限制为k时子集的最大大小
     * 当前i这个元素只有选和不选，设它的0和1的个数分别为 xi 和 yi
     * 当选择i这个元素时
     * 问题转换为 【在前面[0,i-1]中，0的个数限制为j-xi，1的个数限制为k-yi时子集的最大大小，再加上此次选择产生的那个1】
     * 此时 dp[i][j][k] = dp[i-1][j-xi][k-yi] + 1
     * 当不选择i这个元素时
     * 问题转换为 【在前面[0,i-1]中，0的个数限制为j，1的个数限制为k时子集的最大大小】
     * 此时 dp[i][j][k] = dp[i-1][j][k]
     * 两种情况中的较大值就是 dp[i][j][k] 的结果
     * 因此 dp[i][j][k] = max{ dp[i-1][j-xi][k-yi] + 1, dp[i-1][j][k] }
     * <br/>
     * 特殊值：
     * dp[0][0][0] = 0，因为0和1的个数都限制为0，此时一个元素也不能选
     * dp[0][j][k]，此时0的个数限制为j，1的个数限制为k，因此当 j >= x0 && k >= y0 时为 1，否则为 0
     * 当 j < xi 或者 k < yi 时，此时一定不能选择第i个元素，只有不选这一种情况，因此 dp[i][j][k] = dp[i-1][j][k]
     * <br/>
     * 所有dp的状态是一个三维数组
     * 但是根据上面的递推公式，求解 dp[i][j][k] 时只需要知道上一行也就是 dp[i] 的所有值就行了
     * 因此可以简化到二维，这个优化和滚动数组类似
     * 但是三维的状态要清楚一些，下面的代码也没写这个优化
     */
    public int findMaxForm(String[] strs, int m, int n) {
        // 题目有假设，不做校验
        // 将字符串转换为[x,y]，x是0的个数，y是1的个数
        int[][] arrays = new int[strs.length][2];
        for (int i = 0; i < strs.length; i++) {
            arrays[i] = transform(strs[i]);
        }
        int[][][] dp = new int[arrays.length][m + 1][n + 1];
        for (int i = 0; i < arrays.length; i++) {
            int x = arrays[i][0];
            int y = arrays[i][1];
            for (int j = 0; j <= m; j++) {
                for (int k = 0; k <= n; k++) {
                    if (i == 0 && j == 0 && k == 0) {
                        dp[i][j][k] = 0;
                    } else if (i == 0) {
                        dp[i][j][k] = j >= x && k >= y ? 1 : 0;
                    } else if (j < x || k < y) {
                        dp[i][j][k] = dp[i - 1][j][k];
                    } else {
                        dp[i][j][k] = Math.max(dp[i - 1][j - x][k - y] + 1, dp[i - 1][j][k]);
                    }
                }
            }
        }
        return dp[arrays.length - 1][m][n];
    }

    /**
     * 求0和1的个数，用一个数组[x, y]返回
     */
    private int[] transform(String s) {
        int countZero = 0;
        int countOne = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '0') {
                countZero++;
            } else {
                countOne++;
            }
        }
        return new int[]{countZero, countOne};
    }
}
