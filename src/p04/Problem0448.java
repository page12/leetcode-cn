package p04;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/find-all-numbers-disappeared-in-an-array/
 */
public class Problem0448 {

    public static void main(String[] args) {
        Problem0448 p = new Problem0448();
        System.err.println(p.findDisappearedNumbers2(new int[]{4, 3, 2, 7, 8, 2, 2, 1}));
        System.err.println(p.findDisappearedNumbers2(new int[]{4, 4, 1, 2, 3}));
    }

    /**
     * 思路：因为不知道少了哪几个数字，所以最开始想到的就是用一个hash表
     * nums长度为n，所求的也是[1, n]之间缺少的数
     * 当不缺少时，与[1, n]一一对应，当缺少时，中有一个数字会映射不上
     * 所以可以用数组当hash表，长度为n，key就是下标加1，下标就是key减1，key就是nums中的元素
     * 因为只需要判断标记一个是不是不存在，所以可以用boolean类型的数组
     * 这样做法需要O(N)的空间复杂度，时间复杂度为O(N)
     * <br/>
     * 死活想不出复杂度为O(1)的方法
     * 看了下官方题解，发现直接用原数组nums当做hash表，这样就省下了O(N)的空间
     * 题目也没描述可以修改原数组，这种情况下，没几个人会去想到修改原数组
     * 心累了
     * <br/>
     * 如果允许直接修改原数组，那么该怎么改进上面的解法？
     * 直接和hash表一样设置为true（使用数字1）会覆盖掉后面的数，这样肯定不行
     * 但是要进行标记就一定会进行set操作，所以要想一种能够逆运算的set操作
     * 通过逆运算能够快速还原nums数组元素本身的值，同时正运算的结果能够很快地计算true or false
     * 因为nums中都是正数，所以最简单的这种运算就是相反数（乘以-1，如果元素是负数，则代表已经标记过了，不需要再乘以-1）
     * 这样就可以把空间复杂度降到O(1)，并且可以还原回原来的数组
     */
    public List<Integer> findDisappearedNumbers(int[] nums) {
        // 这是新建hash表的解法
        boolean[] hash = new boolean[nums.length];
        for (int i = 0; i < nums.length; i++) {
            hash[nums[i] - 1] = true;
        }
        List<Integer> missing = new ArrayList<>();
        // 找出所有false，即未被标记的
        for (int i = 0; i < hash.length; i++) {
            if (!hash[i]) {
                missing.add(i + 1);
            }
        }
        return missing;
    }

    public List<Integer> findDisappearedNumbers2(int[] nums) {
        // 这是直接用原数组当hash表的解法
        for (int i = 0; i < nums.length; i++) {
            int originValue = nums[i] > 0 ? nums[i] : -nums[i];
            // 如果元素是负数，则代表已经标记过了，是true，不需要再乘以-1了
            if (nums[originValue - 1] > 0) {
                nums[originValue - 1] *= -1;
            }
        }
        List<Integer> missing = new ArrayList<>();
        // 找出所有false，即未被标记的，也就是正数
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                missing.add(i + 1);
            } else {
                // 还原数组
                // 本题没必要，还浪费执行时间
                // 但是实际编程中这样很有必要
                nums[i] *= -1;
            }
        }
        return missing;
    }
}
