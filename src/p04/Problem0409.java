package p04;

/**
 * https://leetcode-cn.com/problems/longest-palindrome/
 */
public class Problem0409 {

    public static void main(String[] args) {
        Problem0409 p = new Problem0409();
        System.err.println(p.longestPalindrome("abcde"));
        System.err.println(p.longestPalindrome("abccccdd"));
    }

    /**
     * 思路：当回文串是偶数长度时，所有字符都是偶数此，当是奇数长度时，只有中间的那个字符是奇数次
     * 因此最大的回文串的长度
     * 等于 所有偶数次数的字符的次数总和 + 奇数次数最大的字符的次数 + 剩下的奇数次数每个都分别减1的总和
     * <br/>
     * 进一步分析
     * 如果有奇数次数，则长度
     * 等于 所有偶数次数的字符的次数总和 + 所有奇数次数每个都分别减1的总和 + 1
     * 也等于 s的长度 减去 奇数字符的个数 + 1
     * 如果没有奇数字符，则长度
     * 等于 s的长度
     * <br/>
     * 这样题目就转换为求字符串的次数，利用一个hash表就可以
     * 假设了只有大写和小写字母，因此可以使用固定的52长度hash表
     * 时间复杂度O(N)
     * 空间复杂度O(字符集的大小)，本题字符集的大小固定为52
     */
    public int longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }
        // 前26个为大写，后26个为小写
        int[] charMap = new int[52];
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int idx;
            if (c <= 'Z') {
                idx = c - 'A';
            } else {
                idx = c - 'a' + 26;
            }
            charMap[idx] = charMap[idx] + 1;
        }
        // 结果总长度
        int result = 0;
        boolean existOdd = false;
        for (int i = 0; i < charMap.length; i++) {
            int times = charMap[i];
            if ((times & 1) != 0) {
                // 奇数判断下是不是最大奇数
                result += times - 1;
                existOdd = true;
            } else {
                // 偶数直接累加
                result += times;
            }
        }
        return existOdd ? result + 1 : result;
    }
}
