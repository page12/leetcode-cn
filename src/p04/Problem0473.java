package p04;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**
 * https://leetcode-cn.com/problems/matchsticks-to-square/
 */
public class Problem0473 {

    public static void main(String[] args) {
        Problem0473 p = new Problem0473();
        System.err.println(p.makesquare(new int[]{2, 2, 2, 2}));
        System.err.println(p.makesquare(new int[]{1, 1, 2, 2, 2}));
        System.err.println(p.makesquare(new int[]{3, 3, 3, 3, 4}));
        System.err.println(p.makesquare(new int[]{13, 11, 1, 8, 6, 7, 8, 8, 6, 7, 8, 9, 8}));
    }

    /**
     * 思路：
     * 能用所有边拼出正方形，则数组和一定是4n，否则一定不能
     * 先用凑出一条边，并标记使用了哪些元素
     * 然后再凑出剩下的三条边
     * 凑出一条边就是选取X个数，让他们的和加起来为n，可以使用递归
     * 为了方便处理，可以先排序，并且总是从剩余的最大的那一个数字开始去尝试
     * 回溯时，刚刚使用过了元素要重新标记为未使用
     */
    public boolean makesquare(int[] matchsticks) {
        // 题目有假设，不做校验
        Arrays.sort(matchsticks);
        int sum = 0;
        for (int i : matchsticks) {
            sum += i;
        }
        if ((sum & 3) != 0) {
            return false;
        }
        int length = sum >> 2;
        boolean[] used = new boolean[matchsticks.length];
        Deque<Integer> stack = new LinkedList<>();
        return recurse(0, matchsticks, used, stack, length, matchsticks.length - 1);
    }

    /**
     * 递归处理
     *
     * @param remainingLength 当前要拼凑的边的剩余长度
     * @param matchsticks     原数组
     * @param used            使用情况数组
     * @param stack           一个栈，记录已经被使用的所有元素
     * @param length          边长，用于拼凑好一条边后，重新设置remainingLength
     * @param maxIndex        总是从剩余的最大的那一个数字开始去尝试，这个就是那个最大数字的下标
     * @return
     */
    private boolean recurse(int remainingLength, int[] matchsticks, boolean[] used, Deque<Integer> stack, int length, int maxIndex) {
        if (remainingLength == 0) {
            if (getHandledCount(stack, length) == 4) {
                return true;
            } else {
                remainingLength = length;
                maxIndex = matchsticks.length - 1;
            }
        }
        for (int i = maxIndex; i >= 0; i--) {
            if (used[i] || matchsticks[i] > remainingLength) {
                continue;
            }
            stack.push(matchsticks[i]);
            used[i] = true;
            boolean result = recurse(remainingLength - matchsticks[i], matchsticks, used, stack, length, i - 1);
            if (result) {
                return true;
            } else {
                stack.pop();
                used[i] = false;
            }
        }
        return false;
    }

    /**
     * 计算已经拼凑好了的边的数量
     */
    private int getHandledCount(Deque<Integer> stack, int length) {
        int sum = 0;
        for (int i : stack) {
            sum += i;
        }
        return sum / length;
    }
}
