package p04;

/**
 * https://leetcode-cn.com/problems/third-maximum-number/comments/
 */
public class Problem0414 {

    public static void main(String[] args) {
        System.err.println(Integer.MIN_VALUE);
        Problem0414 p = new Problem0414();
        System.err.println(p.thirdMax(new int[]{3, 2, 1}) == 1);
        System.err.println(p.thirdMax(new int[]{1, 2}) == 2);
        System.err.println(p.thirdMax(new int[]{2, 2, 3, 1}) == 1);
        System.err.println(p.thirdMax(new int[]{1, 1, 2}) == 2);
        System.err.println(p.thirdMax(new int[]{2, 1, Integer.MIN_VALUE}) == Integer.MIN_VALUE);
        System.err.println(p.thirdMax(new int[]{1, 2, 2, 5, 3, 5}) == 2);
        System.err.println(p.thirdMax(new int[]{1, Integer.MIN_VALUE, 2}) == Integer.MIN_VALUE);
    }

    /**
     * 思路：三个变量分别表示最大first，第二大second，第三大的数third
     * 一次遍历同时处理三个数，时刻满足first > second > third
     * 注意，要求返回第三大的数，是指在所有不同数字中排第三大的数，所以if判断时要排除掉相等的情况
     * 为了避免Integer.MIN_VALUE这种恶心数字，使用包装类，能够大大减少判断逻辑
     */
    public int thirdMax(int[] nums) {
        // 题目假设了非空数组
        // 提前进行一些判断，避免后面麻烦的if-else
        if (nums.length == 1) {
            return nums[0];
        }
        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        // 初始化赋值，利用包装类，避免Integer.MIN_VALUE这种恶心数字
        Integer first = nums[0];
        Integer second = null;
        Integer third = null;
        for (int i = 0; i < nums.length; i++) {
            int n = nums[i];
            if (n > first) {
                third = second;
                second = first;
                first = n;
            } else if (first > n && (second == null || n > second)) {
                third = second;
                second = n;
            } else if (second != null && second > n && (third == null || n > third)) {
                third = n;
            }
        }
        // first初始化成第一个值，如果second和third都不为null，则代表它们都是通过正确的比较关系进行了赋值
        // 正确的比较关系是指 first > second > third
        return second != null && third != null ? third : first;
    }
}
