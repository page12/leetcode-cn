package p04;

/**
 * https://leetcode-cn.com/problems/license-key-formatting/
 */
public class Problem0482 {

    public static void main(String[] args) {
        Problem0482 p = new Problem0482();
        System.err.println(p.licenseKeyFormatting("5F3Z-2e-9-w", 4));
        System.err.println(p.licenseKeyFormatting("2-5g-3-J", 2));
    }

    /**
     * 思路：因为第一部分允许少，其余部分都不允许少，所以可以从右往左遍历，最后反转下就得到结果
     * 为了判断是否添加破折号，引入一个变量nonDashCount表示已经添加的非破折号字符的个数
     * 时间复杂度O(N)
     * 空间复杂度O(1)（不考虑返回字符串的空间消耗）
     */
    public String licenseKeyFormatting(String s, int k) {
        // 题目s非空
        int nonDashCount = 0;
        StringBuilder builder = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; i--) {
            char c = s.charAt(i);
            if (c == '-') {
                continue;
            }
            // 小写字母转换为大写
            if (c >= 'a') {
                c = (char) (c - 32);
            }
            // 为了避免最后字符串左边添加了无用的破折号，循环里面先判断是否添加破折号
            if (nonDashCount > 0 && nonDashCount % k == 0) {
                builder.append('-');
            }
            builder.append(c);
            nonDashCount++;
        }
        return builder.reverse().toString();
    }
}
