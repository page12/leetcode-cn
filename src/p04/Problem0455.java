package p04;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/assign-cookies/
 */
public class Problem0455 {

    public static void main(String[] args) {
        Problem0455 p = new Problem0455();
        System.err.println(p.findContentChildren(new int[]{1, 2, 3}, new int[]{1, 1}) == 1);
        System.err.println(p.findContentChildren(new int[]{1, 2}, new int[]{1, 2, 3}) == 2);
    }

    /**
     * 思路：如果是有序数组，那么就很简单了
     * 一个指针pG遍历g，一个指针pS遍历s
     * 当pG++时，遍历找到第一个pS使得s[pS] >= g[pG]，此时计数加1，然后pS++
     * 任意一个数组遍历完了都算是整个流程结束
     * 因为题目没有说是否排序，所以要先排序
     * 所以总的时间空间复杂度为对应的排序算法的复杂度在两个数组上的和
     */
    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int pG = 0;
        int pS = 0;
        int result = 0;
        while (pG < g.length) {
            while (pS < s.length && s[pS] < g[pG]) {
                pS++;
            }
            if (pS == s.length) {
                break;
            }
            result++;
            pS++;
            pG++;
        }
        return result;
    }
}
