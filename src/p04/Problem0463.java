package p04;

/**
 * https://leetcode-cn.com/problems/island-perimeter/
 */
public class Problem0463 {

    public static void main(String[] args) {

    }

    /**
     * 思路：直接遍历，累加当前点的有效的边的数量
     * 主要就是判断当前点边的数量的if-else
     */
    public int islandPerimeter(int[][] grid) {
        // 题目有假设，不做校验
        int result = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                // 跳过海水节点
                if (grid[i][j] == 0) {
                    continue;
                }
                // 存在上边
                if (i == 0 || grid[i - 1][j] == 0) {
                    result++;
                }
                // 存在下边
                if (i == grid.length - 1 || grid[i + 1][j] == 0) {
                    result++;
                }
                // 存在左边
                if (j == 0 || grid[i][j - 1] == 0) {
                    result++;
                }
                // 存在右边
                if (j == grid[0].length - 1 || grid[i][j + 1] == 0) {
                    result++;
                }
            }
        }
        return result;
    }
}
