package p04;

import common.TreeNode;

/**
 * https://leetcode-cn.com/problems/sum-of-left-leaves/
 */
public class Problem0404 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(0);
        root.left = new TreeNode(2);
        root.right = new TreeNode(4);
        root.left.left = new TreeNode(1);
        root.left.left.left = new TreeNode(5);
        root.left.right = new TreeNode(1);
        root.right.left = new TreeNode(3);
        root.right.left.right = new TreeNode(6);
        root.right.right = new TreeNode(-1);
        root.right.right.right = new TreeNode(8);

        Problem0404 p = new Problem0404();
        System.err.println(p.sumOfLeftLeaves(root));
    }

    /**
     * 思路：先根遍历递归
     * 遍历操作具体指的是：如果一个节点有左叶子，则累加
     * 左叶子 = 左叶子存在，其左叶子的左右叶子都不存在
     */
    public int sumOfLeftLeaves(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int sum = 0;
        // 如果当前节点有左叶子，则累加
        if (root.left != null && root.left.left == null && root.left.right == null) {
            sum += root.left.val;
        }
        // 递归左子树
        sum += sumOfLeftLeaves(root.left);
        // 递归右子树
        sum += sumOfLeftLeaves(root.right);
        return sum;
    }
}
