package p04;

import java.util.Arrays;

/**
 * https://leetcode-cn.com/problems/construct-the-rectangle/
 */
public class Problem0492 {

    public static void main(String[] args) {
        Problem0492 p = new Problem0492();
        System.err.println(Arrays.toString(p.constructRectangle(4)));
        System.err.println(Arrays.toString(p.constructRectangle(5)));
        System.err.println(Arrays.toString(p.constructRectangle(10)));
    }

    /**
     * 思路：利用数学性质
     * 正方形在同等面积的矩形中，周长是最小的，两边之差也是最小的（就是0）
     * 越接近正方形，则两边之差越小
     * <br/>
     * 这个很好证明
     * 设两边分别为a和b，规定a >= b > 0，面积S = ab，那么b = S/a
     * 两边之差 = a - b = a - S/a >= 0
     * 导数为 1 + S/a^a 恒大于 0，所以a - S/a为单调递增函数
     * 也就是a越大则两边之差最大，a越小两边之差最小
     * 所以a = b时两边之差最小
     * <br/>
     * 本题中a和b为整数，且S = ab，也就是求最接近sqrt(S)的整数a，使得b同时为整数
     * 因为ab可以互换，所以改为求b，也就是找[1, sqrt(S)]区间内符合条件的最大整数
     * 时间复杂度为O(sqrt(n))，空间复杂度为O(1)
     */
    public int[] constructRectangle(int area) {
        // 题目有假设 area > 0
        int[] result = new int[2];
        int sqrt = (int) Math.sqrt(area);
        for (int i = sqrt; i >= 1; i--) {
            if (area % i == 0) {
                result[0] = area / i;
                result[1] = i;
                break;
            }
        }
        return result;
    }
}
