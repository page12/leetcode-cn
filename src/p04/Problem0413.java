package p04;

/**
 * https://leetcode-cn.com/problems/arithmetic-slices/
 */
public class Problem0413 {

    public static void main(String[] args) {
        Problem0413 p = new Problem0413();
        System.err.println(p.numberOfArithmeticSlices(new int[]{1, 2, 3, 4}) == 3);
        System.err.println(p.numberOfArithmeticSlices(new int[]{1}) == 0);
    }

    /**
     * 思路：
     * 子数组是连续在一起的，也就是不能跳着判断是不是等差，必须是相邻判断，不要想复杂了
     * 如果 [i,j] 是等差数列，j+1 不满足，则不用再管 [i,j] 这个区间了
     * 对于等差数列，它的满足连续子数组（子数组长度大于等于3）的等差数列的个数为 1 + 2 + ... + (n-2) = (n-1)*(n-2)/2
     * 找到所有满足等差的 [i,j] 的最长区间，然后计算个数，并累加即可
     */
    public int numberOfArithmeticSlices(int[] nums) {
        // 题目有假设，不做校验
        int result = 0;
        int length = 2;
        for (int i = 2; i < nums.length; i++) {
            if (nums[i] - nums[i - 1] == nums[i - 1] - nums[i - 2]) {
                length++;
            } else {
                result += (length - 1) * (length - 2) / 2;
                length = 2;
            }
        }
        // 最后一次在循环中没有累加，放在这里
        return result + (length - 1) * (length - 2) / 2;
    }

}
