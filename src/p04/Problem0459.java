package p04;

/**
 * https://leetcode-cn.com/problems/repeated-substring-pattern/
 */
public class Problem0459 {

    public static void main(String[] args) {

    }

    /**
     * 思路：
     * 这题我之前在别的地方遇见过，因为解法的代码很简单，所以记下来了
     * 但是忘了推导过程了
     * 看了官方题解，有差不多知道了为什么了（严格来说是知道结果代码，去反过来证明它能回推回去）
     * 这跟数学里面有些公式挺像的，知道公式本身去验证它是正确的很简单
     * 但是再完全不知道公式的情况下，根据别的东西去推导出这个公式很难
     */
    public boolean repeatedSubstringPattern(String s) {
        return (s + s).substring(1, s.length() * 2 - 1).contains(s);
    }
}
