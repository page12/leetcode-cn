package p04;

/**
 * https://leetcode-cn.com/problems/partition-equal-subset-sum/
 */
public class Problem0416 {

    public static void main(String[] args) {
        Problem0416 p = new Problem0416();
        System.err.println(p.canPartition(new int[]{1, 5, 11, 5}));
        System.err.println(p.canPartition(new int[]{1, 2, 3, 6}));
        System.err.println(p.canPartition(new int[]{8, 4}));
    }

    /**
     * 思路：
     * 首先，数组必须能分割，也就是长度大于等于2
     * 其次，计算整个数组的和 s
     * 如果 s 是奇数，则一定不能分割
     * 如果 s 是偶数，则转换为 数组中是否有 x 个元素的和为 t = s/2
     * 这样就和01背包问题很像了
     * <br/>
     * 设dp为二维数组，行为 nums.length，列为 t+1
     * dp[i][j] = true 表示 [0,i] 中能够找出几个数使得它们的和等于 j
     * 把j用nums中的数进行分解，有两种情况
     * 1、如果 j 的分解中包含 nums[i]，则剩下的 [0,i-1] 中必须存在一个和等于 j-nums[i]，此时 dp[i][j] = dp[i-1][j-nums[i]]
     * 2、如果 j 的分解中不包含 nums[i]，则 dp[i][j] = true 等价于 剩下的 [0,i-1] 中必须存在一个和等于j，此时 dp[i][j] = dp[i-1][j]
     * 两种情况有一种满足就可以了因此 dp[i][j] = dp[i-1][j-nums[i]] || dp[i-1][j]
     * <br/>
     * 特殊情况
     * 如果 j < nums[i]，则j的分解中一定不会包含nums[i]，此时情况1没有意义，只能使用情况2
     * 如果 i = 0，表示只有一个可以选择的数，此时只有 dp[0][nums[0]] = true，要满足 nums[0] <= t
     * 如果 j = 0，代表要选几个数使得它们的和为0，此时可以当作 false，也可以当作true（比如11分解成11+0，此时0相当于剩下的中找出和为0的）
     * 如果 i = 0 && j == 0，此时同 j = 0 的情况
     * 最终结果就是dp数组的右下角，也就是dp[nums.length-1][t]
     */
    public boolean canPartition(int[] nums) {
        // 题目有假设，不做校验
        if (nums.length == 1) {
            return false;
        }
        // 先求和
        int s = 0;
        for (int num : nums) {
            s += num;
        }
        // 和为奇数时一定不存在
        if ((s & 1) == 1) {
            return false;
        }
        int t = s >> 1;
        // 可以使用滚动数组优化
        boolean[][] dp = new boolean[nums.length][t + 1];
        // 第一行只用处理一个值
        if (nums[0] <= t) {
            dp[0][nums[0]] = true;
        }
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j <= t; j++) {
                if (j == 0) {
                    dp[i][j] = false;
                } else if (j < nums[i]) {
                    dp[i][j] = dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j - nums[i]] || dp[i - 1][j];
                }
            }
        }
        return dp[nums.length - 1][t];
    }
}
