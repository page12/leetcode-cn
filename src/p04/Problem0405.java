package p04;

/**
 * https://leetcode-cn.com/problems/convert-a-number-to-hexadecimal/
 */
public class Problem0405 {

    public static void main(String[] args) {
        Problem0405 p = new Problem0405();
//        System.err.println(p.toHex(0));
//        System.err.println(p.toHex(1));
//        System.err.println(p.toHex(-1));
//        System.err.println(p.toHex(26));
//        System.err.println(p.toHex(16));
//        System.err.println(p.toHex(10000));
        System.err.println(p.toHex(111111));
    }

    /**
     * 思路：通过二进制移位控制循环，一次处理四位
     * 不断把num左移位，当num == 0时，不论移位了几次，都不需要再处理了，剩下的都是左边的0，本身就不需要包含在结果中
     */
    public String toHex(int num) {
        if (num == 0) {
            return "0";
        }
        StringBuilder builder = new StringBuilder();
        int mask = 0xf;
        // num != 0把左边的0全部排除掉了，后面不用再处理
        while (num != 0) {
            builder.append(bit2Hex(num & mask));
            // 使用无符号移位
            num >>>= 4;
        }
        return builder.reverse().toString();
    }

    /**
     * 四个bit位转换为十六进制字符
     */
    private char bit2Hex(int n) {
        switch (n) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return (char) (n + 48);
            case 10:
                return 'a';
            case 11:
                return 'b';
            case 12:
                return 'c';
            case 13:
                return 'd';
            case 14:
                return 'e';
            case 15:
                return 'f';
            default:
                throw new IllegalArgumentException("error arg: " + n);
        }
    }
}
