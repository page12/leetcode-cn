package p04;

/**
 * https://leetcode-cn.com/problems/max-consecutive-ones/
 */
public class Problem0485 {

    public static void main(String[] args) {
        Problem0485 p = new Problem0485();
        System.err.println(p.findMaxConsecutiveOnes(new int[]{1, 1, 0, 1, 1, 1}));
    }

    /**
     * 思路：简单的遍历计数
     * 遍历数组，当前长度初始化为0
     * 如果元素为1，则当前长度加1
     * 否则，更新下最大长度，同时把当前长度重置为0
     */
    public int findMaxConsecutiveOnes(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int maxLength = 0;
        int currentLength = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                currentLength++;
            } else {
                maxLength = Math.max(maxLength, currentLength);
                currentLength = 0;
            }
        }
        // 因为数组最右边是1时，走不到循环内部的else分支，因此这里还有判断下
        return Math.max(maxLength, currentLength);
    }
}
