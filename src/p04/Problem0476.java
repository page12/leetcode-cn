package p04;

/**
 * https://leetcode-cn.com/problems/number-complement/
 */
public class Problem0476 {

    public static void main(String[] args) {
        Problem0476 p = new Problem0476();
        System.err.println(p.findComplement(5) == 2);
        System.err.println(p.findComplement(1) == 0);
        System.err.println(p.findComplement(0) == 1);
        System.err.println(p.findComplement(2) == 1);
    }

    /**
     * 思路：通过异或运算求二进制补码
     * 异或的数就是bit位数相同，但是所有bit位全为1的二进制数
     * 二进制全为1时
     * 如果是正数，代表它是2^n - 1，其中2^n是大于num的最小2^n
     * 如果是负数，代表它是-1
     * Integer.highestOneBit(num)是不大于num的最大2^n，把它右移一位就得到大于num的最小2^n
     */
    public int findComplement(int num) {
        if (num == 0) {
            return 1;
        }
        return num < 0 ? -1 : num ^ ((Integer.highestOneBit(num) << 1) - 1);
    }
}
