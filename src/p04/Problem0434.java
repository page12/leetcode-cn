package p04;

/**
 * https://leetcode-cn.com/problems/number-of-segments-in-a-string/
 */
public class Problem0434 {

    public static void main(String[] args) {
        Problem0434 p = new Problem0434();
        System.err.println(p.countSegments("Hello, my name is John") == 5);
        System.err.println(p.countSegments("a") == 1);
        System.err.println(p.countSegments("     ") == 0);
    }

    /**
     * 思路：使用两个标志位 findNonSpace = 是否发现非空格字符，findSpace = 标记是否发现空格字符
     * 两个标志位初始化为false
     * 顺序遍历字符串中的每一个字符
     * 找到非空格字符时令findNonSpace = true，到空格字符时，如果findNonSpace = true，则findSpace = true
     * 如果两个标志位都是true，则单词数加1，并令findNonSpace = false，findSpace = false
     * 如果遍历完成了但是findNonSpace = true，代表没有尾部空格，此时单词数要加1
     * 时间复杂度O(N)，空间复杂度O(1)
     */
    public int countSegments(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int result = 0;
        // 标记是否发现非空格字符
        boolean findNonSpace = false;
        // 标记是否发现空格字符
        boolean findSpace = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c != ' ') {
                findNonSpace = true;
            }
            if (findNonSpace && c == ' ') {
                findSpace = true;
            }
            if (findNonSpace && findSpace) {
                result++;
                findNonSpace = false;
                findSpace = false;
            }
        }
        if (findNonSpace) {
            result++;
        }
        return result;
    }
}
