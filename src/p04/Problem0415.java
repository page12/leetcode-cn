package p04;

/**
 * https://leetcode-cn.com/problems/add-strings/
 */
public class Problem0415 {

    public static void main(String[] args) {
        Problem0415 p = new Problem0415();
        System.err.println(p.addStrings("1111", "2222"));
        System.err.println(p.addStrings("9", "1"));
        System.err.println(p.addStrings("99999", "99999"));
        System.err.println(p.addStrings("11", "123"));
    }

    /**
     * 思路：字符串我们一般左对齐，但是数字相加是右对齐相加，循环时注意下标i的使用即可
     * 十进制进位后本位数字大于10变成两个字符，因此直接在循环中判断是否进位并处理
     * 最高位的进位留在最后处理，进位了直接补充字符1即可
     * 时间复杂度O(max(N, M))
     * 空间复杂度O(1)（不考虑返回结果的字符串操作相关的复制空间消耗，java字符串是不变类，对其进行操作会返回一个新的字符串）
     * 类似的题：第66题，第67题
     */
    public String addStrings(String num1, String num2) {
        // 题目有假设，不做校验
        char[] chars = new char[Math.max(num1.length(), num2.length())];
        // 进位标志
        boolean flag = false;
        // 下面循环中i代表右数第i位
        for (int i = 0; i < chars.length; i++) {
            // 左对齐，倒序取出对应的位
            int x = i >= num1.length() ? 0 : num1.charAt(num1.length() - 1 - i) - '0';
            int y = i >= num2.length() ? 0 : num2.charAt(num2.length() - 1 - i) - '0';
            int digit = flag ? x + y + 1 : x + y;
            // 进位标志
            flag = digit >= 10;
            if (flag) {
                digit -= 10;
            }
            // 倒序set
            chars[chars.length - 1 - i] = (char) (digit + '0');
        }
        if (flag) {
            return "1" + new String(chars);
        } else {
            return new String(chars);
        }
    }
}
