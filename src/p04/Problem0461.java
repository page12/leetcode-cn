package p04;

/**
 * https://leetcode-cn.com/problems/hamming-distance/
 */
public class Problem0461 {

    public static void main(String[] args) {

    }

    /**
     * 思路：根据异或运算的定义，如果比特位不同，则返回1，相同则返回0
     * 因此x，y的汉明距离就是x^y的二进制中1的个数
     * 二进制中1的个数参考第191题
     */
    public int hammingDistance(int x, int y) {
        return Integer.bitCount(x ^ y);
    }
}
