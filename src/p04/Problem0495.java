package p04;

/**
 * https://leetcode-cn.com/problems/teemo-attacking/
 */
public class Problem0495 {

    public static void main(String[] args) {
        Problem0495 p = new Problem0495();
        System.err.println(p.findPoisonedDuration(new int[]{1, 4}, 2) == 4);
        System.err.println(p.findPoisonedDuration(new int[]{1, 2}, 2) == 3);
        System.err.println(p.findPoisonedDuration(new int[]{1, 5, 10, 15}, 1) == 4);
        System.err.println(p.findPoisonedDuration(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 10) == 19);
        System.err.println(p.findPoisonedDuration(new int[]{0, 1, 2, 3}, 1) == 4);
    }

    /**
     * 思路：很直观的遍历
     * <br/>
     * 设x为中毒结束时间，sum为总的持续时间
     * 遍历t in timeSeries，有ab两种情况要考虑
     * a、如果x < t，那么x = t + duration - 1，sum += duration
     * b、如果x >= t，那么此时刻之前就已经中毒
     * 此刻再中毒，则中毒状态能持续到t + duration - 1，此时刻之前的那次中毒能持续到x，两者之差就是增加的时间，因此sum += t + duration - 1 - x
     * 之后要刷新中毒结束时间，令x = t + duration - 1
     * <br/>
     * 总共只用遍历一次数组，所以时间复杂度为O(N)
     * 只需要常数个变量，所以空间复杂度为O(1)
     */
    public int findPoisonedDuration(int[] timeSeries, int duration) {
        if (timeSeries == null || timeSeries.length == 0 || duration == 0) {
            return 0;
        }
        int x = -1;
        int sum = 0;
        for (int t : timeSeries) {
            if (x < t) {
                sum += duration;
            } else {
                sum += t + duration - 1 - x;
            }
            x = t + duration - 1;
        }
        return sum;
    }
}
