package p04;

/**
 * https://leetcode-cn.com/problems/arranging-coins/
 */
public class Problem0441 {

    public static void main(String[] args) {
        Problem0441 p = new Problem0441();
        System.err.println(p.arrangeCoins(5));
        System.err.println(p.arrangeCoins(8));
        System.err.println(p.arrangeCoins(1804289383));
    }

    /**
     * 思路：就是二分查找，改下判断条件就行
     * 利用简单是数学性质缩减初始范围
     * 假设k > 0，则k行总共有(k + 1)k/2 = n，可以简单推导出 k*k >= n，当且仅当n=1时取等号
     * 因此可以可以直接使用[sqrt(n)向下取整, n-1]这个区间
     */
    public int arrangeCoins(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        // 使用long避免溢出
        long start = (int) Math.sqrt(n);
        long end = n - 1;
        long mid = 0;
        while (start <= end) {
            mid = (end - start) / 2 + start;
            long total = mid * (mid + 1) / 2;
            if (0 <= n - total && n - total <= mid) {
                // 刚好填充完第mid行，或者填充完第mid行但是填充不完第mid+1行
                // 这两种情况都算找到了
                break;
            } else if (n < total) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        // 一定能找到，所以不要管找不到情况，直接返回mid即可
        return (int) mid;
    }
}
