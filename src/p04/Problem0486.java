package p04;

/**
 * https://leetcode-cn.com/problems/predict-the-winner/
 */
public class Problem0486 {

    public static void main(String[] args) {
        Problem0486 p = new Problem0486();
        System.err.println(p.PredictTheWinner(new int[]{1, 5, 2}));
        System.err.println(p.PredictTheWinner(new int[]{1, 5, 233, 7}));
    }

    /**
     * 思路：
     * 先理解净胜分
     * 玩家1的净胜分 = 玩家1的所有选择的和减去玩家2的所有选择的和
     * 因为玩家1自己选择时x，玩家1的净胜分加x，玩家2选择y时，玩家1的净胜分减y
     * 当玩家1最终的净胜分大于0时，玩家1获胜
     * 只要有一种情况净胜分大于0，则能获胜
     * 因此只要最终的最大净胜分大于0就能获胜，反之则不能获胜
     * <br/>
     * 设dp[i][j]表示对于下标[i,j]，轮到玩家1选择时，玩家1的最大净胜分
     * 这个题目所求结果等价于 dp[0][length-1] >= 0
     * 此时玩家1只能选择nums[i]或者nums[j]
     * 1、当选择nums[i]时，玩家1加分nums[i]
     * 然后轮到玩家2选择，此时区间为[i+1,j]
     * 玩家2也按最优选择的话，此区间内玩家2对玩家1的最大净胜分为 dp[i+1][j]
     * 反过来也就是此区间玩家1对玩家2的最大净胜分为 -dp[i+1][j]
     * 2、当选择nums[j]时，和情况1一样，可以知道此区间玩家1对玩家2的最大净胜分为 nums[j] - dp[i][j-1]
     * <br/>
     * 两种情况的最大净胜分中较大的那一个就是最终的最大净胜分
     * 因此 dp[i][j] = max { nums[i] - dp[i+1][j], nums[j] - dp[i][j-1] }
     * 特殊情况：
     * 当i=j时，也就是只有一个可选，此时轮到玩家1选，玩家2没得选，因此玩家1此时的最大净胜分为 nums[i]
     * 当i>j时，没意义，不用计算
     * <br/>
     * dp状态是一个二维数组
     * 按照递推公式，需要知道下方的，和左边的dp值
     * 因此需要从下到上从左到右来循环
     * 二维数组也可以使用滚动数组来优化成一维数组（下面的代码没有使用这个优化技巧）
     */
    public boolean PredictTheWinner(int[] nums) {
        // 题目有假设，不做校验
        // 可以使用滚动数组来优化
        int[][] dp = new int[nums.length][nums.length];
        for (int i = nums.length - 1; i >= 0; i--) {
            for (int j = i; j <= nums.length - 1; j++) {
                if (i == j) {
                    dp[i][j] = nums[i];
                } else {
                    dp[i][j] = Math.max(nums[i] - dp[i + 1][j], nums[j] - dp[i][j - 1]);
                }
            }
        }
        return dp[0][nums.length - 1] >= 0;
    }
}
