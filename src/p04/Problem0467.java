package p04;

/**
 * https://leetcode-cn.com/problems/unique-substrings-in-wraparound-string/
 */
public class Problem0467 {

    public static void main(String[] args) {
        Problem0467 p = new Problem0467();
        System.err.println(p.findSubstringInWraproundString("a") == 1);
        System.err.println(p.findSubstringInWraproundString("cac") == 2);
        System.err.println(p.findSubstringInWraproundString("zab") == 6);
        System.err.println(p.findSubstringInWraproundString("zabcabcd") == 14);
    }

    /**
     * 思路：
     * 题目的意思是求子串个数，并且这些子串要从左至右满足字符的连续字典顺序，不能跳跃
     * 外加一个特殊情况，那就是如果a在z右边，也就是za也是满足的
     * 比如abc，它满足条件的子串有 a,ab,abc,b,bc,c 共六种，没有重复的
     * 比如zabcabcd，它的zabc和abcd都是满足条件的，因此满足条件的子串有 z,za,zab,zabc,a,ab,abc,abcd,b,bc,bcd,c,cd,d 共14种，其中a->c之间有重复的
     * <br/>
     * 没有重复时数量子串数量为 n(n+1)/2，这个比较简单
     * 观察abc，发现它的所有满足条件的子串，分别等于其中所有字符开头的子串的并集
     * 对于zabcabcd，发现它尽管有重复的，但是也满足这一点
     * <br/>
     * 没有重复字符时，并集之间不相交，因此各自数量可以直接累加
     * 并且各自的数量 = 以它开头的最长子串的长度
     * 例如：以a开头的最长长度为abc也就是3，以b开头的最长长度是bc也就是2，以c开头的只有c也就是1，加起来是6
     * 有重复时，比如zabcabcd，其中abc是abcd的子串，因此可以只计算abcd的结果
     * 有多个构成子集关系的时，应该使用范围最大的那一个，也就是长度最大的那一个
     * abc的长度为3，abcd的长度为4，因此使用4
     * 这样本题就转换为求字符串p中以26个字母开头的最长连续长度，最终结果是所有这些长度的和
     * <br/>
     * 对于abc这种，如果从左开始遍历，则不好统计已经遍历过的字符对应的长度
     * 如果改为从右开始遍历，则要方便不少
     */
    public int findSubstringInWraproundString(String p) {
        // 题目有假设，不做校验
        if (p.length() == 1) {
            return 1;
        }
        int[] charHash = new int[26];
        int length = p.length();
        // 最后一个字母没有后继，提前设置它的值
        charHash[p.charAt(length - 1) - 'a'] = 1;
        int len = 1;
        // 从倒数第二个开始
        for (int i = p.length() - 2; i >= 0; i--) {
            char current = p.charAt(i);
            char next = p.charAt(i + 1);
            if (current == next - 1 || (current == 'z' && next == 'a')) {
                len++;
            } else {
                len = 1;
            }
            charHash[current - 'a'] = Math.max(charHash[current - 'a'], len);
        }
        // 求和
        int result = 0;
        for (int maxLength : charHash) {
            result += maxLength;
        }
        return result;
    }
}
