package p04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * https://leetcode-cn.com/problems/binary-watch/
 */
public class Problem0401 {

    public static void main(String[] args) {
        Problem0401 p = new Problem0401();
        System.err.println(p.readBinaryWatch(0));
        System.err.println(p.readBinaryWatch(1));
        System.err.println(p.readBinaryWatch(9));
    }

    /**
     * 思路：
     * 上面最多亮3个灯，下面最多亮5个灯，上面的数值[0, 11]，下面的数值[0, 59]
     * 循环求出所有组合形式，同时排除掉不符合格式的
     * 剩下的就是所求结果
     */
    public List<String> readBinaryWatch(int turnedOn) {
        if (turnedOn > 8 || turnedOn < 0) {
            return Collections.emptyList();
        }
        List<String> result = new ArrayList<>();
        if (turnedOn == 0) {
            result.add("0:00");
            return result;
        }
        // 直接枚举
        String format = "%d:%02d";
        for (int i = 1 << (turnedOn - 1); i < (1 << 10); i++) {
            if (turnedOn != Integer.bitCount(i)) {
                continue;
            }
            int hour = i >>> 6;
            if (hour > 11) {
                continue;
            }
            int minute = i & 0x3f;
            if (minute > 59) {
                continue;
            }
            result.add(String.format(format, hour, minute));

        }
        return result;
    }
}
