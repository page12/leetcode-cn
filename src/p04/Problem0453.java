package p04;

/**
 * https://leetcode-cn.com/problems/minimum-moves-to-equal-array-elements/
 */
public class Problem0453 {

    public static void main(String[] args) {
        Problem0453 p = new Problem0453();
        System.err.println(p.minMoves(new int[]{1, 2, 3}) == 3);
        System.err.println(p.minMoves(new int[]{1, 2, 4, 4}) == 7);
    }

    /**
     * 思路：没好的思路
     * 自己在纸上演算，发现每次把最小的n-1个数都加1，重复这个操作，则最后一定能全部相等
     * 设min为最小数字，n为数组长度，总操作次数 = 对(array[i] - min)累加求和，i=0到n-1
     * 但是我证明不了这个公式
     * 时间复杂度为O(N)，空间复杂度为O(1)
     */
    public int minMoves(int[] nums) {
        // 题目有假设数组非空
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] < min) {
                min = nums[i];
            }
        }
        // 不考虑溢出，内存足够的极端情况下long也会溢出
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            result = result + nums[i] - min;
        }
        return result;
    }
}
