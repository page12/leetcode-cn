package p04;

/**
 * https://leetcode-cn.com/problems/target-sum/
 */
public class Problem0494 {

    public static void main(String[] args) {
        Problem0494 p = new Problem0494();
        System.err.println(p.findTargetSumWays(new int[]{1, 1, 1, 1, 1}, 3) == 5);
        System.err.println(p.findTargetSumWays(new int[]{1}, 1) == 1);
        System.err.println(p.findTargetSumWays(new int[]{2}, 0) == 0);
    }

    /**
     * 思路：
     * 设nums的总和为s，s >= 0
     * 变成负数的那些数原本的和为 x，x >= 0
     * 剩下的正数的和为 s - x > 0
     * 整个表达式的和为正数加负数，也就是 (s - x) - x = target
     * 得到 x = (s - target) / 2
     * x > 0，所以 s - target 必须是非负偶数
     * s和target都是已知值，因此 x 也是可以算出来的
     * 整个题目转换为nums中任意个数加起来的和为x，求组合方式的数量
     * <br/>
     * 如果是判断是否存在这样的方式，则和第416题很像
     * 求数量时，把第416题中dp递推公式中 或 逻辑的地方改成加号就行了
     * 具体分析如下：
     * 设dp为二维数组，行为 nums.length+1，列为 x+1，第0列是一个元素都不选的情况
     * dp[i][j]表示 [0,i] 中能够找出几个数使得它们的和等于 j 的组合方式的数量
     * 把j用nums中的数进行分解，有两种情况
     * 1、如果 j 的分解中包含 nums[i]，因为nums[i]固定了只有一种选择，因此组合方式的数量就是 dp[i-1][j-nums[i]] * 1
     * 2、如果 j 的分解中不包含 nums[i]，组合方式的数量是[0, i-1]区间中组合的数量 dp[i-1][j]
     * 两种情况下任意一种组合方式都可以，因此 dp[i][j] = dp[i-1][j-nums[i]] + dp[i-1][j]
     * <br/>
     * 特殊情况
     * 如果 j < nums[i]，则j的分解中一定不会包含nums[i]，此时情况1没有意义，只能使用情况2
     * 如果 i = 0，表示一个都不选，此时和为0时，有1种方式，其余情况没有，因此dp[0][0] = 1，dp[0][j>=1] = 0
     * 最终结果就是dp数组的右下角，也就是dp[nums.length-1][t]
     */
    public int findTargetSumWays(int[] nums, int target) {
        // 题目有假设，不做校验
        // 先求和
        int s = 0;
        for (int num : nums) {
            s += num;
        }
        // 和为负数或者奇数时一定不存在
        if (s - target < 0 || (s - target & 1) == 1) {
            return 0;
        }
        int x = (s - target) >> 1;
        // 可以使用滚动数组优化
        int[][] dp = new int[nums.length + 1][x + 1];
        // 第一行只用处理一个值
        dp[0][0] = 1;
        // i用作原数组下标时，要减1
        for (int i = 1; i < nums.length + 1; i++) {
            for (int j = 0; j <= x; j++) {
                if (j < nums[i - 1]) {
                    dp[i][j] = dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j - nums[i - 1]] + dp[i - 1][j];
                }
            }
        }
        return dp[nums.length][x];
    }
}
