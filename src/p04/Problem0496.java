package p04;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * https://leetcode-cn.com/problems/next-greater-element-i/
 */
public class Problem0496 {

    public static void main(String[] args) {
        Problem0496 p = new Problem0496();
        System.err.println(Arrays.toString(p.nextGreaterElement2(new int[]{4, 1, 2}, new int[]{1, 3, 4, 2})));
        System.err.println(Arrays.toString(p.nextGreaterElement2(new int[]{2, 4}, new int[]{1, 2, 3, 4})));
    }

    /**
     * 思路：最简单的思路是直接暴力双重循环遍历
     * 对于nums1中的每个元素，找到它在nums2中的位置，再找到右边比他大的元素，并记录下来
     * 如果找不到它在num1中的位置，或者这个位置右边没有比它大的元素，则记录-1
     * 设nums1的长度为N，nums2的长度为M
     * 则时间复杂为O(N*M)，空间复杂度为O(1)
     * <br/>
     * 实际上还可以这么做，令nums1 = nums2
     * 也就是求出nums2中每个元素的右边第一个比它大的元素
     * 把这些结果用hash的方式提前保存起来
     * 然后遍历nums1，去提前保存的结果中查找
     * 整个题目就转换为求出nums2中每个元素的右边第一个比它大的元素nextBigger
     * 把整体的结果为nextBigger[]
     * <br/>
     * 用上面的暴力方法求nextBigger[]时，举个极端的例子，如果当前遍历的元素是最大元素，它右边的所有元素都是递增的
     * 则当前元素的nextBigger是-1，但是遍历过程中，因为它右边的所有元素都是递增的，确实是找到了很多其他元素的nextBigger，但没有把它们都记录下来
     * 现在就是要改进方法，把这些额外找到的nextBigger记录下来，后续就不用再对它们进行遍历了
     * <br/>
     * 设abcd是nums2中四个连续相邻的元素，a > b并且b < c，则b的nextBigger = c
     * 找到b后，要有一个指针往前回推到a，再比较a和c
     * 如果a < c，则a的nextBigger = c，否则，要有一个指针向后推进到d，再比较a和d
     * 因为需要回溯指针，又需要往前推进指针，所以需要一个能支持两种顺序的数据结构
     * 栈正好满足，入栈 = 往前推进指针，出栈 = 回溯指针
     * 根据abcd的小例子分析得到，如果找到了一个元素n的nextBigger，则需要进行出栈（回溯），反之则需要入栈（推进指针）
     * 因为栈只能读取栈顶元素，所以元素n总是栈顶元素，和它比较的就是下一个要入栈的元素，下一个要入栈的元素总是小于等于栈顶元素
     * 如果一次性连续入栈x个元素，则根据入栈条件分析，它们从顶至底，满足连续小于等于的关系，也就是单调不大于
     * 这就是单调栈这个名称的由来，专门解决 找某个元素右边第一个比它大的元素 这种问题
     * 所有nums2的元素都会入单调栈，但是没有找到nextBigger的元素不会出栈
     * <br/>
     * 设nums1的长度为N，nums2的长度为M
     * 使用单调栈，要额外使用O(M)空间的栈，要额外使用O(M)空间的hash表，所以空间复杂度为O(M)
     * 整个过程要遍历一次nums2并入栈出栈，再遍历一次nums1，因为出栈次数 <= 入栈次数 == M，所以时间复杂度为O(N + M)
     */
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        // 这是暴力双循环的解法
        // 题目有假设数组非空，不做校验
        int[] result = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            // 先把结果设置为-1
            result[i] = -1;
            // nums2中是否找得到nums1中对应的元素
            boolean find = false;
            for (int j = 0; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    find = true;
                }
                if (find && nums1[i] < nums2[j]) {
                    result[i] = nums2[j];
                    break;
                }
            }
        }
        return result;
    }

    public int[] nextGreaterElement2(int[] nums1, int[] nums2) {
        // 这是利用单调栈的解法
        // 题目有假设数组非空，不做校验
        LinkedList<Integer> stack = new LinkedList<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums2.length; ) {
            if (stack.isEmpty() || nums2[i] <= stack.peek()) {
                stack.push(nums2[i]);
                i++;
            } else {
                map.put(stack.pop(), nums2[i]);
            }
        }
        int[] result = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            result[i] = map.getOrDefault(nums1[i], -1);
        }
        return result;
    }
}
